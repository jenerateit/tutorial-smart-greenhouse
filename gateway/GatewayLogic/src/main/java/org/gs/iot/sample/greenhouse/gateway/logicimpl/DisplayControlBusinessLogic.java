package org.gs.iot.sample.greenhouse.gateway.logicimpl;


import org.gs.iot.sample.greenhouse.gateway.logic.DisplayControlBusinessLogicI;
import java.util.Map;
import org.slf4j.Logger;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType;


/**
 * iterate sensor values through display
 */
public class DisplayControlBusinessLogic extends AbstractSmartGreenhouseAppBusinessLogic implements DisplayControlBusinessLogicI { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DisplayControlBusinessLogic.class);
    
    /**
     * creates an instance of DisplayControlBusinessLogic
     */
    public DisplayControlBusinessLogic() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
         super.activate(componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.modified.ComponentContext:DA-ELSE
         super.modified(componentContext);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
         super.deactivate(reason, componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void executePlantIlluminance(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, DigitalLightTSL2561GroveData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.executePlantIlluminance.SmartGreenhouseAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-START
    	
    	if (i >= 4) {
    		i = 0;
    	} else {
    		i++;
    	}
    	AdafruitI2cRgbLcdData data = new AdafruitI2cRgbLcdData();
    	AdafruitI2cRgbLcdStatus status = new AdafruitI2cRgbLcdStatus();
    	status.setBacklight(AdafruitI2cRgbLcdStatus.BACKLIGHT_ON);
    	
    	switch(i) {
    	case 0:
    		DigitalLightTSL2561GroveStatusType illu = deviceData.getData(); //getDeviceService().getPlantIlluminance().getData();
    		if (illu != null) {
	    		status.setRow0("Illuminance");
	    		status.setRow1(String.format("%.2f", illu.getStatus()) + " lux");
    		}
    		break;
    	case 1:
    		TemperatureSensorGroveStatusType temp = getDeviceService().getTemperature().getData();
    		if (temp != null) {
	    		status.setRow0("Temperature");
	    		status.setRow1(String.format("%.2f", temp.getStatus()) + " C");
    		}
    		break;
    	case 2:
    		BarometerBMP180GroveStatus baro = getDeviceService().getPlantBarometer().getData();
    		if (baro != null) {
	    		status.setRow0("Barometer");
	    		status.setRow1(String.format("%.2f", baro.getPressure()) + " hPa");
    		}
    		break;
    	case 3:
    		Mq2GasSensorGroveStatus gas = getDeviceService().getMq2Gas().getData();
    		if (gas != null) {
	    		status.setRow0("MQ2 Gas");
	    		status.setRow1(String.format("%.2f", gas.getRatio()) + " ratio");
    		}
    		break;
    	case 4:
    		MoistureSensorGroveStatusType moisture = getDeviceService().getPlantMoisture().getData();
    		if (moisture != null) {
	    		status.setRow0("Moisture");
	    		status.setRow1(String.format("%.2f", moisture.getStatus()) + " %");
    		}
    		break;
    	}
    	data.setData(status);
    	getDeviceService().setDisplay(data);
    	
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.executePlantIlluminance.SmartGreenhouseAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-ELSE
        //System.out.println("called business logic for changed data for sensor usage 'PlantIlluminance', data is: " + deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.executePlantIlluminance.SmartGreenhouseAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.additional.elements.in.type:DA-START
    
    private int i = -1;
    
    
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.DisplayControlBusinessLogic.additional.elements.in.type:DA-END
} // end of java type