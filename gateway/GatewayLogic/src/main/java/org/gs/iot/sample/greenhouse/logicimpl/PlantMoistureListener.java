package org.gs.iot.sample.greenhouse.logicimpl;


import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.util.Map;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.logic.MositureControlBusinessLogicI;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;

public class PlantMoistureListener<T extends MoistureSensorGroveData> implements SmartGreenhouseAppSensorListenerI<T> { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantMoistureListener.class);
    
    private MositureControlBusinessLogicI gatewayLogicExecutorMositureControlBusinessLogic;
    
    /**
     * creates an instance of PlantMoistureListener
     */
    public PlantMoistureListener() {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.activate.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void onChange(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, T deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-ELSE
        logger.info("sensor listener called for sensor usage 'PlantMoisture'");
        logger.info("calling business logic for sensor usage 'PlantMoisture': MositureControl");
        this.gatewayLogicExecutorMositureControlBusinessLogic.executePlantMoisture(deviceConfiguration, deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param th  the th
     */
    @Override
    public void onError(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, Throwable th) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-ELSE
        logger.error("problem while reading sensor data sensor data for sensor/actuator usage 'PlantMoisture'", th);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-END
    }
    /**
     * setter for the field gatewayLogicExecutorMositureControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorMositureControlBusinessLogic  the gatewayLogicExecutorMositureControlBusinessLogic
     */
    public void setGatewayLogicExecutorMositureControlBusinessLogic(MositureControlBusinessLogicI gatewayLogicExecutorMositureControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.setGatewayLogicExecutorMositureControlBusinessLogic.MositureControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.setGatewayLogicExecutorMositureControlBusinessLogic.MositureControlBusinessLogicI:DA-ELSE
        this.gatewayLogicExecutorMositureControlBusinessLogic = gatewayLogicExecutorMositureControlBusinessLogic;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.setGatewayLogicExecutorMositureControlBusinessLogic.MositureControlBusinessLogicI:DA-END
    }
    /**
     * unsetter for the field gatewayLogicExecutorMositureControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorMositureControlBusinessLogic  the gatewayLogicExecutorMositureControlBusinessLogic
     */
    public void unsetGatewayLogicExecutorMositureControlBusinessLogic(MositureControlBusinessLogicI gatewayLogicExecutorMositureControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.unsetGatewayLogicExecutorMositureControlBusinessLogic.MositureControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.unsetGatewayLogicExecutorMositureControlBusinessLogic.MositureControlBusinessLogicI:DA-ELSE
        if (this.gatewayLogicExecutorMositureControlBusinessLogic == gatewayLogicExecutorMositureControlBusinessLogic) {
            this.gatewayLogicExecutorMositureControlBusinessLogic = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.unsetGatewayLogicExecutorMositureControlBusinessLogic.MositureControlBusinessLogicI:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantMoistureListener.additional.elements.in.type:DA-END
} // end of java type