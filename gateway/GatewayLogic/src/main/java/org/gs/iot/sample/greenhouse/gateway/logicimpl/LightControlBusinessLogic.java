package org.gs.iot.sample.greenhouse.gateway.logicimpl;


import org.gs.iot.sample.greenhouse.gateway.logic.LightControlBusinessLogicI;
import java.util.Map;
import org.slf4j.Logger;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;


/**
 * switch light on when too dark, switch light off when very bright
 */
public class LightControlBusinessLogic extends AbstractSmartGreenhouseAppBusinessLogic implements LightControlBusinessLogicI { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(LightControlBusinessLogic.class);
    
    /**
     * creates an instance of LightControlBusinessLogic
     */
    public LightControlBusinessLogic() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
         super.activate(componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.modified.ComponentContext:DA-ELSE
         super.modified(componentContext);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
         super.deactivate(reason, componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void executePlantIlluminance(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, DigitalLightTSL2561GroveData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.executePlantIlluminance.SmartGreenhouseAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-START
    	if (deviceData.getData() != null) {
	    	float lux = deviceData.getData().getStatus();
			double brightness = lux / 400;
			brightness = brightness < 0.95 ? 1 - brightness : 0.1;
			LightStripData light = getDeviceService().getLight();
			light.getData().setBrightness(brightness);
			getDeviceService().setLight(light);
    	}
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.executePlantIlluminance.SmartGreenhouseAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-ELSE
        //System.out.println("called business logic for changed data for sensor usage 'PlantIlluminance', data is: " + deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.executePlantIlluminance.SmartGreenhouseAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.LightControlBusinessLogic.additional.elements.in.type:DA-END
} // end of java type