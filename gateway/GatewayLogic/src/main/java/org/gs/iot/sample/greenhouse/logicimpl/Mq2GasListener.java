package org.gs.iot.sample.greenhouse.logicimpl;


import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.util.Map;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.logic.AlarmControlBusinessLogicI;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;

/**
 * Analog Sensors
 */
public class Mq2GasListener<T extends Mq2GasSensorGroveData> implements SmartGreenhouseAppSensorListenerI<T> { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Mq2GasListener.class);
    
    private AlarmControlBusinessLogicI gatewayLogicExecutorAlarmControlBusinessLogic;
    
    /**
     * creates an instance of Mq2GasListener
     */
    public Mq2GasListener() {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.activate.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void onChange(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, T deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-ELSE
        logger.info("sensor listener called for sensor usage 'Mq2Gas'");
        logger.info("calling business logic for sensor usage 'Mq2Gas': AlarmControl");
        this.gatewayLogicExecutorAlarmControlBusinessLogic.executeMq2Gas(deviceConfiguration, deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param th  the th
     */
    @Override
    public void onError(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, Throwable th) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-ELSE
        logger.error("problem while reading sensor data sensor data for sensor/actuator usage 'Mq2Gas'", th);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-END
    }
    /**
     * setter for the field gatewayLogicExecutorAlarmControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorAlarmControlBusinessLogic  the gatewayLogicExecutorAlarmControlBusinessLogic
     */
    public void setGatewayLogicExecutorAlarmControlBusinessLogic(AlarmControlBusinessLogicI gatewayLogicExecutorAlarmControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.setGatewayLogicExecutorAlarmControlBusinessLogic.AlarmControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.setGatewayLogicExecutorAlarmControlBusinessLogic.AlarmControlBusinessLogicI:DA-ELSE
        this.gatewayLogicExecutorAlarmControlBusinessLogic = gatewayLogicExecutorAlarmControlBusinessLogic;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.setGatewayLogicExecutorAlarmControlBusinessLogic.AlarmControlBusinessLogicI:DA-END
    }
    /**
     * unsetter for the field gatewayLogicExecutorAlarmControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorAlarmControlBusinessLogic  the gatewayLogicExecutorAlarmControlBusinessLogic
     */
    public void unsetGatewayLogicExecutorAlarmControlBusinessLogic(AlarmControlBusinessLogicI gatewayLogicExecutorAlarmControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.unsetGatewayLogicExecutorAlarmControlBusinessLogic.AlarmControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.unsetGatewayLogicExecutorAlarmControlBusinessLogic.AlarmControlBusinessLogicI:DA-ELSE
        if (this.gatewayLogicExecutorAlarmControlBusinessLogic == gatewayLogicExecutorAlarmControlBusinessLogic) {
            this.gatewayLogicExecutorAlarmControlBusinessLogic = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.unsetGatewayLogicExecutorAlarmControlBusinessLogic.AlarmControlBusinessLogicI:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.logicimpl.Mq2GasListener.additional.elements.in.type:DA-END
} // end of java type