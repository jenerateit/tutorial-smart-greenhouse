package org.gs.iot.sample.greenhouse.gateway.logic;

import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;

/**
 * show read color when temperature is low, show blue color when temperature is high
 */
public interface HeatingControlBusinessLogicI { // start of interface

    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    void executeTemperature(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, TemperatureSensorGroveData deviceData);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logic.HeatingControlBusinessLogicI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logic.HeatingControlBusinessLogicI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logic.HeatingControlBusinessLogicI.additional.elements.in.type:DA-END
} // end of java type