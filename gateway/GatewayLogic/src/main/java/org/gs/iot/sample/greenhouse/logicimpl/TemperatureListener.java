package org.gs.iot.sample.greenhouse.logicimpl;


import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.util.Map;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.logic.HeatingControlBusinessLogicI;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;

public class TemperatureListener<T extends TemperatureSensorGroveData> implements SmartGreenhouseAppSensorListenerI<T> { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TemperatureListener.class);
    
    private HeatingControlBusinessLogicI gatewayLogicExecutorHeatingControlBusinessLogic;
    
    /**
     * creates an instance of TemperatureListener
     */
    public TemperatureListener() {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.activate.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void onChange(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, T deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-ELSE
        logger.info("sensor listener called for sensor usage 'Temperature'");
        logger.info("calling business logic for sensor usage 'Temperature': HeatingControl");
        this.gatewayLogicExecutorHeatingControlBusinessLogic.executeTemperature(deviceConfiguration, deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param th  the th
     */
    @Override
    public void onError(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, Throwable th) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-ELSE
        logger.error("problem while reading sensor data sensor data for sensor/actuator usage 'Temperature'", th);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-END
    }
    /**
     * setter for the field gatewayLogicExecutorHeatingControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorHeatingControlBusinessLogic  the gatewayLogicExecutorHeatingControlBusinessLogic
     */
    public void setGatewayLogicExecutorHeatingControlBusinessLogic(HeatingControlBusinessLogicI gatewayLogicExecutorHeatingControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.setGatewayLogicExecutorHeatingControlBusinessLogic.HeatingControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.setGatewayLogicExecutorHeatingControlBusinessLogic.HeatingControlBusinessLogicI:DA-ELSE
        this.gatewayLogicExecutorHeatingControlBusinessLogic = gatewayLogicExecutorHeatingControlBusinessLogic;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.setGatewayLogicExecutorHeatingControlBusinessLogic.HeatingControlBusinessLogicI:DA-END
    }
    /**
     * unsetter for the field gatewayLogicExecutorHeatingControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorHeatingControlBusinessLogic  the gatewayLogicExecutorHeatingControlBusinessLogic
     */
    public void unsetGatewayLogicExecutorHeatingControlBusinessLogic(HeatingControlBusinessLogicI gatewayLogicExecutorHeatingControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.unsetGatewayLogicExecutorHeatingControlBusinessLogic.HeatingControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.unsetGatewayLogicExecutorHeatingControlBusinessLogic.HeatingControlBusinessLogicI:DA-ELSE
        if (this.gatewayLogicExecutorHeatingControlBusinessLogic == gatewayLogicExecutorHeatingControlBusinessLogic) {
            this.gatewayLogicExecutorHeatingControlBusinessLogic = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.unsetGatewayLogicExecutorHeatingControlBusinessLogic.HeatingControlBusinessLogicI:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.logicimpl.TemperatureListener.additional.elements.in.type:DA-END
} // end of java type