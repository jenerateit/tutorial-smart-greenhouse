package org.gs.iot.sample.greenhouse.gateway.logic;

import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;

/**
 * switch light on when too dark, switch light off when very bright
 */
public interface LightControlBusinessLogicI { // start of interface

    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    void executePlantIlluminance(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, DigitalLightTSL2561GroveData deviceData);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logic.LightControlBusinessLogicI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logic.LightControlBusinessLogicI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logic.LightControlBusinessLogicI.additional.elements.in.type:DA-END
} // end of java type