package org.gs.iot.sample.greenhouse.gateway.logicimpl;


import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import java.util.Map;
import org.slf4j.Logger;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;

public abstract class AbstractSmartGreenhouseAppBusinessLogic implements SmartGreenhouseAppPublishable { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AbstractSmartGreenhouseAppBusinessLogic.class);
    
    private ServiceTracker<?,?> serviceTrackerForPublishable;
    
    private SmartGreenhouseAppDeviceServiceI deviceService;
    
    /**
     * creates an instance of AbstractSmartGreenhouseAppBusinessLogic
     */
    public AbstractSmartGreenhouseAppBusinessLogic() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
        
        
        this.serviceTrackerForPublishable = new ServiceTracker<>(bundleContext, SmartGreenhouseAppPublishable.class, null);
        this.serviceTrackerForPublishable.open();
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param resource  the resource
     * @param payload  the payload
     */
    @Override
    public void publish(String resource, byte[] payload) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.publish.String.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.publish.String.byte.ARRAY:DA-ELSE
        
        if (this.serviceTrackerForPublishable != null) {
        	Object[] services = this.serviceTrackerForPublishable.getServices();
        	if (services != null) {
        		for (Object service : services) {
        			SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        			publishable.publish(resource, payload);
        		}
        	}
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.publish.String.byte.ARRAY:DA-END
    }
    /**
     * setter for the field deviceService
     * 
     * 
     * 
     * @param deviceService  the deviceService
     */
    public void setDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-ELSE
        this.deviceService = deviceService;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-END
    }
    /**
     * unsetter for the field deviceService
     * 
     * 
     * 
     * @param deviceService  the deviceService
     */
    public void unsetDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-ELSE
        if (this.deviceService == deviceService) {
            this.deviceService = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-END
    }
    /**
     * getter for the field deviceService
     * 
     * 
     * 
     * @return
     */
    public SmartGreenhouseAppDeviceServiceI getDeviceService() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.getDeviceService.SmartGreenhouseAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.getDeviceService.SmartGreenhouseAppDeviceServiceI:DA-ELSE
        return this.deviceService;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.getDeviceService.SmartGreenhouseAppDeviceServiceI:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AbstractSmartGreenhouseAppBusinessLogic.additional.elements.in.type:DA-END
} // end of java type