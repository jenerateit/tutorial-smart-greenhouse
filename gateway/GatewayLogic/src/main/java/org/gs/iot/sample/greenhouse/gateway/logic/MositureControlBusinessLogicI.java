package org.gs.iot.sample.greenhouse.gateway.logic;

import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;

/**
 * turn on water pump if plant is dry
 */
public interface MositureControlBusinessLogicI { // start of interface

    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    void executePlantMoisture(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, MoistureSensorGroveData deviceData);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logic.MositureControlBusinessLogicI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logic.MositureControlBusinessLogicI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logic.MositureControlBusinessLogicI.additional.elements.in.type:DA-END
} // end of java type