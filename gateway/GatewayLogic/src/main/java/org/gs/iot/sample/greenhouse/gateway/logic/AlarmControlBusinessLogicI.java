package org.gs.iot.sample.greenhouse.gateway.logic;

import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;

/**
 * BEEP if gas detected
 */
public interface AlarmControlBusinessLogicI { // start of interface

    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    void executeMq2Gas(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, Mq2GasSensorGroveData deviceData);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logic.AlarmControlBusinessLogicI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logic.AlarmControlBusinessLogicI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logic.AlarmControlBusinessLogicI.additional.elements.in.type:DA-END
} // end of java type