package org.gs.iot.sample.greenhouse.gateway.logicimpl;


import org.gs.iot.sample.greenhouse.gateway.logic.MositureControlBusinessLogicI;
import java.util.Map;
import org.slf4j.Logger;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;
/*
 * Imports from last generation
 */
import java.util.Timer;
import java.util.TimerTask;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType;


/**
 * turn on water pump if plant is dry
 */
public class MositureControlBusinessLogic extends AbstractSmartGreenhouseAppBusinessLogic implements MositureControlBusinessLogicI { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(MositureControlBusinessLogic.class);
    
    /**
     * creates an instance of MositureControlBusinessLogic
     */
    public MositureControlBusinessLogic() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
         super.activate(componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.modified.ComponentContext:DA-ELSE
         super.modified(componentContext);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
         super.deactivate(reason, componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void executePlantMoisture(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, MoistureSensorGroveData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.executePlantMoisture.SmartGreenhouseAppDeviceConfigEnum.MoistureSensorGroveData:DA-START
    	if (deviceData.getData() != null) {
	    	if (deviceData.getData().getStatus() < 0.05) {
	    		if (!pumpRunning) {
		    		RelayGroveData data = new RelayGroveData();
		    		data.setData(new RelayGroveStatusType(true));
		    		getDeviceService().setWaterPump(data);
		    		pumpRunning = true;
		    		
		    		new Timer().schedule(new TimerTask() {
						
						@Override
						public void run() {
							RelayGroveData data = new RelayGroveData();
				    		data.setData(new RelayGroveStatusType(false));
				    		getDeviceService().setWaterPump(data);
						}
					}, 5000);
	    		}
	    	} else {
	    		pumpRunning = false;
	    	}
    	}
    	
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.executePlantMoisture.SmartGreenhouseAppDeviceConfigEnum.MoistureSensorGroveData:DA-ELSE
        //System.out.println("called business logic for changed data for sensor usage 'PlantMoisture', data is: " + deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.executePlantMoisture.SmartGreenhouseAppDeviceConfigEnum.MoistureSensorGroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.additional.elements.in.type:DA-START
    private boolean pumpRunning = false;
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.MositureControlBusinessLogic.additional.elements.in.type:DA-END
} // end of java type