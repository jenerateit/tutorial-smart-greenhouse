package org.gs.iot.sample.greenhouse.gateway.logicimpl;


import org.gs.iot.sample.greenhouse.gateway.logic.HeatingControlBusinessLogicI;
import java.util.Map;
import org.slf4j.Logger;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;


/**
 * show read color when temperature is low, show blue color when temperature is high
 */
public class HeatingControlBusinessLogic extends AbstractSmartGreenhouseAppBusinessLogic implements HeatingControlBusinessLogicI { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(HeatingControlBusinessLogic.class);
    
    /**
     * creates an instance of HeatingControlBusinessLogic
     */
    public HeatingControlBusinessLogic() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
         super.activate(componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.modified.ComponentContext:DA-ELSE
         super.modified(componentContext);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
         super.deactivate(reason, componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void executeTemperature(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, TemperatureSensorGroveData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.executeTemperature.SmartGreenhouseAppDeviceConfigEnum.TemperatureSensorGroveData:DA-START
    	if (deviceData.getData() == null) return;
    	
    	LightStripData light = getDeviceService().getLight();
    	if (light != null && light.getData() != null) {
	    	TemperatureSensorGroveData temp = deviceData;
	    	short r,b;
	    	float ratio;
	    	float t = temp.getData().getStatus();
	    	
	    	if (t < 20) {
	    		ratio = 0f;
	    	} else if(t > 27) {
	    		ratio = 1f;
	    	} else {
	    		ratio = 1 - ((27 - t) / 7);
	    	}
	    	
	    	b = (short) (ratio * 255);
	    	r = (short) ((1 - ratio) * 255);
	    	light.getData().setB(b);
	    	light.getData().setR(r);
	    	light.getData().setG((short) 0);
	    	
	    	if (light.getData().getBrightness() < 0.1) {
	    		light.getData().setBrightness(0.5);
	    	}
	    	
	    	getDeviceService().setLight(light);
    	}
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.executeTemperature.SmartGreenhouseAppDeviceConfigEnum.TemperatureSensorGroveData:DA-ELSE
        //System.out.println("called business logic for changed data for sensor usage 'Temperature', data is: " + deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.executeTemperature.SmartGreenhouseAppDeviceConfigEnum.TemperatureSensorGroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.HeatingControlBusinessLogic.additional.elements.in.type:DA-END
} // end of java type