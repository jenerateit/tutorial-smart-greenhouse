package org.gs.iot.sample.greenhouse.gateway.logicimpl;


import org.gs.iot.sample.greenhouse.gateway.logic.AlarmControlBusinessLogicI;
import java.util.Map;
import org.slf4j.Logger;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType;


/**
 * BEEP if gas detected
 */
public class AlarmControlBusinessLogic extends AbstractSmartGreenhouseAppBusinessLogic implements AlarmControlBusinessLogicI { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AlarmControlBusinessLogic.class);
    
    /**
     * creates an instance of AlarmControlBusinessLogic
     */
    public AlarmControlBusinessLogic() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
         super.activate(componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.modified.ComponentContext:DA-ELSE
         super.modified(componentContext);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
         super.deactivate(reason, componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void executeMq2Gas(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, Mq2GasSensorGroveData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.executeMq2Gas.SmartGreenhouseAppDeviceConfigEnum.Mq2GasSensorGroveData:DA-START
    	if (deviceData.getData() != null && deviceData.getData().getRatio() < 2) {
    		BuzzerGroveData data = new BuzzerGroveData();
    		try {
	    		data.setData(new BuzzerGroveStatusType(true));
	    		getDeviceService().setAlarm(data);
	    		Thread.sleep(100);
	    		data.setData(new BuzzerGroveStatusType(false));
	    		getDeviceService().setAlarm(data);
	    		Thread.sleep(200);
	    		data.setData(new BuzzerGroveStatusType(true));
	    		getDeviceService().setAlarm(data);
	    		Thread.sleep(100);
	    		data.setData(new BuzzerGroveStatusType(false));
	    		getDeviceService().setAlarm(data);
	    		Thread.sleep(200);
	    		data.setData(new BuzzerGroveStatusType(true));
	    		getDeviceService().setAlarm(data);
	    		Thread.sleep(100);
	    		data.setData(new BuzzerGroveStatusType(false));
	    		getDeviceService().setAlarm(data);
    		}
    		catch(Throwable th) {
	    		data.setData(new BuzzerGroveStatusType(false));
	    		getDeviceService().setAlarm(data);
    		}
    	}
    	
    	
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.executeMq2Gas.SmartGreenhouseAppDeviceConfigEnum.Mq2GasSensorGroveData:DA-ELSE
        //System.out.println("called business logic for changed data for sensor usage 'Mq2Gas', data is: " + deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.executeMq2Gas.SmartGreenhouseAppDeviceConfigEnum.Mq2GasSensorGroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.logicimpl.AlarmControlBusinessLogic.additional.elements.in.type:DA-END
} // end of java type