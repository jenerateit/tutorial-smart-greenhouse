package org.gs.iot.sample.greenhouse.logicimpl;


import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.util.Map;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.logic.LightControlBusinessLogicI;
import org.gs.iot.sample.greenhouse.gateway.logic.DisplayControlBusinessLogicI;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;

/**
 * I2C Sensors
 */
public class PlantIlluminanceListener<T extends DigitalLightTSL2561GroveData> implements SmartGreenhouseAppSensorListenerI<T> { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantIlluminanceListener.class);
    
    private LightControlBusinessLogicI gatewayLogicExecutorLightControlBusinessLogic;
    
    private DisplayControlBusinessLogicI gatewayLogicExecutorDisplayControlBusinessLogic;
    
    /**
     * creates an instance of PlantIlluminanceListener
     */
    public PlantIlluminanceListener() {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.activate.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void onChange(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, T deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-ELSE
        logger.info("sensor listener called for sensor usage 'PlantIlluminance'");
        logger.info("calling business logic for sensor usage 'PlantIlluminance': LightControl");
        this.gatewayLogicExecutorLightControlBusinessLogic.executePlantIlluminance(deviceConfiguration, deviceData);
        logger.info("calling business logic for sensor usage 'PlantIlluminance': DisplayControl");
        this.gatewayLogicExecutorDisplayControlBusinessLogic.executePlantIlluminance(deviceConfiguration, deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.onChange.SmartGreenhouseAppDeviceConfigEnum.T:DA-END
    }
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param th  the th
     */
    @Override
    public void onError(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, Throwable th) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-ELSE
        logger.error("problem while reading sensor data sensor data for sensor/actuator usage 'PlantIlluminance'", th);
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.onError.SmartGreenhouseAppDeviceConfigEnum.Throwable:DA-END
    }
    /**
     * setter for the field gatewayLogicExecutorLightControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorLightControlBusinessLogic  the gatewayLogicExecutorLightControlBusinessLogic
     */
    public void setGatewayLogicExecutorLightControlBusinessLogic(LightControlBusinessLogicI gatewayLogicExecutorLightControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.setGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.setGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-ELSE
        this.gatewayLogicExecutorLightControlBusinessLogic = gatewayLogicExecutorLightControlBusinessLogic;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.setGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-END
    }
    /**
     * unsetter for the field gatewayLogicExecutorLightControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorLightControlBusinessLogic  the gatewayLogicExecutorLightControlBusinessLogic
     */
    public void unsetGatewayLogicExecutorLightControlBusinessLogic(LightControlBusinessLogicI gatewayLogicExecutorLightControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.unsetGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.unsetGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-ELSE
        if (this.gatewayLogicExecutorLightControlBusinessLogic == gatewayLogicExecutorLightControlBusinessLogic) {
            this.gatewayLogicExecutorLightControlBusinessLogic = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.unsetGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-END
    }
    /**
     * setter for the field gatewayLogicExecutorDisplayControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorDisplayControlBusinessLogic  the gatewayLogicExecutorDisplayControlBusinessLogic
     */
    public void setGatewayLogicExecutorDisplayControlBusinessLogic(DisplayControlBusinessLogicI gatewayLogicExecutorDisplayControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.setGatewayLogicExecutorDisplayControlBusinessLogic.DisplayControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.setGatewayLogicExecutorDisplayControlBusinessLogic.DisplayControlBusinessLogicI:DA-ELSE
        this.gatewayLogicExecutorDisplayControlBusinessLogic = gatewayLogicExecutorDisplayControlBusinessLogic;
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.setGatewayLogicExecutorDisplayControlBusinessLogic.DisplayControlBusinessLogicI:DA-END
    }
    /**
     * unsetter for the field gatewayLogicExecutorDisplayControlBusinessLogic
     * 
     * 
     * 
     * @param gatewayLogicExecutorDisplayControlBusinessLogic  the gatewayLogicExecutorDisplayControlBusinessLogic
     */
    public void unsetGatewayLogicExecutorDisplayControlBusinessLogic(DisplayControlBusinessLogicI gatewayLogicExecutorDisplayControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.unsetGatewayLogicExecutorDisplayControlBusinessLogic.DisplayControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.unsetGatewayLogicExecutorDisplayControlBusinessLogic.DisplayControlBusinessLogicI:DA-ELSE
        if (this.gatewayLogicExecutorDisplayControlBusinessLogic == gatewayLogicExecutorDisplayControlBusinessLogic) {
            this.gatewayLogicExecutorDisplayControlBusinessLogic = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.unsetGatewayLogicExecutorDisplayControlBusinessLogic.DisplayControlBusinessLogicI:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.logicimpl.PlantIlluminanceListener.additional.elements.in.type:DA-END
} // end of java type