package org.gs.iot.sample.greenhouse.gateway.paho;


import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import java.util.Map;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.osgi.util.tracker.ServiceTracker;
import java.util.LinkedHashMap;
import java.util.concurrent.RunnableFuture;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppActionable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraData;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;

public class SmartGreenhouseAppPahoDataService implements SmartGreenhouseAppPublishable, MqttCallback { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SmartGreenhouseAppPahoDataService.class);
    
    private SmartGreenhouseAppDeviceServiceI deviceService;
    
    /**
     * the Eclipse Paho client to handle MQTT communication with brokers
     */
    private MqttClient pahoClient;
    
    /**
     * connect options for the connect() call
     */
    private final MqttConnectOptions pahoClientConnectOptions = new MqttConnectOptions();
    
    /**
     * counter for the number of connection attempts before waiting to retry the connection
     */
    private int pahoClientConnectAttemptsCounter = 0;
    
    /**
     * a map where the key is a string-typed topic and the value is a service tracker instance
     */
    private final Map<String, ServiceTracker<Object, Object>> serviceTrackersForActionables = new LinkedHashMap<String, ServiceTracker<Object, Object>>();
    
    /**
     * a runnable that has the task to connect to an MQTT broker
     */
    private RunnableFuture<?> mqttConnectorTask;
    
    public static final String PUBLISH_TOPICPREFIX_PROP_NAME = "publish.appTopicPrefix";
    
    public static final String PUBLISH_QOS_PROP_NAME = "publish.qos";
    
    public static final String PUBLISH_RETAIN_PROP_NAME = "publish.retain";
    
    public static final String PUBLISH_PRIORITY_PROP_NAME = "publish.priority";
    
    /**
     * creates an instance of SmartGreenhouseAppPahoDataService
     */
    public SmartGreenhouseAppPahoDataService() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.activate.ComponentContext.BundleContext.Map:DA-ELSE
        this.properties = properties;
        
        // there is no business logic defined in the model => no calls to services that implement actionable interface are going to be generated 
        
        // finally, initialize the MQTT client 
        this.initClient();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param arg0
     * @return
     */
    @Override
    public void connectionLost(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.connectionLost.Throwable.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.connectionLost.Throwable.void:DA-ELSE
        
        logger.info("connection lost, reason: " + arg0);
        logger.info("restarting connection thread ...");
        Thread thread = new Thread(this.mqttConnectorTask);
        thread.start();
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.connectionLost.Throwable.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @return
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.deliveryComplete.IMqttDeliveryToken.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.deliveryComplete.IMqttDeliveryToken.void:DA-ELSE
        // nothing to be generated here so far 
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.deliveryComplete.IMqttDeliveryToken.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @param arg1
     * @return
     */
    @Override
    public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.messageArrived.String.MqttMessage.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.messageArrived.String.MqttMessage.void:DA-ELSE
        
        String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
        prefix = prefix == null ? "client-id/app-id/" : prefix;
                
        String topicPart = null;
        if(arg0.startsWith(prefix)) {
        	topicPart = arg0.substring(prefix.length());
        	if (topicPart.startsWith("/")) topicPart = topicPart.substring(1);
        }
        
        String resource = null;
        if (topicPart.startsWith("sensor/")) {
        	resource = topicPart.substring("sensor/".length());
        } else if (topicPart.startsWith("actuator/")) {
        	resource = topicPart.substring("actuator/".length());
        } else if (topicPart.startsWith("logic/")) {
        	resource = topicPart.substring("logic/".length());
        }
        
        if (resource != null) {
        
        	// --- calling business logic through a service tracker
        	ServiceTracker<?,?> serviceTracker = this.serviceTrackersForActionables.get(resource);
        	if (serviceTracker != null) {
        		SmartGreenhouseAppActionable actionable = (SmartGreenhouseAppActionable) serviceTracker.getService();
        		actionable.action(arg1.getPayload());
        	}
        	
        	
        	// --- handling requests to get sensor data or to set actuator data
        	SmartGreenhouseAppDeviceConfigEnum identifiedSensorUsage = null;
        	for (SmartGreenhouseAppDeviceConfigEnum configEnumEntry : SmartGreenhouseAppDeviceConfigEnum.values()) {
        		if (configEnumEntry.getResource().equalsIgnoreCase(resource)) {
        			identifiedSensorUsage = configEnumEntry;
        			break;
        		}
        	}
        	
        	if (identifiedSensorUsage != null) {
        		switch (identifiedSensorUsage) {
        	
        	    case PLANTMOTION:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	PIRMotionGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(PIRMotionGroveData.class, arg1.getPayload());
        			    // PIRMotionGrove is not an actuator => we cannot set a value here
        	        } else {
        				PIRMotionGroveData deviceData = deviceService.getPlantMotion();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case WATERPUMP:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            
        	        	RelayGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(RelayGroveData.class, arg1.getPayload());
        			    deviceService.setWaterPump(deviceData);
        	        } else {
        				RelayGroveData deviceData = deviceService.getWaterPump();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case ALARM:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            
        	        	BuzzerGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(BuzzerGroveData.class, arg1.getPayload());
        			    deviceService.setAlarm(deviceData);
        	        } else {
        				BuzzerGroveData deviceData = deviceService.getAlarm();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case PLANTILLUMINANCE:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	DigitalLightTSL2561GroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(DigitalLightTSL2561GroveData.class, arg1.getPayload());
        			    // DigitalLightTSL2561Grove is not an actuator => we cannot set a value here
        	        } else {
        				DigitalLightTSL2561GroveData deviceData = deviceService.getPlantIlluminance();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case DISPLAY:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            
        	        	AdafruitI2cRgbLcdData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(AdafruitI2cRgbLcdData.class, arg1.getPayload());
        			    deviceService.setDisplay(deviceData);
        	        } else {
        				AdafruitI2cRgbLcdData deviceData = deviceService.getDisplay();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case PLANTBAROMETER:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	BarometerBMP180GroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(BarometerBMP180GroveData.class, arg1.getPayload());
        			    // BarometerBMP180Grove is not an actuator => we cannot set a value here
        	        } else {
        				BarometerBMP180GroveData deviceData = deviceService.getPlantBarometer();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case MQ2GAS:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	Mq2GasSensorGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(Mq2GasSensorGroveData.class, arg1.getPayload());
        			    // Mq2GasSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				Mq2GasSensorGroveData deviceData = deviceService.getMq2Gas();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case TEMPERATURE:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	TemperatureSensorGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(TemperatureSensorGroveData.class, arg1.getPayload());
        			    // TemperatureSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				TemperatureSensorGroveData deviceData = deviceService.getTemperature();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case PLANTMOISTURE:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	MoistureSensorGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(MoistureSensorGroveData.class, arg1.getPayload());
        			    // MoistureSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				MoistureSensorGroveData deviceData = deviceService.getPlantMoisture();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case LIGHT:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            
        	        	LightStripData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(LightStripData.class, arg1.getPayload());
        			    deviceService.setLight(deviceData);
        	        } else {
        				LightStripData deviceData = deviceService.getLight();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case CAMERA:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	RpiCameraData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(RpiCameraData.class, arg1.getPayload());
        			    // RpiCamera is not an actuator => we cannot set a value here
        	        } else {
        				RpiCameraData deviceData = deviceService.getCamera();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        	    default:
        			throw new RuntimeException("unhandled enum entry found: '" + identifiedSensorUsage + "'");
        	    }
        	}
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.messageArrived.String.MqttMessage.void:DA-END
    }
    /**
     * setter for the field deviceService
     * 
     * 
     * 
     * @param deviceService  the deviceService
     */
    public void setDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-ELSE
        this.deviceService = deviceService;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-END
    }
    /**
     * unsetter for the field deviceService
     * 
     * 
     * 
     * @param deviceService  the deviceService
     */
    public void unsetDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-ELSE
        if (this.deviceService == deviceService) {
            this.deviceService = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-END
    }
    /**
     * 
     * @param resource  the resource
     * @param payload  the payload
     */
    @Override
    public void publish(String resource, byte[] payload) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.publish.String.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.publish.String.byte.ARRAY:DA-ELSE
        
        if (this.pahoClient != null && this.pahoClient.isConnected()) {
            String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
            prefix = prefix == null ? "client-id/app-id/" : prefix;
        	String topic = prefix + (prefix.endsWith("/") ? "" : "/") + resource;
        	try {
        		this.pahoClient.publish(topic, payload, 0, true);
        	} catch (MqttException ex) {
                logger.error("failed to publish data with Paho client for topic '" + topic + "'", ex);
        		ex.printStackTrace();  // TODO handle this in a better way, e.g. use a logging framework
        	}
        } else {
            System.out.println("cannot send message, pahoClient either null (" + this.pahoClient + ") or not connected (connected=" + this.pahoClient.isConnected() + ")! resource:" + resource + ", payload:" + new String(payload));
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.publish.String.byte.ARRAY:DA-END
    }
    /**
     * called by a separate thread, the method will never return
     */
    private void connect() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.connect:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.connect:DA-ELSE
        
        boolean connected = false;
        while (!connected) {
        	try {
        		System.out.println("trying to connect ...");
        		this.pahoClient.connect(this.pahoClientConnectOptions);
                System.out.println("successfully connected");
        		connected = true;
        		this.pahoClientConnectAttemptsCounter = 0;
        
                // --- once we are successfully connected, we can subscribe
        	    String prefix = ((String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME));
        	    prefix = prefix == null ? "client-id/app-id/" : prefix;
        	    String subscriptionTopicForActuators = prefix + (prefix.endsWith("/") ? "" : "/") + "actuator/#";
        	    String subscriptionTopicForSensors = prefix + (prefix.endsWith("/") ? "" : "/") + "sensor/#";
        	    String subscriptionTopicForBusinessLogic = prefix + (prefix.endsWith("/") ? "" : "/") + "logic/#";
        		this.pahoClient.subscribe(new String[] {subscriptionTopicForSensors, subscriptionTopicForActuators, subscriptionTopicForBusinessLogic}, new int[] {0, 0, 0});
        	} catch (MqttException ex) {
        		this.pahoClientConnectAttemptsCounter++;
                logger.error("connection failed, number of failures: " + this.pahoClientConnectAttemptsCounter);
        		if (this.pahoClientConnectAttemptsCounter > 0 && this.pahoClientConnectAttemptsCounter % 3 == 0) {
        			// sleep longer and then try again, three times in a row
        			long longSleepMillis = 60000;
                    logger.info("now sleeping longer, for " + longSleepMillis + " milliseconds ...");
        			try { Thread.sleep(longSleepMillis); } catch (InterruptedException e) {}
        		} else {
        		    long shortSleepMillis = 1000;
                    logger.info("now sleeping for " + shortSleepMillis + " milliseconds ...");
        			try { Thread.sleep(shortSleepMillis); } catch (InterruptedException e) {}
        		}
        	}
        }
        logger.info("exiting from connect() ...");
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.connect:DA-END
    }
    /**
     * initialize the Paho MQTT client
     */
    private void initClient() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.initClient:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.initClient:DA-ELSE
        
        try {
        	MemoryPersistence persistence = new MemoryPersistence();
        	this.pahoClient = new MqttClient("tcp://localhost:1883", "SmartGreenhouseApp", persistence);
        	this.pahoClient.setCallback(this);
            this.pahoClientConnectOptions.setCleanSession(false);
            this.pahoClientConnectOptions.setKeepAliveInterval(120);
        
        	this.mqttConnectorTask = new RunnableFuture<Object>() {
        
        		@Override
        		public boolean cancel(boolean mayInterruptIfRunning) { return false; }
        
        		@Override
        		public boolean isCancelled() { return false; }
        
        		@Override
        		public boolean isDone() { return false; }
        
        		@Override
        		public Object get() throws InterruptedException, ExecutionException { return null; }
        
        		@Override
        		public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException { return null; }
        
        		@Override
        		public void run() {
        			connect();
        		}
        		
        	};
        	
        	Thread thread = new Thread(mqttConnectorTask);
        	thread.start();
        } catch (MqttException ex) {
        	throw new RuntimeException("problems creating MQTT client, cannot recover from here", ex);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.initClient:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppPahoDataService.additional.elements.in.type:DA-END
} // end of java type