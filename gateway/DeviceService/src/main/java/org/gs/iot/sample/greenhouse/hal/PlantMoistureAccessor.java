package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.MoistureSensorGroveDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

public class PlantMoistureAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<MoistureSensorGroveData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantMoistureAccessor.class);
    
    private MoistureSensorGroveDeviceDriver driver;
    
    private MoistureSensorGroveData data;
    
    private MoistureSensorGroveData sentData;
    
    private final LinkedList<MoistureSensorGroveData> historyData = new LinkedList<MoistureSensorGroveData>();
    
    /**
     * creates an instance of PlantMoistureAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public PlantMoistureAccessor(MoistureSensorGroveDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.MoistureSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.MoistureSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.MoistureSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.run.void:DA-ELSE
        
        try {
            MoistureSensorGroveData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'PlantMoisture'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public MoistureSensorGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getData.MoistureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getData.MoistureSensorGroveData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'PlantMoisture'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'PlantMoisture'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getData.MoistureSensorGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public MoistureSensorGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getSentData.MoistureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getSentData.MoistureSensorGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getSentData.MoistureSensorGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<MoistureSensorGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.PLANTMOISTURE.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<MoistureSensorGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<MoistureSensorGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.PLANTMOISTURE, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'PlantMoisture'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(MoistureSensorGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.onInterrupt.MoistureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.onInterrupt.MoistureSensorGroveData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.onInterrupt.MoistureSensorGroveData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(MoistureSensorGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.processData.MoistureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.processData.MoistureSensorGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.processData.MoistureSensorGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'PlantMoisture'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'PlantMoisture'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor.additional.elements.in.type:DA-END
} // end of java type