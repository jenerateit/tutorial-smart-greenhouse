package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

public class TemperatureAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<TemperatureSensorGroveData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TemperatureAccessor.class);
    
    private TemperatureSensorGroveDeviceDriver driver;
    
    private TemperatureSensorGroveData data;
    
    private TemperatureSensorGroveData sentData;
    
    private final LinkedList<TemperatureSensorGroveData> historyData = new LinkedList<TemperatureSensorGroveData>();
    
    /**
     * creates an instance of TemperatureAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public TemperatureAccessor(TemperatureSensorGroveDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.TemperatureSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.TemperatureSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.TemperatureSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.run.void:DA-ELSE
        
        try {
            TemperatureSensorGroveData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Temperature'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public TemperatureSensorGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getData.TemperatureSensorGroveData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Temperature'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Temperature'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getData.TemperatureSensorGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public TemperatureSensorGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getSentData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getSentData.TemperatureSensorGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getSentData.TemperatureSensorGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<TemperatureSensorGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.isNotificationRequired.boolean:DA-START
    	if (sentData == data) {
    		return false;
    	}
    	if (sentData == null) {
    		return true;
    	}
    	if (Math.abs(data.getData().getStatus() - sentData.getData().getStatus()) > 0.2f) {
    		return true;
    	}
    	
    	return false;
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.isNotificationRequired.boolean:DA-ELSE
        //return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.TEMPERATURE.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<TemperatureSensorGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<TemperatureSensorGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.TEMPERATURE, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Temperature'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(TemperatureSensorGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.onInterrupt.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.onInterrupt.TemperatureSensorGroveData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.onInterrupt.TemperatureSensorGroveData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(TemperatureSensorGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.processData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.processData.TemperatureSensorGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.processData.TemperatureSensorGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Temperature'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Temperature'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.TemperatureAccessor.additional.elements.in.type:DA-END
} // end of java type