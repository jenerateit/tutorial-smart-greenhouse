package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

public class AlarmAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<BuzzerGroveData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AlarmAccessor.class);
    
    private BuzzerGroveDeviceDriver driver;
    
    private BuzzerGroveData data;
    
    private BuzzerGroveData sentData;
    
    private final LinkedList<BuzzerGroveData> historyData = new LinkedList<BuzzerGroveData>();
    
    /**
     * creates an instance of AlarmAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public AlarmAccessor(BuzzerGroveDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.BuzzerGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.BuzzerGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.BuzzerGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.run.void:DA-ELSE
        
        try {
            BuzzerGroveData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Alarm'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public BuzzerGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getData.BuzzerGroveData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Alarm'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Alarm'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getData.BuzzerGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public BuzzerGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getSentData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getSentData.BuzzerGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getSentData.BuzzerGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<BuzzerGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.ALARM.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<BuzzerGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<BuzzerGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.ALARM, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Alarm'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(BuzzerGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.onInterrupt.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.onInterrupt.BuzzerGroveData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.onInterrupt.BuzzerGroveData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(BuzzerGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.processData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.processData.BuzzerGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.processData.BuzzerGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Alarm'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Alarm'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.deactivate:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(BuzzerGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.setData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.setData.BuzzerGroveData:DA-ELSE
        try {
            logger.info("setting data by driver for sensor usage 'Alarm'");
            this.driver.setData(data);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while setting data for actuator usage 'Alarm'", ex);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.setData.BuzzerGroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.AlarmAccessor.additional.elements.in.type:DA-END
} // end of java type