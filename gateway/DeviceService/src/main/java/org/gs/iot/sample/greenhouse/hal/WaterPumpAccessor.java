package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

public class WaterPumpAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<RelayGroveData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(WaterPumpAccessor.class);
    
    private RelayGroveDeviceDriver driver;
    
    private RelayGroveData data;
    
    private RelayGroveData sentData;
    
    private final LinkedList<RelayGroveData> historyData = new LinkedList<RelayGroveData>();
    
    /**
     * creates an instance of WaterPumpAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public WaterPumpAccessor(RelayGroveDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.RelayGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.RelayGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.RelayGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.run.void:DA-ELSE
        
        try {
            RelayGroveData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'WaterPump'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public RelayGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getData.RelayGroveData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'WaterPump'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'WaterPump'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getData.RelayGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public RelayGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getSentData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getSentData.RelayGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getSentData.RelayGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<RelayGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.WATERPUMP.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<RelayGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<RelayGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.WATERPUMP, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'WaterPump'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(RelayGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.onInterrupt.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.onInterrupt.RelayGroveData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.onInterrupt.RelayGroveData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(RelayGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.processData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.processData.RelayGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.processData.RelayGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'WaterPump'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'WaterPump'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.deactivate:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(RelayGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.setData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.setData.RelayGroveData:DA-ELSE
        try {
            logger.info("setting data by driver for sensor usage 'WaterPump'");
            this.driver.setData(data);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while setting data for actuator usage 'WaterPump'", ex);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.setData.RelayGroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor.additional.elements.in.type:DA-END
} // end of java type