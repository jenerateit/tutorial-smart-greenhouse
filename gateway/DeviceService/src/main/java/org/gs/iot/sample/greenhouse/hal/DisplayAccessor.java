package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

public class DisplayAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<AdafruitI2cRgbLcdData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DisplayAccessor.class);
    
    private AdafruitI2cRgbLcdDeviceDriver driver;
    
    private AdafruitI2cRgbLcdData data;
    
    private AdafruitI2cRgbLcdData sentData;
    
    private final LinkedList<AdafruitI2cRgbLcdData> historyData = new LinkedList<AdafruitI2cRgbLcdData>();
    
    /**
     * creates an instance of DisplayAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public DisplayAccessor(AdafruitI2cRgbLcdDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.AdafruitI2cRgbLcdDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.AdafruitI2cRgbLcdDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.AdafruitI2cRgbLcdDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.run.void:DA-ELSE
        
        try {
            AdafruitI2cRgbLcdData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Display'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public AdafruitI2cRgbLcdData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getData.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getData.AdafruitI2cRgbLcdData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Display'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Display'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getData.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public AdafruitI2cRgbLcdData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getSentData.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getSentData.AdafruitI2cRgbLcdData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getSentData.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<AdafruitI2cRgbLcdData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.DISPLAY.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<AdafruitI2cRgbLcdData> sensorListener = (SmartGreenhouseAppSensorListenerI<AdafruitI2cRgbLcdData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.DISPLAY, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Display'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(AdafruitI2cRgbLcdData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.onInterrupt.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.onInterrupt.AdafruitI2cRgbLcdData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.onInterrupt.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(AdafruitI2cRgbLcdData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.processData.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.processData.AdafruitI2cRgbLcdData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.processData.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Display'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Display'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.deactivate:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(AdafruitI2cRgbLcdData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.setData.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.setData.AdafruitI2cRgbLcdData:DA-ELSE
        try {
            logger.info("setting data by driver for sensor usage 'Display'");
            this.driver.setData(data);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while setting data for actuator usage 'Display'", ex);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.setData.AdafruitI2cRgbLcdData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.DisplayAccessor.additional.elements.in.type:DA-END
} // end of java type