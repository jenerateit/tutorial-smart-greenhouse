package org.gs.iot.sample.greenhouse.gateway.hal;


import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.MoistureSensorGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver;
import org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.AnalogConnection;
import org.gs.iot.sample.greenhouse.devicedata.DigitalConnection;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveGainEnum;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum;
import org.gs.iot.sample.greenhouse.devicedata.I2cConnection;


public class SmartGreenhouseAppDeviceSetupFactory { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SmartGreenhouseAppDeviceSetupFactory.class);
    
    /**
     * 
     * @return
     */
    public static PIRMotionGroveDeviceDriver.DeviceSetup getSetupForPlantMotion() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMotion.DeviceSetup:DA-START
    	DigitalConnection connection = new DigitalConnection();
    	connection.setPinNumber(PCB.IO_0);
//    	connection.setTrigger();
    	return new PIRMotionGroveDeviceDriver.DeviceSetup(connection);
    	
    	
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMotion.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMotion.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static RelayGroveDeviceDriver.DeviceSetup getSetupForWaterPump() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForWaterPump.DeviceSetup:DA-START
    	DigitalConnection connection = new DigitalConnection();
    	connection.setPinNumber(PCB.IO_1);
    	return new RelayGroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForWaterPump.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForWaterPump.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static BuzzerGroveDeviceDriver.DeviceSetup getSetupForAlarm() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForAlarm.DeviceSetup:DA-START
    	DigitalConnection connection = new DigitalConnection();
    	connection.setPinNumber(PCB.IO_2);
    	return new BuzzerGroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForAlarm.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForAlarm.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static DigitalLightTSL2561GroveDeviceDriver.DeviceSetup getSetupForPlantIlluminance() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantIlluminance.DeviceSetup:DA-START
    	I2cConnection connection = new I2cConnection();
    	connection.seti2cBus(1);
    	connection.seti2cAddress(0);
    	DigitalLightTSL2561GroveConfiguration configuration = new DigitalLightTSL2561GroveConfiguration();
    	configuration.setGain(DigitalLightTSL2561GroveGainEnum.X1);
    	configuration.setIntegrateTime(DigitalLightTSL2561GroveIntegrateTimeEnum.OneHoundredOne);
    	return new DigitalLightTSL2561GroveDeviceDriver.DeviceSetup(configuration, connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantIlluminance.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantIlluminance.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static AdafruitI2cRgbLcdDeviceDriver.DeviceSetup getSetupForDisplay() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForDisplay.DeviceSetup:DA-START
    	I2cConnection connection = new I2cConnection();
    	connection.seti2cBus(1);
    	connection.seti2cAddress(0x20);
    	return new AdafruitI2cRgbLcdDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForDisplay.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForDisplay.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static BarometerBMP180GroveDeviceDriver.DeviceSetup getSetupForPlantBarometer() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantBarometer.DeviceSetup:DA-START
    	I2cConnection connection = new I2cConnection();
    	connection.seti2cBus(1);
    	connection.seti2cAddress(0);
    	return new BarometerBMP180GroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantBarometer.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantBarometer.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static Mq2GasSensorGroveDeviceDriver.DeviceSetup getSetupForMq2Gas() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForMq2Gas.DeviceSetup:DA-START
    	AnalogConnection connection = new AnalogConnection();
    	connection.setChannel(PCB.ADC_0);
    	return new Mq2GasSensorGroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForMq2Gas.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForMq2Gas.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static TemperatureSensorGroveDeviceDriver.DeviceSetup getSetupForTemperature() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForTemperature.DeviceSetup:DA-START
    	AnalogConnection connection = new AnalogConnection();
    	connection.setChannel(PCB.ADC_2);
    	return new TemperatureSensorGroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForTemperature.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForTemperature.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static MoistureSensorGroveDeviceDriver.DeviceSetup getSetupForPlantMoisture() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMoisture.DeviceSetup:DA-START
    	AnalogConnection connection = new AnalogConnection();
    	connection.setChannel(PCB.ADC_1);
    	return new MoistureSensorGroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMoisture.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMoisture.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static LightStripDeviceDriver.DeviceSetup getSetupForLight() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForLight.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForLight.DeviceSetup:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForLight.DeviceSetup:DA-END
    }
    /**
     * 
     * @return
     */
    public static RpiCameraDeviceDriver.DeviceSetup getSetupForCamera() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForCamera.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForCamera.DeviceSetup:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.getSetupForCamera.DeviceSetup:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory.additional.elements.in.type:DA-END
} // end of java type