package org.gs.iot.sample.greenhouse.gateway.hal;

public final class PCB {

	public static final int IO_0 = 21;
	public static final int IO_1 = 20;
	public static final int IO_2 = 16;
	public static final int IO_3 = 12;
	public static final int IO_4 = 25;
	public static final int IO_5 = 24;
	
	public static final int ADC_0 = 0;
	public static final int ADC_1 = 1;
	public static final int ADC_2 = 2;
	public static final int ADC_3 = 3;
	public static final int ADC_4 = 4;
	public static final int ADC_5 = 5;
	public static final int ADC_6 = 6;
	public static final int ADC_7 = 7;
	
}
