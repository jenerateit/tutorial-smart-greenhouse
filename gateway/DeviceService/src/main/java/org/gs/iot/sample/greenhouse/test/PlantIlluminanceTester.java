package org.gs.iot.sample.greenhouse.test;


import org.slf4j.Logger;

/**
 * I2C Sensors
 */
public class PlantIlluminanceTester { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantIlluminanceTester.class);
    
    
    //DA-START:org.gs.iot.sample.greenhouse.test.PlantIlluminanceTester.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.test.PlantIlluminanceTester.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.test.PlantIlluminanceTester.additional.elements.in.type:DA-END
} // end of java type