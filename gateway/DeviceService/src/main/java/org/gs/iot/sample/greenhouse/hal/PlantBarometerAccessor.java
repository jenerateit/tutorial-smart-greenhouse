package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

public class PlantBarometerAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<BarometerBMP180GroveData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantBarometerAccessor.class);
    
    private BarometerBMP180GroveDeviceDriver driver;
    
    private BarometerBMP180GroveData data;
    
    private BarometerBMP180GroveData sentData;
    
    private final LinkedList<BarometerBMP180GroveData> historyData = new LinkedList<BarometerBMP180GroveData>();
    
    /**
     * creates an instance of PlantBarometerAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public PlantBarometerAccessor(BarometerBMP180GroveDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.BarometerBMP180GroveDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.BarometerBMP180GroveDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.BarometerBMP180GroveDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.run.void:DA-ELSE
        
        try {
            BarometerBMP180GroveData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'PlantBarometer'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public BarometerBMP180GroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getData.BarometerBMP180GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getData.BarometerBMP180GroveData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'PlantBarometer'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'PlantBarometer'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getData.BarometerBMP180GroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public BarometerBMP180GroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getSentData.BarometerBMP180GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getSentData.BarometerBMP180GroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getSentData.BarometerBMP180GroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<BarometerBMP180GroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.PLANTBAROMETER.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<BarometerBMP180GroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<BarometerBMP180GroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.PLANTBAROMETER, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'PlantBarometer'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(BarometerBMP180GroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.onInterrupt.BarometerBMP180GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.onInterrupt.BarometerBMP180GroveData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.onInterrupt.BarometerBMP180GroveData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(BarometerBMP180GroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.processData.BarometerBMP180GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.processData.BarometerBMP180GroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.processData.BarometerBMP180GroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'PlantBarometer'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'PlantBarometer'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor.additional.elements.in.type:DA-END
} // end of java type