package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

public class CameraAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<RpiCameraData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(CameraAccessor.class);
    
    private RpiCameraDeviceDriver driver;
    
    private RpiCameraData data;
    
    private RpiCameraData sentData;
    
    private final LinkedList<RpiCameraData> historyData = new LinkedList<RpiCameraData>();
    
    /**
     * creates an instance of CameraAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public CameraAccessor(RpiCameraDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.RpiCameraDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.RpiCameraDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.RpiCameraDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.run.void:DA-ELSE
        
        try {
            RpiCameraData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Camera'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public RpiCameraData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getData.RpiCameraData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getData.RpiCameraData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Camera'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Camera'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getData.RpiCameraData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public RpiCameraData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getSentData.RpiCameraData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getSentData.RpiCameraData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getSentData.RpiCameraData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<RpiCameraData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.CAMERA.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<RpiCameraData> sensorListener = (SmartGreenhouseAppSensorListenerI<RpiCameraData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.CAMERA, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Camera'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(RpiCameraData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.onInterrupt.RpiCameraData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.onInterrupt.RpiCameraData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.onInterrupt.RpiCameraData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(RpiCameraData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.processData.RpiCameraData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.processData.RpiCameraData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.processData.RpiCameraData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Camera'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Camera'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.CameraAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.CameraAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.CameraAccessor.additional.elements.in.type:DA-END
} // end of java type