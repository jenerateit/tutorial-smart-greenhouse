package org.gs.iot.sample.greenhouse.gateway.impl;


import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI;
import java.util.Map;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.hal.PlantMotionAccessor;
import org.gs.iot.sample.greenhouse.hal.WaterPumpAccessor;
import org.gs.iot.sample.greenhouse.hal.AlarmAccessor;
import org.gs.iot.sample.greenhouse.hal.PlantIlluminanceAccessor;
import org.gs.iot.sample.greenhouse.hal.DisplayAccessor;
import org.gs.iot.sample.greenhouse.hal.PlantBarometerAccessor;
import org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor;
import org.gs.iot.sample.greenhouse.hal.TemperatureAccessor;
import org.gs.iot.sample.greenhouse.hal.PlantMoistureAccessor;
import org.gs.iot.sample.greenhouse.hal.LightAccessor;
import org.gs.iot.sample.greenhouse.hal.CameraAccessor;
import org.osgi.util.tracker.ServiceTracker;
import java.util.concurrent.ScheduledFuture;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.gs.iot.sample.greenhouse.gateway.hal.SmartGreenhouseAppDeviceSetupFactory;
import org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData;
import org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;
import org.gs.iot.sample.greenhouse.driver.MoistureSensorGroveDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;
import org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraData;
import org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver;
import java.util.Collection;
import java.util.Collections;

public class SmartGreenhouseAppDeviceServiceImpl implements SmartGreenhouseAppDeviceServiceI { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SmartGreenhouseAppDeviceServiceImpl.class);
    
    private PlantMotionAccessor accessorPlantMotion;
    
    private WaterPumpAccessor accessorWaterPump;
    
    private AlarmAccessor accessorAlarm;
    
    private PlantIlluminanceAccessor accessorPlantIlluminance;
    
    private DisplayAccessor accessorDisplay;
    
    private PlantBarometerAccessor accessorPlantBarometer;
    
    private Mq2GasAccessor accessorMq2Gas;
    
    private TemperatureAccessor accessorTemperature;
    
    private PlantMoistureAccessor accessorPlantMoisture;
    
    private LightAccessor accessorLight;
    
    private CameraAccessor accessorCamera;
    
    private ServiceTracker<?,?> serviceTrackerForPublishable;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForPlantMotion;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForWaterPump;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForAlarm;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForPlantIlluminance;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForDisplay;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForPlantBarometer;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForMq2Gas;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForTemperature;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForPlantMoisture;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForLight;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForCamera;
    
    private ScheduledFuture<?> scheduledFutureForPlantMotion;
    
    private ScheduledFuture<?> scheduledFutureForWaterPump;
    
    private ScheduledFuture<?> scheduledFutureForAlarm;
    
    private ScheduledFuture<?> scheduledFutureForPlantIlluminance;
    
    private ScheduledFuture<?> scheduledFutureForDisplay;
    
    private ScheduledFuture<?> scheduledFutureForPlantBarometer;
    
    private ScheduledFuture<?> scheduledFutureForMq2Gas;
    
    private ScheduledFuture<?> scheduledFutureForTemperature;
    
    private ScheduledFuture<?> scheduledFutureForPlantMoisture;
    
    private ScheduledFuture<?> scheduledFutureForLight;
    
    private ScheduledFuture<?> scheduledFutureForCamera;
    
    /**
     * creates an instance of SmartGreenhouseAppDeviceServiceImpl
     */
    public SmartGreenhouseAppDeviceServiceImpl() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.activate.ComponentContext.BundleContext.Map:DA-ELSE
        this.properties = properties;
        this.serviceTrackerForPublishable = new ServiceTracker<>(bundleContext, SmartGreenhouseAppPublishable.class, null);
        this.serviceTrackerForPublishable.open();
        
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantMotion 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + PIRMotionGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.PLANTMOTION.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForPlantMotion = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForPlantMotion.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'PlantMotion' in activation of device service", ex);
        }
        
        this.accessorPlantMotion = new PlantMotionAccessor(new PIRMotionGroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMotion()), serviceTrackerForSensorListenerForPlantMotion, serviceTrackerForPublishable);
        try {
            this.accessorPlantMotion.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'PlantMotion'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage WaterPump 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + RelayGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.WATERPUMP.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForWaterPump = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForWaterPump.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'WaterPump' in activation of device service", ex);
        }
        
        this.accessorWaterPump = new WaterPumpAccessor(new RelayGroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForWaterPump()), serviceTrackerForSensorListenerForWaterPump, serviceTrackerForPublishable);
        try {
            this.accessorWaterPump.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'WaterPump'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Alarm 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + BuzzerGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.ALARM.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForAlarm = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForAlarm.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Alarm' in activation of device service", ex);
        }
        
        this.accessorAlarm = new AlarmAccessor(new BuzzerGroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForAlarm()), serviceTrackerForSensorListenerForAlarm, serviceTrackerForPublishable);
        try {
            this.accessorAlarm.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Alarm'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantIlluminance 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + DigitalLightTSL2561GroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.PLANTILLUMINANCE.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForPlantIlluminance = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForPlantIlluminance.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'PlantIlluminance' in activation of device service", ex);
        }
        
        this.accessorPlantIlluminance = new PlantIlluminanceAccessor(new DigitalLightTSL2561GroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantIlluminance()), serviceTrackerForSensorListenerForPlantIlluminance, serviceTrackerForPublishable);
        try {
            this.accessorPlantIlluminance.activate();
            this.scheduledFutureForPlantIlluminance = SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorPlantIlluminance, 5000L, 5000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'PlantIlluminance'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Display 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + AdafruitI2cRgbLcdData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.DISPLAY.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForDisplay = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForDisplay.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Display' in activation of device service", ex);
        }
        
        this.accessorDisplay = new DisplayAccessor(new AdafruitI2cRgbLcdDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForDisplay()), serviceTrackerForSensorListenerForDisplay, serviceTrackerForPublishable);
        try {
            this.accessorDisplay.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Display'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantBarometer 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + BarometerBMP180GroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.PLANTBAROMETER.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForPlantBarometer = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForPlantBarometer.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'PlantBarometer' in activation of device service", ex);
        }
        
        this.accessorPlantBarometer = new PlantBarometerAccessor(new BarometerBMP180GroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantBarometer()), serviceTrackerForSensorListenerForPlantBarometer, serviceTrackerForPublishable);
        try {
            this.accessorPlantBarometer.activate();
            this.scheduledFutureForPlantBarometer = SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorPlantBarometer, 5000L, 5000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'PlantBarometer'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Mq2Gas 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + Mq2GasSensorGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.MQ2GAS.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForMq2Gas = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForMq2Gas.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Mq2Gas' in activation of device service", ex);
        }
        
        this.accessorMq2Gas = new Mq2GasAccessor(new Mq2GasSensorGroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForMq2Gas()), serviceTrackerForSensorListenerForMq2Gas, serviceTrackerForPublishable);
        try {
            this.accessorMq2Gas.activate();
            this.scheduledFutureForMq2Gas = SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorMq2Gas, 5000L, 5000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Mq2Gas'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Temperature 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + TemperatureSensorGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.TEMPERATURE.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForTemperature = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForTemperature.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Temperature' in activation of device service", ex);
        }
        
        this.accessorTemperature = new TemperatureAccessor(new TemperatureSensorGroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForTemperature()), serviceTrackerForSensorListenerForTemperature, serviceTrackerForPublishable);
        try {
            this.accessorTemperature.activate();
            this.scheduledFutureForTemperature = SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorTemperature, 5000L, 5000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Temperature'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantMoisture 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + MoistureSensorGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.PLANTMOISTURE.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForPlantMoisture = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForPlantMoisture.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'PlantMoisture' in activation of device service", ex);
        }
        
        this.accessorPlantMoisture = new PlantMoistureAccessor(new MoistureSensorGroveDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForPlantMoisture()), serviceTrackerForSensorListenerForPlantMoisture, serviceTrackerForPublishable);
        try {
            this.accessorPlantMoisture.activate();
            this.scheduledFutureForPlantMoisture = SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorPlantMoisture, 5000L, 5000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'PlantMoisture'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Light 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + LightStripData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.LIGHT.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForLight = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForLight.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Light' in activation of device service", ex);
        }
        
        this.accessorLight = new LightAccessor(new LightStripDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForLight()), serviceTrackerForSensorListenerForLight, serviceTrackerForPublishable);
        try {
            this.accessorLight.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Light'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Camera 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + SmartGreenhouseAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + RpiCameraData.class.getSimpleName() + ")"
        			+ "(usage=" + SmartGreenhouseAppDeviceConfigEnum.CAMERA.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForCamera = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForCamera.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Camera' in activation of device service", ex);
        }
        
        this.accessorCamera = new CameraAccessor(new RpiCameraDeviceDriver(SmartGreenhouseAppDeviceSetupFactory.getSetupForCamera()), serviceTrackerForSensorListenerForCamera, serviceTrackerForPublishable);
        try {
            this.accessorCamera.activate();
            this.scheduledFutureForCamera = SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorCamera, 600000L, 600000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Camera'", ex);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantMotion 
        try {
            if (this.scheduledFutureForPlantMotion != null) this.scheduledFutureForPlantMotion.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'PlantMotion'", th);
        }
        try {
            this.accessorPlantMotion.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'PlantMotion'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage WaterPump 
        try {
            if (this.scheduledFutureForWaterPump != null) this.scheduledFutureForWaterPump.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'WaterPump'", th);
        }
        try {
            this.accessorWaterPump.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'WaterPump'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Alarm 
        try {
            if (this.scheduledFutureForAlarm != null) this.scheduledFutureForAlarm.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Alarm'", th);
        }
        try {
            this.accessorAlarm.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Alarm'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantIlluminance 
        try {
            if (this.scheduledFutureForPlantIlluminance != null) this.scheduledFutureForPlantIlluminance.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'PlantIlluminance'", th);
        }
        try {
            this.accessorPlantIlluminance.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'PlantIlluminance'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Display 
        try {
            if (this.scheduledFutureForDisplay != null) this.scheduledFutureForDisplay.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Display'", th);
        }
        try {
            this.accessorDisplay.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Display'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantBarometer 
        try {
            if (this.scheduledFutureForPlantBarometer != null) this.scheduledFutureForPlantBarometer.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'PlantBarometer'", th);
        }
        try {
            this.accessorPlantBarometer.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'PlantBarometer'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Mq2Gas 
        try {
            if (this.scheduledFutureForMq2Gas != null) this.scheduledFutureForMq2Gas.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Mq2Gas'", th);
        }
        try {
            this.accessorMq2Gas.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Mq2Gas'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Temperature 
        try {
            if (this.scheduledFutureForTemperature != null) this.scheduledFutureForTemperature.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Temperature'", th);
        }
        try {
            this.accessorTemperature.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Temperature'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage PlantMoisture 
        try {
            if (this.scheduledFutureForPlantMoisture != null) this.scheduledFutureForPlantMoisture.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'PlantMoisture'", th);
        }
        try {
            this.accessorPlantMoisture.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'PlantMoisture'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Light 
        try {
            if (this.scheduledFutureForLight != null) this.scheduledFutureForLight.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Light'", th);
        }
        try {
            this.accessorLight.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Light'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Camera 
        try {
            if (this.scheduledFutureForCamera != null) this.scheduledFutureForCamera.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Camera'", th);
        }
        try {
            this.accessorCamera.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Camera'", ex);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @return
     */
    public PIRMotionGroveData getPlantMotion() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotion.PIRMotionGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotion.PIRMotionGroveData:DA-ELSE
        return this.accessorPlantMotion.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotion.PIRMotionGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public PIRMotionGroveData getPlantMotionLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotionLastSentData.PIRMotionGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotionLastSentData.PIRMotionGroveData:DA-ELSE
        return this.accessorPlantMotion.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotionLastSentData.PIRMotionGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<PIRMotionGroveData> getPlantMotionHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotionHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotionHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorPlantMotion.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMotionHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public RelayGroveData getWaterPump() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPump.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPump.RelayGroveData:DA-ELSE
        return this.accessorWaterPump.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPump.RelayGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public RelayGroveData getWaterPumpLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPumpLastSentData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPumpLastSentData.RelayGroveData:DA-ELSE
        return this.accessorWaterPump.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPumpLastSentData.RelayGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<RelayGroveData> getWaterPumpHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPumpHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPumpHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorWaterPump.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getWaterPumpHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public BuzzerGroveData getAlarm() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarm.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarm.BuzzerGroveData:DA-ELSE
        return this.accessorAlarm.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarm.BuzzerGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public BuzzerGroveData getAlarmLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarmLastSentData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarmLastSentData.BuzzerGroveData:DA-ELSE
        return this.accessorAlarm.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarmLastSentData.BuzzerGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<BuzzerGroveData> getAlarmHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarmHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarmHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorAlarm.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getAlarmHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public DigitalLightTSL2561GroveData getPlantIlluminance() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminance.DigitalLightTSL2561GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminance.DigitalLightTSL2561GroveData:DA-ELSE
        return this.accessorPlantIlluminance.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminance.DigitalLightTSL2561GroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public DigitalLightTSL2561GroveData getPlantIlluminanceLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminanceLastSentData.DigitalLightTSL2561GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminanceLastSentData.DigitalLightTSL2561GroveData:DA-ELSE
        return this.accessorPlantIlluminance.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminanceLastSentData.DigitalLightTSL2561GroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<DigitalLightTSL2561GroveData> getPlantIlluminanceHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminanceHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminanceHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorPlantIlluminance.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantIlluminanceHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public AdafruitI2cRgbLcdData getDisplay() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplay.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplay.AdafruitI2cRgbLcdData:DA-ELSE
        return this.accessorDisplay.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplay.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     * 
     * @return
     */
    public AdafruitI2cRgbLcdData getDisplayLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplayLastSentData.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplayLastSentData.AdafruitI2cRgbLcdData:DA-ELSE
        return this.accessorDisplay.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplayLastSentData.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<AdafruitI2cRgbLcdData> getDisplayHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplayHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplayHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorDisplay.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getDisplayHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public BarometerBMP180GroveData getPlantBarometer() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometer.BarometerBMP180GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometer.BarometerBMP180GroveData:DA-ELSE
        return this.accessorPlantBarometer.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometer.BarometerBMP180GroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public BarometerBMP180GroveData getPlantBarometerLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometerLastSentData.BarometerBMP180GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometerLastSentData.BarometerBMP180GroveData:DA-ELSE
        return this.accessorPlantBarometer.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometerLastSentData.BarometerBMP180GroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<BarometerBMP180GroveData> getPlantBarometerHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometerHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometerHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorPlantBarometer.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantBarometerHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public Mq2GasSensorGroveData getMq2Gas() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2Gas.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2Gas.Mq2GasSensorGroveData:DA-ELSE
        return this.accessorMq2Gas.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2Gas.Mq2GasSensorGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Mq2GasSensorGroveData getMq2GasLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2GasLastSentData.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2GasLastSentData.Mq2GasSensorGroveData:DA-ELSE
        return this.accessorMq2Gas.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2GasLastSentData.Mq2GasSensorGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<Mq2GasSensorGroveData> getMq2GasHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2GasHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2GasHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorMq2Gas.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getMq2GasHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public TemperatureSensorGroveData getTemperature() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperature.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperature.TemperatureSensorGroveData:DA-ELSE
        return this.accessorTemperature.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperature.TemperatureSensorGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public TemperatureSensorGroveData getTemperatureLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperatureLastSentData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperatureLastSentData.TemperatureSensorGroveData:DA-ELSE
        return this.accessorTemperature.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperatureLastSentData.TemperatureSensorGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<TemperatureSensorGroveData> getTemperatureHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperatureHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperatureHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorTemperature.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getTemperatureHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public MoistureSensorGroveData getPlantMoisture() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoisture.MoistureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoisture.MoistureSensorGroveData:DA-ELSE
        return this.accessorPlantMoisture.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoisture.MoistureSensorGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public MoistureSensorGroveData getPlantMoistureLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoistureLastSentData.MoistureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoistureLastSentData.MoistureSensorGroveData:DA-ELSE
        return this.accessorPlantMoisture.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoistureLastSentData.MoistureSensorGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<MoistureSensorGroveData> getPlantMoistureHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoistureHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoistureHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorPlantMoisture.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getPlantMoistureHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public LightStripData getLight() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLight.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLight.LightStripData:DA-ELSE
        return this.accessorLight.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLight.LightStripData:DA-END
    }
    /**
     * 
     * @return
     */
    public LightStripData getLightLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLightLastSentData.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLightLastSentData.LightStripData:DA-ELSE
        return this.accessorLight.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLightLastSentData.LightStripData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<LightStripData> getLightHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLightHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLightHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorLight.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getLightHistoryData.Collection:DA-END
    }
    /**
     * 
     * @return
     */
    public RpiCameraData getCamera() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCamera.RpiCameraData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCamera.RpiCameraData:DA-ELSE
        return this.accessorCamera.getData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCamera.RpiCameraData:DA-END
    }
    /**
     * 
     * @return
     */
    public RpiCameraData getCameraLastSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCameraLastSentData.RpiCameraData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCameraLastSentData.RpiCameraData:DA-ELSE
        return this.accessorCamera.getSentData();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCameraLastSentData.RpiCameraData:DA-END
    }
    /**
     * 
     * @return
     */
    public Collection<RpiCameraData> getCameraHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCameraHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCameraHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorCamera.getHistoryData() );
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.getCameraHistoryData.Collection:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void setWaterPump(RelayGroveData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setWaterPump.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setWaterPump.RelayGroveData:DA-ELSE
        this.accessorWaterPump.setData(deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setWaterPump.RelayGroveData:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void setAlarm(BuzzerGroveData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setAlarm.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setAlarm.BuzzerGroveData:DA-ELSE
        this.accessorAlarm.setData(deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setAlarm.BuzzerGroveData:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void setDisplay(AdafruitI2cRgbLcdData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setDisplay.AdafruitI2cRgbLcdData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setDisplay.AdafruitI2cRgbLcdData:DA-ELSE
        this.accessorDisplay.setData(deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setDisplay.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void setLight(LightStripData deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setLight.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setLight.LightStripData:DA-ELSE
        this.accessorLight.setData(deviceData);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.setLight.LightStripData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.SmartGreenhouseAppDeviceServiceImpl.additional.elements.in.type:DA-END
} // end of java type