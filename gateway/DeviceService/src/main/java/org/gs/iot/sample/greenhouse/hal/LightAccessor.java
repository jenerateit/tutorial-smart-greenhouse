package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

/**
 * Other Sensors
 */
public class LightAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<LightStripData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(LightAccessor.class);
    
    private LightStripDeviceDriver driver;
    
    private LightStripData data;
    
    private LightStripData sentData;
    
    private final LinkedList<LightStripData> historyData = new LinkedList<LightStripData>();
    
    /**
     * creates an instance of LightAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public LightAccessor(LightStripDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.LightStripDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.LightStripDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.LightStripDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.run.void:DA-ELSE
        
        try {
            LightStripData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Light'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public LightStripData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.getData.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.getData.LightStripData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Light'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Light'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.getData.LightStripData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public LightStripData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.getSentData.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.getSentData.LightStripData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.getSentData.LightStripData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<LightStripData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.LIGHT.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<LightStripData> sensorListener = (SmartGreenhouseAppSensorListenerI<LightStripData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.LIGHT, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Light'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(LightStripData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.onInterrupt.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.onInterrupt.LightStripData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.onInterrupt.LightStripData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(LightStripData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.processData.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.processData.LightStripData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.processData.LightStripData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Light'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Light'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.deactivate:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(LightStripData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.setData.LightStripData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.setData.LightStripData:DA-ELSE
        try {
            logger.info("setting data by driver for sensor usage 'Light'");
            this.driver.setData(data);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while setting data for actuator usage 'Light'", ex);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.setData.LightStripData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.LightAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.LightAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.LightAccessor.additional.elements.in.type:DA-END
} // end of java type