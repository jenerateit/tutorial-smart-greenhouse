package org.gs.iot.sample.greenhouse.hal;


import org.gs.iot.sample.greenhouse.gateway.hal.AbstractSmartGreenhouseAppAbstractAccessor;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver;
import java.util.LinkedList;
import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;

/**
 * Analog Sensors
 */
public class Mq2GasAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable, SmartGreenhouseAppInterruptCallback<Mq2GasSensorGroveData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Mq2GasAccessor.class);
    
    private Mq2GasSensorGroveDeviceDriver driver;
    
    private Mq2GasSensorGroveData data;
    
    private Mq2GasSensorGroveData sentData;
    
    private final LinkedList<Mq2GasSensorGroveData> historyData = new LinkedList<Mq2GasSensorGroveData>();
    
    /**
     * creates an instance of Mq2GasAccessor
     * 
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public Mq2GasAccessor(Mq2GasSensorGroveDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.Mq2GasSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.Mq2GasSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.Mq2GasSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.run.void:DA-ELSE
        
        try {
            Mq2GasSensorGroveData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Mq2Gas'", th);
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public Mq2GasSensorGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getData.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getData.Mq2GasSensorGroveData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Mq2Gas'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Mq2Gas'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getData.Mq2GasSensorGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public Mq2GasSensorGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getSentData.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getSentData.Mq2GasSensorGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getSentData.Mq2GasSensorGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<Mq2GasSensorGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.MQ2GAS.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<Mq2GasSensorGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<Mq2GasSensorGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.MQ2GAS, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Mq2Gas'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    @Override
    public void onInterrupt(Mq2GasSensorGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.onInterrupt.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.onInterrupt.Mq2GasSensorGroveData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.onInterrupt.Mq2GasSensorGroveData:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(Mq2GasSensorGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.processData.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.processData.Mq2GasSensorGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.processData.Mq2GasSensorGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Mq2Gas'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Mq2Gas'");
        //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.Mq2GasAccessor.additional.elements.in.type:DA-END
} // end of java type