package org.gs.iot.sample.greenhouse.devicedata;





public enum DigitalLightTSL2561GroveIntegrateTimeEnum { // start of class
    
    FourHundredTwo,
    
    OneHoundredOne,
    
    ThirteenPointSeven
    //DA-START:devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum.enum.constants:DA-START
    //DA-ELSE:devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum.enum.constants:DA-END
    ;
    
    
    
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum.additional.elements.in.type:DA-END
} // end of java type