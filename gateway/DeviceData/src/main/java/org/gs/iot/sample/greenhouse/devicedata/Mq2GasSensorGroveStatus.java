package org.gs.iot.sample.greenhouse.devicedata;



public class Mq2GasSensorGroveStatus { // start of class

    private float rsAir;
    
    private float r0;
    
    private float rsGas;
    
    private float ratio;
    
    /**
     * getter for the field rsAir
     * 
     * 
     * 
     * @return
     */
    public float getRsAir() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRsAir.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRsAir.float:DA-ELSE
        return this.rsAir;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRsAir.float:DA-END
    }
    /**
     * setter for the field rsAir
     * 
     * 
     * 
     * @param rsAir  the rsAir
     */
    public void setRsAir(float rsAir) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRsAir.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRsAir.float:DA-ELSE
        this.rsAir = rsAir;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRsAir.float:DA-END
    }
    /**
     * getter for the field r0
     * 
     * 
     * 
     * @return
     */
    public float getr0() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getr0.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getr0.float:DA-ELSE
        return this.r0;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getr0.float:DA-END
    }
    /**
     * setter for the field r0
     * 
     * 
     * 
     * @param r0  the r0
     */
    public void setr0(float r0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setr0.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setr0.float:DA-ELSE
        this.r0 = r0;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setr0.float:DA-END
    }
    /**
     * getter for the field rsGas
     * 
     * 
     * 
     * @return
     */
    public float getRsGas() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRsGas.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRsGas.float:DA-ELSE
        return this.rsGas;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRsGas.float:DA-END
    }
    /**
     * setter for the field rsGas
     * 
     * 
     * 
     * @param rsGas  the rsGas
     */
    public void setRsGas(float rsGas) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRsGas.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRsGas.float:DA-ELSE
        this.rsGas = rsGas;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRsGas.float:DA-END
    }
    /**
     * getter for the field ratio
     * 
     * 
     * 
     * @return
     */
    public float getRatio() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRatio.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRatio.float:DA-ELSE
        return this.ratio;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.getRatio.float:DA-END
    }
    /**
     * setter for the field ratio
     * 
     * 
     * 
     * @param ratio  the ratio
     */
    public void setRatio(float ratio) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRatio.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRatio.float:DA-ELSE
        this.ratio = ratio;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.setRatio.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus.additional.elements.in.type:DA-END
} // end of java type