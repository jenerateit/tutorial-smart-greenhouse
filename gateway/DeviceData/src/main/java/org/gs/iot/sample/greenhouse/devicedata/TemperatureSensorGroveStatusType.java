package org.gs.iot.sample.greenhouse.devicedata;



public class TemperatureSensorGroveStatusType { // start of class

    private float status;
    
    /**
     * creates an instance of TemperatureSensorGroveStatusType
     * 
     * @param status  the status
     */
    public TemperatureSensorGroveStatusType(float status) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.float:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.float:DA-END
    }
    
    /**
     * creates an instance of TemperatureSensorGroveStatusType
     */
    public TemperatureSensorGroveStatusType() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    public float getStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.getStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.getStatus.float:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.getStatus.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType.additional.elements.in.type:DA-END
} // end of java type