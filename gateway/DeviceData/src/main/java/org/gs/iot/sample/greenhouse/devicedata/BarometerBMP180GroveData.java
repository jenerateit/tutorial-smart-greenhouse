package org.gs.iot.sample.greenhouse.devicedata;


import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.slf4j.Logger;

public class BarometerBMP180GroveData extends AbstractSmartGreenhouseAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(BarometerBMP180GroveData.class);
    
    private BarometerBMP180GroveStatus data;
    
    private BarometerBMP180GroveFaultException fault;
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public BarometerBMP180GroveStatus getData() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getData.BarometerBMP180GroveStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getData.BarometerBMP180GroveStatus:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getData.BarometerBMP180GroveStatus:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(BarometerBMP180GroveStatus data) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.setData.BarometerBMP180GroveStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.setData.BarometerBMP180GroveStatus:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.setData.BarometerBMP180GroveStatus:DA-END
    }
    /**
     * getter for the field fault
     * 
     * 
     * 
     * @return
     */
    public BarometerBMP180GroveFaultException getFault() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getFault.BarometerBMP180GroveFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getFault.BarometerBMP180GroveFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getFault.BarometerBMP180GroveFaultException:DA-END
    }
    /**
     * setter for the field fault
     * 
     * 
     * 
     * @param fault  the fault
     */
    public void setFault(BarometerBMP180GroveFaultException fault) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.setFault.BarometerBMP180GroveFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.setFault.BarometerBMP180GroveFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.setFault.BarometerBMP180GroveFaultException:DA-END
    }
    /**
     * 
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getPayload.boolean.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.additional.elements.in.type:DA-END
} // end of java type