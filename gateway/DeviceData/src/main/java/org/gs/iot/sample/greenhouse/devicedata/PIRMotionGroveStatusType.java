package org.gs.iot.sample.greenhouse.devicedata;



public class PIRMotionGroveStatusType { // start of class

    private boolean status;
    
    /**
     * creates an instance of PIRMotionGroveStatusType
     * 
     * @param status  the status
     */
    public PIRMotionGroveStatusType(boolean status) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.boolean:DA-END
    }
    
    /**
     * creates an instance of PIRMotionGroveStatusType
     */
    public PIRMotionGroveStatusType() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.isStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType.additional.elements.in.type:DA-END
} // end of java type