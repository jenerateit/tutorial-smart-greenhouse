package org.gs.iot.sample.greenhouse.devicedata;



/**
 * Digital Light TSL2561 Grove
 */
public class DigitalLightTSL2561GroveConfiguration { // start of class

    private DigitalLightTSL2561GroveGainEnum gain;
    
    private DigitalLightTSL2561GroveIntegrateTimeEnum integrateTime;
    
    /**
     * getter for the field gain
     * 
     * 
     * 
     * @return
     */
    public DigitalLightTSL2561GroveGainEnum getGain() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.getGain.DigitalLightTSL2561GroveGainEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.getGain.DigitalLightTSL2561GroveGainEnum:DA-ELSE
        return this.gain;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.getGain.DigitalLightTSL2561GroveGainEnum:DA-END
    }
    /**
     * setter for the field gain
     * 
     * 
     * 
     * @param gain  the gain
     */
    public void setGain(DigitalLightTSL2561GroveGainEnum gain) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.setGain.DigitalLightTSL2561GroveGainEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.setGain.DigitalLightTSL2561GroveGainEnum:DA-ELSE
        this.gain = gain;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.setGain.DigitalLightTSL2561GroveGainEnum:DA-END
    }
    /**
     * getter for the field integrateTime
     * 
     * 
     * 
     * @return
     */
    public DigitalLightTSL2561GroveIntegrateTimeEnum getIntegrateTime() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.getIntegrateTime.DigitalLightTSL2561GroveIntegrateTimeEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.getIntegrateTime.DigitalLightTSL2561GroveIntegrateTimeEnum:DA-ELSE
        return this.integrateTime;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.getIntegrateTime.DigitalLightTSL2561GroveIntegrateTimeEnum:DA-END
    }
    /**
     * setter for the field integrateTime
     * 
     * 
     * 
     * @param integrateTime  the integrateTime
     */
    public void setIntegrateTime(DigitalLightTSL2561GroveIntegrateTimeEnum integrateTime) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.setIntegrateTime.DigitalLightTSL2561GroveIntegrateTimeEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.setIntegrateTime.DigitalLightTSL2561GroveIntegrateTimeEnum:DA-ELSE
        this.integrateTime = integrateTime;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.setIntegrateTime.DigitalLightTSL2561GroveIntegrateTimeEnum:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration.additional.elements.in.type:DA-END
} // end of java type