package org.gs.iot.sample.greenhouse.devicedata;


import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.slf4j.Logger;

public class LightStripData extends AbstractSmartGreenhouseAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(LightStripData.class);
    
    private LightStripStatus data;
    
    private LightStripFaultException fault;
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public LightStripStatus getData() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getData.LightStripStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getData.LightStripStatus:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getData.LightStripStatus:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(LightStripStatus data) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripData.setData.LightStripStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripData.setData.LightStripStatus:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripData.setData.LightStripStatus:DA-END
    }
    /**
     * getter for the field fault
     * 
     * 
     * 
     * @return
     */
    public LightStripFaultException getFault() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getFault.LightStripFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getFault.LightStripFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getFault.LightStripFaultException:DA-END
    }
    /**
     * setter for the field fault
     * 
     * 
     * 
     * @param fault  the fault
     */
    public void setFault(LightStripFaultException fault) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripData.setFault.LightStripFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripData.setFault.LightStripFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripData.setFault.LightStripFaultException:DA-END
    }
    /**
     * 
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getPayload.boolean.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripData.additional.elements.in.type:DA-END
} // end of java type