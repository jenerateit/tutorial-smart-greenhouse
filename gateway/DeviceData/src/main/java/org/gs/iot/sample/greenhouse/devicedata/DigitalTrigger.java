package org.gs.iot.sample.greenhouse.devicedata;





public enum DigitalTrigger { // start of class
    
    RISING,
    
    FALLING,
    
    BOTH
    //DA-START:devicedata.DigitalTrigger.enum.constants:DA-START
    //DA-ELSE:devicedata.DigitalTrigger.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:devicedata.DigitalTrigger.enum.constants:DA-END
    ;
    
    
    
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalTrigger.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalTrigger.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalTrigger.additional.elements.in.type:DA-END
} // end of java type