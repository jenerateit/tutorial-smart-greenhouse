package org.gs.iot.sample.greenhouse.devicedata;


import java.io.IOException;

public class LightStripFaultException extends IOException { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * creates an instance of LightStripFaultException
     * 
     * @param arg0  the arg0
     */
    public LightStripFaultException(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of LightStripFaultException
     * 
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public LightStripFaultException(String arg0, Throwable arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.String.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of LightStripFaultException
     * 
     * @param arg0  the arg0
     */
    public LightStripFaultException(String arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.String:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.String:DA-END
    }
    
    /**
     * creates an instance of LightStripFaultException
     */
    public LightStripFaultException() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException:DA-END
    }
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripFaultException.additional.elements.in.type:DA-END
} // end of java type