package org.gs.iot.sample.greenhouse.devicedata;


import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.slf4j.Logger;

public class RpiCameraData extends AbstractSmartGreenhouseAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(RpiCameraData.class);
    
    private RpiCameraStatus data;
    
    private RpiCameraFaultException fault;
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public RpiCameraStatus getData() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getData.RpiCameraStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getData.RpiCameraStatus:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getData.RpiCameraStatus:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(RpiCameraStatus data) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.setData.RpiCameraStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.setData.RpiCameraStatus:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.setData.RpiCameraStatus:DA-END
    }
    /**
     * getter for the field fault
     * 
     * 
     * 
     * @return
     */
    public RpiCameraFaultException getFault() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getFault.RpiCameraFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getFault.RpiCameraFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getFault.RpiCameraFaultException:DA-END
    }
    /**
     * setter for the field fault
     * 
     * 
     * 
     * @param fault  the fault
     */
    public void setFault(RpiCameraFaultException fault) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.setFault.RpiCameraFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.setFault.RpiCameraFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.setFault.RpiCameraFaultException:DA-END
    }
    /**
     * 
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getPayload.boolean.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.additional.elements.in.type:DA-END
} // end of java type