package org.gs.iot.sample.greenhouse.devicedata;



public class LightStripStatus { // start of class

    private short r;
    
    private short g;
    
    private short b;
    
    private double brightness;
    
    /**
     * getter for the field r
     * 
     * 
     * 
     * @return
     */
    public short getR() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getR.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getR.short:DA-ELSE
        return this.r;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getR.short:DA-END
    }
    /**
     * setter for the field r
     * 
     * 
     * 
     * @param r  the r
     */
    public void setR(short r) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setR.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setR.short:DA-ELSE
        this.r = r;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setR.short:DA-END
    }
    /**
     * getter for the field g
     * 
     * 
     * 
     * @return
     */
    public short getG() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getG.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getG.short:DA-ELSE
        return this.g;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getG.short:DA-END
    }
    /**
     * setter for the field g
     * 
     * 
     * 
     * @param g  the g
     */
    public void setG(short g) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setG.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setG.short:DA-ELSE
        this.g = g;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setG.short:DA-END
    }
    /**
     * getter for the field b
     * 
     * 
     * 
     * @return
     */
    public short getB() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getB.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getB.short:DA-ELSE
        return this.b;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getB.short:DA-END
    }
    /**
     * setter for the field b
     * 
     * 
     * 
     * @param b  the b
     */
    public void setB(short b) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setB.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setB.short:DA-ELSE
        this.b = b;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setB.short:DA-END
    }
    /**
     * getter for the field brightness
     * 
     * 
     * 
     * @return
     */
    public double getBrightness() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getBrightness.double:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getBrightness.double:DA-ELSE
        return this.brightness;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.getBrightness.double:DA-END
    }
    /**
     * setter for the field brightness
     * 
     * 
     * 
     * @param brightness  the brightness
     */
    public void setBrightness(double brightness) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setBrightness.double:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setBrightness.double:DA-ELSE
        this.brightness = brightness;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.setBrightness.double:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.LightStripStatus.additional.elements.in.type:DA-END
} // end of java type