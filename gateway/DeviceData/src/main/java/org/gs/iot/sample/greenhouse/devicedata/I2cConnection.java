package org.gs.iot.sample.greenhouse.devicedata;



public class I2cConnection { // start of class

    private int i2cBus;
    
    private int i2cAddress;
    
    /**
     * getter for the field i2cBus
     * 
     * 
     * 
     * @return
     */
    public int geti2cBus() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.geti2cBus.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.geti2cBus.int:DA-ELSE
        return this.i2cBus;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.geti2cBus.int:DA-END
    }
    /**
     * setter for the field i2cBus
     * 
     * 
     * 
     * @param i2cBus  the i2cBus
     */
    public void seti2cBus(int i2cBus) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.seti2cBus.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.seti2cBus.int:DA-ELSE
        this.i2cBus = i2cBus;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.seti2cBus.int:DA-END
    }
    /**
     * getter for the field i2cAddress
     * 
     * 
     * 
     * @return
     */
    public int geti2cAddress() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.geti2cAddress.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.geti2cAddress.int:DA-ELSE
        return this.i2cAddress;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.geti2cAddress.int:DA-END
    }
    /**
     * setter for the field i2cAddress
     * 
     * 
     * 
     * @param i2cAddress  the i2cAddress
     */
    public void seti2cAddress(int i2cAddress) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.seti2cAddress.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.seti2cAddress.int:DA-ELSE
        this.i2cAddress = i2cAddress;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.seti2cAddress.int:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.I2cConnection.additional.elements.in.type:DA-END
} // end of java type