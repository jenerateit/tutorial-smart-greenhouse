package org.gs.iot.sample.greenhouse.devicedata;





public enum AdafruitI2cRgbLcdButtonsEnum { // start of class
    
    BUTTON_SELECT,
    
    BUTTON_RIGHT,
    
    BUTTON_DOWN,
    
    BUTTON_UP,
    
    BUTTON_LEFT,
    
    NOT_PRESSED,
    
    UNKNOWN
    //DA-START:devicedata.AdafruitI2cRgbLcdButtonsEnum.enum.constants:DA-START
    //DA-ELSE:devicedata.AdafruitI2cRgbLcdButtonsEnum.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:devicedata.AdafruitI2cRgbLcdButtonsEnum.enum.constants:DA-END
    ;
    
    
    
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdButtonsEnum.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdButtonsEnum.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdButtonsEnum.additional.elements.in.type:DA-END
} // end of java type