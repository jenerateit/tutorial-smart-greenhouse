package org.gs.iot.sample.greenhouse.devicedata;


import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.slf4j.Logger;

public class AdafruitI2cRgbLcdData extends AbstractSmartGreenhouseAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AdafruitI2cRgbLcdData.class);
    
    private AdafruitI2cRgbLcdStatus data;
    
    private AdafruitI2cRgbLcdFaultException fault;
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public AdafruitI2cRgbLcdStatus getData() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getData.AdafruitI2cRgbLcdStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getData.AdafruitI2cRgbLcdStatus:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getData.AdafruitI2cRgbLcdStatus:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(AdafruitI2cRgbLcdStatus data) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.setData.AdafruitI2cRgbLcdStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.setData.AdafruitI2cRgbLcdStatus:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.setData.AdafruitI2cRgbLcdStatus:DA-END
    }
    /**
     * getter for the field fault
     * 
     * 
     * 
     * @return
     */
    public AdafruitI2cRgbLcdFaultException getFault() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getFault.AdafruitI2cRgbLcdFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getFault.AdafruitI2cRgbLcdFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getFault.AdafruitI2cRgbLcdFaultException:DA-END
    }
    /**
     * setter for the field fault
     * 
     * 
     * 
     * @param fault  the fault
     */
    public void setFault(AdafruitI2cRgbLcdFaultException fault) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.setFault.AdafruitI2cRgbLcdFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.setFault.AdafruitI2cRgbLcdFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.setFault.AdafruitI2cRgbLcdFaultException:DA-END
    }
    /**
     * 
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getPayload.boolean.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.additional.elements.in.type:DA-END
} // end of java type