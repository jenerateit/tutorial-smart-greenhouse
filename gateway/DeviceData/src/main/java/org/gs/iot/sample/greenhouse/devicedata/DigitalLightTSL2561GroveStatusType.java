package org.gs.iot.sample.greenhouse.devicedata;



public class DigitalLightTSL2561GroveStatusType { // start of class

    private float status;
    
    /**
     * creates an instance of DigitalLightTSL2561GroveStatusType
     * 
     * @param status  the status
     */
    public DigitalLightTSL2561GroveStatusType(float status) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.float:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.float:DA-END
    }
    
    /**
     * creates an instance of DigitalLightTSL2561GroveStatusType
     */
    public DigitalLightTSL2561GroveStatusType() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    public float getStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.getStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.getStatus.float:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.getStatus.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType.additional.elements.in.type:DA-END
} // end of java type