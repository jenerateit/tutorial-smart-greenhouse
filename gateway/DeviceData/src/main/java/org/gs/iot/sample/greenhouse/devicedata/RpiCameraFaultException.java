package org.gs.iot.sample.greenhouse.devicedata;


import java.io.IOException;

public class RpiCameraFaultException extends IOException { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * creates an instance of RpiCameraFaultException
     * 
     * @param arg0  the arg0
     */
    public RpiCameraFaultException(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of RpiCameraFaultException
     * 
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public RpiCameraFaultException(String arg0, Throwable arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.String.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of RpiCameraFaultException
     * 
     * @param arg0  the arg0
     */
    public RpiCameraFaultException(String arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.String:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.String:DA-END
    }
    
    /**
     * creates an instance of RpiCameraFaultException
     */
    public RpiCameraFaultException() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException:DA-END
    }
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException.additional.elements.in.type:DA-END
} // end of java type