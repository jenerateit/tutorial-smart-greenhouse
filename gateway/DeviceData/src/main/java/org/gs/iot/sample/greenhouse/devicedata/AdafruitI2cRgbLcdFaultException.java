package org.gs.iot.sample.greenhouse.devicedata;


import java.io.IOException;

public class AdafruitI2cRgbLcdFaultException extends IOException { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * creates an instance of AdafruitI2cRgbLcdFaultException
     * 
     * @param arg0  the arg0
     */
    public AdafruitI2cRgbLcdFaultException(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of AdafruitI2cRgbLcdFaultException
     * 
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public AdafruitI2cRgbLcdFaultException(String arg0, Throwable arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.String.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of AdafruitI2cRgbLcdFaultException
     * 
     * @param arg0  the arg0
     */
    public AdafruitI2cRgbLcdFaultException(String arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.String:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.String:DA-END
    }
    
    /**
     * creates an instance of AdafruitI2cRgbLcdFaultException
     */
    public AdafruitI2cRgbLcdFaultException() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException:DA-END
    }
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdFaultException.additional.elements.in.type:DA-END
} // end of java type