package org.gs.iot.sample.greenhouse.devicedata;



public class MoistureSensorGroveStatusType { // start of class

    private float status;
    
    /**
     * creates an instance of MoistureSensorGroveStatusType
     * 
     * @param status  the status
     */
    public MoistureSensorGroveStatusType(float status) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.float:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.float:DA-END
    }
    
    /**
     * creates an instance of MoistureSensorGroveStatusType
     */
    public MoistureSensorGroveStatusType() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    public float getStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.getStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.getStatus.float:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.getStatus.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveStatusType.additional.elements.in.type:DA-END
} // end of java type