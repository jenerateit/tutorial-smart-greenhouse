package org.gs.iot.sample.greenhouse.devicedata;



public class RelayGroveStatusType { // start of class

    private boolean status;
    
    /**
     * creates an instance of RelayGroveStatusType
     * 
     * @param status  the status
     */
    public RelayGroveStatusType(boolean status) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.boolean:DA-END
    }
    
    /**
     * creates an instance of RelayGroveStatusType
     */
    public RelayGroveStatusType() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.isStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType.additional.elements.in.type:DA-END
} // end of java type