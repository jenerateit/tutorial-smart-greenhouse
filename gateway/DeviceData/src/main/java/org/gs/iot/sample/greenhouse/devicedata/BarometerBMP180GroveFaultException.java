package org.gs.iot.sample.greenhouse.devicedata;


import java.io.IOException;

public class BarometerBMP180GroveFaultException extends IOException { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * creates an instance of BarometerBMP180GroveFaultException
     * 
     * @param arg0  the arg0
     */
    public BarometerBMP180GroveFaultException(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of BarometerBMP180GroveFaultException
     * 
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public BarometerBMP180GroveFaultException(String arg0, Throwable arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.String.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of BarometerBMP180GroveFaultException
     * 
     * @param arg0  the arg0
     */
    public BarometerBMP180GroveFaultException(String arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.String:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.String:DA-END
    }
    
    /**
     * creates an instance of BarometerBMP180GroveFaultException
     */
    public BarometerBMP180GroveFaultException() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException:DA-END
    }
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveFaultException.additional.elements.in.type:DA-END
} // end of java type