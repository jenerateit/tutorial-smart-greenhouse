package org.gs.iot.sample.greenhouse.devicedata;



/**
 * Adafruit I2C RGB LCD
 */
public class AdafruitI2cRgbLcdStatus { // start of class

    private int backlight;
    
    private String row0;
    
    private String row1;
    
    private AdafruitI2cRgbLcdButtonsEnum button;
    
    /**
     * getter for the field backlight
     * 
     * 
     * 
     * @return
     */
    public int getBacklight() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getBacklight.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getBacklight.int:DA-ELSE
        return this.backlight;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getBacklight.int:DA-END
    }
    /**
     * setter for the field backlight
     * 
     * 
     * 
     * @param backlight  the backlight
     */
    public void setBacklight(int backlight) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setBacklight.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setBacklight.int:DA-ELSE
        this.backlight = backlight;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setBacklight.int:DA-END
    }
    /**
     * getter for the field row0
     * 
     * 
     * 
     * @return
     */
    public String getRow0() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getRow0.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getRow0.String:DA-ELSE
        return this.row0;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getRow0.String:DA-END
    }
    /**
     * setter for the field row0
     * 
     * 
     * 
     * @param row0  the row0
     */
    public void setRow0(String row0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setRow0.String:DA-START
		if(row0.length() > COLUMNS) {
			row0 = row0.substring(0, COLUMNS);
		}
		this.row0 = String.format("%1$-" + COLUMNS  + "s", row0);
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setRow0.String:DA-ELSE
        //this.row0 = row0;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setRow0.String:DA-END
    }
    /**
     * getter for the field row1
     * 
     * 
     * 
     * @return
     */
    public String getRow1() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getRow1.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getRow1.String:DA-ELSE
        return this.row1;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getRow1.String:DA-END
    }
    /**
     * setter for the field row1
     * 
     * 
     * 
     * @param row1  the row1
     */
    public void setRow1(String row1) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setRow1.String:DA-START
		if(row1.length() > COLUMNS) {
			row1 = row1.substring(0, COLUMNS);
		}
		this.row1 = String.format("%1$-" + COLUMNS  + "s", row1);
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setRow1.String:DA-ELSE
        //this.row1 = row1;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setRow1.String:DA-END
    }
    /**
     * getter for the field button
     * 
     * 
     * 
     * @return
     */
    public AdafruitI2cRgbLcdButtonsEnum getButton() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-ELSE
        return this.button;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-END
    }
    /**
     * setter for the field button
     * 
     * 
     * 
     * @param button  the button
     */
    public void setButton(AdafruitI2cRgbLcdButtonsEnum button) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-ELSE
        this.button = button;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.additional.elements.in.type:DA-START
    public static final int BACKLIGHT_OFF = 0x00;
	public static final int BACKLIGHT_RED = 0x01;
	public static final int BACKLIGHT_GREEN = 0x02;
	public static final int BACKLIGHT_BLUE = 0x04;
	public static final int BACKLIGHT_YELLOW = BACKLIGHT_RED + BACKLIGHT_GREEN;
	public static final int BACKLIGHT_TEAL = BACKLIGHT_GREEN + BACKLIGHT_BLUE;
	public static final int BACKLIGHT_VIOLET = BACKLIGHT_RED + BACKLIGHT_BLUE;
	public static final int BACKLIGHT_WHITE = BACKLIGHT_RED + BACKLIGHT_GREEN + BACKLIGHT_BLUE;
	public static final int BACKLIGHT_ON = BACKLIGHT_RED + BACKLIGHT_GREEN + BACKLIGHT_BLUE;

	public static final int COLUMNS = 16;
	public static final int ROWS = 2;
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus.additional.elements.in.type:DA-END
} // end of java type