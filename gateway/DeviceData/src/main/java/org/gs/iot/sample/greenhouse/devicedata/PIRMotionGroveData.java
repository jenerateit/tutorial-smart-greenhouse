package org.gs.iot.sample.greenhouse.devicedata;


import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.slf4j.Logger;

public class PIRMotionGroveData extends AbstractSmartGreenhouseAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PIRMotionGroveData.class);
    
    private PIRMotionGroveStatusType data;
    
    private DigitalFaultException fault;
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public PIRMotionGroveStatusType getData() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getData.PIRMotionGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getData.PIRMotionGroveStatusType:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getData.PIRMotionGroveStatusType:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(PIRMotionGroveStatusType data) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.setData.PIRMotionGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.setData.PIRMotionGroveStatusType:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.setData.PIRMotionGroveStatusType:DA-END
    }
    /**
     * getter for the field fault
     * 
     * 
     * 
     * @return
     */
    public DigitalFaultException getFault() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getFault.DigitalFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getFault.DigitalFaultException:DA-END
    }
    /**
     * setter for the field fault
     * 
     * 
     * 
     * @param fault  the fault
     */
    public void setFault(DigitalFaultException fault) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.setFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.setFault.DigitalFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.setFault.DigitalFaultException:DA-END
    }
    /**
     * 
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getPayload.boolean.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.additional.elements.in.type:DA-END
} // end of java type