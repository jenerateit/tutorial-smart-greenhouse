package org.gs.iot.sample.greenhouse.devicedata;


import java.io.IOException;

public class AnalogFaultException extends IOException { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * creates an instance of AnalogFaultException
     * 
     * @param arg0  the arg0
     */
    public AnalogFaultException(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of AnalogFaultException
     * 
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public AnalogFaultException(String arg0, Throwable arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.String.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of AnalogFaultException
     * 
     * @param arg0  the arg0
     */
    public AnalogFaultException(String arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.String:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.String:DA-END
    }
    
    /**
     * creates an instance of AnalogFaultException
     */
    public AnalogFaultException() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException:DA-END
    }
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException.additional.elements.in.type:DA-END
} // end of java type