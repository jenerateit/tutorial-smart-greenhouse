package org.gs.iot.sample.greenhouse.devicedata;



/**
 * Barometer BMP180 Grove
 */
public class BarometerBMP180GroveStatus { // start of class

    private float pressure;
    
    private float temperature;
    
    private float altitude;
    
    /**
     * getter for the field pressure
     * 
     * 
     * 
     * @return
     */
    public float getPressure() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getPressure.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getPressure.float:DA-ELSE
        return this.pressure;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getPressure.float:DA-END
    }
    /**
     * setter for the field pressure
     * 
     * 
     * 
     * @param pressure  the pressure
     */
    public void setPressure(float pressure) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setPressure.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setPressure.float:DA-ELSE
        this.pressure = pressure;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setPressure.float:DA-END
    }
    /**
     * getter for the field temperature
     * 
     * 
     * 
     * @return
     */
    public float getTemperature() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getTemperature.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getTemperature.float:DA-ELSE
        return this.temperature;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getTemperature.float:DA-END
    }
    /**
     * setter for the field temperature
     * 
     * 
     * 
     * @param temperature  the temperature
     */
    public void setTemperature(float temperature) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setTemperature.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setTemperature.float:DA-ELSE
        this.temperature = temperature;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setTemperature.float:DA-END
    }
    /**
     * getter for the field altitude
     * 
     * 
     * 
     * @return
     */
    public float getAltitude() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getAltitude.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getAltitude.float:DA-ELSE
        return this.altitude;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.getAltitude.float:DA-END
    }
    /**
     * setter for the field altitude
     * 
     * 
     * 
     * @param altitude  the altitude
     */
    public void setAltitude(float altitude) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setAltitude.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setAltitude.float:DA-ELSE
        this.altitude = altitude;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.setAltitude.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus.additional.elements.in.type:DA-END
} // end of java type