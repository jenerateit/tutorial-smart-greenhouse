package org.gs.iot.sample.greenhouse.devicedata;


import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.slf4j.Logger;

public class BuzzerGroveData extends AbstractSmartGreenhouseAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(BuzzerGroveData.class);
    
    private BuzzerGroveStatusType data;
    
    private DigitalFaultException fault;
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public BuzzerGroveStatusType getData() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getData.BuzzerGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getData.BuzzerGroveStatusType:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getData.BuzzerGroveStatusType:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(BuzzerGroveStatusType data) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.setData.BuzzerGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.setData.BuzzerGroveStatusType:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.setData.BuzzerGroveStatusType:DA-END
    }
    /**
     * getter for the field fault
     * 
     * 
     * 
     * @return
     */
    public DigitalFaultException getFault() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getFault.DigitalFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getFault.DigitalFaultException:DA-END
    }
    /**
     * setter for the field fault
     * 
     * 
     * 
     * @param fault  the fault
     */
    public void setFault(DigitalFaultException fault) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.setFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.setFault.DigitalFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.setFault.DigitalFaultException:DA-END
    }
    /**
     * 
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getPayload.boolean.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.additional.elements.in.type:DA-END
} // end of java type