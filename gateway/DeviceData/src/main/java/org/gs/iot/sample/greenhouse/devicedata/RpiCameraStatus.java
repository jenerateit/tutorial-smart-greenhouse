package org.gs.iot.sample.greenhouse.devicedata;


/*
 * Imports from last generation
 */
import java.util.Base64;


public class RpiCameraStatus { // start of class

    private String mimetype;
    
    private String image;
    
    /**
     * getter for the field mimetype
     * 
     * 
     * 
     * @return
     */
    public String getMimetype() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.getMimetype.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.getMimetype.String:DA-ELSE
        return this.mimetype;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.getMimetype.String:DA-END
    }
    /**
     * setter for the field mimetype
     * 
     * 
     * 
     * @param mimetype  the mimetype
     */
    public void setMimetype(String mimetype) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.setMimetype.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.setMimetype.String:DA-ELSE
        this.mimetype = mimetype;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.setMimetype.String:DA-END
    }
    /**
     * getter for the field image
     * 
     * 
     * 
     * @return
     */
    public String getImage() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.getImage.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.getImage.String:DA-ELSE
        return this.image;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.getImage.String:DA-END
    }
    /**
     * setter for the field image
     * 
     * 
     * 
     * @param image  the image
     */
    public void setImage(String image) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.setImage.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.setImage.String:DA-ELSE
        this.image = image;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.setImage.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.additional.elements.in.type:DA-START
    public byte[] getImageAsByteArray() {
    	return Base64.getDecoder().decode(this.image);
    }
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus.additional.elements.in.type:DA-END
} // end of java type