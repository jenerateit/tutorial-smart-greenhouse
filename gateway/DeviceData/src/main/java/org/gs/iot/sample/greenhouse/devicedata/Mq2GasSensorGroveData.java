package org.gs.iot.sample.greenhouse.devicedata;


import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.slf4j.Logger;

public class Mq2GasSensorGroveData extends AbstractSmartGreenhouseAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Mq2GasSensorGroveData.class);
    
    private Mq2GasSensorGroveStatus data;
    
    private AnalogFaultException fault;
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public Mq2GasSensorGroveStatus getData() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getData.Mq2GasSensorGroveStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getData.Mq2GasSensorGroveStatus:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getData.Mq2GasSensorGroveStatus:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(Mq2GasSensorGroveStatus data) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.setData.Mq2GasSensorGroveStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.setData.Mq2GasSensorGroveStatus:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.setData.Mq2GasSensorGroveStatus:DA-END
    }
    /**
     * getter for the field fault
     * 
     * 
     * 
     * @return
     */
    public AnalogFaultException getFault() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getFault.AnalogFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getFault.AnalogFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getFault.AnalogFaultException:DA-END
    }
    /**
     * setter for the field fault
     * 
     * 
     * 
     * @param fault  the fault
     */
    public void setFault(AnalogFaultException fault) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.setFault.AnalogFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.setFault.AnalogFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.setFault.AnalogFaultException:DA-END
    }
    /**
     * 
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getPayload.boolean.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.additional.elements.in.type:DA-END
} // end of java type