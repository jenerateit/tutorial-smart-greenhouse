package org.gs.iot.sample.greenhouse.devicedata;



public class BuzzerGroveStatusType { // start of class

    private boolean status;
    
    /**
     * creates an instance of BuzzerGroveStatusType
     * 
     * @param status  the status
     */
    public BuzzerGroveStatusType(boolean status) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.boolean:DA-END
    }
    
    /**
     * creates an instance of BuzzerGroveStatusType
     */
    public BuzzerGroveStatusType() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.isStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType.additional.elements.in.type:DA-END
} // end of java type