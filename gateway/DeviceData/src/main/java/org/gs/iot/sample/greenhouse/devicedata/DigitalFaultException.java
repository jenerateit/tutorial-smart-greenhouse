package org.gs.iot.sample.greenhouse.devicedata;


import java.io.IOException;

public class DigitalFaultException extends IOException { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * creates an instance of DigitalFaultException
     * 
     * @param arg0  the arg0
     */
    public DigitalFaultException(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of DigitalFaultException
     * 
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public DigitalFaultException(String arg0, Throwable arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.String.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of DigitalFaultException
     * 
     * @param arg0  the arg0
     */
    public DigitalFaultException(String arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.String:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.String:DA-END
    }
    
    /**
     * creates an instance of DigitalFaultException
     */
    public DigitalFaultException() {
        //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException:DA-END
    }
    
    
    //DA-START:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException.additional.elements.in.type:DA-END
} // end of java type