package org.gs.iot.sample.greenhouse.gateway.californium;

import java.util.Map;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.Resource;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmartGreenhouseAppCaliforniumDataService {

	private static final Logger logger = LoggerFactory.getLogger(SmartGreenhouseAppCaliforniumDataService.class);

	private SmartGreenhouseAppDeviceServiceI deviceService;
	private Map<String, Object> properties;
	private CoapServer coapServer;

	private class SmartGreenhouseCoapRessource extends CoapResource {

		SmartGreenhouseAppDeviceConfigEnum configEnum;

		public SmartGreenhouseCoapRessource(String name, SmartGreenhouseAppDeviceConfigEnum configEnum) {
			super(name);
			this.configEnum = configEnum;
		}

		@Override
		public void handleGET(CoapExchange exchange) {
			ResponseCode responseCode = ResponseCode.CONTENT;

			switch (configEnum) {

			case PLANTMOTION:
				exchange.respond(responseCode, deviceService.getPlantMotion().getPayload());
				break;

			case WATERPUMP:
				exchange.respond(responseCode, deviceService.getWaterPump().getPayload());
				break;

			case ALARM:
				exchange.respond(responseCode, deviceService.getAlarm().getPayload());
				break;

			case PLANTILLUMINANCE:
				exchange.respond(responseCode, deviceService.getPlantIlluminance().getPayload());
				break;

			case DISPLAY:
				exchange.respond(responseCode, deviceService.getDisplay().getPayload());
				break;

			case PLANTBAROMETER:
				exchange.respond(responseCode, deviceService.getPlantBarometer().getPayload());
				break;

			case MQ2GAS:
				exchange.respond(responseCode, deviceService.getMq2Gas().getPayload());
				break;

			case TEMPERATURE:
				exchange.respond(responseCode, deviceService.getTemperature().getPayload());
				break;

			case PLANTMOISTURE:
				exchange.respond(responseCode, deviceService.getPlantMoisture().getPayload());
				break;

			case LIGHT:
				exchange.respond(responseCode, deviceService.getLight().getPayload());
				break;

			case CAMERA:
				exchange.respond(responseCode, deviceService.getCamera().getPayload());
				break;
			default:
				throw new RuntimeException("unhandled enum entry found: '" + configEnum + "'");
			}
		}

		@Override
		public void handlePUT(CoapExchange exchange) {
			byte[] payload = exchange.getRequestPayload();

			switch (configEnum) {
			case WATERPUMP:
				if (payload != null && payload.length > 0) {
					RelayGroveData deviceData = AbstractSmartGreenhouseAppDeviceData
							.createInstance(RelayGroveData.class, payload);
					deviceService.setWaterPump(deviceData);
				}
				break;

			case ALARM:
				if (payload != null && payload.length > 0) {
					BuzzerGroveData deviceData = AbstractSmartGreenhouseAppDeviceData
							.createInstance(BuzzerGroveData.class, payload);
					deviceService.setAlarm(deviceData);
				}
				break;

			case DISPLAY:
				if (payload != null && payload.length > 0) {
					AdafruitI2cRgbLcdData deviceData = AbstractSmartGreenhouseAppDeviceData
							.createInstance(AdafruitI2cRgbLcdData.class, payload);
					deviceService.setDisplay(deviceData);
				}
				break;

			case LIGHT:
				if (payload != null && payload.length > 0) {
					LightStripData deviceData = AbstractSmartGreenhouseAppDeviceData
							.createInstance(LightStripData.class, payload);
					deviceService.setLight(deviceData);
				}
				break;

			default:
				exchange.respond(ResponseCode.METHOD_NOT_ALLOWED);
				return;

			}
			exchange.respond(ResponseCode.CHANGED);
		}

	}

	/**
	 * 
	 * @param componentContext
	 *            the componentContext
	 * @param bundleContext
	 *            the bundleContext
	 * @param properties
	 *            the properties
	 */
	protected void activate(ComponentContext componentContext, BundleContext bundleContext,
			Map<String, Object> properties) {
		this.properties = properties;

		logger.info("activating coap server...");

		coapServer = new CoapServer();

		for (SmartGreenhouseAppDeviceConfigEnum configEnum : SmartGreenhouseAppDeviceConfigEnum.values()) {
			String resource = configEnum.getResource();

			String[] splittedResources = resource.split("/");
			Resource coapResource = coapServer.getRoot();
			for (int i = 0; i < splittedResources.length - 1; i++) {
				Resource tmpCoapRessource = coapResource.getChild(splittedResources[i]);
				if (tmpCoapRessource == null) {
					tmpCoapRessource = new CoapResource(splittedResources[i]);
					coapResource.add(tmpCoapRessource);
				}
				coapResource = tmpCoapRessource;
			}

			SmartGreenhouseCoapRessource greenhouseResource = new SmartGreenhouseCoapRessource(
					splittedResources[splittedResources.length - 1], configEnum);
			coapResource.add(greenhouseResource);

			logger.info("added {} with URI {}", configEnum.getDeviceId(), greenhouseResource.getURI());
		}

		// StringBuilder builder = new StringBuilder();
		//
		// for (Resource rootChild : coapServer.getRoot().getChildren()) {
		// builder.append(rootChild.getURI()).append('\n');
		// for (Resource child : rootChild.getChildren()) {
		// builder.append(child.getURI()).append('\n');
		// }
		// }
		//
		// logger.error("coap URI tree:\n{}\n", builder.toString());

		coapServer.start();
		logger.info("activated coap server");
	}

	/**
	 * 
	 * @param componentContext
	 *            the componentContext
	 */
	protected void modified(ComponentContext componentContext) {
	}

	/**
	 * 
	 * @param reason
	 *            the reason
	 * @param componentContext
	 *            the componentContext
	 * @param bundleContext
	 *            the bundleContext
	 * @param properties
	 *            the properties
	 */
	protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext,
			Map<String, Object> properties) {
		coapServer.stop();
		logger.info("deativated coap server");
	}

	/**
	 * setter for the field deviceService
	 * 
	 * 
	 * 
	 * @param deviceService
	 *            the deviceService
	 */
	public void setDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
		this.deviceService = deviceService;
	}

	/**
	 * unsetter for the field deviceService
	 * 
	 * 
	 * 
	 * @param deviceService
	 *            the deviceService
	 */
	public void unsetDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
		if (this.deviceService == deviceService) {
			this.deviceService = null;
		}
	}

}
