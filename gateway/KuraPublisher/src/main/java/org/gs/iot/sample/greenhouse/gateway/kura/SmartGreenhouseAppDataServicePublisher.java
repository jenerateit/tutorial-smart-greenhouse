package org.gs.iot.sample.greenhouse.gateway.kura;


import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.eclipse.kura.data.DataServiceListener;
import org.eclipse.kura.configuration.ConfigurableComponent;
import java.util.Map;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI;
import org.eclipse.kura.data.DataService;
import org.osgi.util.tracker.ServiceTracker;
import java.util.LinkedHashMap;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppActionable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraData;

public class SmartGreenhouseAppDataServicePublisher implements SmartGreenhouseAppPublishable, DataServiceListener, ConfigurableComponent { // start of class

    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SmartGreenhouseAppDataServicePublisher.class);
    
    private SmartGreenhouseAppDeviceServiceI deviceService;
    
    private DataService dataService;
    
    /**
     * a map where the key is a string-typed topic and the value is a service tracker instance
     */
    private final Map<String, ServiceTracker<Object, Object>> serviceTrackersForActionables = new LinkedHashMap<String, ServiceTracker<Object, Object>>();
    
    public static final String PUBLISH_TOPICPREFIX_PROP_NAME = "publish.appTopicPrefix";
    
    public static final String PUBLISH_QOS_PROP_NAME = "publish.qos";
    
    public static final String PUBLISH_RETAIN_PROP_NAME = "publish.retain";
    
    public static final String PUBLISH_PRIORITY_PROP_NAME = "publish.priority";
    
    /**
     * creates an instance of SmartGreenhouseAppDataServicePublisher
     */
    public SmartGreenhouseAppDataServicePublisher() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher:DA-END
    }
    
    /**
     * 
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.activate.ComponentContext.BundleContext.Map:DA-ELSE
        this.properties = properties;
        
        
        // --- connect if not yet connected
        try {
            if (!this.dataService.isConnected()) {
                this.dataService.connect();
            }
        
            onConnectionEstablished();  // call this callback from here since it is not getting called when autoconnect=true (is this a Kura bug?)
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to connect in component activation", th);
        }
        
        // there is no business logic defined in the model => no calls to services that implement actionable interface are going to be generated 
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.activate.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.modified.ComponentContext:DA-END
    }
    /**
     * 
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    /**
     * 
     * @param resource  the resource
     * @param payload  the payload
     */
    @Override
    public void publish(String resource, byte[] payload) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.publish.String.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.publish.String.byte.ARRAY:DA-ELSE
        
        String topic = null;
        try {
        	String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
        	Integer qos = (Integer) properties.get(PUBLISH_QOS_PROP_NAME);
        	Boolean retain = (Boolean) properties.get(PUBLISH_RETAIN_PROP_NAME);
        	Integer priority = (Integer) properties.get(PUBLISH_PRIORITY_PROP_NAME);
        
        	topic = prefix + (prefix.endsWith("/") ? "" : "/") + resource;
        	int effectivePriority = priority != null ? priority : 2;
            if (dataService != null) {
        	    @SuppressWarnings("unused")
        		int messageId = dataService.publish(topic, payload, qos, retain, effectivePriority);
            } else {
                // TODO what do do in this case?
            }
        
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("failed to publish message for topic '" + topic + "'", th);
            // TODO handle this in a better way
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.publish.String.byte.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public void onDisconnecting() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onDisconnecting.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onDisconnecting.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onDisconnecting.void:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public void onDisconnected() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onDisconnected.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onDisconnected.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onDisconnected.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @return
     */
    @Override
    public void onConnectionLost(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onConnectionLost.Throwable.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onConnectionLost.Throwable.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onConnectionLost.Throwable.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     * @return
     */
    @Override
    public void onMessageArrived(String arg0, byte[] arg1, int arg2, boolean arg3) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessageArrived.String.byte.ARRAY.int.boolean.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessageArrived.String.byte.ARRAY.int.boolean.void:DA-ELSE
        logger.info("message arrived, topic: " + arg0 + ", message: " + arg1);
        
        String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
                
        String topicPart = null;
        if(arg0.startsWith(prefix)) {
        	topicPart = arg0.substring(prefix.length());
        	if (topicPart.startsWith("/")) topicPart = topicPart.substring(1);
        }
        
        String resource = null;
        if (topicPart.startsWith("sensor/")) {
        	resource = topicPart.substring("sensor/".length());
        } else if (topicPart.startsWith("actuator/")) {
        	resource = topicPart.substring("actuator/".length());
        } else if (topicPart.startsWith("logic/")) {
        	resource = topicPart.substring("logic/".length());
        }
        
        if (resource != null) {
        	
        	// --- calling business logic through a service tracker
        	ServiceTracker<?,?> serviceTracker = this.serviceTrackersForActionables.get(resource);
        	if (serviceTracker != null) {
        		SmartGreenhouseAppActionable actionable = (SmartGreenhouseAppActionable) serviceTracker.getService();
        		actionable.action(arg1);
        	}
        	
        	
        	// --- handling requests to get sensor data or to set actuator data
        	SmartGreenhouseAppDeviceConfigEnum identifiedSensorUsage = null;
        	for (SmartGreenhouseAppDeviceConfigEnum configEnumEntry : SmartGreenhouseAppDeviceConfigEnum.values()) {
        		if (configEnumEntry.getResource().equalsIgnoreCase(resource)) {
        			identifiedSensorUsage = configEnumEntry;
        			break;
        		}
        	}
        	
        	if (identifiedSensorUsage != null) {
        		switch (identifiedSensorUsage) {
        	
        	    case PLANTMOTION:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	PIRMotionGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(PIRMotionGroveData.class, arg1);
        			    // PIRMotionGrove is not an actuator => we cannot set a value here
        	        } else {
        				PIRMotionGroveData deviceData = deviceService.getPlantMotion();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case WATERPUMP:
        	        if (arg1 != null && arg1.length > 0) {
        	            
        	        	RelayGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(RelayGroveData.class, arg1);
        			    deviceService.setWaterPump(deviceData);
        	        } else {
        				RelayGroveData deviceData = deviceService.getWaterPump();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case ALARM:
        	        if (arg1 != null && arg1.length > 0) {
        	            
        	        	BuzzerGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(BuzzerGroveData.class, arg1);
        			    deviceService.setAlarm(deviceData);
        	        } else {
        				BuzzerGroveData deviceData = deviceService.getAlarm();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case PLANTILLUMINANCE:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	DigitalLightTSL2561GroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(DigitalLightTSL2561GroveData.class, arg1);
        			    // DigitalLightTSL2561Grove is not an actuator => we cannot set a value here
        	        } else {
        				DigitalLightTSL2561GroveData deviceData = deviceService.getPlantIlluminance();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case DISPLAY:
        	        if (arg1 != null && arg1.length > 0) {
        	            
        	        	AdafruitI2cRgbLcdData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(AdafruitI2cRgbLcdData.class, arg1);
        			    deviceService.setDisplay(deviceData);
        	        } else {
        				AdafruitI2cRgbLcdData deviceData = deviceService.getDisplay();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case PLANTBAROMETER:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	BarometerBMP180GroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(BarometerBMP180GroveData.class, arg1);
        			    // BarometerBMP180Grove is not an actuator => we cannot set a value here
        	        } else {
        				BarometerBMP180GroveData deviceData = deviceService.getPlantBarometer();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case MQ2GAS:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	Mq2GasSensorGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(Mq2GasSensorGroveData.class, arg1);
        			    // Mq2GasSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				Mq2GasSensorGroveData deviceData = deviceService.getMq2Gas();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case TEMPERATURE:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	TemperatureSensorGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(TemperatureSensorGroveData.class, arg1);
        			    // TemperatureSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				TemperatureSensorGroveData deviceData = deviceService.getTemperature();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case PLANTMOISTURE:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	MoistureSensorGroveData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(MoistureSensorGroveData.class, arg1);
        			    // MoistureSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				MoistureSensorGroveData deviceData = deviceService.getPlantMoisture();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case LIGHT:
        	        if (arg1 != null && arg1.length > 0) {
        	            
        	        	LightStripData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(LightStripData.class, arg1);
        			    deviceService.setLight(deviceData);
        	        } else {
        				LightStripData deviceData = deviceService.getLight();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case CAMERA:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	RpiCameraData deviceData = AbstractSmartGreenhouseAppDeviceData.createInstance(RpiCameraData.class, arg1);
        			    // RpiCamera is not an actuator => we cannot set a value here
        	        } else {
        				RpiCameraData deviceData = deviceService.getCamera();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        	    default:
        			throw new RuntimeException("unhandled enum entry found: '" + identifiedSensorUsage + "'");
        	    }
        	}
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessageArrived.String.byte.ARRAY.int.boolean.void:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public void onConnectionEstablished() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onConnectionEstablished.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onConnectionEstablished.void:DA-ELSE
        try {
        	String prefix = ((String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME));
            String subscriptionTopicForActuators = prefix + (prefix.endsWith("/") ? "" : "/") + "actuator/#";
            String subscriptionTopicForSensors = prefix + (prefix.endsWith("/") ? "" : "/") + "sensor/#";
            String subscriptionTopicForBusinessLogic = prefix + (prefix.endsWith("/") ? "" : "/") + "logic/#";
        	dataService.subscribe(subscriptionTopicForSensors, 0);
            dataService.subscribe(subscriptionTopicForActuators, 0);
            dataService.subscribe(subscriptionTopicForBusinessLogic, 0);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to subscribe for topics", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onConnectionEstablished.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @param arg1
     * @return
     */
    @Override
    public void onMessagePublished(int arg0, String arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessagePublished.int.String.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessagePublished.int.String.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessagePublished.int.String.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @param arg1
     * @return
     */
    @Override
    public void onMessageConfirmed(int arg0, String arg1) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessageConfirmed.int.String.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessageConfirmed.int.String.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.onMessageConfirmed.int.String.void:DA-END
    }
    /**
     * setter for the field deviceService
     * 
     * 
     * 
     * @param deviceService  the deviceService
     */
    public void setDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-ELSE
        this.deviceService = deviceService;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.setDeviceService.SmartGreenhouseAppDeviceServiceI:DA-END
    }
    /**
     * unsetter for the field deviceService
     * 
     * 
     * 
     * @param deviceService  the deviceService
     */
    public void unsetDeviceService(SmartGreenhouseAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-ELSE
        if (this.deviceService == deviceService) {
            this.deviceService = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.unsetDeviceService.SmartGreenhouseAppDeviceServiceI:DA-END
    }
    /**
     * setter for the field dataService
     * 
     * 
     * 
     * @param dataService  the dataService
     */
    public void setDataService(DataService dataService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.setDataService.DataService:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.setDataService.DataService:DA-ELSE
        this.dataService = dataService;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.setDataService.DataService:DA-END
    }
    /**
     * unsetter for the field dataService
     * 
     * 
     * 
     * @param dataService  the dataService
     */
    public void unsetDataService(DataService dataService) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.unsetDataService.DataService:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.unsetDataService.DataService:DA-ELSE
        if (this.dataService == dataService) {
            this.dataService = null;
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.unsetDataService.DataService:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.kura.SmartGreenhouseAppDataServicePublisher.additional.elements.in.type:DA-END
} // end of java type