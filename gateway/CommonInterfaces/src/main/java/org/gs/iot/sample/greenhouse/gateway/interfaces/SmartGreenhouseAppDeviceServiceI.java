package org.gs.iot.sample.greenhouse.gateway.interfaces;

import org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData;
import java.util.Collection;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData;
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraData;

public interface SmartGreenhouseAppDeviceServiceI { // start of interface

    /**
     * 
     * @return
     */
    PIRMotionGroveData getPlantMotion();
    /**
     * 
     * @return
     */
    PIRMotionGroveData getPlantMotionLastSentData();
    /**
     * 
     * @return
     */
    Collection<PIRMotionGroveData> getPlantMotionHistoryData();
    /**
     * 
     * @return
     */
    RelayGroveData getWaterPump();
    /**
     * 
     * @return
     */
    RelayGroveData getWaterPumpLastSentData();
    /**
     * 
     * @return
     */
    Collection<RelayGroveData> getWaterPumpHistoryData();
    /**
     * 
     * @return
     */
    BuzzerGroveData getAlarm();
    /**
     * 
     * @return
     */
    BuzzerGroveData getAlarmLastSentData();
    /**
     * 
     * @return
     */
    Collection<BuzzerGroveData> getAlarmHistoryData();
    /**
     * 
     * @return
     */
    DigitalLightTSL2561GroveData getPlantIlluminance();
    /**
     * 
     * @return
     */
    DigitalLightTSL2561GroveData getPlantIlluminanceLastSentData();
    /**
     * 
     * @return
     */
    Collection<DigitalLightTSL2561GroveData> getPlantIlluminanceHistoryData();
    /**
     * 
     * @return
     */
    AdafruitI2cRgbLcdData getDisplay();
    /**
     * 
     * @return
     */
    AdafruitI2cRgbLcdData getDisplayLastSentData();
    /**
     * 
     * @return
     */
    Collection<AdafruitI2cRgbLcdData> getDisplayHistoryData();
    /**
     * 
     * @return
     */
    BarometerBMP180GroveData getPlantBarometer();
    /**
     * 
     * @return
     */
    BarometerBMP180GroveData getPlantBarometerLastSentData();
    /**
     * 
     * @return
     */
    Collection<BarometerBMP180GroveData> getPlantBarometerHistoryData();
    /**
     * 
     * @return
     */
    Mq2GasSensorGroveData getMq2Gas();
    /**
     * 
     * @return
     */
    Mq2GasSensorGroveData getMq2GasLastSentData();
    /**
     * 
     * @return
     */
    Collection<Mq2GasSensorGroveData> getMq2GasHistoryData();
    /**
     * 
     * @return
     */
    TemperatureSensorGroveData getTemperature();
    /**
     * 
     * @return
     */
    TemperatureSensorGroveData getTemperatureLastSentData();
    /**
     * 
     * @return
     */
    Collection<TemperatureSensorGroveData> getTemperatureHistoryData();
    /**
     * 
     * @return
     */
    MoistureSensorGroveData getPlantMoisture();
    /**
     * 
     * @return
     */
    MoistureSensorGroveData getPlantMoistureLastSentData();
    /**
     * 
     * @return
     */
    Collection<MoistureSensorGroveData> getPlantMoistureHistoryData();
    /**
     * 
     * @return
     */
    LightStripData getLight();
    /**
     * 
     * @return
     */
    LightStripData getLightLastSentData();
    /**
     * 
     * @return
     */
    Collection<LightStripData> getLightHistoryData();
    /**
     * 
     * @return
     */
    RpiCameraData getCamera();
    /**
     * 
     * @return
     */
    RpiCameraData getCameraLastSentData();
    /**
     * 
     * @return
     */
    Collection<RpiCameraData> getCameraHistoryData();
    /**
     * 
     * @param deviceData  the deviceData
     */
    void setWaterPump(RelayGroveData deviceData);
    /**
     * 
     * @param deviceData  the deviceData
     */
    void setAlarm(BuzzerGroveData deviceData);
    /**
     * 
     * @param deviceData  the deviceData
     */
    void setDisplay(AdafruitI2cRgbLcdData deviceData);
    /**
     * 
     * @param deviceData  the deviceData
     */
    void setLight(LightStripData deviceData);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceServiceI.additional.elements.in.type:DA-END
} // end of java type