package org.gs.iot.sample.greenhouse.gateway.interfaces;

import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;

public interface SmartGreenhouseAppSensorListenerI<T extends AbstractSmartGreenhouseAppDeviceData> { // start of interface

    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    void onChange(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, T deviceData);
    /**
     * 
     * @param deviceConfiguration  the deviceConfiguration
     * @param th  the th
     */
    void onError(SmartGreenhouseAppDeviceConfigEnum deviceConfiguration, Throwable th);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI.additional.elements.in.type:DA-END
} // end of java type