package org.gs.iot.sample.greenhouse.gateway.interfaces;


public interface SmartGreenhouseAppActionable { // start of interface

    /**
     * 
     * @param payload  the payload
     */
    void action(byte[] payload);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppActionable.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppActionable.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppActionable.additional.elements.in.type:DA-END
} // end of java type