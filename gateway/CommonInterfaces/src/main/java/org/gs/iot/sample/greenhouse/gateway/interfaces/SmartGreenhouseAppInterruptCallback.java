package org.gs.iot.sample.greenhouse.gateway.interfaces;

import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;

public interface SmartGreenhouseAppInterruptCallback<T extends AbstractSmartGreenhouseAppDeviceData> { // start of interface

    /**
     * 
     * @param data  the data
     */
    void onInterrupt(T data);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback.additional.elements.in.type:DA-END
} // end of java type