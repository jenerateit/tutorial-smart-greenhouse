package org.gs.iot.sample.greenhouse.gateway.interfaces;


public interface SmartGreenhouseAppDeviceSetupI { // start of interface

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceSetupI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceSetupI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceSetupI.additional.elements.in.type:DA-END
} // end of java type