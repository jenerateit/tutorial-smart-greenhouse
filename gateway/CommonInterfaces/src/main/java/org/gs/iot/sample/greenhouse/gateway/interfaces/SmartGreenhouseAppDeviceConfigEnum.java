package org.gs.iot.sample.greenhouse.gateway.interfaces;



import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;


public enum SmartGreenhouseAppDeviceConfigEnum { // start of class
    
    PLANTMOTION("PlantMotion", "PIRMotionGrove", "0.1.0", "plant/motion", org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData.class),
    
    WATERPUMP("WaterPump", "RelayGrove", "0.1.0", "waterpump", org.gs.iot.sample.greenhouse.devicedata.RelayGroveData.class),
    
    ALARM("Alarm", "BuzzerGrove", "0.1.0", "alarm", org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData.class),
    
    PLANTILLUMINANCE("PlantIlluminance", "DigitalLightTSL2561Grove", "0.1.0", "plant/illuminance", org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData.class),
    
    DISPLAY("Display", "AdafruitI2cRgbLcd", "0.1.0", "display", org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData.class),
    
    PLANTBAROMETER("PlantBarometer", "BarometerBMP180Grove", "0.1.0", "plant/barometer", org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData.class),
    
    MQ2GAS("Mq2Gas", "Mq2GasSensorGrove", "0.1.0", "gas/mq2", org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData.class),
    
    TEMPERATURE("Temperature", "TemperatureSensorGrove", "0.1.0", "temperature", org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData.class),
    
    PLANTMOISTURE("PlantMoisture", "MoistureSensorGrove", "0.1.0", "plant/moisture", org.gs.iot.sample.greenhouse.devicedata.MoistureSensorGroveData.class),
    
    LIGHT("Light", "LightStrip", "0.1.0", "plant/light", org.gs.iot.sample.greenhouse.devicedata.LightStripData.class),
    
    CAMERA("Camera", "RpiCamera", "0.1.0", "plant/camera", org.gs.iot.sample.greenhouse.devicedata.RpiCameraData.class)
    //DA-START:interfaces.SmartGreenhouseAppDeviceConfigEnum.enum.constants:DA-START
    //DA-ELSE:interfaces.SmartGreenhouseAppDeviceConfigEnum.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:interfaces.SmartGreenhouseAppDeviceConfigEnum.enum.constants:DA-END
    ;
    
    private final String name;
    
    private final String deviceId;
    
    private final String deviceVersion;
    
    private final String resource;
    
    private final Class<? extends AbstractSmartGreenhouseAppDeviceData> dataClass;
    
    
    /**
     * creates an instance of SmartGreenhouseAppDeviceConfigEnum
     * 
     * @param name  the name
     * @param deviceId  the deviceId
     * @param deviceVersion  the deviceVersion
     * @param resource  the resource
     * @param dataClass  the dataClass
     */
    private SmartGreenhouseAppDeviceConfigEnum(String name, String deviceId, String deviceVersion, String resource, Class<? extends AbstractSmartGreenhouseAppDeviceData> dataClass) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.String.String.String.String.Class:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.String.String.String.String.Class:DA-ELSE
        this.name = name;
        this.deviceId = deviceId;
        this.deviceVersion = deviceVersion;
        this.resource = resource;
        this.dataClass = dataClass;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.String.String.String.String.Class:DA-END
    }
    
    
    /**
     * getter for the field name
     * 
     * 
     * 
     * @return
     */
    public String getName() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getName.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getName.String:DA-ELSE
        return this.name;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getName.String:DA-END
    }
    /**
     * getter for the field deviceId
     * 
     * 
     * 
     * @return
     */
    public String getDeviceId() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDeviceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDeviceId.String:DA-ELSE
        return this.deviceId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDeviceId.String:DA-END
    }
    /**
     * getter for the field deviceVersion
     * 
     * 
     * 
     * @return
     */
    public String getDeviceVersion() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDeviceVersion.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDeviceVersion.String:DA-ELSE
        return this.deviceVersion;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDeviceVersion.String:DA-END
    }
    /**
     * getter for the field resource
     * 
     * 
     * 
     * @return
     */
    public String getResource() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getResource.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getResource.String:DA-ELSE
        return this.resource;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getResource.String:DA-END
    }
    /**
     * getter for the field dataClass
     * 
     * 
     * 
     * @return
     */
    public Class<? extends AbstractSmartGreenhouseAppDeviceData> getDataClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDataClass.Class:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDataClass.Class:DA-ELSE
        return this.dataClass;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.getDataClass.Class:DA-END
    }
    
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum.additional.elements.in.type:DA-END
} // end of java type