package org.gs.iot.sample.greenhouse.hal.simulation;


import org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.io.IOException;

/**
 * Analog Sensors
 */
public class Mq2GasAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Mq2GasAccessor.class);
    
    private Mq2GasSensorGroveData data;
    
    private Mq2GasSensorGroveData sentData;
    
    private final LinkedList<Mq2GasSensorGroveData> historyData = new LinkedList<Mq2GasSensorGroveData>();
    
    private final List<Mq2GasSensorGroveData> simulationData = new ArrayList<Mq2GasSensorGroveData>();
    
    /**
     * creates an instance of Mq2GasAccessor
     * 
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public Mq2GasAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(SmartGreenhouseAppDeviceConfigEnum.MQ2GAS.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == Mq2GasSensorGroveData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(Mq2GasSensorGroveData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<Mq2GasSensorGroveData>>() {}.getType();
        List<Mq2GasSensorGroveData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public Mq2GasSensorGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getData.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getData.Mq2GasSensorGroveData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getData.Mq2GasSensorGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public Mq2GasSensorGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getSentData.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getSentData.Mq2GasSensorGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getSentData.Mq2GasSensorGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<Mq2GasSensorGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * 
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.loadTestData.String.String:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.MQ2GAS.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<Mq2GasSensorGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<Mq2GasSensorGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.MQ2GAS, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Mq2Gas'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(Mq2GasSensorGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.processData.Mq2GasSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.processData.Mq2GasSensorGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.processData.Mq2GasSensorGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.Mq2GasAccessor.additional.elements.in.type:DA-END
} // end of java type