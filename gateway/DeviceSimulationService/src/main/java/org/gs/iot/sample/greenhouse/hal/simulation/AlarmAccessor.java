package org.gs.iot.sample.greenhouse.hal.simulation;


import org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.io.IOException;

public class AlarmAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AlarmAccessor.class);
    
    private BuzzerGroveData data;
    
    private BuzzerGroveData sentData;
    
    private final LinkedList<BuzzerGroveData> historyData = new LinkedList<BuzzerGroveData>();
    
    private final List<BuzzerGroveData> simulationData = new ArrayList<BuzzerGroveData>();
    
    /**
     * creates an instance of AlarmAccessor
     * 
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public AlarmAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(SmartGreenhouseAppDeviceConfigEnum.ALARM.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == BuzzerGroveData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(BuzzerGroveData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<BuzzerGroveData>>() {}.getType();
        List<BuzzerGroveData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public BuzzerGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getData.BuzzerGroveData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getData.BuzzerGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public BuzzerGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getSentData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getSentData.BuzzerGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getSentData.BuzzerGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<BuzzerGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * 
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.loadTestData.String.String:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.ALARM.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<BuzzerGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<BuzzerGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.ALARM, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Alarm'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(BuzzerGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.processData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.processData.BuzzerGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.processData.BuzzerGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.deactivate:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(BuzzerGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.setData.BuzzerGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.setData.BuzzerGroveData:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.setData.BuzzerGroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.AlarmAccessor.additional.elements.in.type:DA-END
} // end of java type