package org.gs.iot.sample.greenhouse.test.simulation;


import org.slf4j.Logger;

/**
 * Analog Sensors
 */
public class Mq2GasTester { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Mq2GasTester.class);
    
    
    //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasTester.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasTester.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasTester.additional.elements.in.type:DA-END
} // end of java type