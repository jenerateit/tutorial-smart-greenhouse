package org.gs.iot.sample.greenhouse.gateway.hal.simulation;


import org.slf4j.Logger;
import org.osgi.util.tracker.ServiceTracker;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
/*
 * Imports from last generation
 */
import java.util.stream.Collectors;


public abstract class AbstractSmartGreenhouseAppAbstractAccessor { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AbstractSmartGreenhouseAppAbstractAccessor.class);
    
    private ServiceTracker<?,?> serviceTrackerForPublishable;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListener;
    
    /**
     * creates an instance of AbstractSmartGreenhouseAppAbstractAccessor
     * 
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public AbstractSmartGreenhouseAppAbstractAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        this.serviceTrackerForSensorListener = serviceTrackerForSensorListener;
        this.serviceTrackerForPublishable = serviceTrackerForPublishable;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * getter for the field serviceTrackerForPublishable
     * 
     * 
     * 
     * @return
     */
    protected ServiceTracker<?,?> getServiceTrackerForPublishable() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.getServiceTrackerForPublishable.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.getServiceTrackerForPublishable.ServiceTracker:DA-ELSE
        return this.serviceTrackerForPublishable;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.getServiceTrackerForPublishable.ServiceTracker:DA-END
    }
    /**
     * setter for the field serviceTrackerForPublishable
     * 
     * 
     * 
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    protected void setServiceTrackerForPublishable(ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.setServiceTrackerForPublishable.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.setServiceTrackerForPublishable.ServiceTracker:DA-ELSE
        this.serviceTrackerForPublishable = serviceTrackerForPublishable;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.setServiceTrackerForPublishable.ServiceTracker:DA-END
    }
    /**
     * getter for the field serviceTrackerForSensorListener
     * 
     * 
     * 
     * @return
     */
    protected ServiceTracker<?,?> getServiceTrackerForSensorListener() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.getServiceTrackerForSensorListener.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.getServiceTrackerForSensorListener.ServiceTracker:DA-ELSE
        return this.serviceTrackerForSensorListener;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.getServiceTrackerForSensorListener.ServiceTracker:DA-END
    }
    /**
     * setter for the field serviceTrackerForSensorListener
     * 
     * 
     * 
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     */
    protected void setServiceTrackerForSensorListener(ServiceTracker<?,?> serviceTrackerForSensorListener) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.setServiceTrackerForSensorListener.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.setServiceTrackerForSensorListener.ServiceTracker:DA-ELSE
        this.serviceTrackerForSensorListener = serviceTrackerForSensorListener;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.setServiceTrackerForSensorListener.ServiceTracker:DA-END
    }
    /**
     * 
     * @param fileName  the fileName
     * @return
     */
    protected String readFile(String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.readFile.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.readFile.String.String:DA-ELSE
        
        String content = "";
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("/" + fileName);
        BufferedReader reader;
        try {
        	reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        	content = reader.lines().collect(java.util.stream.Collectors.joining("\n"));
        } catch (UnsupportedEncodingException ex) {
        	ex.printStackTrace();
        	throw new RuntimeException("unable to read simulation data", ex);
        }
        	
        return content;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.readFile.String.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor.additional.elements.in.type:DA-END
} // end of java type