package org.gs.iot.sample.greenhouse.test.simulation;


import org.slf4j.Logger;

/**
 * Other Sensors
 */
public class LightTester { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(LightTester.class);
    
    
    //DA-START:org.gs.iot.sample.greenhouse.test.simulation.LightTester.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.LightTester.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.test.simulation.LightTester.additional.elements.in.type:DA-END
} // end of java type