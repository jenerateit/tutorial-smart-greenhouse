package org.gs.iot.sample.greenhouse.test.simulation;


import org.slf4j.Logger;

public class PlantMoistureTester { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantMoistureTester.class);
    
    
    //DA-START:org.gs.iot.sample.greenhouse.test.simulation.PlantMoistureTester.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.PlantMoistureTester.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.test.simulation.PlantMoistureTester.additional.elements.in.type:DA-END
} // end of java type