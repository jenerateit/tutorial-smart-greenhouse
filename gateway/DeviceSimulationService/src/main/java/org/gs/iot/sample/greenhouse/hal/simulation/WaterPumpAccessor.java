package org.gs.iot.sample.greenhouse.hal.simulation;


import org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.io.IOException;

public class WaterPumpAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(WaterPumpAccessor.class);
    
    private RelayGroveData data;
    
    private RelayGroveData sentData;
    
    private final LinkedList<RelayGroveData> historyData = new LinkedList<RelayGroveData>();
    
    private final List<RelayGroveData> simulationData = new ArrayList<RelayGroveData>();
    
    /**
     * creates an instance of WaterPumpAccessor
     * 
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public WaterPumpAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(SmartGreenhouseAppDeviceConfigEnum.WATERPUMP.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == RelayGroveData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(RelayGroveData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<RelayGroveData>>() {}.getType();
        List<RelayGroveData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public RelayGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getData.RelayGroveData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getData.RelayGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public RelayGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getSentData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getSentData.RelayGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getSentData.RelayGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<RelayGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * 
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.loadTestData.String.String:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.WATERPUMP.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<RelayGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<RelayGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.WATERPUMP, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'WaterPump'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(RelayGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.processData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.processData.RelayGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.processData.RelayGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.deactivate:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(RelayGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.setData.RelayGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.setData.RelayGroveData:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.setData.RelayGroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.WaterPumpAccessor.additional.elements.in.type:DA-END
} // end of java type