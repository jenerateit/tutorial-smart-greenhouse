package org.gs.iot.sample.greenhouse.gateway.impl.simulation;


import org.slf4j.Logger;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class SmartGreenhouseAppThreadPool { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SmartGreenhouseAppThreadPool.class);
    
    private static final ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(10, new ThreadPoolFactory("Default Thread Pool"));
    
    /**
     * getter for the field scheduledThreadPoolExecutor
     * 
     * 
     * 
     * @return
     */
    public static ScheduledThreadPoolExecutor getScheduledThreadPoolExecutor() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor.ScheduledThreadPoolExecutor:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor.ScheduledThreadPoolExecutor:DA-ELSE
        return SmartGreenhouseAppThreadPool.scheduledThreadPoolExecutor;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.getScheduledThreadPoolExecutor.ScheduledThreadPoolExecutor:DA-END
    }
    
    private static final class ThreadPoolFactory implements ThreadFactory { // start of class
    
    
        private final String name;
        
        private final AtomicInteger ai;
        
        
        /**
         * creates an instance of ThreadPoolFactory
         * 
         * @param name  the name
         */
        private ThreadPoolFactory(String name) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.String:DA-ELSE
            this.name = name;
            this.ai = new AtomicInteger(0);
            //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.String:DA-END
        }
        
        /**
         * creates an instance of ThreadPoolFactory
         */
        private ThreadPoolFactory() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory:DA-ELSE
            throw new AssertionError();
            //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory:DA-END
        }
        
        
        /**
         * 
         * @param arg0
         * @return
         */
        @Override
        public Thread newThread(Runnable arg0) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.newThread.Runnable.Thread:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.newThread.Runnable.Thread:DA-ELSE
            return new Thread(arg0, name + " - " + ai.getAndIncrement());
            //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.newThread.Runnable.Thread:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.ThreadPoolFactory.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.impl.simulation.SmartGreenhouseAppThreadPool.additional.elements.in.type:DA-END
} // end of java type