package org.gs.iot.sample.greenhouse.hal.simulation;


import org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.io.IOException;

/**
 * Digital Sensors
 */
public class PlantMotionAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantMotionAccessor.class);
    
    private PIRMotionGroveData data;
    
    private PIRMotionGroveData sentData;
    
    private final LinkedList<PIRMotionGroveData> historyData = new LinkedList<PIRMotionGroveData>();
    
    private final List<PIRMotionGroveData> simulationData = new ArrayList<PIRMotionGroveData>();
    
    /**
     * creates an instance of PlantMotionAccessor
     * 
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public PlantMotionAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(SmartGreenhouseAppDeviceConfigEnum.PLANTMOTION.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == PIRMotionGroveData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(PIRMotionGroveData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<PIRMotionGroveData>>() {}.getType();
        List<PIRMotionGroveData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public PIRMotionGroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getData.PIRMotionGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getData.PIRMotionGroveData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getData.PIRMotionGroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public PIRMotionGroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getSentData.PIRMotionGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getSentData.PIRMotionGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getSentData.PIRMotionGroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<PIRMotionGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * 
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.loadTestData.String.String:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.PLANTMOTION.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<PIRMotionGroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<PIRMotionGroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.PLANTMOTION, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'PlantMotion'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(PIRMotionGroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.processData.PIRMotionGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.processData.PIRMotionGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.processData.PIRMotionGroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantMotionAccessor.additional.elements.in.type:DA-END
} // end of java type