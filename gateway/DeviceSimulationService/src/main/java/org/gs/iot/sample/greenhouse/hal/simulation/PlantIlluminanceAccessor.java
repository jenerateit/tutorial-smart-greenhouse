package org.gs.iot.sample.greenhouse.hal.simulation;


import org.gs.iot.sample.greenhouse.gateway.hal.simulation.AbstractSmartGreenhouseAppAbstractAccessor;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppPublishable;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppSensorListenerI;
import java.io.IOException;

/**
 * I2C Sensors
 */
public class PlantIlluminanceAccessor extends AbstractSmartGreenhouseAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PlantIlluminanceAccessor.class);
    
    private DigitalLightTSL2561GroveData data;
    
    private DigitalLightTSL2561GroveData sentData;
    
    private final LinkedList<DigitalLightTSL2561GroveData> historyData = new LinkedList<DigitalLightTSL2561GroveData>();
    
    private final List<DigitalLightTSL2561GroveData> simulationData = new ArrayList<DigitalLightTSL2561GroveData>();
    
    /**
     * creates an instance of PlantIlluminanceAccessor
     * 
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public PlantIlluminanceAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(SmartGreenhouseAppDeviceConfigEnum.PLANTILLUMINANCE.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == DigitalLightTSL2561GroveData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(DigitalLightTSL2561GroveData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<DigitalLightTSL2561GroveData>>() {}.getType();
        List<DigitalLightTSL2561GroveData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    /**
     * 
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.run.void:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    public DigitalLightTSL2561GroveData getData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getData.DigitalLightTSL2561GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getData.DigitalLightTSL2561GroveData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getData.DigitalLightTSL2561GroveData:DA-END
    }
    /**
     * getter for the field sentData
     * 
     * 
     * 
     * @return
     */
    public DigitalLightTSL2561GroveData getSentData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getSentData.DigitalLightTSL2561GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getSentData.DigitalLightTSL2561GroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getSentData.DigitalLightTSL2561GroveData:DA-END
    }
    /**
     * getter for the field historyData
     * 
     * 
     * 
     * @return
     */
    public LinkedList<DigitalLightTSL2561GroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.getHistoryData.LinkedList:DA-END
    }
    /**
     * 
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.loadTestData.String.String:DA-END
    }
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     * 
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.isNotificationRequired.boolean:DA-END
    }
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				SmartGreenhouseAppPublishable publishable = (SmartGreenhouseAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(SmartGreenhouseAppDeviceConfigEnum.PLANTILLUMINANCE.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				SmartGreenhouseAppSensorListenerI<DigitalLightTSL2561GroveData> sensorListener = (SmartGreenhouseAppSensorListenerI<DigitalLightTSL2561GroveData>) service;
        			    sensorListener.onChange(SmartGreenhouseAppDeviceConfigEnum.PLANTILLUMINANCE, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'PlantIlluminance'", th);
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.notifyServices:DA-END
    }
    /**
     * 
     * @param data  the data
     */
    protected void processData(DigitalLightTSL2561GroveData data) {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.processData.DigitalLightTSL2561GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.processData.DigitalLightTSL2561GroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.processData.DigitalLightTSL2561GroveData:DA-END
    }
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.activate:DA-END
    }
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.hal.simulation.PlantIlluminanceAccessor.additional.elements.in.type:DA-END
} // end of java type