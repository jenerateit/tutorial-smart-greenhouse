package org.gs.iot.sample.greenhouse.test.simulation;


import org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppAbstractSimulationDataCreator;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import java.util.Collection;
import java.util.ArrayList;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import java.util.Date;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus;


/**
 * Analog Sensors
 */
public class Mq2GasSimulationDataCreator extends SmartGreenhouseAppAbstractSimulationDataCreator { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Mq2GasSimulationDataCreator.class);
    
    /**
     * creates an instance of Mq2GasSimulationDataCreator
     * 
     * @param rangeStart  the rangeStart
     * @param rangeEnd  the rangeEnd
     * @param countOfDataRecords  the countOfDataRecords
     * @param directory  the directory
     * @param fileName  the fileName
     */
    public Mq2GasSimulationDataCreator(String rangeStart, String rangeEnd, int countOfDataRecords, String directory, String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.String.String.int.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.String.String.int.String.String:DA-ELSE
        super(rangeStart, rangeEnd, countOfDataRecords, directory, fileName);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.String.String.int.String.String:DA-END
    }
    
    /**
     * 
     * @return
     */
    public int createScenario1() {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario1.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario1.int:DA-ELSE
        Collection<AbstractSmartGreenhouseAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario1(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario1.int:DA-END
    }
    /**
     * 
     * @return
     */
    public int createScenario2() {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario2.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario2.int:DA-ELSE
        Collection<AbstractSmartGreenhouseAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario2(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario2.int:DA-END
    }
    /**
     * 
     * @return
     */
    public int createScenario3() {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario3.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario3.int:DA-ELSE
        Collection<AbstractSmartGreenhouseAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario3(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createScenario3.int:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario1(Collection<AbstractSmartGreenhouseAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario1.Collection:DA-START
    	Mq2GasSensorGroveData data = new Mq2GasSensorGroveData();
        data.setTimestamp(new Date());
        Mq2GasSensorGroveStatus mq2GasSensorGroveStatus = new Mq2GasSensorGroveStatus();
        mq2GasSensorGroveStatus.setr0(5.5000f);
        mq2GasSensorGroveStatus.setRatio(1.2345678901f);
        mq2GasSensorGroveStatus.setRsAir(1.2345678901f);
        mq2GasSensorGroveStatus.setRsGas(1.2345678901f);
        data.setData(mq2GasSensorGroveStatus);
        deviceData.add(data);
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario1.Collection:DA-ELSE
        //Mq2GasSensorGroveData data = new Mq2GasSensorGroveData();
        //data.setTimestamp(new Date());
        //deviceData.add(data);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario1.Collection:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario2(Collection<AbstractSmartGreenhouseAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario2.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario2.Collection:DA-ELSE
        Mq2GasSensorGroveData data = new Mq2GasSensorGroveData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario2.Collection:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario3(Collection<AbstractSmartGreenhouseAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario3.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario3.Collection:DA-ELSE
        Mq2GasSensorGroveData data = new Mq2GasSensorGroveData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.createObjectForScenario3.Collection:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator.additional.elements.in.type:DA-END
} // end of java type