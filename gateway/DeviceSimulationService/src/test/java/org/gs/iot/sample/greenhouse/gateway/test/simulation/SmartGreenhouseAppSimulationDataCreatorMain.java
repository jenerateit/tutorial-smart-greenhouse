package org.gs.iot.sample.greenhouse.gateway.test.simulation;


import org.slf4j.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import java.util.List;
import java.util.Arrays;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import java.util.ArrayList;
import org.gs.iot.sample.greenhouse.test.simulation.PlantMotionSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.WaterPumpSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.PlantIlluminanceSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.DisplaySimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.PlantBarometerSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.Mq2GasSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.TemperatureSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.PlantMoistureSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.LightSimulationDataCreator;
import org.gs.iot.sample.greenhouse.test.simulation.CameraSimulationDataCreator;
import org.apache.commons.cli.Option;

public class SmartGreenhouseAppSimulationDataCreatorMain { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SmartGreenhouseAppSimulationDataCreatorMain.class);
    
    public static final String OPTION_HELP = "h";
    
    public static final String OPTION_LIST_HARDWARE_USAGES = "l";
    
    public static final String OPTION_HARDWARE_USAGES = "u";
    
    public static final String OPTION_SCENARIO_NUMBER = "n";
    
    public static final String OPTION_COUNT_OF_DATA_RECORDS = "c";
    
    public static final String OPTION_RANGE_START = "s";
    
    public static final String OPTION_RANGE_END = "e";
    
    public static final String OPTION_PROTOTYPE = "p";
    
    public static final String OPTION_DIRECTORY = "d";
    
    private final CommandLine commandLine;
    
    /**
     * creates an instance of SmartGreenhouseAppSimulationDataCreatorMain
     * 
     * @param options  the options
     * @param arg0  the arg0
     */
    private SmartGreenhouseAppSimulationDataCreatorMain(Options options, String[] arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.Options.String[]:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.Options.String[]:DA-ELSE
        
        HelpFormatter helpFormatter = new HelpFormatter();
        CommandLineParser commandLineParser = new DefaultParser();
        try {
            this.commandLine = commandLineParser.parse(options, arg0);
        } catch (ParseException ex) {
        	helpFormatter.printHelp("SmartGreenhouseAppSimulationDataCreatorMain", options);
            throw new RuntimeException("invalid command line given", ex);
        }
        
        if (this.commandLine.hasOption(OPTION_HELP)) {
        	helpFormatter.printHelp("SimulationDataCreator", options);
        	return;
        }
        
        if (this.commandLine.hasOption(OPTION_LIST_HARDWARE_USAGES)) {
        	listHardwareUsages();
        	return;
        }
        
        executeSimulationDataCreation();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.Options.String[]:DA-END
    }
    
    /**
     * 
     * @param arg0  the arg0
     */
    public static void main(String[] arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.main.String.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.main.String.ARRAY:DA-ELSE
        new SmartGreenhouseAppSimulationDataCreatorMain(getOptions(), arg0);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.main.String.ARRAY:DA-END
    }
    /**
     * 
     * @return
     */
    public String getDirectory() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getDirectory.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getDirectory.String:DA-ELSE
        
        if (this.commandLine.hasOption(OPTION_DIRECTORY)) {
        	return this.commandLine.getOptionValue(OPTION_DIRECTORY);
        }
        
        return "src/main/resources";  // that's the default directory
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getDirectory.String:DA-END
    }
    /**
     * 
     * @return
     */
    public int getScenarioNumber() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getScenarioNumber.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getScenarioNumber.int:DA-ELSE
        
        String value = "1";  // that's the default scenario number
        if (this.commandLine.hasOption(OPTION_SCENARIO_NUMBER)) {
        	value = this.commandLine.getOptionValue(OPTION_SCENARIO_NUMBER);
        }
        
        try {
        	Integer scenarioNumber = Integer.valueOf(value);
        	return scenarioNumber;
        } catch (NumberFormatException ex) {
        	throw new RuntimeException("invalid scenario number given: '" + value + "'");
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getScenarioNumber.int:DA-END
    }
    /**
     * 
     * @return
     */
    public int getCountOfDataRecords() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getCountOfDataRecords.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getCountOfDataRecords.int:DA-ELSE
        
        String value = "100";  // that's the default value
        if (this.commandLine.hasOption(OPTION_COUNT_OF_DATA_RECORDS)) {
        	value = this.commandLine.getOptionValue(OPTION_COUNT_OF_DATA_RECORDS);
        }
        
        try {
        	Integer countOfDataRecords = Integer.valueOf(value);
        	return countOfDataRecords;
        } catch (NumberFormatException ex) {
        	throw new RuntimeException("invalid number of data records given: '" + value + "'");
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getCountOfDataRecords.int:DA-END
    }
    /**
     * 
     * @return
     */
    public List<String> getHardwareUsageNames() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getHardwareUsageNames.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getHardwareUsageNames.List:DA-ELSE
        
        String[] values = this.commandLine.getOptionValues(OPTION_HARDWARE_USAGES);
        		
        if (values != null && values.length == 1 && "all".equalsIgnoreCase(values[0])) {
            return getAllHardwareUsageNames();
        }
        
        return Arrays.asList(values);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getHardwareUsageNames.List:DA-END
    }
    /**
     * 
     * @return
     */
    public List<String> getAllHardwareUsageNames() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getAllHardwareUsageNames.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getAllHardwareUsageNames.List:DA-ELSE
        
        List<String> result = new ArrayList<>();
        for (SmartGreenhouseAppDeviceConfigEnum deviceConfigEnumEntry : SmartGreenhouseAppDeviceConfigEnum.values()) {
        	result.add(deviceConfigEnumEntry.getName());
        }
        
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getAllHardwareUsageNames.List:DA-END
    }
    /**
     */
    public void listHardwareUsages() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.listHardwareUsages:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.listHardwareUsages:DA-ELSE
        
        for (SmartGreenhouseAppDeviceConfigEnum deviceConfigEnumEntry : SmartGreenhouseAppDeviceConfigEnum.values()) {
        	System.out.println(deviceConfigEnumEntry.getName());
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.listHardwareUsages:DA-END
    }
    /**
     */
    public void executeSimulationDataCreation() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.executeSimulationDataCreation:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.executeSimulationDataCreation:DA-ELSE
        
        if (this.commandLine.hasOption(OPTION_HARDWARE_USAGES)) {
        	for (String hardwareUsageName : getHardwareUsageNames()) {
        		int numberOfCreatedDataRecords = 0;
        		switch (hardwareUsageName.toLowerCase()) {
        
                
        		case "plantmotion":
        			PlantMotionSimulationDataCreator plantmotionDataCreator =
        			    new PlantMotionSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "PlantMotionSimulationData.txt");
        			numberOfCreatedDataRecords = plantmotionDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "waterpump":
        			WaterPumpSimulationDataCreator waterpumpDataCreator =
        			    new WaterPumpSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "WaterPumpSimulationData.txt");
        			numberOfCreatedDataRecords = waterpumpDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "alarm":
        			AlarmSimulationDataCreator alarmDataCreator =
        			    new AlarmSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "AlarmSimulationData.txt");
        			numberOfCreatedDataRecords = alarmDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "plantilluminance":
        			PlantIlluminanceSimulationDataCreator plantilluminanceDataCreator =
        			    new PlantIlluminanceSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "PlantIlluminanceSimulationData.txt");
        			numberOfCreatedDataRecords = plantilluminanceDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "display":
        			DisplaySimulationDataCreator displayDataCreator =
        			    new DisplaySimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "DisplaySimulationData.txt");
        			numberOfCreatedDataRecords = displayDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "plantbarometer":
        			PlantBarometerSimulationDataCreator plantbarometerDataCreator =
        			    new PlantBarometerSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "PlantBarometerSimulationData.txt");
        			numberOfCreatedDataRecords = plantbarometerDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "mq2gas":
        			Mq2GasSimulationDataCreator mq2gasDataCreator =
        			    new Mq2GasSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "Mq2GasSimulationData.txt");
        			numberOfCreatedDataRecords = mq2gasDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "temperature":
        			TemperatureSimulationDataCreator temperatureDataCreator =
        			    new TemperatureSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "TemperatureSimulationData.txt");
        			numberOfCreatedDataRecords = temperatureDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "plantmoisture":
        			PlantMoistureSimulationDataCreator plantmoistureDataCreator =
        			    new PlantMoistureSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "PlantMoistureSimulationData.txt");
        			numberOfCreatedDataRecords = plantmoistureDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "light":
        			LightSimulationDataCreator lightDataCreator =
        			    new LightSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "LightSimulationData.txt");
        			numberOfCreatedDataRecords = lightDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		case "camera":
        			CameraSimulationDataCreator cameraDataCreator =
        			    new CameraSimulationDataCreator("0", "100",
                                                                    getCountOfDataRecords(),
                                                                    getDirectory(),
                                                                    "CameraSimulationData.txt");
        			numberOfCreatedDataRecords = cameraDataCreator.createScenario(getScenarioNumber());
        		    break;
        
        		
        		default:
        			numberOfCreatedDataRecords = -1;
        			break;
        		}
        		
        		if (numberOfCreatedDataRecords >= 0) {
        			System.out.println("created " + numberOfCreatedDataRecords + " data records for '" + hardwareUsageName + "', scenario " + getScenarioNumber());
        		} else {
        			System.out.println("created no data records for unknown hardware usage name '" + hardwareUsageName + "'");
        		}
        	}
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.executeSimulationDataCreation:DA-END
    }
    /**
     * 
     * @return
     */
    public static Options getOptions() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getOptions.Options:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getOptions.Options:DA-ELSE
        
        Options result = new Options();
        Option option = null;
        
        // --- HELP
        option = new Option(OPTION_HELP,
                            "help",
                            true,
                            "show this help text");
        option.setArgs(0);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- LIST_HARDWARE_USAGES
        option = new Option(OPTION_LIST_HARDWARE_USAGES,
                            "list",
                            true,
                            "list names of all hardware usages");
        option.setArgs(0);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- HARDWARE_USAGES
        option = new Option(OPTION_HARDWARE_USAGES,
                            "usages",
                            false,
                            "named of hardware usages where simulation data has to be created for (comma separated)");
        option.setArgs(-2);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- SCENARIO_NUMBER
        option = new Option(OPTION_SCENARIO_NUMBER,
                            "scenario-number",
                            true,
                            "scenario number (1-5), scenario 1 is used if nothing is specified");
        option.setArgs(1);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- COUNT_OF_DATA_RECORDS
        option = new Option(OPTION_COUNT_OF_DATA_RECORDS,
                            "count-data-records",
                            true,
                            "number of data records to be created");
        option.setArgs(1);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- RANGE_START
        option = new Option(OPTION_RANGE_START,
                            "range-start",
                            true,
                            "start value for value range");
        option.setArgs(1);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- RANGE_END
        option = new Option(OPTION_RANGE_END,
                            "range-end",
                            true,
                            "end value for value range");
        option.setArgs(1);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- PROTOTYPE
        option = new Option(OPTION_PROTOTYPE,
                            "prototype",
                            true,
                            "name of file that contains json sample object");
        option.setArgs(1);
        option.setValueSeparator(',');
        result.addOption(option);
        
        // --- DIRECTORY
        option = new Option(OPTION_DIRECTORY,
                            "directory",
                            true,
                            "name of directory where simulation data files are written to");
        option.setArgs(1);
        option.setValueSeparator(',');
        result.addOption(option);
        
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.getOptions.Options:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppSimulationDataCreatorMain.additional.elements.in.type:DA-END
} // end of java type