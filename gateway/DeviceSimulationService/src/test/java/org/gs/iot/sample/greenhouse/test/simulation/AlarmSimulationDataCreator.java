package org.gs.iot.sample.greenhouse.test.simulation;


import org.gs.iot.sample.greenhouse.gateway.test.simulation.SmartGreenhouseAppAbstractSimulationDataCreator;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.devicedata.AbstractSmartGreenhouseAppDeviceData;
import java.util.Collection;
import java.util.ArrayList;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import java.util.Date;

public class AlarmSimulationDataCreator extends SmartGreenhouseAppAbstractSimulationDataCreator { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AlarmSimulationDataCreator.class);
    
    /**
     * creates an instance of AlarmSimulationDataCreator
     * 
     * @param rangeStart  the rangeStart
     * @param rangeEnd  the rangeEnd
     * @param countOfDataRecords  the countOfDataRecords
     * @param directory  the directory
     * @param fileName  the fileName
     */
    public AlarmSimulationDataCreator(String rangeStart, String rangeEnd, int countOfDataRecords, String directory, String fileName) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.String.String.int.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.String.String.int.String.String:DA-ELSE
        super(rangeStart, rangeEnd, countOfDataRecords, directory, fileName);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.String.String.int.String.String:DA-END
    }
    
    /**
     * 
     * @return
     */
    public int createScenario1() {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario1.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario1.int:DA-ELSE
        Collection<AbstractSmartGreenhouseAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario1(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario1.int:DA-END
    }
    /**
     * 
     * @return
     */
    public int createScenario2() {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario2.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario2.int:DA-ELSE
        Collection<AbstractSmartGreenhouseAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario2(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario2.int:DA-END
    }
    /**
     * 
     * @return
     */
    public int createScenario3() {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario3.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario3.int:DA-ELSE
        Collection<AbstractSmartGreenhouseAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario3(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createScenario3.int:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario1(Collection<AbstractSmartGreenhouseAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario1.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario1.Collection:DA-ELSE
        BuzzerGroveData data = new BuzzerGroveData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario1.Collection:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario2(Collection<AbstractSmartGreenhouseAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario2.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario2.Collection:DA-ELSE
        BuzzerGroveData data = new BuzzerGroveData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario2.Collection:DA-END
    }
    /**
     * 
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario3(Collection<AbstractSmartGreenhouseAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario3.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario3.Collection:DA-ELSE
        BuzzerGroveData data = new BuzzerGroveData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.createObjectForScenario3.Collection:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.test.simulation.AlarmSimulationDataCreator.additional.elements.in.type:DA-END
} // end of java type