package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.LightStripData;
import java.io.IOException;
/*
 * Imports from last generation
 */
import javax.xml.bind.attachment.AttachmentUnmarshaller;
import org.gs.iot.sample.greenhouse.devicedata.LightStripStatus;
import com.gs.ledstrip.LedStrip;
import com.gs.ledstrip.ws2811.LedStripWs2811;


public class LightStripDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(LightStripDeviceDriver.class);
    
    private LightStripDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<LightStripData> interruptCallback;
    
    /**
     * creates an instance of LightStripDeviceDriver
     * 
     * @param setup  the setup
     */
    public LightStripDeviceDriver(LightStripDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public LightStripDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<LightStripData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @param data
     */
    public synchronized void setData(LightStripData data) throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.setData.LightStripData:DA-START
    	status = data.getData();
    	strip.setAllPixelColors(status.getR(), status.getG(), status.getB());
    	strip.setBrightness(status.getBrightness());
    	strip.show();
    	interruptCallback.onInterrupt(data);
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.setData.LightStripData:DA-ELSE
        //return;
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.setData.LightStripData:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized LightStripData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.getData.LightStripData:DA-START
    	LightStripData data = new LightStripData();
    	data.setData(status);
    	return data;
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.getData.LightStripData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.getData.LightStripData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.activate:DA-START
    	strip = new LedStripWs2811(19, 18, 800000, 5, 255);
    	status = new LightStripStatus();
    	status.setR((short) 0);
    	status.setG((short) 0);
    	status.setB((short) 0);
    	status.setBrightness(0);
    	strip.setAllPixelColors(status.getR(), status.getG(), status.getB());
    	strip.setBrightness(status.getBrightness());
    	strip.show();
    	logger.debug("activated Light Strip");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.deactivate:DA-START
    	strip.clear();
    	strip.shutdown();
    	logger.debug("deactivated Light Strip");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        
        
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.additional.elements.in.type:DA-START
    private LedStrip strip;
    private LightStripStatus status;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.LightStripDeviceDriver.additional.elements.in.type:DA-END
} // end of java type