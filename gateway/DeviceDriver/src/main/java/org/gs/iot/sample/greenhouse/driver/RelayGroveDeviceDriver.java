package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.DigitalConnection;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException;
import org.gs.iot.sample.greenhouse.devicedata.RelayGroveStatusType;
import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;


public class RelayGroveDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(RelayGroveDeviceDriver.class);
    
    private RelayGroveDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<RelayGroveData> interruptCallback;
    
    /**
     * creates an instance of RelayGroveDeviceDriver
     * 
     * @param setup  the setup
     */
    public RelayGroveDeviceDriver(RelayGroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public RelayGroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<RelayGroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @param data
     */
    public synchronized void setData(RelayGroveData data) throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.setData.RelayGroveData:DA-START
    	try {
    		pin.setValue(data.getData().isStatus());
    		interruptCallback.onInterrupt(data);
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.setData.RelayGroveData:DA-ELSE
        //return;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.setData.RelayGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized RelayGroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.getData.RelayGroveData:DA-START
    	try {
    		RelayGroveData data = new RelayGroveData();
			data.setData(new RelayGroveStatusType(pin.getValue()));
			return data;
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.getData.RelayGroveData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.getData.RelayGroveData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.activate:DA-START
    	try {
    		GPIOPinConfig pinConfig = new GPIOPinConfig(0, setup.getConnection().getPinNumber(), GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_BOTH_EDGES, false);
			pin = DeviceManager.open(GPIOPin.class, pinConfig);
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	logger.debug("activated Relay Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.deactivate:DA-START
    	try {
			pin.close();
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	pin = null;
    	logger.debug("deactivated Relay Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private DigitalConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param connection  the connection
         */
        public DeviceSetup(DigitalConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public DigitalConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.additional.elements.in.type:DA-START
    private GPIOPin pin;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.RelayGroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type