package org.gs.iot.sample.greenhouse.lib;

import java.io.IOException;

import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveStatus;

public class GasSensorMq2 {

	private MCP3008 mcp3008;
	private int analogChannel;
	private float r0 = Float.NEGATIVE_INFINITY;
	private float rsAir = Float.NEGATIVE_INFINITY;

	public void activate(int analogChannel) throws IOException {
		this.analogChannel = analogChannel;
		mcp3008 = MCP3008.getInstance(MCP3008.MCP3008_CSPin, MCP3008.MCP3008_V_Ref);
		approximateR0();
	}

	public void deactivate() throws IOException {
		mcp3008.close();
	}

	public float approximateR0() throws IOException {
		int avgSize = 50;
		int value = 0;
		for (int i = 0; i < avgSize; i++) {
			value += mcp3008.readChannel(analogChannel);
		}
		float voltage = MCP3008.calculateVoltage(value / avgSize);

		this.rsAir = (5f - voltage) / voltage; // omit *RL
		this.r0 = rsAir / 9.8f; // The ratio of RS/R0 is 9.8 in a clear air from
								// Graph (Found using WebPlotDigitizer)
		return r0;
	}

	public Mq2GasSensorGroveStatus getStatus() throws IOException {
		if (r0 < 0) {
			throw new IOException("invalid R0: " + r0);
		}
		Mq2GasSensorGroveStatus status = new Mq2GasSensorGroveStatus();
		float value = MCP3008.calculateVoltage(mcp3008.readChannel(analogChannel));
		status.setRsAir(rsAir);
		status.setr0(r0);
		float rsGas = (5f - value) / value; // omit *RL
		status.setRsGas(rsGas);
		float ratio = rsGas / r0;
		status.setRatio(ratio);
		return status;
	}
}