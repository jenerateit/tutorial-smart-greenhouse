package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveConfiguration;
import org.gs.iot.sample.greenhouse.devicedata.I2cConnection;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.lib.DigitalLightTSL2561;
import org.gs.iot.sample.greenhouse.lib.DigitalLightTSL2561.Gain;
import org.gs.iot.sample.greenhouse.lib.DigitalLightTSL2561.IntegrateTime;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveStatusType;


public class DigitalLightTSL2561GroveDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DigitalLightTSL2561GroveDeviceDriver.class);
    
    private DigitalLightTSL2561GroveDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<DigitalLightTSL2561GroveData> interruptCallback;
    
    /**
     * creates an instance of DigitalLightTSL2561GroveDeviceDriver
     * 
     * @param setup  the setup
     */
    public DigitalLightTSL2561GroveDeviceDriver(DigitalLightTSL2561GroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public DigitalLightTSL2561GroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<DigitalLightTSL2561GroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized DigitalLightTSL2561GroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.getData.DigitalLightTSL2561GroveData:DA-START
    	DigitalLightTSL2561GroveData data = new DigitalLightTSL2561GroveData();
    	data.setData(new DigitalLightTSL2561GroveStatusType(light.getVisibleLux()));
    	return data;
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.getData.DigitalLightTSL2561GroveData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.getData.DigitalLightTSL2561GroveData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.activate:DA-START
    	light = new DigitalLightTSL2561(Gain.getGain(setup.getConfiguration().getGain()), IntegrateTime.getIntegrateTime(setup.getConfiguration().getIntegrateTime()));
    	logger.debug("activated Digital Light TSL2561");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.deactivate:DA-START
    	light.close();
    	light = null;
    	logger.debug("deactivated Digital Light TSL2561");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private DigitalLightTSL2561GroveConfiguration configuration;
        
        private I2cConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param configuration  the configuration
         * @param connection  the connection
         */
        public DeviceSetup(DigitalLightTSL2561GroveConfiguration configuration, I2cConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.DigitalLightTSL2561GroveConfiguration.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.DigitalLightTSL2561GroveConfiguration.I2cConnection:DA-ELSE
            this.configuration = configuration;
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.DigitalLightTSL2561GroveConfiguration.I2cConnection:DA-END
        }
        
        
        /**
         * getter for the field configuration
         * 
         * 
         * 
         * @return
         */
        public DigitalLightTSL2561GroveConfiguration getConfiguration() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConfiguration.DigitalLightTSL2561GroveConfiguration:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConfiguration.DigitalLightTSL2561GroveConfiguration:DA-ELSE
            return this.configuration;
            //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConfiguration.DigitalLightTSL2561GroveConfiguration:DA-END
        }
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public I2cConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.additional.elements.in.type:DA-START
    private DigitalLightTSL2561 light;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.DigitalLightTSL2561GroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type