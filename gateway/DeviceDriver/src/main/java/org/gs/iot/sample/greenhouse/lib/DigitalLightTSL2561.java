package org.gs.iot.sample.greenhouse.lib;

import java.io.IOException;

import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveGainEnum;
import org.gs.iot.sample.greenhouse.devicedata.DigitalLightTSL2561GroveIntegrateTimeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jdk.dio.DeviceManager;
import jdk.dio.i2cbus.I2CDevice;
import jdk.dio.i2cbus.I2CDeviceConfig;

public class DigitalLightTSL2561 {

	private static final Logger logger = LoggerFactory.getLogger(DigitalLightTSL2561.class);

	private static final int TSL2561_Address = 0x29;

	private static final byte TSL2561_Control = (byte) 0x80;
	private static final byte TSL2561_Timing = (byte) 0x81;
	private static final byte TSL2561_Interrupt = (byte) 0x86;
	private static final byte TSL2561_Channal0L = (byte) 0x8C;
	private static final byte TSL2561_Channal0H = (byte) 0x8D;
	private static final byte TSL2561_Channal1L = (byte) 0x8E;
	private static final byte TSL2561_Channal1H = (byte) 0x8F;

	private static final float K1T = 0.50f;
	private static final float B1T = 0.0304f;
	private static final float M1T = 0.062f;
	private static final float K2T = 0.61f;
	private static final float B2T = 0.0224f;
	private static final float M2T = 0.031f;
	private static final float K3T = 0.80f;
	private static final float B3T = 0.0128f;
	private static final float M3T = 0.0153f;
	private static final float K4T = 1.30f;
	private static final float B4T = 0.00146f;
	private static final float M4T = 0.00112f;

	public static enum Gain {
		x1((byte) 0x00, 16), x16((byte) 0x10, 1);

		private byte b;
		private int scale;

		Gain(byte b, int scale) {
			this.b = b;
			this.scale = scale;
		}

		public byte b() {
			return b;
		}

		public int scale() {
			return scale;
		}

		public static Gain getGain(DigitalLightTSL2561GroveGainEnum gainE) {
			switch (gainE) {
				case X1:
					return Gain.x1;
				case X16:
					return Gain.x16;
				default:
					return null;
			}
			
		}
	}

	public static enum IntegrateTime {
		FourHundredTwo(402, (byte) 0x02, 1f), OneHoundredOne(101, (byte) 0x01, 0.252f), ThirteenPointSeven(14,
				(byte) 0x00, 0.034f);

		private int time;
		private byte b;
		private float scale;

		IntegrateTime(int time, byte b, float scale) {
			this.time = time;
			this.b = b;
			this.scale = scale;
		}

		public int time() {
			return time;
		}

		public byte b() {
			return b;
		}

		public float scale() {
			return scale;
		}
		
		public static IntegrateTime getIntegrateTime(DigitalLightTSL2561GroveIntegrateTimeEnum integrateTimeE) {
			switch(integrateTimeE) {
				case ThirteenPointSeven:
					return IntegrateTime.ThirteenPointSeven;
				case OneHoundredOne:
					return IntegrateTime.OneHoundredOne;
				case FourHundredTwo:
					return IntegrateTime.FourHundredTwo;
				default:
					return null;
			}
		}
	}

	private I2CDevice i2cDevice;
	private Gain gain;
	private IntegrateTime integrateTime;

	public DigitalLightTSL2561(Gain gain, IntegrateTime integrateTime) throws IOException {
		this.gain = gain;
		this.integrateTime = integrateTime;
		I2CDeviceConfig i2cDeviceConfig = new I2CDeviceConfig(1, TSL2561_Address, 7, I2CDeviceUtil.I2C_CLOCK_FREQUENCY);
		i2cDevice = DeviceManager.open(i2cDeviceConfig);

		try {
			i2cDevice.tryLock(I2CDeviceUtil.I2C_TIMEOUT);

			I2CDeviceUtil.writeTransactional(i2cDevice, TSL2561_Control, 0x03);
			I2CDeviceUtil.writeTransactional(i2cDevice, TSL2561_Timing, this.gain.b() | this.integrateTime.b());
			I2CDeviceUtil.writeTransactional(i2cDevice, TSL2561_Interrupt, 0x00);
			I2CDeviceUtil.writeTransactional(i2cDevice, TSL2561_Control, 0x00);
		} finally {
			i2cDevice.unlock();
		}
	}

	public void close() throws IOException {
		i2cDevice.close();
	}

	private int[] getChannels() throws IOException {
		try {
			i2cDevice.tryLock(I2CDeviceUtil.I2C_TIMEOUT);
			int[] channels = new int[2];
			I2CDeviceUtil.writeTransactional(i2cDevice, TSL2561_Control, 0x03);
			try {
				Thread.sleep(this.integrateTime.time());
			} catch (InterruptedException e) {
				throw new IOException("got interrupted: sophisticated result");
			}

			int ch0_low = I2CDeviceUtil.read(i2cDevice, TSL2561_Channal0L); // channel
																			// 0.
																			// visible
																			// light
																			// and
																			// IR
			int ch0_high = I2CDeviceUtil.read(i2cDevice, TSL2561_Channal0H);
			// read two bytes from registers 0x0E and 0x0F
			int ch1_low = I2CDeviceUtil.read(i2cDevice, TSL2561_Channal1L); // channel
																			// 1.
																			// IR
			int ch1_high = I2CDeviceUtil.read(i2cDevice, TSL2561_Channal1H);

			I2CDeviceUtil.writeTransactional(i2cDevice, TSL2561_Control, 0x00);

			channels[0] = ((ch0_high & 0xFF) << 8) | (ch0_low & 0xFF);
			channels[1] = ((ch1_high & 0xFF) << 8) | (ch1_low & 0xFF);

			return channels;
		} finally {
			i2cDevice.unlock();
		}
	}

	// Visible Lux calclulated for TMB Package
	public float getVisibleLux() throws IOException {
		int[] channels = getChannels();

		if (channels[0] == 0xFFFF || channels[1] == 0xFFFF) {
			return -1f;
		} else if (channels[0] == 0) { // we cannot devide by zero (ratio >
										// K4T), lux = 0
			return 0f;
		}

		float lux;
		float ch0 = (channels[0] * this.gain.scale) / this.integrateTime.scale;
		float ch1 = (channels[1] * this.gain.scale) / this.integrateTime.scale;
		float ratio = ch1 / ch0;

		if (0 < ratio && ratio <= K1T) { // http://www.seeedstudio.com/wiki/images/d/de/TSL2561T.pdf
			lux = B1T * ch0 - M1T * ch0 * (float) Math.pow(ratio, 1.4f);
		} else if (ratio <= K2T) {
			lux = B2T * ch0 - M2T * ch1;
		} else if (ratio <= K3T) {
			lux = B3T * ch0 - M3T * ch1;
		} else if (ratio <= K4T) {
			lux = B4T * ch0 - M4T * ch1;
		} else { // ratio > K4T
			lux = 0f;
		}

		return lux;
	}
}
