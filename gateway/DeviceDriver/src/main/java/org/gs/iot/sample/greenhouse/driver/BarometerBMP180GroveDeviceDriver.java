package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.I2cConnection;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.lib.BMP180;
import org.gs.iot.sample.greenhouse.devicedata.BarometerBMP180GroveStatus;


public class BarometerBMP180GroveDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(BarometerBMP180GroveDeviceDriver.class);
    
    private BarometerBMP180GroveDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<BarometerBMP180GroveData> interruptCallback;
    
    /**
     * creates an instance of BarometerBMP180GroveDeviceDriver
     * 
     * @param setup  the setup
     */
    public BarometerBMP180GroveDeviceDriver(BarometerBMP180GroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public BarometerBMP180GroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<BarometerBMP180GroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized BarometerBMP180GroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.getData.BarometerBMP180GroveData:DA-START
		int ut = bmp180.readUncompensatedTemperature();
		int up = bmp180.readUncompensatedPressure();
		float temp = bmp180.calcTemperature(ut);
		float pressure = bmp180.calcPressure(up);
		float alt = bmp180.calcAltitude(pressure);
		BarometerBMP180GroveStatus status = new BarometerBMP180GroveStatus();
    	status.setTemperature(temp);
    	status.setPressure(pressure);
    	status.setAltitude(alt);
    	BarometerBMP180GroveData data = new BarometerBMP180GroveData();
    	data.setData(status);
    	return data;
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.getData.BarometerBMP180GroveData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.getData.BarometerBMP180GroveData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.activate:DA-START
    	bmp180 = new BMP180();
    	logger.debug("activated Barometer BMP180");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.deactivate:DA-START
    	bmp180.close();
    	bmp180 = null;
    	logger.debug("deactivated Barometer BMP180");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private I2cConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param connection  the connection
         */
        public DeviceSetup(I2cConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.I2cConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.I2cConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public I2cConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.additional.elements.in.type:DA-START
    private BMP180 bmp180;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.BarometerBMP180GroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type