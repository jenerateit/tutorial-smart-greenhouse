package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.DigitalConnection;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException;
import org.gs.iot.sample.greenhouse.devicedata.PIRMotionGroveStatusType;
import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;
import jdk.dio.gpio.PinEvent;
import jdk.dio.gpio.PinListener;


public class PIRMotionGroveDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PIRMotionGroveDeviceDriver.class);
    
    private PIRMotionGroveDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<PIRMotionGroveData> interruptCallback;
    
    /**
     * creates an instance of PIRMotionGroveDeviceDriver
     * 
     * @param setup  the setup
     */
    public PIRMotionGroveDeviceDriver(PIRMotionGroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public PIRMotionGroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<PIRMotionGroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized PIRMotionGroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.getData.PIRMotionGroveData:DA-START
    	try {
    		PIRMotionGroveData data = new PIRMotionGroveData();
			data.setData(new PIRMotionGroveStatusType(pin.getValue()));
			return data;
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.getData.PIRMotionGroveData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.getData.PIRMotionGroveData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.activate:DA-START
    	try {
    		GPIOPinConfig pinConfig = new GPIOPinConfig(0, setup.getConnection().getPinNumber(), GPIOPinConfig.DIR_INPUT_ONLY, GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_BOTH_EDGES, false);
			pin = DeviceManager.open(GPIOPin.class, pinConfig);
			
			pin.setInputListener(new PinListener() {
				
				@Override
				public void valueChanged(PinEvent event) {
					PIRMotionGroveData data = new PIRMotionGroveData();
					data.setData(new PIRMotionGroveStatusType(event.getValue()));
					interruptCallback.onInterrupt(data);
				}
			});
			
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	logger.debug("activated PIR Motion Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.deactivate:DA-START
    	try {
			pin.close();
		} catch (IOException e) {
//			throw new DigitalFaultException(e);
		}
    	pin = null;
    	logger.debug("deactivated PIR Motion Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private DigitalConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param connection  the connection
         */
        public DeviceSetup(DigitalConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public DigitalConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.additional.elements.in.type:DA-START
    private GPIOPin pin;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.PIRMotionGroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type