package org.gs.iot.sample.greenhouse.lib;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import jdk.dio.Device;
import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;
import jdk.dio.spibus.SPIDevice;
import jdk.dio.spibus.SPIDeviceConfig;

public class MCP3008 {

	public static final float MCP3008_V_Ref = 5.0f;
	public static final int MCP3008_CSPin = 8;

	private static Lock lock = new ReentrantLock(true);
	private static volatile int instances = 0;
	private static int pinNumberCS = -1;
	private static float V_Ref = 0.0f;
	private static MCP3008 mcp3008 = null;

	public static MCP3008 getInstance(int pinNumberCS, float V_Ref) throws IOException {
		try {
			lock.lock();
			if(MCP3008.pinNumberCS == -1) {
				MCP3008.pinNumberCS = pinNumberCS;
			}
			if(MCP3008.pinNumberCS != pinNumberCS) {
				String msg = "CS pin not possible";
				throw new IOException(msg);
			}
			if(MCP3008.V_Ref == 0.0f) {
				MCP3008.V_Ref = V_Ref;
			}
			if(MCP3008.V_Ref != V_Ref) {
				String msg = "V_Ref has changed, not possible";
				throw new IOException(msg);
			}
			if(instances == 0) {
				MCP3008.mcp3008 = new MCP3008();
			}
			instances++;
			return MCP3008.mcp3008;
		}
		finally {
			lock.unlock();
		}
	}

	private GPIOPin cs;
	private SPIDevice spiDevice;

	private MCP3008() throws IOException {
		/*
		 * args are:
		 * controllerNumber: 0
		 * address: 0
		 * csActive: spi is active when cs is low
		 * clockFrequency: 10000
		 * clockMode: CPOL: 0 = Active-high clocks selected
		 * CPHA: 1 = Sampling of data occurs at even edges of the SCK clock
		 * https://docs.oracle.com/javame/config/cldc/opt-pkgs/api/dio-jmee8/api/jdk/dio/spibus/SPIDeviceConfig.html
		 * wordLength: 8-bit
		 * bitOrdering: big endian
		 */
		SPIDeviceConfig config = new SPIDeviceConfig(0, 0, SPIDeviceConfig.CS_ACTIVE_LOW, 10000, 1, 8,
				Device.BIG_ENDIAN);
		spiDevice = DeviceManager.open(SPIDevice.class, config);

		GPIOPinConfig pinConfig = new GPIOPinConfig(0, MCP3008.pinNumberCS, GPIOPinConfig.DIR_OUTPUT_ONLY,
				GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_NONE, true);

		cs = DeviceManager.open(GPIOPin.class, pinConfig);

	}

	public void close() throws IOException {
		MCP3008.instances--;
		if(MCP3008.instances == 0) {
			spiDevice.close();
			cs.close();
		}
	}

	//TODO result = 0 if mcp is disconnected
	public int readChannel(int channel) throws IOException {
		try {
			lock.lock();
			cs.setValue(false);

			byte[] packet = { 1, (byte) ((8 + channel) << 4), 0 };
			ByteBuffer src = ByteBuffer.wrap(packet);
			ByteBuffer dst = ByteBuffer.allocate(3);
			spiDevice.writeAndRead(src, dst);
			packet = dst.array();

			cs.setValue(true);
			int result = (packet[1] & 0x03) << 8 | (packet[2] & 0xFF);
			return result;
		}
		finally {
			lock.unlock();
		}
	}

	public static float calculateVoltage(int digitalOutputCode) {
		float voltage = ((digitalOutputCode * MCP3008.V_Ref) / 1023);
		return voltage;
	}

	// private String getHex(byte[] data) {
	// StringBuffer buffer = new StringBuffer();
	// char[] c = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	// for(byte b : data) {
	// buffer.append(c[(b >>> 4) & 0x0F]);
	// buffer.append(c[b & 0x0F]);
	// buffer.append(' ');
	// }
	// return buffer.toString();
	// }

}
