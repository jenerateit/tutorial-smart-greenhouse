package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraData;
import java.io.IOException;
/*
 * Imports from last generation
 */
import java.util.Base64;
import java.util.concurrent.CountDownLatch;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraFaultException;
import org.gs.iot.sample.greenhouse.devicedata.RpiCameraStatus;
import au.edu.jcu.v4l4j.CaptureCallback;
import au.edu.jcu.v4l4j.FrameGrabber;
import au.edu.jcu.v4l4j.V4L4JConstants;
import au.edu.jcu.v4l4j.VideoDevice;
import au.edu.jcu.v4l4j.VideoFrame;
import au.edu.jcu.v4l4j.exceptions.V4L4JException;


public class RpiCameraDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(RpiCameraDeviceDriver.class);
    
    private RpiCameraDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<RpiCameraData> interruptCallback;
    
    /**
     * creates an instance of RpiCameraDeviceDriver
     * 
     * @param setup  the setup
     */
    public RpiCameraDeviceDriver(RpiCameraDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public RpiCameraDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<RpiCameraData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized RpiCameraData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.getData.RpiCameraData:DA-START
    	
    	try {
			if(!device.supportJPEGConversion()) {
				throw new RpiCameraFaultException("jpeg not supported");
			}
			
			int input = 0;
			int std = V4L4JConstants.STANDARD_WEBCAM;
			
			final CountDownLatch countDownLatch = new CountDownLatch(1);
	
			FrameGrabber frame = device.getJPEGFrameGrabber(320, 240, input, std, 50);
			
			CaptureCallbackImpl capturer = new CaptureCallbackImpl(countDownLatch);
			frame.setCaptureCallback(capturer);
			frame.startCapture();
			countDownLatch.await();
			frame.stopCapture();
			device.releaseFrameGrabber();
			byte[] image = capturer.getImage();
			
			RpiCameraData data = new RpiCameraData();
			RpiCameraStatus status = new RpiCameraStatus();
			status.setMimetype("image/jpg;base64");
			status.setImage(Base64.getEncoder().encodeToString(image));
			data.setData(status);
			return data;
    	}
    	catch(V4L4JException | InterruptedException e) {
    		throw new RpiCameraFaultException(e);
    	}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.getData.RpiCameraData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.getData.RpiCameraData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.activate:DA-START
    	
    	try {
			device = new VideoDevice("/dev/video0");
		} catch (V4L4JException e) {
			throw new RpiCameraFaultException(e);
		}

    	logger.debug("activated Rpi Camera");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.deactivate:DA-START
    	device.release();
    	logger.debug("deactivated Rpi Camera");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        
        
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.additional.elements.in.type:DA-START
    private VideoDevice device;

	private static class CaptureCallbackImpl implements CaptureCallback {

		private CountDownLatch latch;
		private byte[] image = null;
		private V4L4JException e = null;

		public CaptureCallbackImpl(CountDownLatch latch) {
			this.latch = latch;
		}

		public byte[] getImage() throws V4L4JException {
			if(e != null) {
				throw e;
			}
			return image;
		}

		@Override
		public void nextFrame(VideoFrame videoFrame) {
			image = videoFrame.getBytes();
			videoFrame.recycle();
			latch.countDown();
		}

		@Override
		public void exceptionReceived(V4L4JException e) {
			this.e = e;
		}
	}
    
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.RpiCameraDeviceDriver.additional.elements.in.type:DA-END
} // end of java type