package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.I2cConnection;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdButtonsEnum;
import org.gs.iot.sample.greenhouse.devicedata.AdafruitI2cRgbLcdStatus;
import org.gs.iot.sample.greenhouse.lib.AdafruitLcdPlate;


public class AdafruitI2cRgbLcdDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AdafruitI2cRgbLcdDeviceDriver.class);
    
    private AdafruitI2cRgbLcdDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<AdafruitI2cRgbLcdData> interruptCallback;
    
    /**
     * creates an instance of AdafruitI2cRgbLcdDeviceDriver
     * 
     * @param setup  the setup
     */
    public AdafruitI2cRgbLcdDeviceDriver(AdafruitI2cRgbLcdDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public AdafruitI2cRgbLcdDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<AdafruitI2cRgbLcdData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @param data
     */
    public synchronized void setData(AdafruitI2cRgbLcdData data) throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.setData.AdafruitI2cRgbLcdData:DA-START
    	AdafruitI2cRgbLcdStatus newStatus = data.getData();
    	boolean changed = false;
    	
    	if(status.getBacklight() != newStatus.getBacklight()) {
    		changed = true;
    		lcdPlate.setBacklight(newStatus.getBacklight());
    	}
    	if(!status.getRow0().equals(newStatus.getRow0())) {
    		changed = true;
    		lcdPlate.setCursorPosition(0, 0);
    		lcdPlate.write(newStatus.getRow0());
    	}
    	if(!status.getRow1().equals(newStatus.getRow1())) {
    		changed = true;
    		lcdPlate.setCursorPosition(1, 0);
    		lcdPlate.write(newStatus.getRow1());
    	}
    	
    	if(changed) {
    		status = newStatus;
    		data = new AdafruitI2cRgbLcdData();
    		data.setData(status);
    		interruptCallback.onInterrupt(data);
    	}
    	
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.setData.AdafruitI2cRgbLcdData:DA-ELSE
        //return;
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.setData.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized AdafruitI2cRgbLcdData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.getData.AdafruitI2cRgbLcdData:DA-START
		logger.error("getData");
    	if(lcdPlate == null) {
    		logger.debug("test");
    		logger.error("\n\n\n\n\n\nnot activated\n\n\n\n\n");
    	}else {
    		logger.debug("test");
    		logger.error("\n\n\n\n\n\n activated\n\n\n\n\n");
    	}
    	status.setButton(lcdPlate.getButton());
    	AdafruitI2cRgbLcdData data = new AdafruitI2cRgbLcdData();
    	data.setData(status);
    	return data;
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.getData.AdafruitI2cRgbLcdData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.getData.AdafruitI2cRgbLcdData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.activate:DA-START
    	lcdPlate = new AdafruitLcdPlate();
    	lcdPlate.activate(setup.getConnection().geti2cBus(), setup.getConnection().geti2cAddress());
    	status = new AdafruitI2cRgbLcdStatus();
    	status.setRow0("");
    	status.setRow1("");
    	status.setBacklight(AdafruitI2cRgbLcdStatus.BACKLIGHT_ON);
    	status.setButton(AdafruitI2cRgbLcdButtonsEnum.UNKNOWN);
		lcdPlate.setBacklight(status.getBacklight());
		lcdPlate.setCursorPosition(0, 0);
		lcdPlate.write(status.getRow0());
		lcdPlate.setCursorPosition(1, 0);
		lcdPlate.write(status.getRow1());
    	AdafruitI2cRgbLcdData data = new AdafruitI2cRgbLcdData();
    	data.setData(status);
    	setData(data);
    	logger.debug("activated AdafruitI2cRgbLcd");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.deactivate:DA-START
    	lcdPlate.deactivate();
    	lcdPlate = null;
    	status = null;
    	logger.debug("deactivated AdafruitI2cRgbLcd");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private I2cConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param connection  the connection
         */
        public DeviceSetup(I2cConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.I2cConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.I2cConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public I2cConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.additional.elements.in.type:DA-START
    private AdafruitLcdPlate lcdPlate;
    private AdafruitI2cRgbLcdStatus status;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.AdafruitI2cRgbLcdDeviceDriver.additional.elements.in.type:DA-END
} // end of java type