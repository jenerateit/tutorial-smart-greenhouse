package org.gs.iot.sample.greenhouse.lib;

import java.io.IOException;

import jdk.dio.DeviceManager;
import jdk.dio.i2cbus.I2CDevice;
import jdk.dio.i2cbus.I2CDeviceConfig;

public class BMP180 {

	private static final int ADDRESS = 0x77;

	private final byte OSS = 0;

	private I2CDevice device;

	private short ac1;
	private short ac2;
	private short ac3;
	private int ac4;
	private int ac5;
	private int ac6;
	private short b1;
	private short b2;
	// private short mb;
	private short mc;
	private short md;
	private int pressureCompensate;

	public BMP180() throws IOException {
			I2CDeviceConfig i2cDeviceConfig = new I2CDeviceConfig(1, ADDRESS, 7, I2CDeviceUtil.I2C_CLOCK_FREQUENCY);
			device = DeviceManager.open(i2cDeviceConfig);

			try {
				device.tryLock(I2CDeviceUtil.I2C_TIMEOUT);
				ac1 = readShort(0xAA);
				ac2 = readShort(0xAC);
				ac3 = readShort(0xAE);
				ac4 = readUnsignedShort(0xB0);
				ac5 = readUnsignedShort(0xB2);
				ac6 = readUnsignedShort(0xB4);
				b1 = readShort(0xB6);
				b2 = readShort(0xB8);
				// mb = readShort(0xBA);
				mc = readShort(0xBC);
				md = readShort(0xBE);
			} finally {
				device.unlock();
			}
			
	}

	
	public void close() throws IOException {
		device.close();
	}

	

	public int readUncompensatedTemperature() throws IOException {
		try {
			device.tryLock(I2CDeviceUtil.I2C_TIMEOUT);
			I2CDeviceUtil.write(device, 0xF4, 0x2E);
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				throw new IOException("got interrupted: sophisticated result");
			}
			return readShort(0xF6);
		} finally {
			device.unlock();
		}
	}

	public int readUncompensatedPressure() throws IOException {
		try {
			device.tryLock(I2CDeviceUtil.I2C_TIMEOUT);
			I2CDeviceUtil.write(device, 0xF4, 0x34 + (OSS << 6));
			try {
				Thread.sleep(2 + (3 << OSS));
			} catch (InterruptedException e) {
				throw new IOException("got interrupted: sophisticated result");
			}
			int msb = I2CDeviceUtil.read(device, 0xF6);
			int lsb = I2CDeviceUtil.read(device, 0xF7);
			int xlsb = I2CDeviceUtil.read(device, 0xF8);

			return (((msb & 0xFF) << 16) | ((lsb & 0xFF) << 8) | (xlsb & 0xFF)) >> (8 - OSS);
		} finally {
			device.unlock();
		}
	}

	private short readShort(int address) throws IOException {
		byte[] buffer = new byte[2];
		I2CDeviceUtil.read(device, address, buffer, 0, 2);
		short result = (short) (((buffer[0] & 0xFF) << 8) | (buffer[1] & 0xFF));
		return result;
	}

	private int readUnsignedShort(int address) throws IOException {
		byte[] buffer = new byte[2];
		I2CDeviceUtil.read(device, address, buffer, 0, 2);
		int result = ((buffer[0] & 0xFF) << 8) | (buffer[1] & 0xFF);
		return result;
	}

	public float calcAltitude(float pressure) {
		float pressure0 = 1013.25f;
		return calcAltitude(pressure, pressure0);
	}

	public float calcAltitude(float pressure, float pressure0) {
		return 44330f * (1 - (float) Math.pow(pressure / pressure0, 1 / 5.255));
	}

	public float calcTemperature(int uncompensatedTemperature) {
		int x1, x2;

		x1 = ((uncompensatedTemperature - ac6) * ac5) >> 15;
		x2 = ((mc) << 11) / (x1 + md);

		pressureCompensate = x1 + x2;

		int temp = ((pressureCompensate + 8) >> 4);
		return (temp) / 10f;
	}

	public float calcPressure(int uncompensatedPressure) {
		int x1, x2, x3, b3, b6, p;
		long b4, b7;
		b6 = pressureCompensate - 4000;
		x1 = (b2 * (b6 * b6) >> 12) >> 11;
		x2 = (ac2 * b6) >> 11;
		x3 = x1 + x2;
		b3 = (((ac1 * 4 + x3) << OSS) + 2) >> 2;

		// Calculate B4
		x1 = (ac3 * b6) >> 13;
		x2 = (b1 * ((b6 * b6) >> 12)) >> 16;
		x3 = ((x1 + x2) + 2) >> 2;
		b4 = (ac4 * (long) (x3 + 32768)) >> 15;

		b7 = ((uncompensatedPressure - b3) * (50000 >> OSS));

		if (b7 < 0x80000000)
			p = (int) ((b7 << 1) / b4);
		else
			p = (int) ((b7 / b4) << 1);

		x1 = (p >> 8) * (p >> 8);
		x1 = (x1 * 3038) >> 16;
		x2 = (-7357 * p) >> 16;
		p += (x1 + x2 + 3791) >> 4;
		return (p) / 100f;
	}
}
