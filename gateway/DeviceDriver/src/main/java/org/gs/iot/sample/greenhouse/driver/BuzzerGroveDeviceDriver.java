package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.DigitalConnection;
/*
 * Imports from last generation
 */
import java.util.Date;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveStatusType;
import org.gs.iot.sample.greenhouse.devicedata.DigitalFaultException;
import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;


public class BuzzerGroveDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(BuzzerGroveDeviceDriver.class);
    
    private BuzzerGroveDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<BuzzerGroveData> interruptCallback;
    
    /**
     * creates an instance of BuzzerGroveDeviceDriver
     * 
     * @param setup  the setup
     */
    public BuzzerGroveDeviceDriver(BuzzerGroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public BuzzerGroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<BuzzerGroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @param data
     */
    public synchronized void setData(BuzzerGroveData data) throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.setData.BuzzerGroveData:DA-START
    	try {
    		pin.setValue(data.getData().isStatus());
    		data.setTimestamp(new Date());
    		interruptCallback.onInterrupt(data);
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.setData.BuzzerGroveData:DA-ELSE
        //return;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.setData.BuzzerGroveData:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized BuzzerGroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.getData.BuzzerGroveData:DA-START
    	try {
    		BuzzerGroveData data = new BuzzerGroveData();
			data.setData( new BuzzerGroveStatusType(pin.getValue()));
			return data;
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.getData.BuzzerGroveData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.getData.BuzzerGroveData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.activate:DA-START
    	try {
    		GPIOPinConfig pinConfig = new GPIOPinConfig(0, setup.getConnection().getPinNumber(), GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_BOTH_EDGES, false);
			pin = DeviceManager.open(GPIOPin.class, pinConfig);
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	logger.debug("activated Buzzer Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.deactivate:DA-START
    	try {
			pin.close();
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	pin = null;
    	logger.debug("deactivated Buzzer Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private DigitalConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param connection  the connection
         */
        public DeviceSetup(DigitalConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.DigitalConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public DigitalConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.additional.elements.in.type:DA-START
    private GPIOPin pin;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.BuzzerGroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type