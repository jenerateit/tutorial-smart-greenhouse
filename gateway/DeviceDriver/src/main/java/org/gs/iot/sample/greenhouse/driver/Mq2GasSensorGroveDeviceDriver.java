package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.Mq2GasSensorGroveData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.AnalogConnection;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException;
import org.gs.iot.sample.greenhouse.lib.GasSensorMq2;


public class Mq2GasSensorGroveDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Mq2GasSensorGroveDeviceDriver.class);
    
    private Mq2GasSensorGroveDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<Mq2GasSensorGroveData> interruptCallback;
    
    /**
     * creates an instance of Mq2GasSensorGroveDeviceDriver
     * 
     * @param setup  the setup
     */
    public Mq2GasSensorGroveDeviceDriver(Mq2GasSensorGroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public Mq2GasSensorGroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<Mq2GasSensorGroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized Mq2GasSensorGroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.getData.Mq2GasSensorGroveData:DA-START
    	try {
    		Mq2GasSensorGroveData data = new Mq2GasSensorGroveData();
	    	data.setData(gasSensorMq2.getStatus());
	    	return data;
		} catch (IOException e) {
			throw new AnalogFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.getData.Mq2GasSensorGroveData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.getData.Mq2GasSensorGroveData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.activate:DA-START
    	try {
			gasSensorMq2.activate(setup.getConnection().getChannel());
		} catch (IOException e) {
			throw new AnalogFaultException(e);
		}
    	logger.debug("activated MQ2 Gas Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.deactivate:DA-START
    	try {
			gasSensorMq2.deactivate();
		} catch (IOException e) {
			throw new AnalogFaultException(e);
		}
    	logger.debug("deactivated MQ2 Gas Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private AnalogConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param connection  the connection
         */
        public DeviceSetup(AnalogConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public AnalogConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.additional.elements.in.type:DA-START
    GasSensorMq2 gasSensorMq2 = new GasSensorMq2();
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.Mq2GasSensorGroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type