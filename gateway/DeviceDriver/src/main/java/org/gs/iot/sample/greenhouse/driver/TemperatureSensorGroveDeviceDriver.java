package org.gs.iot.sample.greenhouse.driver;


import org.gs.iot.sample.greenhouse.gateway.driverimpl.AbstractSmartGreenhouseAppDeviceDriver;
import org.slf4j.Logger;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppInterruptCallback;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import java.io.IOException;
import org.gs.iot.sample.greenhouse.devicedata.AnalogConnection;
/*
 * Imports from last generation
 */
import org.gs.iot.sample.greenhouse.devicedata.AnalogFaultException;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveStatusType;
import org.gs.iot.sample.greenhouse.lib.MCP3008;


public class TemperatureSensorGroveDeviceDriver extends AbstractSmartGreenhouseAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TemperatureSensorGroveDeviceDriver.class);
    
    private TemperatureSensorGroveDeviceDriver.DeviceSetup setup;
    
    private SmartGreenhouseAppInterruptCallback<TemperatureSensorGroveData> interruptCallback;
    
    /**
     * creates an instance of TemperatureSensorGroveDeviceDriver
     * 
     * @param setup  the setup
     */
    public TemperatureSensorGroveDeviceDriver(TemperatureSensorGroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup:DA-END
    }
    
    /**
     * getter for the field setup
     * 
     * 
     * 
     * @return
     */
    public TemperatureSensorGroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    /**
     * setter for the field interruptCallback
     * 
     * 
     * 
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(SmartGreenhouseAppInterruptCallback<TemperatureSensorGroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.setInterruptCallback.SmartGreenhouseAppInterruptCallback:DA-END
    }
    /**
     * 
     * @return
     */
    @Override
    public synchronized TemperatureSensorGroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.getData.TemperatureSensorGroveData:DA-START
    	try {
    		int doc = mcp3008.readChannel(setup.getConnection().getChannel());
    		int B = 3975; // B value of the thermistor
    		float R = (1023f - doc) * 10000 / doc;
    		float T = 1f / (((float) Math.log(R / 10000) / B) + (1 / 298.15f));
    		float temp = T - 273.15f;
    		TemperatureSensorGroveData data = new TemperatureSensorGroveData();
    		data.setData(new TemperatureSensorGroveStatusType(temp));
    		return data;
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.getData.TemperatureSensorGroveData:DA-ELSE
        //// TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.getData.TemperatureSensorGroveData:DA-END
    }
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.activate:DA-START
    	try {
    		mcp3008 = MCP3008.getInstance(MCP3008.MCP3008_CSPin, MCP3008.MCP3008_V_Ref);
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
    	logger.debug("activated Temperature Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.activate:DA-ELSE
        //// TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.activate:DA-END
    }
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.deactivate:DA-START
    	try {
    		mcp3008.close();
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
    	logger.debug("deactivated Temperature Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.deactivate:DA-ELSE
        //// TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.deactivate:DA-END
    }
    
    public static class DeviceSetup { // start of class
    
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private AnalogConnection connection;
        
        
        /**
         * creates an instance of DeviceSetup
         * 
         * @param connection  the connection
         */
        public DeviceSetup(AnalogConnection connection) {
            //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         * 
         * 
         * 
         * @return
         */
        public AnalogConnection getConnection() {
            //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.additional.elements.in.type:DA-START
    private MCP3008 mcp3008;
    //DA-ELSE:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.driver.TemperatureSensorGroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type