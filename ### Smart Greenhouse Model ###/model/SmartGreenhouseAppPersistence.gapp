namespace org.gs.iot.sample.greenhouse.gateway;

module SmartGreenhouseAppPersistence kind = Basic, Persistence;


/*
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
entity BaseEntity {
	set Storable = false;  // with this, there is no table created for this entity, fields are created in table of child entity
    set UtilityFields = true;

	field pk : sint64 { set Id = true; }


	/*
	Identifies a group of devices and users. It can be seen
	as partition of the MQTT topic namespace.
    For example, access control lists can be defined so that users
	are only given access to the child topics of a given account_name. 
	*/
	field accountName : string;
	
	/*
	Identifies a single gateway device within an
    account (typically the MAC address of a
    gateway?s primary network interface).
    The client_id maps to the Client Identifier (Client ID)
    as defined in the MQTT specifications. 
    */
	field clientId : string {
	    set Mandatory = true;
	}
	
	/*
	Identifies an application running on the gateway device. 
    To support multiple versions of the application, it is recommended 
    that a version number be assigned with the app_id (e.g., ?CONF-V1?,
               ?CONF-V2?, etc.)
    */
	field appId : string {
	    set Mandatory = true;
	}
	
	/*
	Identifies a resource(s) that is owned and managed by a particular application.
	Management of resources (e.g., sensors, actuators, local files, or configuration options)
    includes listing them, reading the latest value, or updating them to a new value.
    A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
    identify a temperature sensor and ?sensor/hum? a humidity sensor.
	*/
	field resourceId : string {
	    set Mandatory = true;
	}

    /*
    Any client that publishes messages can include geo coordinates in it.
    Those coordinates will be stored here. Otherwise, the geo coordinates
    are left empty.
    */
    field geoCoordinates : GeoCoordinates;

    field measurementInfo : MeasurementInfo;

    field timeOfMessageReception : Datetime;


}

entity GeoCoordinates {
	field lng : sint32;
	field lat : sint32;
	field alt : sint32;
}

entity MeasurementInfo {
    field timeOfMeasurement : Datetime;
	field timeZoneOfMeasurement : string;
}


entity PlantMotionEntity extends BaseEntity {
    field data : PlantMotion;
}
/*
 persistent data structure for hardware of type 'PIRMotionGrove'
*/
entity PlantMotion {
    field status : uint1;
}

entity WaterPumpEntity extends BaseEntity {
    field data : WaterPump;
}
/*
 persistent data structure for hardware of type 'RelayGrove'
*/
entity WaterPump {
    field status : uint1;
}

entity AlarmEntity extends BaseEntity {
    field data : Alarm;
}
/*
 persistent data structure for hardware of type 'BuzzerGrove'
*/
entity Alarm {
    field status : uint1;
}

entity PlantIlluminanceEntity extends BaseEntity {
    field data : PlantIlluminance;
}
/*
 persistent data structure for hardware of type 'DigitalLightTSL2561Grove'
*/
entity PlantIlluminance {
    field status : float32;
}

entity DisplayEntity extends BaseEntity {
    field data : Display;
}
/*
 persistent data structure for hardware of type 'AdafruitI2cRgbLcd'
*/
entity Display {
    field backlight : sint32;
    field row0 : string;
    field row1 : string;
    field button : AdafruitI2cRgbLcdButtonsEnum;
}

enumeration AdafruitI2cRgbLcdButtonsEnum {
    entry BUTTON_SELECT;
    entry BUTTON_RIGHT;
    entry BUTTON_DOWN;
    entry BUTTON_UP;
    entry BUTTON_LEFT;
    entry NOT_PRESSED;
    entry UNKNOWN;
}

entity PlantBarometerEntity extends BaseEntity {
    field data : PlantBarometer;
}
/*
 persistent data structure for hardware of type 'BarometerBMP180Grove'
*/
entity PlantBarometer {
    field pressure : float32;
    field temperature : float32;
    field altitude : float32;
}

entity Mq2GasEntity extends BaseEntity {
    field data : Mq2Gas;
}
/*
 persistent data structure for hardware of type 'Mq2GasSensorGrove'
*/
entity Mq2Gas {
    field rsAir : float32;
    field r0 : float32;
    field rsGas : float32;
    field ratio : float32;
}

entity TemperatureEntity extends BaseEntity {
    field data : Temperature;
}
/*
 persistent data structure for hardware of type 'TemperatureSensorGrove'
*/
entity Temperature {
    field status : float32;
}

entity PlantMoistureEntity extends BaseEntity {
    field data : PlantMoisture;
}
/*
 persistent data structure for hardware of type 'MoistureSensorGrove'
*/
entity PlantMoisture {
    field status : float32;
}

entity LightEntity extends BaseEntity {
    field data : Light;
}
/*
 persistent data structure for hardware of type 'LightStrip'
*/
entity Light {
    field r : sint16;
    field g : sint16;
    field b : sint16;
    field brightness : float64;
}

entity CameraEntity extends BaseEntity {
    field data : Camera;
}
/*
 persistent data structure for hardware of type 'RpiCamera'
*/
entity Camera {
    field mimetype : string;
    field image : string {
        set Length = 512000;
        set Binary = true;
    }
}
