namespace org.gs.iot.sample.greenhouse;

import org.gs.iot.sample.greenhouse.SensorsI2C;
import org.gs.iot.sample.greenhouse.SensorsAnalog;
import org.gs.iot.sample.greenhouse.SensorsDigital;
import org.gs.iot.sample.greenhouse.SensorsOther;
import org.gs.iot.sample.greenhouse.Topics;

module Device kind = Device, IoT;

device Greenhouse {
	
	/* Digital Sensors */
	sensorUsage PlantMotion : PIRMotionGrove {
		set SamplingInterval = 0;
		link TopicLevels = MotionTopic;
	}
	actuatorUsage WaterPump : RelayGrove {
		set SamplingInterval = 0;
		link TopicLevels = WaterPumpTopic;
	}
	
	actuatorUsage Alarm : BuzzerGrove {
		set SamplingInterval = 0;
		link TopicLevels = AlarmTopic;
	}
	
	
	/* I2C Sensors */
	sensorUsage PlantIlluminance : DigitalLightTSL2561Grove {
		link TopicLevels = IlluminanceTopic;
	}
	
	actuatorUsage Display : AdafruitI2cRgbLcd {
		set SamplingInterval = 0;
		link TopicLevels = DisplayTopic;
	}
	
	sensorUsage PlantBarometer : BarometerBMP180Grove {
		link TopicLevels = BarometerTopic;
	}
	
	/* Analog Sensors */
	sensorUsage Mq2Gas : Mq2GasSensorGrove {
		link TopicLevels = Mq2Topic;
	}
	
	sensorUsage Temperature : TemperatureSensorGrove {
		link TopicLevels = TemperatureTopic;
	}
	
	sensorUsage PlantMoisture : MoistureSensorGrove {
		link TopicLevels = MoistureTopic;
	}
	
	/* Other Sensors */
	actuatorUsage Light : LightStrip {
		set SamplingInterval = 0;
		link TopicLevels = LightTopic;
	}
	
	sensorUsage Camera : RpiCamera {
		link TopicLevels = CameraTopic;
		set SamplingInterval = 600000;
	}
}
