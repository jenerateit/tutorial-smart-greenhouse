package org.gs.iot.sample.greenhouse.gateway.paho;


import javax.ejb.Singleton;
import javax.inject.Named;
import java.util.logging.Logger;
/*
 * Imports from last generation
 */
import javax.enterprise.context.ApplicationScoped;


//DA-START:paho.SmartGreenhouseAppConfigurationEjb:DA-START
//DA-ELSE:paho.SmartGreenhouseAppConfigurationEjb:DA-ELSE
@Singleton
@Named
//DA-END:paho.SmartGreenhouseAppConfigurationEjb:DA-END

public class SmartGreenhouseAppConfigurationEjb { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(SmartGreenhouseAppConfigurationEjb.class.getName());
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningHost:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningHost:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningHost:DA-END
    private String dataProvisioningHost;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningContextRoot:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningContextRoot:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningContextRoot:DA-END
    private String dataProvisioningContextRoot;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningApplicationPath:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningApplicationPath:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.dataProvisioningApplicationPath:DA-END
    private String dataProvisioningApplicationPath;
    
    /**
     * creates an instance of SmartGreenhouseAppConfigurationEjb
     */
    public SmartGreenhouseAppConfigurationEjb() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb:DA-START
    	this.dataProvisioningHost = "http://localhost:8080";
    	this.dataProvisioningContextRoot = "greenhouse-data-provisioning-web-0.0.1-SNAPSHOT";
    	this.dataProvisioningContextRoot = "greenhouse-data-provisioning-web";
    	this.dataProvisioningApplicationPath = "apppath/v1";
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb:DA-END
    }
    
    /**
     * getter for the field dataProvisioningHost
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningHost.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningHost.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningHost.annotations:DA-END
    public String getDataProvisioningHost() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningHost.String:DA-ELSE
        return this.dataProvisioningHost;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningHost.String:DA-END
    }
    /**
     * setter for the field dataProvisioningHost
     * 
     * 
     * 
     * @param dataProvisioningHost  the dataProvisioningHost
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-END
    public void setDataProvisioningHost(String dataProvisioningHost) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningHost.String:DA-ELSE
        this.dataProvisioningHost = dataProvisioningHost;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningHost.String:DA-END
    }
    /**
     * getter for the field dataProvisioningContextRoot
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-END
    public String getDataProvisioningContextRoot() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-ELSE
        return this.dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-END
    }
    /**
     * setter for the field dataProvisioningContextRoot
     * 
     * 
     * 
     * @param dataProvisioningContextRoot  the dataProvisioningContextRoot
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-END
    public void setDataProvisioningContextRoot(String dataProvisioningContextRoot) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-ELSE
        this.dataProvisioningContextRoot = dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-END
    }
    /**
     * getter for the field dataProvisioningApplicationPath
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-END
    public String getDataProvisioningApplicationPath() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-ELSE
        return this.dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-END
    }
    /**
     * setter for the field dataProvisioningApplicationPath
     * 
     * 
     * 
     * @param dataProvisioningApplicationPath  the dataProvisioningApplicationPath
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-END
    public void setDataProvisioningApplicationPath(String dataProvisioningApplicationPath) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-ELSE
        this.dataProvisioningApplicationPath = dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.SmartGreenhouseAppConfigurationEjb.additional.elements.in.type:DA-END
} // end of java type