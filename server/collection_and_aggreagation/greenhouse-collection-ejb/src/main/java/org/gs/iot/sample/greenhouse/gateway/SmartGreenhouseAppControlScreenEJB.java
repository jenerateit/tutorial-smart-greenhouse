package org.gs.iot.sample.greenhouse.gateway;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto;
import com.vd.SessionDataHolder;

//DA-START:gateway.SmartGreenhouseAppControlScreenEJB:DA-START
//DA-ELSE:gateway.SmartGreenhouseAppControlScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:gateway.SmartGreenhouseAppControlScreenEJB:DA-END

public class SmartGreenhouseAppControlScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of SmartGreenhouseAppControlScreenEJB
     */
    public SmartGreenhouseAppControlScreenEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.load.SmartGreenhouseAppControlScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.load.SmartGreenhouseAppControlScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.load.SmartGreenhouseAppControlScreenDto.SessionDataHolder.annotations:DA-END
    public SmartGreenhouseAppControlScreenDto load(SmartGreenhouseAppControlScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.load.SmartGreenhouseAppControlScreenDto.SessionDataHolder.SmartGreenhouseAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.load.SmartGreenhouseAppControlScreenDto.SessionDataHolder.SmartGreenhouseAppControlScreenDto:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.load.SmartGreenhouseAppControlScreenDto.SessionDataHolder.SmartGreenhouseAppControlScreenDto:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB.additional.elements.in.type:DA-END
} // end of java type