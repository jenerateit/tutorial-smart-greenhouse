package org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui;


import com.vd.AbstractDto;
import java.io.Serializable;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'SmartGreenhouseAppControlScreen' in module 'SmartGreenhouseAppUi'.
 * Support for dirty flag is added by the DTO generator.
 */
public class SmartGreenhouseAppControlScreenDto extends AbstractDto implements Serializable { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.serialVersionUID:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.serialVersionUID:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.serialVersionUID:DA-END
    private static final long serialVersionUID = 1L;
    
    /**
     * final instance of the data container named 'SmartGreenhouseAppControlForm' in UI model 'SmartGreenhouseAppUi'
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.smartGreenhouseAppControlForm:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.smartGreenhouseAppControlForm:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.smartGreenhouseAppControlForm:DA-END
    private final SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto smartGreenhouseAppControlForm = new SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto();
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.fields:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.fields:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.fields:DA-END
    /**
     * creates an instance of SmartGreenhouseAppControlScreenDto
     */
    public SmartGreenhouseAppControlScreenDto() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto:DA-END
    }
    
    /**
     * getter for the field smartGreenhouseAppControlForm
     * 
     * final instance of the data container named 'SmartGreenhouseAppControlForm' in UI model 'SmartGreenhouseAppUi'
     * 
     * @return
     */
    public SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto getSmartGreenhouseAppControlForm() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.getSmartGreenhouseAppControlForm.SmartGreenhouseAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.getSmartGreenhouseAppControlForm.SmartGreenhouseAppControlFormDto:DA-ELSE
        return this.smartGreenhouseAppControlForm;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.getSmartGreenhouseAppControlForm.SmartGreenhouseAppControlFormDto:DA-END
    }
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        result = result || smartGreenhouseAppControlForm.isDirty();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.methods:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.methods:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.methods:DA-END
    
    /**
     * Private inner DTO class for UIDataContainer ('display' element) 'SmartGreenhouseAppControlForm' within UIStructuralContainer ('layout' element) 'SmartGreenhouseAppControlScreen' in module 'SmartGreenhouseAppUi'.
     * Support for dirty flag is added by the DTO generator.
     */
    public static class SmartGreenhouseAppControlFormDto extends AbstractDto implements Serializable { // start of class
    
    
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.serialVersionUID:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.serialVersionUID:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.serialVersionUID:DA-END
        private static final long serialVersionUID = 1L;
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningHost:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningHost:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningHost:DA-END
        private String dataProvisioningHost;
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningContextRoot:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningContextRoot:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningContextRoot:DA-END
        private String dataProvisioningContextRoot;
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningApplicationPath:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningApplicationPath:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.dataProvisioningApplicationPath:DA-END
        private String dataProvisioningApplicationPath;
        
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.fields:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.fields:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.fields:DA-END
        
        /**
         * creates an instance of SmartGreenhouseAppControlFormDto
         */
        public SmartGreenhouseAppControlFormDto() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto:DA-ELSE
            super();
            //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto:DA-END
        }
        
        
        /**
         * setter for the field dataProvisioningHost
         * 
         * 
         * 
         * @param dataProvisioningHost  the dataProvisioningHost
         */
        public void setDataProvisioningHost(String dataProvisioningHost) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningHost.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningHost.String:DA-ELSE
            if (isDifferent(this.dataProvisioningHost, dataProvisioningHost)) {
                this.setDirty(true);
                this.dataProvisioningHost = dataProvisioningHost;
            }
            //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningHost.String:DA-END
        }
        /**
         * getter for the field dataProvisioningHost
         * 
         * 
         * 
         * @return
         */
        public String getDataProvisioningHost() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningHost.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningHost.String:DA-ELSE
            return this.dataProvisioningHost;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningHost.String:DA-END
        }
        /**
         * setter for the field dataProvisioningContextRoot
         * 
         * 
         * 
         * @param dataProvisioningContextRoot  the dataProvisioningContextRoot
         */
        public void setDataProvisioningContextRoot(String dataProvisioningContextRoot) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningContextRoot.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningContextRoot.String:DA-ELSE
            if (isDifferent(this.dataProvisioningContextRoot, dataProvisioningContextRoot)) {
                this.setDirty(true);
                this.dataProvisioningContextRoot = dataProvisioningContextRoot;
            }
            //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningContextRoot.String:DA-END
        }
        /**
         * getter for the field dataProvisioningContextRoot
         * 
         * 
         * 
         * @return
         */
        public String getDataProvisioningContextRoot() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningContextRoot.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningContextRoot.String:DA-ELSE
            return this.dataProvisioningContextRoot;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningContextRoot.String:DA-END
        }
        /**
         * setter for the field dataProvisioningApplicationPath
         * 
         * 
         * 
         * @param dataProvisioningApplicationPath  the dataProvisioningApplicationPath
         */
        public void setDataProvisioningApplicationPath(String dataProvisioningApplicationPath) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningApplicationPath.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningApplicationPath.String:DA-ELSE
            if (isDifferent(this.dataProvisioningApplicationPath, dataProvisioningApplicationPath)) {
                this.setDirty(true);
                this.dataProvisioningApplicationPath = dataProvisioningApplicationPath;
            }
            //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.setDataProvisioningApplicationPath.String:DA-END
        }
        /**
         * getter for the field dataProvisioningApplicationPath
         * 
         * 
         * 
         * @return
         */
        public String getDataProvisioningApplicationPath() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningApplicationPath.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningApplicationPath.String:DA-ELSE
            return this.dataProvisioningApplicationPath;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.getDataProvisioningApplicationPath.String:DA-END
        }
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.methods:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.methods:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.methods:DA-END
        
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.SmartGreenhouseAppControlFormDto.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto.additional.elements.in.type:DA-END
} // end of java type