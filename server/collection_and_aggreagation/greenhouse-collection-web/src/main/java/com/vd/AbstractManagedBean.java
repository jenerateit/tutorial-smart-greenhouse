package com.vd;


import java.io.Serializable;
import com.vd.SessionDataHolder;
import javax.faces.event.ComponentSystemEvent;

public abstract class AbstractManagedBean implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * a property to hold session data for a more convenient access by code of managed beans, e.g. event handlers
     */
    private SessionDataHolder sessionDataHolder;
    
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    public void init() {
        //DA-START:com.vd.AbstractManagedBean.init:DA-START
        //DA-ELSE:com.vd.AbstractManagedBean.init:DA-ELSE
        return;
        //DA-END:com.vd.AbstractManagedBean.init:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     */
    public void preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:com.vd.AbstractManagedBean.preRenderViewListener.ComponentSystemEvent:DA-START
        //DA-ELSE:com.vd.AbstractManagedBean.preRenderViewListener.ComponentSystemEvent:DA-ELSE
        return;
        //DA-END:com.vd.AbstractManagedBean.preRenderViewListener.ComponentSystemEvent:DA-END
    }
    /**
     * setter for the field sessionDataHolder
     * 
     * a property to hold session data for a more convenient access by code of managed beans, e.g. event handlers
     * 
     * @param sessionDataHolder  the sessionDataHolder
     */
    public void setSessionDataHolder(SessionDataHolder sessionDataHolder) {
        //DA-START:com.vd.AbstractManagedBean.setSessionDataHolder.SessionDataHolder:DA-START
        //DA-ELSE:com.vd.AbstractManagedBean.setSessionDataHolder.SessionDataHolder:DA-ELSE
        this.sessionDataHolder = sessionDataHolder;
        //DA-END:com.vd.AbstractManagedBean.setSessionDataHolder.SessionDataHolder:DA-END
    }
    /**
     * getter for the field sessionDataHolder
     * 
     * a property to hold session data for a more convenient access by code of managed beans, e.g. event handlers
     * 
     * @return
     */
    public SessionDataHolder getSessionDataHolder() {
        //DA-START:com.vd.AbstractManagedBean.getSessionDataHolder.SessionDataHolder:DA-START
        //DA-ELSE:com.vd.AbstractManagedBean.getSessionDataHolder.SessionDataHolder:DA-ELSE
        return this.sessionDataHolder;
        //DA-END:com.vd.AbstractManagedBean.getSessionDataHolder.SessionDataHolder:DA-END
    }
    
    //DA-START:com.vd.AbstractManagedBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.AbstractManagedBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.AbstractManagedBean.additional.elements.in.type:DA-END
} // end of java type