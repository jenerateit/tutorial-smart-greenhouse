package org.gs.iot.sample.greenhouse.push.paho;


import java.util.logging.Logger;
import java.util.Date;

/**
 * Analog Sensors
 */
public class Mq2GasConnectionStatus { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(Mq2GasConnectionStatus.class.getName());
    
    private final String status;
    
    private final String cause;
    
    private final Date timestamp;
    
    /**
     * creates an instance of Mq2GasConnectionStatus
     * 
     * @param status  the status
     * @param cause  the cause
     * @param timestamp  the timestamp
     */
    public Mq2GasConnectionStatus(String status, String cause, Date timestamp) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.String.String.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.String.String.Date:DA-ELSE
        this.status = status;
        this.cause = cause;
        this.timestamp = timestamp;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.String.String.Date:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    public String getStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getStatus.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getStatus.String:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getStatus.String:DA-END
    }
    /**
     * getter for the field cause
     * 
     * 
     * 
     * @return
     */
    public String getCause() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getCause.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getCause.String:DA-ELSE
        return this.cause;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getCause.String:DA-END
    }
    /**
     * getter for the field timestamp
     * 
     * 
     * 
     * @return
     */
    public Date getTimestamp() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getTimestamp.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getTimestamp.Date:DA-ELSE
        return this.timestamp;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.getTimestamp.Date:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.push.paho.Mq2GasConnectionStatus.additional.elements.in.type:DA-END
} // end of java type