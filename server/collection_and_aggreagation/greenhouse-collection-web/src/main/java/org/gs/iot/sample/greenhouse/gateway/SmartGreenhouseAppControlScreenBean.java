package org.gs.iot.sample.greenhouse.gateway;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.vd.AbstractManagedBean;
import java.io.Serializable;
import org.gs.iot.sample.greenhouse.gateway.smartgreenhouseappui.SmartGreenhouseAppControlScreenDto;
import javax.ejb.EJB;
import org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenEJB;
import javax.annotation.PostConstruct;
import javax.faces.event.ComponentSystemEvent;

@ManagedBean(name="smartGreenhouseAppControlScreenBean")
@ViewScoped
public class SmartGreenhouseAppControlScreenBean extends AbstractManagedBean implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private SmartGreenhouseAppControlScreenDto dto = new SmartGreenhouseAppControlScreenDto();
    
    @EJB
    private SmartGreenhouseAppControlScreenEJB ejb;
    
    /**
     * getter for the field dto
     * 
     * 
     * 
     * @return
     */
    public SmartGreenhouseAppControlScreenDto getDto() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.getDto.SmartGreenhouseAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.getDto.SmartGreenhouseAppControlScreenDto:DA-ELSE
        return this.dto;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.getDto.SmartGreenhouseAppControlScreenDto:DA-END
    }
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @PostConstruct
    @Override
    public void init() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.init:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.init:DA-ELSE
        dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.init:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     */
    @Override
    public void preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-ELSE
         super.preRenderViewListener(event);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseAppControlScreenBean.additional.elements.in.type:DA-END
} // end of java type