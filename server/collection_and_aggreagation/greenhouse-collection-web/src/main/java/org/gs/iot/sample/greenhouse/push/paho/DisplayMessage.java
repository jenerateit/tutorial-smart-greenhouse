package org.gs.iot.sample.greenhouse.push.paho;


import java.util.logging.Logger;
import java.util.Date;

public class DisplayMessage { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(DisplayMessage.class.getName());
    
    private final String topic;
    
    private final String appId;
    
    private final String clientId;
    
    private String resourceId;
    
    private String content;
    
    private int qos;
    
    private boolean retained;
    
    private boolean duplicate;
    
    private final Date messageArrival;
    
    /**
     * creates an instance of DisplayMessage
     * 
     * @param topic  the topic
     * @param appId  the appId
     * @param clientId  the clientId
     * @param messageArrival  the messageArrival
     */
    public DisplayMessage(String topic, String appId, String clientId, Date messageArrival) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.String.String.String.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.String.String.String.Date:DA-ELSE
        this.topic = topic;
        this.appId = appId;
        this.clientId = clientId;
        this.messageArrival = messageArrival;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.String.String.String.Date:DA-END
    }
    
    /**
     * getter for the field topic
     * 
     * 
     * 
     * @return
     */
    public String getTopic() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getTopic.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getTopic.String:DA-ELSE
        return this.topic;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getTopic.String:DA-END
    }
    /**
     * getter for the field appId
     * 
     * 
     * 
     * @return
     */
    public String getAppId() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getAppId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getAppId.String:DA-ELSE
        return this.appId;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getAppId.String:DA-END
    }
    /**
     * getter for the field clientId
     * 
     * 
     * 
     * @return
     */
    public String getClientId() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getClientId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getClientId.String:DA-ELSE
        return this.clientId;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getClientId.String:DA-END
    }
    /**
     * getter for the field resourceId
     * 
     * 
     * 
     * @return
     */
    public String getResourceId() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getResourceId.String:DA-ELSE
        return this.resourceId;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getResourceId.String:DA-END
    }
    /**
     * setter for the field resourceId
     * 
     * 
     * 
     * @param resourceId  the resourceId
     */
    public void setResourceId(String resourceId) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setResourceId.String:DA-ELSE
        this.resourceId = resourceId;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setResourceId.String:DA-END
    }
    /**
     * getter for the field content
     * 
     * 
     * 
     * @return
     */
    public String getContent() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getContent.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getContent.String:DA-ELSE
        return this.content;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getContent.String:DA-END
    }
    /**
     * setter for the field content
     * 
     * 
     * 
     * @param content  the content
     */
    public void setContent(String content) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setContent.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setContent.String:DA-ELSE
        this.content = content;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setContent.String:DA-END
    }
    /**
     * getter for the field qos
     * 
     * 
     * 
     * @return
     */
    public int getQos() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getQos.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getQos.int:DA-ELSE
        return this.qos;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getQos.int:DA-END
    }
    /**
     * setter for the field qos
     * 
     * 
     * 
     * @param qos  the qos
     */
    public void setQos(int qos) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setQos.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setQos.int:DA-ELSE
        this.qos = qos;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setQos.int:DA-END
    }
    /**
     * getter for the field retained
     * 
     * 
     * 
     * @return
     */
    public boolean isRetained() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.isRetained.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.isRetained.boolean:DA-ELSE
        return this.retained;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.isRetained.boolean:DA-END
    }
    /**
     * setter for the field retained
     * 
     * 
     * 
     * @param retained  the retained
     */
    public void setRetained(boolean retained) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setRetained.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setRetained.boolean:DA-ELSE
        this.retained = retained;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setRetained.boolean:DA-END
    }
    /**
     * getter for the field duplicate
     * 
     * 
     * 
     * @return
     */
    public boolean isDuplicate() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.isDuplicate.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.isDuplicate.boolean:DA-ELSE
        return this.duplicate;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.isDuplicate.boolean:DA-END
    }
    /**
     * setter for the field duplicate
     * 
     * 
     * 
     * @param duplicate  the duplicate
     */
    public void setDuplicate(boolean duplicate) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setDuplicate.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setDuplicate.boolean:DA-ELSE
        this.duplicate = duplicate;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.setDuplicate.boolean:DA-END
    }
    /**
     * getter for the field messageArrival
     * 
     * 
     * 
     * @return
     */
    public Date getMessageArrival() {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getMessageArrival.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getMessageArrival.Date:DA-ELSE
        return this.messageArrival;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.getMessageArrival.Date:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.push.paho.DisplayMessage.additional.elements.in.type:DA-END
} // end of java type