package org.gs.iot.sample.greenhouse.gateway.function;

import retrofit2.http.Headers;
import retrofit2.http.DELETE;
import retrofit2.Call;
import javax.ws.rs.core.Response;
import retrofit2.http.Path;
import retrofit2.http.GET;
import org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean;
import retrofit2.http.Query;
import java.util.List;
import retrofit2.http.PUT;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LightEntityResourceService { // start of interface

    /**
     * 
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @DELETE(value="lightentity/{id}")
    Call<Response> delete(@Path(value="id") long id);
    /**
     * 
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="lightentity/{id}")
    Call<LightEntityBean> read(@Path(value="id") long id);
    /**
     * 
     * @param offset  the offset
     * @param limit  the limit
     * @param ids  the ids
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="lightentity")
    Call<LightEntityListBean> readList(@Query(value="offset") int offset, @Query(value="limit") int limit, @Query(value="ids") List<String> ids);
    /**
     * 
     * @param id  the id
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @PUT(value="lightentity/{id}")
    Call<LightEntityBean> update(@Path(value="id") long id, @Body LightEntityBean bean);
    /**
     * 
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @POST(value="lightentity")
    Call<LightEntityBean> create(@Body LightEntityBean bean);
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityResourceService.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityResourceService.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityResourceService.additional.elements.in.type:DA-END
} // end of java type