
import javax.persistence.EntityManager;

public class SmartGreenhouseAppPersistenceTestDataGenerator { // start of class

    /**
     * 
     * @param entityManager
     */
    public void createTestData(EntityManager entityManager) {
        //DA-START:SmartGreenhouseAppPersistenceTestDataGenerator.createTestData.EntityManager:DA-START
        //DA-ELSE:SmartGreenhouseAppPersistenceTestDataGenerator.createTestData.EntityManager:DA-ELSE
        return;
        //DA-END:SmartGreenhouseAppPersistenceTestDataGenerator.createTestData.EntityManager:DA-END
    }
    
    //DA-START:SmartGreenhouseAppPersistenceTestDataGenerator.additional.elements.in.type:DA-START
    //DA-ELSE:SmartGreenhouseAppPersistenceTestDataGenerator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:SmartGreenhouseAppPersistenceTestDataGenerator.additional.elements.in.type:DA-END
} // end of java type