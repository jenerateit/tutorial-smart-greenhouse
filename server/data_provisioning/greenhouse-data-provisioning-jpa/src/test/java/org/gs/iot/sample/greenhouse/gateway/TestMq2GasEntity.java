package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestMq2GasEntity { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testFind:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testFind:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testPersist:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testPersist:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testMerge:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testMerge:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testRemove:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testRemove:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.testRemove:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestMq2GasEntity.additional.elements.in.type:DA-END
} // end of java type