package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestLightEntityDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static LightEntityDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        dao = new LightEntityDAO();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntityDAO.additional.elements.in.type:DA-END
} // end of java type