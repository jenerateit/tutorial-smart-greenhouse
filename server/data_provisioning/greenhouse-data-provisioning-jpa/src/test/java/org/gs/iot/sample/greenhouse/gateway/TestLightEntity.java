package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestLightEntity { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testFind:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testFind:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testPersist:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testPersist:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testMerge:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testMerge:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testRemove:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testRemove:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.testRemove:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestLightEntity.additional.elements.in.type:DA-END
} // end of java type