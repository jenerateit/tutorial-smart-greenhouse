package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestWaterPumpEntity { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testFind:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testFind:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testPersist:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testPersist:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testMerge:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testMerge:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testRemove:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testRemove:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.testRemove:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestWaterPumpEntity.additional.elements.in.type:DA-END
} // end of java type