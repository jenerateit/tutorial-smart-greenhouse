package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestPlantMoistureEntityDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static PlantMoistureEntityDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        dao = new PlantMoistureEntityDAO();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMoistureEntityDAO.additional.elements.in.type:DA-END
} // end of java type