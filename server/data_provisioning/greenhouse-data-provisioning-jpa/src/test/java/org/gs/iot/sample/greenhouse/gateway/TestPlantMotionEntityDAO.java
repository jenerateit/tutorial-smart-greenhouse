package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestPlantMotionEntityDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static PlantMotionEntityDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        dao = new PlantMotionEntityDAO();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntityDAO.additional.elements.in.type:DA-END
} // end of java type