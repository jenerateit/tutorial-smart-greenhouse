package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestDisplayEntityDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static DisplayEntityDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        dao = new DisplayEntityDAO();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestDisplayEntityDAO.additional.elements.in.type:DA-END
} // end of java type