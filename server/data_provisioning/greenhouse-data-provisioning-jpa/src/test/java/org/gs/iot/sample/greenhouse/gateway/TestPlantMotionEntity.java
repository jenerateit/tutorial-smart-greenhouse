package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestPlantMotionEntity { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testFind:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testFind:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testPersist:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testPersist:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testMerge:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testMerge:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testRemove:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testRemove:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.testRemove:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TestPlantMotionEntity.additional.elements.in.type:DA-END
} // end of java type