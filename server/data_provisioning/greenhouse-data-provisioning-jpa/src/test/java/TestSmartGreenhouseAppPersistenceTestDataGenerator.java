
import org.junit.Test;

public class TestSmartGreenhouseAppPersistenceTestDataGenerator { // start of class

    /**
     */
    @Test
    public void testCreateTestData() {
        //DA-START:TestSmartGreenhouseAppPersistenceTestDataGenerator.testCreateTestData:DA-START
        //DA-ELSE:TestSmartGreenhouseAppPersistenceTestDataGenerator.testCreateTestData:DA-ELSE
        //DA-END:TestSmartGreenhouseAppPersistenceTestDataGenerator.testCreateTestData:DA-END
    }
    
    //DA-START:TestSmartGreenhouseAppPersistenceTestDataGenerator.additional.elements.in.type:DA-START
    //DA-ELSE:TestSmartGreenhouseAppPersistenceTestDataGenerator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:TestSmartGreenhouseAppPersistenceTestDataGenerator.additional.elements.in.type:DA-END
} // end of java type