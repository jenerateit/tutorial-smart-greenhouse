package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class WaterPumpEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static WaterPumpEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.get.EntityManager.Object.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.get.EntityManager.Object.WaterPumpEntity:DA-ELSE
        return entityManager.find(WaterPumpEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.get.EntityManager.Object.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<WaterPumpEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<WaterPumpEntity> cq = cb.createQuery(WaterPumpEntity.class);
        cq.from(WaterPumpEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static WaterPumpEntity create(EntityManager entityManager, WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.create.EntityManager.WaterPumpEntity.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.create.EntityManager.WaterPumpEntity.WaterPumpEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.create.EntityManager.WaterPumpEntity.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static WaterPumpEntity update(EntityManager entityManager, WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.update.EntityManager.WaterPumpEntity.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.update.EntityManager.WaterPumpEntity.WaterPumpEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.update.EntityManager.WaterPumpEntity.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.delete.EntityManager.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.delete.EntityManager.WaterPumpEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.delete.EntityManager.WaterPumpEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO.additional.elements.in.type:DA-END
} // end of java type