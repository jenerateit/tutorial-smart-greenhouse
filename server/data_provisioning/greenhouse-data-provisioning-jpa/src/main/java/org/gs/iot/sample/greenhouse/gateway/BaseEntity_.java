package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Date;
import java.sql.Timestamp;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity}.
 * 
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.BaseEntity.class)
public class BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getPk()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getPk()
     */
    public static volatile SingularAttribute<BaseEntity, Long> pk;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getAccountName()}.
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * For example, access control lists can be defined so that users
     * are only given access to the child topics of a given account_name.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getAccountName()
     */
    public static volatile SingularAttribute<BaseEntity, String> accountName;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getClientId()}.
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway?s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getClientId()
     */
    public static volatile SingularAttribute<BaseEntity, String> clientId;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getAppId()}.
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ?CONF-V1?,
     * ?CONF-V2?, etc.)
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getAppId()
     */
    public static volatile SingularAttribute<BaseEntity, String> appId;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getResourceId()}.
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
     * identify a temperature sensor and ?sensor/hum? a humidity sensor.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getResourceId()
     */
    public static volatile SingularAttribute<BaseEntity, String> resourceId;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getGeoCoordinates()}.
     * 
     * Any client that publishes messages can include geo coordinates in it.
     * Those coordinates will be stored here. Otherwise, the geo coordinates
     * are left empty.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getGeoCoordinates()
     */
    public static volatile SingularAttribute<BaseEntity, GeoCoordinates> geoCoordinates;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getMeasurementInfo()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getMeasurementInfo()
     */
    public static volatile SingularAttribute<BaseEntity, MeasurementInfo> measurementInfo;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getTimeOfMessageReception()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getTimeOfMessageReception()
     */
    public static volatile SingularAttribute<BaseEntity, Date> timeOfMessageReception;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getCreationTimestamp()}.
     * 
     * The creation timestamp gets set, when a data record is getting
     * inserted into the database for the very first time. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getCreationTimestamp()
     */
    public static volatile SingularAttribute<BaseEntity, Timestamp> creationTimestamp;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getModificationTimestamp()}.
     * 
     * The modification timestamp gets set, when a data record is getting
     * inserted into the database for the very first time _and_ whenever an update occurs. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getModificationTimestamp()
     */
    public static volatile SingularAttribute<BaseEntity, Timestamp> modificationTimestamp;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getCreatedBy()}.
     * 
     * The 'created by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting inserted into the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getCreatedBy()
     */
    public static volatile SingularAttribute<BaseEntity, String> createdBy;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.BaseEntity#getModifiedBy()}.
     * 
     * The 'modified by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting updated in the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.BaseEntity#getModifiedBy()
     */
    public static volatile SingularAttribute<BaseEntity, String> modifiedBy;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity_.additional.elements.in.type:DA-END
} // end of java type