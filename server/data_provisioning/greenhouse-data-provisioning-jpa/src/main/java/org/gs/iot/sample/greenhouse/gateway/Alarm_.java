package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.Alarm}.
 * 
 * persistent data structure for hardware of type 'BuzzerGrove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.Alarm
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.Alarm.class)
public class Alarm_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Alarm#isStatus()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Alarm#isStatus()
     */
    public static volatile SingularAttribute<Alarm, Boolean> status;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Alarm_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Alarm_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Alarm_.additional.elements.in.type:DA-END
} // end of java type