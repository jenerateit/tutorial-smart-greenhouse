package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class PlantMotionEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.preUpdate.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.preUpdate.PlantMotionEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.preUpdate.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postUpdate.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postUpdate.PlantMotionEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postUpdate.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.prePersist.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.prePersist.PlantMotionEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.prePersist.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postPersist.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postPersist.PlantMotionEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postPersist.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.preRemove.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.preRemove.PlantMotionEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.preRemove.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postRemove.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postRemove.PlantMotionEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postRemove.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postLoad.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postLoad.PlantMotionEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.postLoad.PlantMotionEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type