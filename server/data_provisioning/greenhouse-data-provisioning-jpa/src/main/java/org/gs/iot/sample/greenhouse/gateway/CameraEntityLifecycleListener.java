package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class CameraEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.preUpdate.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.preUpdate.CameraEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.preUpdate.CameraEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postUpdate.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postUpdate.CameraEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postUpdate.CameraEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.prePersist.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.prePersist.CameraEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.prePersist.CameraEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postPersist.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postPersist.CameraEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postPersist.CameraEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.preRemove.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.preRemove.CameraEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.preRemove.CameraEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postRemove.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postRemove.CameraEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postRemove.CameraEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postLoad.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postLoad.CameraEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.postLoad.CameraEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type