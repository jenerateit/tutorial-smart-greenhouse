package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.Temperature}.
 * 
 * persistent data structure for hardware of type 'TemperatureSensorGrove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.Temperature
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.Temperature.class)
public class Temperature_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Temperature#getStatus()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Temperature#getStatus()
     */
    public static volatile SingularAttribute<Temperature, Float> status;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Temperature_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Temperature_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Temperature_.additional.elements.in.type:DA-END
} // end of java type