package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Embeddable;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

/**
 * persistent data structure for hardware of type 'Mq2GasSensorGrove'
 */
@Embeddable
public class Mq2Gas implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private float rsAir;
    
    private float r0;
    
    private float rsGas;
    
    private float ratio;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field rsAir
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="rsair", unique=false, nullable=true)
    public float getRsAir() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRsAir.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRsAir.float:DA-ELSE
        return this.rsAir;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRsAir.float:DA-END
    }
    /**
     * setter for the field rsAir
     * 
     * 
     * 
     * @param rsAir  the rsAir
     */
    public void setRsAir(float rsAir) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRsAir.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRsAir.float:DA-ELSE
        this.rsAir = rsAir;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRsAir.float:DA-END
    }
    /**
     * getter for the field r0
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="r0", unique=false, nullable=true)
    public float getr0() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getr0.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getr0.float:DA-ELSE
        return this.r0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getr0.float:DA-END
    }
    /**
     * setter for the field r0
     * 
     * 
     * 
     * @param r0  the r0
     */
    public void setr0(float r0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setr0.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setr0.float:DA-ELSE
        this.r0 = r0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setr0.float:DA-END
    }
    /**
     * getter for the field rsGas
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="rsgas", unique=false, nullable=true)
    public float getRsGas() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRsGas.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRsGas.float:DA-ELSE
        return this.rsGas;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRsGas.float:DA-END
    }
    /**
     * setter for the field rsGas
     * 
     * 
     * 
     * @param rsGas  the rsGas
     */
    public void setRsGas(float rsGas) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRsGas.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRsGas.float:DA-ELSE
        this.rsGas = rsGas;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRsGas.float:DA-END
    }
    /**
     * getter for the field ratio
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="ratio", unique=false, nullable=true)
    public float getRatio() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRatio.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRatio.float:DA-ELSE
        return this.ratio;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.getRatio.float:DA-END
    }
    /**
     * setter for the field ratio
     * 
     * 
     * 
     * @param ratio  the ratio
     */
    public void setRatio(float ratio) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRatio.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRatio.float:DA-ELSE
        this.ratio = ratio;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.setRatio.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas.additional.elements.in.type:DA-END
} // end of java type