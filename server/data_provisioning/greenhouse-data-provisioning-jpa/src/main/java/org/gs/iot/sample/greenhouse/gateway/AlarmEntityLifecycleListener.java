package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class AlarmEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.preUpdate.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.preUpdate.AlarmEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.preUpdate.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postUpdate.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postUpdate.AlarmEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postUpdate.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.prePersist.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.prePersist.AlarmEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.prePersist.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postPersist.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postPersist.AlarmEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postPersist.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.preRemove.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.preRemove.AlarmEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.preRemove.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postRemove.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postRemove.AlarmEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postRemove.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postLoad.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postLoad.AlarmEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.postLoad.AlarmEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type