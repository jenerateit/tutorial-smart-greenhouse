package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class TemperatureEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.preUpdate.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.preUpdate.TemperatureEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.preUpdate.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postUpdate.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postUpdate.TemperatureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postUpdate.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.prePersist.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.prePersist.TemperatureEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.prePersist.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postPersist.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postPersist.TemperatureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postPersist.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.preRemove.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.preRemove.TemperatureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.preRemove.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postRemove.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postRemove.TemperatureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postRemove.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postLoad.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postLoad.TemperatureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.postLoad.TemperatureEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type