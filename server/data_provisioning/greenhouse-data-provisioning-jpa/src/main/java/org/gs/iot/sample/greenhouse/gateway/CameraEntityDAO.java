package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class CameraEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static CameraEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.get.EntityManager.Object.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.get.EntityManager.Object.CameraEntity:DA-ELSE
        return entityManager.find(CameraEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.get.EntityManager.Object.CameraEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<CameraEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<CameraEntity> cq = cb.createQuery(CameraEntity.class);
        cq.from(CameraEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static CameraEntity create(EntityManager entityManager, CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.create.EntityManager.CameraEntity.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.create.EntityManager.CameraEntity.CameraEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.create.EntityManager.CameraEntity.CameraEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static CameraEntity update(EntityManager entityManager, CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.update.EntityManager.CameraEntity.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.update.EntityManager.CameraEntity.CameraEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.update.EntityManager.CameraEntity.CameraEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, CameraEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.delete.EntityManager.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.delete.EntityManager.CameraEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.delete.EntityManager.CameraEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO.additional.elements.in.type:DA-END
} // end of java type