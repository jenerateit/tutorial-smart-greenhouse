package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class PlantIlluminanceEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static PlantIlluminanceEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.get.EntityManager.Object.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.get.EntityManager.Object.PlantIlluminanceEntity:DA-ELSE
        return entityManager.find(PlantIlluminanceEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.get.EntityManager.Object.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<PlantIlluminanceEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<PlantIlluminanceEntity> cq = cb.createQuery(PlantIlluminanceEntity.class);
        cq.from(PlantIlluminanceEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantIlluminanceEntity create(EntityManager entityManager, PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.create.EntityManager.PlantIlluminanceEntity.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.create.EntityManager.PlantIlluminanceEntity.PlantIlluminanceEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.create.EntityManager.PlantIlluminanceEntity.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantIlluminanceEntity update(EntityManager entityManager, PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.update.EntityManager.PlantIlluminanceEntity.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.update.EntityManager.PlantIlluminanceEntity.PlantIlluminanceEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.update.EntityManager.PlantIlluminanceEntity.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.delete.EntityManager.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.delete.EntityManager.PlantIlluminanceEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.delete.EntityManager.PlantIlluminanceEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO.additional.elements.in.type:DA-END
} // end of java type