package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class PlantIlluminanceEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.preUpdate.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.preUpdate.PlantIlluminanceEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.preUpdate.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postUpdate.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postUpdate.PlantIlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postUpdate.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.prePersist.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.prePersist.PlantIlluminanceEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.prePersist.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postPersist.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postPersist.PlantIlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postPersist.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.preRemove.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.preRemove.PlantIlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.preRemove.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postRemove.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postRemove.PlantIlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postRemove.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(PlantIlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postLoad.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postLoad.PlantIlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.postLoad.PlantIlluminanceEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type