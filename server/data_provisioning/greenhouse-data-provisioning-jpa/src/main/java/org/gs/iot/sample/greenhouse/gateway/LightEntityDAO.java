package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class LightEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static LightEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.get.EntityManager.Object.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.get.EntityManager.Object.LightEntity:DA-ELSE
        return entityManager.find(LightEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.get.EntityManager.Object.LightEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<LightEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<LightEntity> cq = cb.createQuery(LightEntity.class);
        cq.from(LightEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static LightEntity create(EntityManager entityManager, LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.create.EntityManager.LightEntity.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.create.EntityManager.LightEntity.LightEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.create.EntityManager.LightEntity.LightEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static LightEntity update(EntityManager entityManager, LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.update.EntityManager.LightEntity.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.update.EntityManager.LightEntity.LightEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.update.EntityManager.LightEntity.LightEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.delete.EntityManager.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.delete.EntityManager.LightEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.delete.EntityManager.LightEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityDAO.additional.elements.in.type:DA-END
} // end of java type