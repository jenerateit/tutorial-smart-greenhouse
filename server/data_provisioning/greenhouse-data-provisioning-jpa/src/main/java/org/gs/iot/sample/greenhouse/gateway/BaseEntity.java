package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.MappedSuperclass;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.util.Date;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Basic;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.persistence.Embedded;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
//DA-START:gateway.BaseEntity:DA-START
//DA-ELSE:gateway.BaseEntity:DA-ELSE
@MappedSuperclass
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.class})
//DA-END:gateway.BaseEntity:DA-END

public class BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Long pk;
    
    /**
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * For example, access control lists can be defined so that users
     * are only given access to the child topics of a given account_name.
     */
    private String accountName;
    
    /**
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway?s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     */
    private String clientId;
    
    /**
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ?CONF-V1?,
     * ?CONF-V2?, etc.)
     */
    private String appId;
    
    /**
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
     * identify a temperature sensor and ?sensor/hum? a humidity sensor.
     */
    private String resourceId;
    
    /**
     * Any client that publishes messages can include geo coordinates in it.
     * Those coordinates will be stored here. Otherwise, the geo coordinates
     * are left empty.
     */
    private GeoCoordinates geoCoordinates;
    
    private MeasurementInfo measurementInfo;
    
    private Date timeOfMessageReception;
    
    /**
     * The creation timestamp gets set, when a data record is getting
     * inserted into the database for the very first time. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     */
    private Timestamp creationTimestamp;
    
    /**
     * The modification timestamp gets set, when a data record is getting
     * inserted into the database for the very first time _and_ whenever an update occurs. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     */
    private Timestamp modificationTimestamp;
    
    /**
     * The 'created by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting inserted into the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     */
    private String createdBy;
    
    /**
     * The 'modified by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting updated in the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     */
    private String modifiedBy;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field creationTimestamp
     * 
     * 
     * The creation timestamp gets set, when a data record is getting
     * inserted into the database for the very first time. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreationTimestamp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreationTimestamp.annotations:DA-ELSE
    @Column(name="creationTimestamp", unique=false, nullable=false, insertable=true, updatable=false, length=10)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreationTimestamp.annotations:DA-END
    public Timestamp getCreationTimestamp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreationTimestamp.Timestamp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreationTimestamp.Timestamp:DA-ELSE
        return this.creationTimestamp;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreationTimestamp.Timestamp:DA-END
    }
    /**
     * setter for the field creationTimestamp
     * 
     * 
     * The creation timestamp gets set, when a data record is getting
     * inserted into the database for the very first time. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     * 
     * 
     * @param creationTimestamp  the creationTimestamp
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreationTimestamp.Timestamp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreationTimestamp.Timestamp.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreationTimestamp.Timestamp.annotations:DA-END
    public void setCreationTimestamp(Timestamp creationTimestamp) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreationTimestamp.Timestamp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreationTimestamp.Timestamp:DA-ELSE
        this.creationTimestamp = creationTimestamp;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreationTimestamp.Timestamp:DA-END
    }
    /**
     * getter for the field modificationTimestamp
     * 
     * 
     * The modification timestamp gets set, when a data record is getting
     * inserted into the database for the very first time _and_ whenever an update occurs. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModificationTimestamp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModificationTimestamp.annotations:DA-ELSE
    @Column(name="modificationTimestamp", unique=false, nullable=true, insertable=true, updatable=true, length=10)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModificationTimestamp.annotations:DA-END
    public Timestamp getModificationTimestamp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModificationTimestamp.Timestamp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModificationTimestamp.Timestamp:DA-ELSE
        return this.modificationTimestamp;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModificationTimestamp.Timestamp:DA-END
    }
    /**
     * setter for the field modificationTimestamp
     * 
     * 
     * The modification timestamp gets set, when a data record is getting
     * inserted into the database for the very first time _and_ whenever an update occurs. Entity lifecycle
     * listeners are taking care of actually setting the timestamp.
     * 
     * 
     * @param modificationTimestamp  the modificationTimestamp
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModificationTimestamp.Timestamp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModificationTimestamp.Timestamp.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModificationTimestamp.Timestamp.annotations:DA-END
    public void setModificationTimestamp(Timestamp modificationTimestamp) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModificationTimestamp.Timestamp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModificationTimestamp.Timestamp:DA-ELSE
        this.modificationTimestamp = modificationTimestamp;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModificationTimestamp.Timestamp:DA-END
    }
    /**
     * getter for the field createdBy
     * 
     * 
     * The 'created by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting inserted into the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreatedBy.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreatedBy.annotations:DA-ELSE
    @Column(name="createdBy", unique=false, nullable=false, insertable=true, updatable=false, length=128)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreatedBy.annotations:DA-END
    public String getCreatedBy() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreatedBy.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreatedBy.String:DA-ELSE
        return this.createdBy;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getCreatedBy.String:DA-END
    }
    /**
     * setter for the field createdBy
     * 
     * 
     * The 'created by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting inserted into the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     * 
     * 
     * @param createdBy  the createdBy
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreatedBy.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreatedBy.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreatedBy.String.annotations:DA-END
    public void setCreatedBy(String createdBy) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreatedBy.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreatedBy.String:DA-ELSE
        this.createdBy = createdBy;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setCreatedBy.String:DA-END
    }
    /**
     * getter for the field modifiedBy
     * 
     * 
     * The 'modified by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting updated in the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModifiedBy.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModifiedBy.annotations:DA-ELSE
    @Column(name="modifiedBy", unique=false, nullable=true, insertable=true, updatable=true, length=128)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModifiedBy.annotations:DA-END
    public String getModifiedBy() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModifiedBy.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModifiedBy.String:DA-ELSE
        return this.modifiedBy;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getModifiedBy.String:DA-END
    }
    /**
     * setter for the field modifiedBy
     * 
     * 
     * The 'modified by' field gets set, when it is possible to get an identifier
     * for a user (user-id, user-name) while an entity lifecycle listener
     * is getting executed. From there the appropriate field in the entity can be set
     * whenever a data record is getting updated in the database.
     * 
     * To get the user-id, the EJBContext is going to be used, which is obtained by a JNDI lookup.
     * If that is not available or does not work, user-id "GUEST"
     * is going to be used (or -1 if the appropriate field in the entity is numeric).
     * 
     * 
     * @param modifiedBy  the modifiedBy
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModifiedBy.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModifiedBy.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModifiedBy.String.annotations:DA-END
    public void setModifiedBy(String modifiedBy) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModifiedBy.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModifiedBy.String:DA-ELSE
        this.modifiedBy = modifiedBy;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setModifiedBy.String:DA-END
    }
    /**
     * getter for the field pk
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getPk.annotations:DA-ELSE
    @Basic
    @Id
    @Column(name="pk", nullable=false)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getPk.Long:DA-ELSE
        return this.pk;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getPk.Long:DA-END
    }
    /**
     * setter for the field pk
     * 
     * 
     * 
     * @param pk  the pk
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setPk.Long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setPk.Long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setPk.Long.annotations:DA-END
    public void setPk(Long pk) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setPk.Long:DA-ELSE
        this.pk = pk;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setPk.Long:DA-END
    }
    /**
     * getter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * For example, access control lists can be defined so that users
     * are only given access to the child topics of a given account_name.
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAccountName.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAccountName.annotations:DA-ELSE
    @Basic
    @Column(name="accountname", unique=false, nullable=true)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAccountName.annotations:DA-END
    public String getAccountName() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAccountName.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAccountName.String:DA-ELSE
        return this.accountName;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAccountName.String:DA-END
    }
    /**
     * setter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * For example, access control lists can be defined so that users
     * are only given access to the child topics of a given account_name.
     * 
     * 
     * @param accountName  the accountName
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAccountName.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAccountName.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAccountName.String.annotations:DA-END
    public void setAccountName(String accountName) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAccountName.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAccountName.String:DA-ELSE
        this.accountName = accountName;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAccountName.String:DA-END
    }
    /**
     * getter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway?s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getClientId.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getClientId.annotations:DA-ELSE
    @Column(name="clientid", unique=false, nullable=false)
    @NotNull
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getClientId.annotations:DA-END
    public String getClientId() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getClientId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getClientId.String:DA-ELSE
        return this.clientId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getClientId.String:DA-END
    }
    /**
     * setter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway?s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @param clientId  the clientId
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setClientId.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setClientId.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setClientId.String.annotations:DA-END
    public void setClientId(String clientId) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setClientId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setClientId.String:DA-ELSE
        this.clientId = clientId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setClientId.String:DA-END
    }
    /**
     * getter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ?CONF-V1?,
     * ?CONF-V2?, etc.)
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAppId.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAppId.annotations:DA-ELSE
    @Column(name="appid", unique=false, nullable=false)
    @NotNull
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAppId.annotations:DA-END
    public String getAppId() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAppId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAppId.String:DA-ELSE
        return this.appId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getAppId.String:DA-END
    }
    /**
     * setter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ?CONF-V1?,
     * ?CONF-V2?, etc.)
     * 
     * 
     * @param appId  the appId
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAppId.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAppId.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAppId.String.annotations:DA-END
    public void setAppId(String appId) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAppId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAppId.String:DA-ELSE
        this.appId = appId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setAppId.String:DA-END
    }
    /**
     * getter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
     * identify a temperature sensor and ?sensor/hum? a humidity sensor.
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getResourceId.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getResourceId.annotations:DA-ELSE
    @Column(name="resourceid", unique=false, nullable=false)
    @NotNull
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getResourceId.annotations:DA-END
    public String getResourceId() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getResourceId.String:DA-ELSE
        return this.resourceId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getResourceId.String:DA-END
    }
    /**
     * setter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
     * identify a temperature sensor and ?sensor/hum? a humidity sensor.
     * 
     * 
     * @param resourceId  the resourceId
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setResourceId.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setResourceId.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setResourceId.String.annotations:DA-END
    public void setResourceId(String resourceId) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setResourceId.String:DA-ELSE
        this.resourceId = resourceId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setResourceId.String:DA-END
    }
    /**
     * getter for the field geoCoordinates
     * 
     * 
     * Any client that publishes messages can include geo coordinates in it.
     * Those coordinates will be stored here. Otherwise, the geo coordinates
     * are left empty.
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getGeoCoordinates.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getGeoCoordinates.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getGeoCoordinates.annotations:DA-END
    public GeoCoordinates getGeoCoordinates() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getGeoCoordinates.GeoCoordinates:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getGeoCoordinates.GeoCoordinates:DA-ELSE
        return this.geoCoordinates;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getGeoCoordinates.GeoCoordinates:DA-END
    }
    /**
     * setter for the field geoCoordinates
     * 
     * 
     * Any client that publishes messages can include geo coordinates in it.
     * Those coordinates will be stored here. Otherwise, the geo coordinates
     * are left empty.
     * 
     * 
     * @param geoCoordinates  the geoCoordinates
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setGeoCoordinates.GeoCoordinates.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setGeoCoordinates.GeoCoordinates.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setGeoCoordinates.GeoCoordinates.annotations:DA-END
    public void setGeoCoordinates(GeoCoordinates geoCoordinates) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setGeoCoordinates.GeoCoordinates:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setGeoCoordinates.GeoCoordinates:DA-ELSE
        this.geoCoordinates = geoCoordinates;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setGeoCoordinates.GeoCoordinates:DA-END
    }
    /**
     * getter for the field measurementInfo
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getMeasurementInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getMeasurementInfo.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getMeasurementInfo.annotations:DA-END
    public MeasurementInfo getMeasurementInfo() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getMeasurementInfo.MeasurementInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getMeasurementInfo.MeasurementInfo:DA-ELSE
        return this.measurementInfo;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getMeasurementInfo.MeasurementInfo:DA-END
    }
    /**
     * setter for the field measurementInfo
     * 
     * 
     * 
     * @param measurementInfo  the measurementInfo
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setMeasurementInfo.MeasurementInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setMeasurementInfo.MeasurementInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setMeasurementInfo.MeasurementInfo.annotations:DA-END
    public void setMeasurementInfo(MeasurementInfo measurementInfo) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setMeasurementInfo.MeasurementInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setMeasurementInfo.MeasurementInfo:DA-ELSE
        this.measurementInfo = measurementInfo;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setMeasurementInfo.MeasurementInfo:DA-END
    }
    /**
     * getter for the field timeOfMessageReception
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getTimeOfMessageReception.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getTimeOfMessageReception.annotations:DA-ELSE
    @Basic
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="timeofmessagereception", unique=false, nullable=true)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getTimeOfMessageReception.annotations:DA-END
    public Date getTimeOfMessageReception() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getTimeOfMessageReception.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getTimeOfMessageReception.Date:DA-ELSE
        return this.timeOfMessageReception;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.getTimeOfMessageReception.Date:DA-END
    }
    /**
     * setter for the field timeOfMessageReception
     * 
     * 
     * 
     * @param timeOfMessageReception  the timeOfMessageReception
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setTimeOfMessageReception.Date.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setTimeOfMessageReception.Date.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setTimeOfMessageReception.Date.annotations:DA-END
    public void setTimeOfMessageReception(Date timeOfMessageReception) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setTimeOfMessageReception.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setTimeOfMessageReception.Date:DA-ELSE
        this.timeOfMessageReception = timeOfMessageReception;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.setTimeOfMessageReception.Date:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntity.additional.elements.in.type:DA-END
} // end of java type