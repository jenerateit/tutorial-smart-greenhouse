package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class PlantMoistureEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.preUpdate.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.preUpdate.PlantMoistureEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.preUpdate.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postUpdate.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postUpdate.PlantMoistureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postUpdate.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.prePersist.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.prePersist.PlantMoistureEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.prePersist.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postPersist.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postPersist.PlantMoistureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postPersist.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.preRemove.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.preRemove.PlantMoistureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.preRemove.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postRemove.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postRemove.PlantMoistureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postRemove.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postLoad.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postLoad.PlantMoistureEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.postLoad.PlantMoistureEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type