package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class DisplayEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.preUpdate.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.preUpdate.DisplayEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.preUpdate.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postUpdate.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postUpdate.DisplayEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postUpdate.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.prePersist.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.prePersist.DisplayEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.prePersist.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postPersist.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postPersist.DisplayEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postPersist.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.preRemove.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.preRemove.DisplayEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.preRemove.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postRemove.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postRemove.DisplayEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postRemove.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postLoad.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postLoad.DisplayEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.postLoad.DisplayEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type