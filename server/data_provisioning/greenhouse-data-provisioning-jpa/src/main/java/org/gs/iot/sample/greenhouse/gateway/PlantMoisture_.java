package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.PlantMoisture}.
 * 
 * persistent data structure for hardware of type 'MoistureSensorGrove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.PlantMoisture
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.PlantMoisture.class)
public class PlantMoisture_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantMoisture#getStatus()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantMoisture#getStatus()
     */
    public static volatile SingularAttribute<PlantMoisture, Float> status;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoisture_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoisture_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoisture_.additional.elements.in.type:DA-END
} // end of java type