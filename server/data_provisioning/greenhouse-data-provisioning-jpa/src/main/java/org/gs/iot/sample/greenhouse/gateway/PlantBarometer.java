package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Embeddable;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

/**
 * persistent data structure for hardware of type 'BarometerBMP180Grove'
 */
@Embeddable
public class PlantBarometer implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private float pressure;
    
    private float temperature;
    
    private float altitude;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field pressure
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="pressure", unique=false, nullable=true)
    public float getPressure() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getPressure.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getPressure.float:DA-ELSE
        return this.pressure;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getPressure.float:DA-END
    }
    /**
     * setter for the field pressure
     * 
     * 
     * 
     * @param pressure  the pressure
     */
    public void setPressure(float pressure) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setPressure.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setPressure.float:DA-ELSE
        this.pressure = pressure;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setPressure.float:DA-END
    }
    /**
     * getter for the field temperature
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="temperature", unique=false, nullable=true)
    public float getTemperature() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getTemperature.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getTemperature.float:DA-ELSE
        return this.temperature;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getTemperature.float:DA-END
    }
    /**
     * setter for the field temperature
     * 
     * 
     * 
     * @param temperature  the temperature
     */
    public void setTemperature(float temperature) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setTemperature.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setTemperature.float:DA-ELSE
        this.temperature = temperature;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setTemperature.float:DA-END
    }
    /**
     * getter for the field altitude
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="altitude", unique=false, nullable=true)
    public float getAltitude() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getAltitude.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getAltitude.float:DA-ELSE
        return this.altitude;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.getAltitude.float:DA-END
    }
    /**
     * setter for the field altitude
     * 
     * 
     * 
     * @param altitude  the altitude
     */
    public void setAltitude(float altitude) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setAltitude.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setAltitude.float:DA-ELSE
        this.altitude = altitude;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.setAltitude.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer.additional.elements.in.type:DA-END
} // end of java type