package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity}.
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity.class)
public class PlantMotionEntity_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity#getData()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity#getData()
     */
    public static volatile SingularAttribute<PlantMotionEntity, PlantMotion> data;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity_.additional.elements.in.type:DA-END
} // end of java type