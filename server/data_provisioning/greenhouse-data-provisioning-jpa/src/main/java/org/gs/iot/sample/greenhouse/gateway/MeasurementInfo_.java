package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Date;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.MeasurementInfo}.
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.MeasurementInfo
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.MeasurementInfo.class)
public class MeasurementInfo_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.MeasurementInfo#getTimeOfMeasurement()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.MeasurementInfo#getTimeOfMeasurement()
     */
    public static volatile SingularAttribute<MeasurementInfo, Date> timeOfMeasurement;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.MeasurementInfo#getTimeZoneOfMeasurement()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.MeasurementInfo#getTimeZoneOfMeasurement()
     */
    public static volatile SingularAttribute<MeasurementInfo, String> timeZoneOfMeasurement;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.MeasurementInfo_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.MeasurementInfo_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.MeasurementInfo_.additional.elements.in.type:DA-END
} // end of java type