package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Embeddable;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

/**
 * persistent data structure for hardware of type 'LightStrip'
 */
@Embeddable
public class Light implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private short r;
    
    private short g;
    
    private short b;
    
    private double brightness;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field r
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="r", unique=false, nullable=true)
    public short getR() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.getR.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.getR.short:DA-ELSE
        return this.r;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.getR.short:DA-END
    }
    /**
     * setter for the field r
     * 
     * 
     * 
     * @param r  the r
     */
    public void setR(short r) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.setR.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.setR.short:DA-ELSE
        this.r = r;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.setR.short:DA-END
    }
    /**
     * getter for the field g
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="g", unique=false, nullable=true)
    public short getG() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.getG.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.getG.short:DA-ELSE
        return this.g;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.getG.short:DA-END
    }
    /**
     * setter for the field g
     * 
     * 
     * 
     * @param g  the g
     */
    public void setG(short g) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.setG.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.setG.short:DA-ELSE
        this.g = g;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.setG.short:DA-END
    }
    /**
     * getter for the field b
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="b", unique=false, nullable=true)
    public short getB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.getB.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.getB.short:DA-ELSE
        return this.b;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.getB.short:DA-END
    }
    /**
     * setter for the field b
     * 
     * 
     * 
     * @param b  the b
     */
    public void setB(short b) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.setB.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.setB.short:DA-ELSE
        this.b = b;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.setB.short:DA-END
    }
    /**
     * getter for the field brightness
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="brightness", unique=false, nullable=true)
    public double getBrightness() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.getBrightness.double:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.getBrightness.double:DA-ELSE
        return this.brightness;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.getBrightness.double:DA-END
    }
    /**
     * setter for the field brightness
     * 
     * 
     * 
     * @param brightness  the brightness
     */
    public void setBrightness(double brightness) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.setBrightness.double:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.setBrightness.double:DA-ELSE
        this.brightness = brightness;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.setBrightness.double:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Light.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Light.additional.elements.in.type:DA-END
} // end of java type