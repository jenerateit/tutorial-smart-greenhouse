package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class TemperatureEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static TemperatureEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.get.EntityManager.Object.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.get.EntityManager.Object.TemperatureEntity:DA-ELSE
        return entityManager.find(TemperatureEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.get.EntityManager.Object.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<TemperatureEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<TemperatureEntity> cq = cb.createQuery(TemperatureEntity.class);
        cq.from(TemperatureEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static TemperatureEntity create(EntityManager entityManager, TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.create.EntityManager.TemperatureEntity.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.create.EntityManager.TemperatureEntity.TemperatureEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.create.EntityManager.TemperatureEntity.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static TemperatureEntity update(EntityManager entityManager, TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.update.EntityManager.TemperatureEntity.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.update.EntityManager.TemperatureEntity.TemperatureEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.update.EntityManager.TemperatureEntity.TemperatureEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, TemperatureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.delete.EntityManager.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.delete.EntityManager.TemperatureEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.delete.EntityManager.TemperatureEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntityDAO.additional.elements.in.type:DA-END
} // end of java type