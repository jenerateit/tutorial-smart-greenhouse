package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class AlarmEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static AlarmEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.get.EntityManager.Object.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.get.EntityManager.Object.AlarmEntity:DA-ELSE
        return entityManager.find(AlarmEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.get.EntityManager.Object.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<AlarmEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<AlarmEntity> cq = cb.createQuery(AlarmEntity.class);
        cq.from(AlarmEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static AlarmEntity create(EntityManager entityManager, AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.create.EntityManager.AlarmEntity.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.create.EntityManager.AlarmEntity.AlarmEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.create.EntityManager.AlarmEntity.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static AlarmEntity update(EntityManager entityManager, AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.update.EntityManager.AlarmEntity.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.update.EntityManager.AlarmEntity.AlarmEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.update.EntityManager.AlarmEntity.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, AlarmEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.delete.EntityManager.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.delete.EntityManager.AlarmEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.delete.EntityManager.AlarmEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO.additional.elements.in.type:DA-END
} // end of java type