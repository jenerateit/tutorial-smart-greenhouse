package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.TemperatureEntity}.
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.TemperatureEntity
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.TemperatureEntity.class)
public class TemperatureEntity_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.TemperatureEntity#getData()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.TemperatureEntity#getData()
     */
    public static volatile SingularAttribute<TemperatureEntity, Temperature> data;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.TemperatureEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.TemperatureEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.TemperatureEntity_.additional.elements.in.type:DA-END
} // end of java type