package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.AlarmEntity:DA-START
//DA-ELSE:gateway.AlarmEntity:DA-ELSE
@Entity
@Table(name="alarmentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.AlarmEntityLifecycleListener.class})
//DA-END:gateway.AlarmEntity:DA-END

public class AlarmEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Alarm data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="alarmentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="alarmentity_seq", sequenceName="alarmentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getData.annotations:DA-END
    public Alarm getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getData.Alarm:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getData.Alarm:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.getData.Alarm:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.setData.Alarm.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.setData.Alarm.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.setData.Alarm.annotations:DA-END
    public void setData(Alarm data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.setData.Alarm:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.setData.Alarm:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.setData.Alarm:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity.additional.elements.in.type:DA-END
} // end of java type