package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Embeddable;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
/*
 * Imports from last generation
 */
import javax.persistence.Enumerated;


/**
 * persistent data structure for hardware of type 'AdafruitI2cRgbLcd'
 */
@Embeddable
public class Display implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private int backlight;
    
    private String row0;
    
    private String row1;
    
    private AdafruitI2cRgbLcdButtonsEnum button;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field backlight
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="backlight", unique=false, nullable=true)
    public int getBacklight() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.getBacklight.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.getBacklight.int:DA-ELSE
        return this.backlight;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.getBacklight.int:DA-END
    }
    /**
     * setter for the field backlight
     * 
     * 
     * 
     * @param backlight  the backlight
     */
    public void setBacklight(int backlight) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.setBacklight.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.setBacklight.int:DA-ELSE
        this.backlight = backlight;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.setBacklight.int:DA-END
    }
    /**
     * getter for the field row0
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="row0", unique=false, nullable=true)
    public String getRow0() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.getRow0.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.getRow0.String:DA-ELSE
        return this.row0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.getRow0.String:DA-END
    }
    /**
     * setter for the field row0
     * 
     * 
     * 
     * @param row0  the row0
     */
    public void setRow0(String row0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.setRow0.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.setRow0.String:DA-ELSE
        this.row0 = row0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.setRow0.String:DA-END
    }
    /**
     * getter for the field row1
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="row1", unique=false, nullable=true)
    public String getRow1() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.getRow1.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.getRow1.String:DA-ELSE
        return this.row1;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.getRow1.String:DA-END
    }
    /**
     * setter for the field row1
     * 
     * 
     * 
     * @param row1  the row1
     */
    public void setRow1(String row1) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.setRow1.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.setRow1.String:DA-ELSE
        this.row1 = row1;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.setRow1.String:DA-END
    }
    /**
     * getter for the field button
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="button", unique=false, nullable=true)
    public AdafruitI2cRgbLcdButtonsEnum getButton() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-ELSE
        return this.button;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-END
    }
    /**
     * setter for the field button
     * 
     * 
     * 
     * @param button  the button
     */
    public void setButton(AdafruitI2cRgbLcdButtonsEnum button) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-ELSE
        this.button = button;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Display.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Display.additional.elements.in.type:DA-END
} // end of java type