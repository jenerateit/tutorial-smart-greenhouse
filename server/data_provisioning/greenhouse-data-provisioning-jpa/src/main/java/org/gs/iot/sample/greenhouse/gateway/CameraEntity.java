package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.CameraEntity:DA-START
//DA-ELSE:gateway.CameraEntity:DA-ELSE
@Entity
@Table(name="cameraentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.CameraEntityLifecycleListener.class})
//DA-END:gateway.CameraEntity:DA-END

public class CameraEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Camera data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="cameraentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="cameraentity_seq", sequenceName="cameraentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getData.annotations:DA-END
    public Camera getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getData.Camera:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getData.Camera:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.getData.Camera:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.setData.Camera.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.setData.Camera.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.setData.Camera.annotations:DA-END
    public void setData(Camera data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.setData.Camera:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.setData.Camera:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.setData.Camera:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.CameraEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.CameraEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.CameraEntity.additional.elements.in.type:DA-END
} // end of java type