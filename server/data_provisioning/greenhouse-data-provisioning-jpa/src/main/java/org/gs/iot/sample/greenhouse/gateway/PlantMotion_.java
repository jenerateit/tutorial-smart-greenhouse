package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.PlantMotion}.
 * 
 * persistent data structure for hardware of type 'PIRMotionGrove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.PlantMotion
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.PlantMotion.class)
public class PlantMotion_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantMotion#isStatus()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantMotion#isStatus()
     */
    public static volatile SingularAttribute<PlantMotion, Boolean> status;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotion_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotion_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotion_.additional.elements.in.type:DA-END
} // end of java type