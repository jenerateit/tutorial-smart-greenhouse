package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.LightEntity:DA-START
//DA-ELSE:gateway.LightEntity:DA-ELSE
@Entity
@Table(name="lightentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.class})
//DA-END:gateway.LightEntity:DA-END

public class LightEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Light data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="lightentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="lightentity_seq", sequenceName="lightentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.getData.annotations:DA-END
    public Light getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.getData.Light:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.getData.Light:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.getData.Light:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.setData.Light.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.setData.Light.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.setData.Light.annotations:DA-END
    public void setData(Light data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.setData.Light:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.setData.Light:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.setData.Light:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntity.additional.elements.in.type:DA-END
} // end of java type