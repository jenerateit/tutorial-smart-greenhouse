package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class LightEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.preUpdate.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.preUpdate.LightEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.preUpdate.LightEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postUpdate.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postUpdate.LightEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postUpdate.LightEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.prePersist.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.prePersist.LightEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.prePersist.LightEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postPersist.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postPersist.LightEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postPersist.LightEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.preRemove.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.preRemove.LightEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.preRemove.LightEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postRemove.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postRemove.LightEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postRemove.LightEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(LightEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postLoad.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postLoad.LightEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.postLoad.LightEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.LightEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type