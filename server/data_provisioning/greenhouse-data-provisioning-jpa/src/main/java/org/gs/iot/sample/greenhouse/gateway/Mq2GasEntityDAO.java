package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class Mq2GasEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static Mq2GasEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.get.EntityManager.Object.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.get.EntityManager.Object.Mq2GasEntity:DA-ELSE
        return entityManager.find(Mq2GasEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.get.EntityManager.Object.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<Mq2GasEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<Mq2GasEntity> cq = cb.createQuery(Mq2GasEntity.class);
        cq.from(Mq2GasEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static Mq2GasEntity create(EntityManager entityManager, Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.create.EntityManager.Mq2GasEntity.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.create.EntityManager.Mq2GasEntity.Mq2GasEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.create.EntityManager.Mq2GasEntity.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static Mq2GasEntity update(EntityManager entityManager, Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.update.EntityManager.Mq2GasEntity.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.update.EntityManager.Mq2GasEntity.Mq2GasEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.update.EntityManager.Mq2GasEntity.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.delete.EntityManager.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.delete.EntityManager.Mq2GasEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.delete.EntityManager.Mq2GasEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO.additional.elements.in.type:DA-END
} // end of java type