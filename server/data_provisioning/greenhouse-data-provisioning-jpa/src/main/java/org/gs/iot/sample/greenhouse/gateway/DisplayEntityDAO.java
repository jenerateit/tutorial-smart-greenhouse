package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class DisplayEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static DisplayEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.get.EntityManager.Object.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.get.EntityManager.Object.DisplayEntity:DA-ELSE
        return entityManager.find(DisplayEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.get.EntityManager.Object.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<DisplayEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<DisplayEntity> cq = cb.createQuery(DisplayEntity.class);
        cq.from(DisplayEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static DisplayEntity create(EntityManager entityManager, DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.create.EntityManager.DisplayEntity.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.create.EntityManager.DisplayEntity.DisplayEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.create.EntityManager.DisplayEntity.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static DisplayEntity update(EntityManager entityManager, DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.update.EntityManager.DisplayEntity.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.update.EntityManager.DisplayEntity.DisplayEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.update.EntityManager.DisplayEntity.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, DisplayEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.delete.EntityManager.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.delete.EntityManager.DisplayEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.delete.EntityManager.DisplayEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO.additional.elements.in.type:DA-END
} // end of java type