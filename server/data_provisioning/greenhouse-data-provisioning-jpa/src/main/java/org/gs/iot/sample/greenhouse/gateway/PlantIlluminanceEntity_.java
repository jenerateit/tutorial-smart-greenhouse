package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity}.
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.class)
public class PlantIlluminanceEntity_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity#getData()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity#getData()
     */
    public static volatile SingularAttribute<PlantIlluminanceEntity, PlantIlluminance> data;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity_.additional.elements.in.type:DA-END
} // end of java type