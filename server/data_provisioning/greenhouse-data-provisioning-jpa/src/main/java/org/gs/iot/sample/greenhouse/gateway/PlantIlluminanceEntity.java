package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.PlantIlluminanceEntity:DA-START
//DA-ELSE:gateway.PlantIlluminanceEntity:DA-ELSE
@Entity
@Table(name="plantilluminanceentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityLifecycleListener.class})
//DA-END:gateway.PlantIlluminanceEntity:DA-END

public class PlantIlluminanceEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private PlantIlluminance data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="plantilluminanceentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="plantilluminanceentity_seq", sequenceName="plantilluminanceentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getData.annotations:DA-END
    public PlantIlluminance getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getData.PlantIlluminance:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getData.PlantIlluminance:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.getData.PlantIlluminance:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.setData.PlantIlluminance.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.setData.PlantIlluminance.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.setData.PlantIlluminance.annotations:DA-END
    public void setData(PlantIlluminance data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.setData.PlantIlluminance:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.setData.PlantIlluminance:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.setData.PlantIlluminance:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity.additional.elements.in.type:DA-END
} // end of java type