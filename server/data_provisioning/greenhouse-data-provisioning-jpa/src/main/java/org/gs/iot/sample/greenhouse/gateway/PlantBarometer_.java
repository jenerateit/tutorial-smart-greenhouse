package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.PlantBarometer}.
 * 
 * persistent data structure for hardware of type 'BarometerBMP180Grove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.PlantBarometer
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.PlantBarometer.class)
public class PlantBarometer_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantBarometer#getPressure()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantBarometer#getPressure()
     */
    public static volatile SingularAttribute<PlantBarometer, Float> pressure;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantBarometer#getTemperature()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantBarometer#getTemperature()
     */
    public static volatile SingularAttribute<PlantBarometer, Float> temperature;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantBarometer#getAltitude()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantBarometer#getAltitude()
     */
    public static volatile SingularAttribute<PlantBarometer, Float> altitude;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometer_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometer_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometer_.additional.elements.in.type:DA-END
} // end of java type