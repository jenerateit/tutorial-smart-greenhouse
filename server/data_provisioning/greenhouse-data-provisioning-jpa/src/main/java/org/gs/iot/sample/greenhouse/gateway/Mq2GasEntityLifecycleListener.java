package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class Mq2GasEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.preUpdate.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.preUpdate.Mq2GasEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.preUpdate.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postUpdate.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postUpdate.Mq2GasEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postUpdate.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.prePersist.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.prePersist.Mq2GasEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.prePersist.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postPersist.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postPersist.Mq2GasEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postPersist.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.preRemove.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.preRemove.Mq2GasEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.preRemove.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postRemove.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postRemove.Mq2GasEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postRemove.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(Mq2GasEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postLoad.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postLoad.Mq2GasEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.postLoad.Mq2GasEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type