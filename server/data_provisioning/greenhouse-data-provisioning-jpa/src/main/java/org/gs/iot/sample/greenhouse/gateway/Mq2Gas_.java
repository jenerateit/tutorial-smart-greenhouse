package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.Mq2Gas}.
 * 
 * persistent data structure for hardware of type 'Mq2GasSensorGrove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.Mq2Gas
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.Mq2Gas.class)
public class Mq2Gas_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getRsAir()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getRsAir()
     */
    public static volatile SingularAttribute<Mq2Gas, Float> rsAir;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getr0()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getr0()
     */
    public static volatile SingularAttribute<Mq2Gas, Float> r0;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getRsGas()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getRsGas()
     */
    public static volatile SingularAttribute<Mq2Gas, Float> rsGas;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getRatio()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Mq2Gas#getRatio()
     */
    public static volatile SingularAttribute<Mq2Gas, Float> ratio;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2Gas_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2Gas_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2Gas_.additional.elements.in.type:DA-END
} // end of java type