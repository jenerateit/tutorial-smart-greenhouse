package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.PlantMoistureEntity:DA-START
//DA-ELSE:gateway.PlantMoistureEntity:DA-ELSE
@Entity
@Table(name="plantmoistureentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityLifecycleListener.class})
//DA-END:gateway.PlantMoistureEntity:DA-END

public class PlantMoistureEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private PlantMoisture data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="plantmoistureentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="plantmoistureentity_seq", sequenceName="plantmoistureentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getData.annotations:DA-END
    public PlantMoisture getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getData.PlantMoisture:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getData.PlantMoisture:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.getData.PlantMoisture:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.setData.PlantMoisture.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.setData.PlantMoisture.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.setData.PlantMoisture.annotations:DA-END
    public void setData(PlantMoisture data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.setData.PlantMoisture:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.setData.PlantMoisture:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.setData.PlantMoisture:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity.additional.elements.in.type:DA-END
} // end of java type