package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.PlantIlluminance}.
 * 
 * persistent data structure for hardware of type 'DigitalLightTSL2561Grove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.PlantIlluminance
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.class)
public class PlantIlluminance_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.PlantIlluminance#getStatus()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.PlantIlluminance#getStatus()
     */
    public static volatile SingularAttribute<PlantIlluminance, Float> status;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance_.additional.elements.in.type:DA-END
} // end of java type