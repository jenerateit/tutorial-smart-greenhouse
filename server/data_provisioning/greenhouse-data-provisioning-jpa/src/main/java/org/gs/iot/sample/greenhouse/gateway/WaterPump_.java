package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.WaterPump}.
 * 
 * persistent data structure for hardware of type 'RelayGrove'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.WaterPump
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.WaterPump.class)
public class WaterPump_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.WaterPump#isStatus()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.WaterPump#isStatus()
     */
    public static volatile SingularAttribute<WaterPump, Boolean> status;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPump_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPump_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPump_.additional.elements.in.type:DA-END
} // end of java type