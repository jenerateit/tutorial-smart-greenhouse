package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class WaterPumpEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.preUpdate.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.preUpdate.WaterPumpEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.preUpdate.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postUpdate.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postUpdate.WaterPumpEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postUpdate.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.prePersist.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.prePersist.WaterPumpEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.prePersist.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postPersist.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postPersist.WaterPumpEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postPersist.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.preRemove.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.preRemove.WaterPumpEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.preRemove.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postRemove.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postRemove.WaterPumpEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postRemove.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(WaterPumpEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postLoad.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postLoad.WaterPumpEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.postLoad.WaterPumpEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type