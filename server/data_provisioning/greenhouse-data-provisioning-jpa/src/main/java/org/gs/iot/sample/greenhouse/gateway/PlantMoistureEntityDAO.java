package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class PlantMoistureEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static PlantMoistureEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.get.EntityManager.Object.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.get.EntityManager.Object.PlantMoistureEntity:DA-ELSE
        return entityManager.find(PlantMoistureEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.get.EntityManager.Object.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<PlantMoistureEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<PlantMoistureEntity> cq = cb.createQuery(PlantMoistureEntity.class);
        cq.from(PlantMoistureEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantMoistureEntity create(EntityManager entityManager, PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.create.EntityManager.PlantMoistureEntity.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.create.EntityManager.PlantMoistureEntity.PlantMoistureEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.create.EntityManager.PlantMoistureEntity.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantMoistureEntity update(EntityManager entityManager, PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.update.EntityManager.PlantMoistureEntity.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.update.EntityManager.PlantMoistureEntity.PlantMoistureEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.update.EntityManager.PlantMoistureEntity.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, PlantMoistureEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.delete.EntityManager.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.delete.EntityManager.PlantMoistureEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.delete.EntityManager.PlantMoistureEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO.additional.elements.in.type:DA-END
} // end of java type