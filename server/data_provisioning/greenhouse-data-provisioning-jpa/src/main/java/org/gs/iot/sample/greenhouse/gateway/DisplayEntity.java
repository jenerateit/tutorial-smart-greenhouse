package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.DisplayEntity:DA-START
//DA-ELSE:gateway.DisplayEntity:DA-ELSE
@Entity
@Table(name="displayentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.DisplayEntityLifecycleListener.class})
//DA-END:gateway.DisplayEntity:DA-END

public class DisplayEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Display data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="displayentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="displayentity_seq", sequenceName="displayentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getData.annotations:DA-END
    public Display getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getData.Display:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getData.Display:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.getData.Display:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.setData.Display.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.setData.Display.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.setData.Display.annotations:DA-END
    public void setData(Display data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.setData.Display:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.setData.Display:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.setData.Display:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.DisplayEntity.additional.elements.in.type:DA-END
} // end of java type