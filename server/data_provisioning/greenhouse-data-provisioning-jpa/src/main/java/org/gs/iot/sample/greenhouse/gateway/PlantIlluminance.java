package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Embeddable;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

/**
 * persistent data structure for hardware of type 'DigitalLightTSL2561Grove'
 */
@Embeddable
public class PlantIlluminance implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private float status;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="status", unique=false, nullable=true)
    public float getStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.getStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.getStatus.float:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.getStatus.float:DA-END
    }
    /**
     * setter for the field status
     * 
     * 
     * 
     * @param status  the status
     */
    public void setStatus(float status) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.setStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.setStatus.float:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.setStatus.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantIlluminance.additional.elements.in.type:DA-END
} // end of java type