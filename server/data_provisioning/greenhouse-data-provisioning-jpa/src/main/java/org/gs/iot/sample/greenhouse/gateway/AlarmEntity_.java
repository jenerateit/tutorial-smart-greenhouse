package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.AlarmEntity}.
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.AlarmEntity
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.AlarmEntity.class)
public class AlarmEntity_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.AlarmEntity#getData()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.AlarmEntity#getData()
     */
    public static volatile SingularAttribute<AlarmEntity, Alarm> data;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.AlarmEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.AlarmEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.AlarmEntity_.additional.elements.in.type:DA-END
} // end of java type