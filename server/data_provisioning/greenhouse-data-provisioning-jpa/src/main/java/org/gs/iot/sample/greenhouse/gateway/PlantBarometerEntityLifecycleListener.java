package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class PlantBarometerEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.preUpdate.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.preUpdate.PlantBarometerEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.preUpdate.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postUpdate.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postUpdate.PlantBarometerEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postUpdate.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.prePersist.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.prePersist.PlantBarometerEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.prePersist.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postPersist.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postPersist.PlantBarometerEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postPersist.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.preRemove.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.preRemove.PlantBarometerEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.preRemove.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postRemove.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postRemove.PlantBarometerEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postRemove.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postLoad.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postLoad.PlantBarometerEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.postLoad.PlantBarometerEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type