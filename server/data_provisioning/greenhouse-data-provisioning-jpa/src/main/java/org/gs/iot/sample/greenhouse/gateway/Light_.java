package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.Light}.
 * 
 * persistent data structure for hardware of type 'LightStrip'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.Light
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.Light.class)
public class Light_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Light#getR()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Light#getR()
     */
    public static volatile SingularAttribute<Light, Short> r;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Light#getG()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Light#getG()
     */
    public static volatile SingularAttribute<Light, Short> g;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Light#getB()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Light#getB()
     */
    public static volatile SingularAttribute<Light, Short> b;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Light#getBrightness()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Light#getBrightness()
     */
    public static volatile SingularAttribute<Light, Double> brightness;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Light_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Light_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Light_.additional.elements.in.type:DA-END
} // end of java type