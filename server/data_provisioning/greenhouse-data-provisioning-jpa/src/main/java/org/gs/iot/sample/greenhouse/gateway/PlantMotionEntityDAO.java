package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class PlantMotionEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static PlantMotionEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.get.EntityManager.Object.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.get.EntityManager.Object.PlantMotionEntity:DA-ELSE
        return entityManager.find(PlantMotionEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.get.EntityManager.Object.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<PlantMotionEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<PlantMotionEntity> cq = cb.createQuery(PlantMotionEntity.class);
        cq.from(PlantMotionEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantMotionEntity create(EntityManager entityManager, PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.create.EntityManager.PlantMotionEntity.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.create.EntityManager.PlantMotionEntity.PlantMotionEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.create.EntityManager.PlantMotionEntity.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantMotionEntity update(EntityManager entityManager, PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.update.EntityManager.PlantMotionEntity.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.update.EntityManager.PlantMotionEntity.PlantMotionEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.update.EntityManager.PlantMotionEntity.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, PlantMotionEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.delete.EntityManager.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.delete.EntityManager.PlantMotionEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.delete.EntityManager.PlantMotionEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO.additional.elements.in.type:DA-END
} // end of java type