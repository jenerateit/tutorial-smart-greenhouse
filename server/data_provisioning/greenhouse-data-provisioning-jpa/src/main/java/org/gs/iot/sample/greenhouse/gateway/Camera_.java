package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.Camera}.
 * 
 * persistent data structure for hardware of type 'RpiCamera'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.Camera
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.Camera.class)
public class Camera_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Camera#getMimetype()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Camera#getMimetype()
     */
    public static volatile SingularAttribute<Camera, String> mimetype;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Camera#getImage()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Camera#getImage()
     */
    public static volatile SingularAttribute<Camera, String> image;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera_.additional.elements.in.type:DA-END
} // end of java type