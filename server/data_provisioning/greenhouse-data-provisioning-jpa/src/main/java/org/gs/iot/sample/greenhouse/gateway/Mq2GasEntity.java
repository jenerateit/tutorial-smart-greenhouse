package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.Mq2GasEntity:DA-START
//DA-ELSE:gateway.Mq2GasEntity:DA-ELSE
@Entity
@Table(name="mq2gasentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityLifecycleListener.class})
//DA-END:gateway.Mq2GasEntity:DA-END

public class Mq2GasEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Mq2Gas data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="mq2gasentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="mq2gasentity_seq", sequenceName="mq2gasentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getData.annotations:DA-END
    public Mq2Gas getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getData.Mq2Gas:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getData.Mq2Gas:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.getData.Mq2Gas:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.setData.Mq2Gas.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.setData.Mq2Gas.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.setData.Mq2Gas.annotations:DA-END
    public void setData(Mq2Gas data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.setData.Mq2Gas:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.setData.Mq2Gas:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.setData.Mq2Gas:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity.additional.elements.in.type:DA-END
} // end of java type