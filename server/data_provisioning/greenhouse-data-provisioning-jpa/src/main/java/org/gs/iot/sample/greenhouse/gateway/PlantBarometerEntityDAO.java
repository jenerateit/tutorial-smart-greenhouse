package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class PlantBarometerEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static PlantBarometerEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.get.EntityManager.Object.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.get.EntityManager.Object.PlantBarometerEntity:DA-ELSE
        return entityManager.find(PlantBarometerEntity.class, id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.get.EntityManager.Object.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<PlantBarometerEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<PlantBarometerEntity> cq = cb.createQuery(PlantBarometerEntity.class);
        cq.from(PlantBarometerEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantBarometerEntity create(EntityManager entityManager, PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.create.EntityManager.PlantBarometerEntity.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.create.EntityManager.PlantBarometerEntity.PlantBarometerEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.create.EntityManager.PlantBarometerEntity.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static PlantBarometerEntity update(EntityManager entityManager, PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.update.EntityManager.PlantBarometerEntity.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.update.EntityManager.PlantBarometerEntity.PlantBarometerEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.update.EntityManager.PlantBarometerEntity.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, PlantBarometerEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.delete.EntityManager.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.delete.EntityManager.PlantBarometerEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.delete.EntityManager.PlantBarometerEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO.additional.elements.in.type:DA-END
} // end of java type