package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Embeddable;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

/**
 * persistent data structure for hardware of type 'RelayGrove'
 */
@Embeddable
public class WaterPump implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private boolean status;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPump.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPump.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPump.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPump.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPump.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPump.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="status", unique=false, nullable=true)
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPump.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPump.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPump.isStatus.boolean:DA-END
    }
    /**
     * setter for the field status
     * 
     * 
     * 
     * @param status  the status
     */
    public void setStatus(boolean status) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPump.setStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPump.setStatus.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPump.setStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPump.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPump.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPump.additional.elements.in.type:DA-END
} // end of java type