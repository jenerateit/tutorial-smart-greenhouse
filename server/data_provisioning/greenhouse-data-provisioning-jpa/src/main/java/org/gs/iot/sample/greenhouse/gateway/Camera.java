package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Embeddable;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.FetchType;
import javax.validation.constraints.Size;

/**
 * persistent data structure for hardware of type 'RpiCamera'
 */
@Embeddable
public class Camera implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private String mimetype;
    
    private String image;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field mimetype
     * 
     * 
     * 
     * @return
     */
    @Basic
    @Column(name="mimetype", unique=false, nullable=true)
    public String getMimetype() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera.getMimetype.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera.getMimetype.String:DA-ELSE
        return this.mimetype;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera.getMimetype.String:DA-END
    }
    /**
     * setter for the field mimetype
     * 
     * 
     * 
     * @param mimetype  the mimetype
     */
    public void setMimetype(String mimetype) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera.setMimetype.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera.setMimetype.String:DA-ELSE
        this.mimetype = mimetype;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera.setMimetype.String:DA-END
    }
    /**
     * getter for the field image
     * 
     * 
     * 
     * @return
     */
    @Lob
    @Basic(fetch=FetchType.LAZY)
    @Column(name="image", unique=false, nullable=true, length=512000)
    @Size(min=0, max=512000)
    public String getImage() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera.getImage.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera.getImage.String:DA-ELSE
        return this.image;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera.getImage.String:DA-END
    }
    /**
     * setter for the field image
     * 
     * 
     * 
     * @param image  the image
     */
    public void setImage(String image) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera.setImage.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera.setImage.String:DA-ELSE
        this.image = image;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera.setImage.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Camera.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Camera.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Camera.additional.elements.in.type:DA-END
} // end of java type