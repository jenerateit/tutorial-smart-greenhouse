package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Embedded;

//DA-START:gateway.WaterPumpEntity:DA-START
//DA-ELSE:gateway.WaterPumpEntity:DA-ELSE
@Entity
@Table(name="waterpumpentity")
@EntityListeners(value={org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityLifecycleListener.class})
//DA-END:gateway.WaterPumpEntity:DA-END

public class WaterPumpEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private WaterPump data;
    
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="waterpumpentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="waterpumpentity_seq", sequenceName="waterpumpentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getPk.Long:DA-END
    }
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getData.annotations:DA-END
    public WaterPump getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getData.WaterPump:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getData.WaterPump:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.getData.WaterPump:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.setData.WaterPump.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.setData.WaterPump.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.setData.WaterPump.annotations:DA-END
    public void setData(WaterPump data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.setData.WaterPump:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.setData.WaterPump:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.setData.WaterPump:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity.additional.elements.in.type:DA-END
} // end of java type