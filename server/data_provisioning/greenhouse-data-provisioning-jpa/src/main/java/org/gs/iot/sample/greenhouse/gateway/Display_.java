package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;

/**
 * JPA Metamodel description for {@link org.gs.iot.sample.greenhouse.gateway.Display}.
 * 
 * persistent data structure for hardware of type 'AdafruitI2cRgbLcd'
 * 
 * @see org.gs.iot.sample.greenhouse.gateway.Display
 */
@StaticMetamodel(value=org.gs.iot.sample.greenhouse.gateway.Display.class)
public class Display_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Display#getBacklight()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Display#getBacklight()
     */
    public static volatile SingularAttribute<Display, Integer> backlight;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Display#getRow0()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Display#getRow0()
     */
    public static volatile SingularAttribute<Display, String> row0;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Display#getRow1()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Display#getRow1()
     */
    public static volatile SingularAttribute<Display, String> row1;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.greenhouse.gateway.Display#getButton()}.
     * 
     * @see org.gs.iot.sample.greenhouse.gateway.Display#getButton()
     */
    public static volatile SingularAttribute<Display, AdafruitI2cRgbLcdButtonsEnum> button;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.Display_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.Display_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.Display_.additional.elements.in.type:DA-END
} // end of java type