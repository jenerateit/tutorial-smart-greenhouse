package org.gs.iot.sample.greenhouse.gateway;


import javax.persistence.PreUpdate;
import com.gs.vd.portal.userdata.ThreadLocalSessionData;
import java.util.Date;
import java.sql.Timestamp;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
public class BaseEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(BaseEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.preUpdate.BaseEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.preUpdate.BaseEntity:DA-ELSE
        entity.setModificationTimestamp(new Timestamp(new Date().getTime()));
        
        ThreadLocalSessionData.SessionData sessionData = ThreadLocalSessionData.getSessionData();
        if (sessionData != null) {
        	entity.setModifiedBy(sessionData.getId());
        } else {
        	entity.setModifiedBy("-");
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.preUpdate.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(BaseEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postUpdate.BaseEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postUpdate.BaseEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postUpdate.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(BaseEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.prePersist.BaseEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.prePersist.BaseEntity:DA-ELSE
        entity.setCreationTimestamp(new Timestamp(new Date().getTime()));
        entity.setModificationTimestamp(entity.getCreationTimestamp());
        
        ThreadLocalSessionData.SessionData sessionData = ThreadLocalSessionData.getSessionData();
        if (sessionData != null) {
        	entity.setCreatedBy(sessionData.getId());
        	entity.setModifiedBy(sessionData.getId());
        } else {
        	entity.setCreatedBy("-");
        	entity.setModifiedBy("-");
        }
        
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.prePersist.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(BaseEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postPersist.BaseEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postPersist.BaseEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postPersist.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(BaseEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.preRemove.BaseEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.preRemove.BaseEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.preRemove.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(BaseEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postRemove.BaseEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postRemove.BaseEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postRemove.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(BaseEntity entity) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postLoad.BaseEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postLoad.BaseEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.postLoad.BaseEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.BaseEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type