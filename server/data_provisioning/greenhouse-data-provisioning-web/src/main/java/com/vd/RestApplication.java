package com.vd;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;
import java.util.HashSet;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import java.util.LinkedHashSet;

@ApplicationPath(value="apppath/v1")
public class RestApplication extends Application { // start of class

    /**
     * 
     * @return
     */
    public Set<Object> getSingletons() {
        //DA-START:com.vd.RestApplication.getSingletons.Class:DA-START
        //DA-ELSE:com.vd.RestApplication.getSingletons.Class:DA-ELSE
        Set<Object> singletons = new HashSet<Object>();
        singletons.add(new JacksonJaxbJsonProvider());
        return singletons;
        //DA-END:com.vd.RestApplication.getSingletons.Class:DA-END
    }
    /**
     * 
     * @return
     */
    public Set<Class<?>> getClasses() {
        //DA-START:com.vd.RestApplication.getClasses.Class:DA-START
        //DA-ELSE:com.vd.RestApplication.getClasses.Class:DA-ELSE
        Set<Class<?>> result = new LinkedHashSet<Class<?>>();
        result.add(org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.TemperatureEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.LightEntityResource.class);
        result.add(org.gs.iot.sample.greenhouse.gateway.function.CameraEntityResource.class);
        result.add(JacksonConfigurator.class);
        return result;
        //DA-END:com.vd.RestApplication.getClasses.Class:DA-END
    }
    
    //DA-START:com.vd.RestApplication.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.RestApplication.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.RestApplication.additional.elements.in.type:DA-END
} // end of java type