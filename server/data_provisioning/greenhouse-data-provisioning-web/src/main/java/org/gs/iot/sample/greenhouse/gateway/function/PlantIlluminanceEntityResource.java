package org.gs.iot.sample.greenhouse.gateway.function;


import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Request;
import javax.ws.rs.ext.Providers;
import javax.ejb.EJB;
import org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.GET;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean;
import javax.ws.rs.QueryParam;
import java.util.List;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

//DA-START:function.PlantIlluminanceEntityResource:DA-START
//DA-ELSE:function.PlantIlluminanceEntityResource:DA-ELSE
@Path(value="/plantilluminanceentity")
@Produces(value={"application/xml", "application/json"})
@Consumes(value={"application/xml", "application/json"})
@RequestScoped
//DA-END:function.PlantIlluminanceEntityResource:DA-END

public class PlantIlluminanceEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.plantIlluminanceEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.plantIlluminanceEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.plantIlluminanceEntityBean:DA-END
    private PlantIlluminanceEntityEJB plantIlluminanceEntityBean;
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.delete.long.annotations:DA-END
    public void delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.delete.long:DA-ELSE
        plantIlluminanceEntityBean.delete(id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.deleteByType.long.String.annotations:DA-END
    public void deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.deleteByType.long.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.deleteByType.long.String:DA-ELSE
        plantIlluminanceEntityBean.delete(id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.deleteByType.long.String:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.read.long.annotations:DA-END
    public PlantIlluminanceEntityBean read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.read.long.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.read.long.PlantIlluminanceEntityBean:DA-ELSE
        PlantIlluminanceEntityBean result = plantIlluminanceEntityBean.read(id);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.read.long.PlantIlluminanceEntityBean:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readByType.long.Response.String:DA-ELSE
        PlantIlluminanceEntityBean result = plantIlluminanceEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readByType.long.Response.String:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readList.int.int.List.annotations:DA-END
    public PlantIlluminanceEntityListBean readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readList.int.int.List.PlantIlluminanceEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readList.int.int.List.PlantIlluminanceEntityListBean:DA-ELSE
        PlantIlluminanceEntityListBean result = plantIlluminanceEntityBean.readList(offset, limit, ids);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readList.int.int.List.PlantIlluminanceEntityListBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        PlantIlluminanceEntityListBean result = plantIlluminanceEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.update.long.PlantIlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.update.long.PlantIlluminanceEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.update.long.PlantIlluminanceEntityBean.annotations:DA-END
    public PlantIlluminanceEntityBean update(@PathParam(value="id") long id, PlantIlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.update.long.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.update.long.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-ELSE
        PlantIlluminanceEntityBean result = plantIlluminanceEntityBean.update(id, bean);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.update.long.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.updateByType.long.PlantIlluminanceEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.updateByType.long.PlantIlluminanceEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.updateByType.long.PlantIlluminanceEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, PlantIlluminanceEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.updateByType.long.PlantIlluminanceEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.updateByType.long.PlantIlluminanceEntityBean.Response.String:DA-ELSE
        PlantIlluminanceEntityBean result = plantIlluminanceEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.updateByType.long.PlantIlluminanceEntityBean.Response.String:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.create.PlantIlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.create.PlantIlluminanceEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.create.PlantIlluminanceEntityBean.annotations:DA-END
    public Response create(PlantIlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.create.PlantIlluminanceEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.create.PlantIlluminanceEntityBean.Response:DA-ELSE
        PlantIlluminanceEntityBean result = plantIlluminanceEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.create.PlantIlluminanceEntityBean.Response:DA-END
    }
    /**
     * 
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.createByType.PlantIlluminanceEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.createByType.PlantIlluminanceEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.createByType.PlantIlluminanceEntityBean.String.annotations:DA-END
    public Response createByType(PlantIlluminanceEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.createByType.PlantIlluminanceEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.createByType.PlantIlluminanceEntityBean.Response.String:DA-ELSE
        PlantIlluminanceEntityBean result = plantIlluminanceEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.createByType.PlantIlluminanceEntityBean.Response.String:DA-END
    }
    /**
     * setter for the field uriInfo
     * 
     * 
     * 
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setUriInfo.UriInfo:DA-END
    }
    /**
     * setter for the field httpHeaders
     * 
     * 
     * 
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    /**
     * setter for the field securityContext
     * 
     * 
     * 
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    /**
     * setter for the field request
     * 
     * 
     * 
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setRequest.Request:DA-END
    }
    /**
     * setter for the field providers
     * 
     * 
     * 
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityResource.additional.elements.in.type:DA-END
} // end of java type