package org.gs.iot.sample.greenhouse.gateway.function;


import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Request;
import javax.ws.rs.ext.Providers;
import javax.ejb.EJB;
import org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.GET;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean;
import javax.ws.rs.QueryParam;
import java.util.List;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

//DA-START:function.PlantBarometerEntityResource:DA-START
//DA-ELSE:function.PlantBarometerEntityResource:DA-ELSE
@Path(value="/plantbarometerentity")
@Produces(value={"application/xml", "application/json"})
@Consumes(value={"application/xml", "application/json"})
@RequestScoped
//DA-END:function.PlantBarometerEntityResource:DA-END

public class PlantBarometerEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.plantBarometerEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.plantBarometerEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.plantBarometerEntityBean:DA-END
    private PlantBarometerEntityEJB plantBarometerEntityBean;
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.delete.long.annotations:DA-END
    public void delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.delete.long:DA-ELSE
        plantBarometerEntityBean.delete(id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.deleteByType.long.String.annotations:DA-END
    public void deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.deleteByType.long.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.deleteByType.long.String:DA-ELSE
        plantBarometerEntityBean.delete(id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.deleteByType.long.String:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.read.long.annotations:DA-END
    public PlantBarometerEntityBean read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.read.long.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.read.long.PlantBarometerEntityBean:DA-ELSE
        PlantBarometerEntityBean result = plantBarometerEntityBean.read(id);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.read.long.PlantBarometerEntityBean:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readByType.long.Response.String:DA-ELSE
        PlantBarometerEntityBean result = plantBarometerEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readByType.long.Response.String:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readList.int.int.List.annotations:DA-END
    public PlantBarometerEntityListBean readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readList.int.int.List.PlantBarometerEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readList.int.int.List.PlantBarometerEntityListBean:DA-ELSE
        PlantBarometerEntityListBean result = plantBarometerEntityBean.readList(offset, limit, ids);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readList.int.int.List.PlantBarometerEntityListBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        PlantBarometerEntityListBean result = plantBarometerEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.update.long.PlantBarometerEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.update.long.PlantBarometerEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.update.long.PlantBarometerEntityBean.annotations:DA-END
    public PlantBarometerEntityBean update(@PathParam(value="id") long id, PlantBarometerEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.update.long.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.update.long.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-ELSE
        PlantBarometerEntityBean result = plantBarometerEntityBean.update(id, bean);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.update.long.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.updateByType.long.PlantBarometerEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.updateByType.long.PlantBarometerEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.updateByType.long.PlantBarometerEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, PlantBarometerEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.updateByType.long.PlantBarometerEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.updateByType.long.PlantBarometerEntityBean.Response.String:DA-ELSE
        PlantBarometerEntityBean result = plantBarometerEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.updateByType.long.PlantBarometerEntityBean.Response.String:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.create.PlantBarometerEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.create.PlantBarometerEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.create.PlantBarometerEntityBean.annotations:DA-END
    public Response create(PlantBarometerEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.create.PlantBarometerEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.create.PlantBarometerEntityBean.Response:DA-ELSE
        PlantBarometerEntityBean result = plantBarometerEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.create.PlantBarometerEntityBean.Response:DA-END
    }
    /**
     * 
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.createByType.PlantBarometerEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.createByType.PlantBarometerEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.createByType.PlantBarometerEntityBean.String.annotations:DA-END
    public Response createByType(PlantBarometerEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.createByType.PlantBarometerEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.createByType.PlantBarometerEntityBean.Response.String:DA-ELSE
        PlantBarometerEntityBean result = plantBarometerEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.createByType.PlantBarometerEntityBean.Response.String:DA-END
    }
    /**
     * setter for the field uriInfo
     * 
     * 
     * 
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setUriInfo.UriInfo:DA-END
    }
    /**
     * setter for the field httpHeaders
     * 
     * 
     * 
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    /**
     * setter for the field securityContext
     * 
     * 
     * 
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    /**
     * setter for the field request
     * 
     * 
     * 
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setRequest.Request:DA-END
    }
    /**
     * setter for the field providers
     * 
     * 
     * 
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityResource.additional.elements.in.type:DA-END
} // end of java type