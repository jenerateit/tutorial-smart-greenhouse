package org.gs.iot.sample.greenhouse.gateway.function;


import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Request;
import javax.ws.rs.ext.Providers;
import javax.ejb.EJB;
import org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.GET;
import org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean;
import javax.ws.rs.QueryParam;
import java.util.List;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

//DA-START:function.DisplayEntityResource:DA-START
//DA-ELSE:function.DisplayEntityResource:DA-ELSE
@Path(value="/displayentity")
@Produces(value={"application/xml", "application/json"})
@Consumes(value={"application/xml", "application/json"})
@RequestScoped
//DA-END:function.DisplayEntityResource:DA-END

public class DisplayEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.displayEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.displayEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.displayEntityBean:DA-END
    private DisplayEntityEJB displayEntityBean;
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.delete.long.annotations:DA-END
    public void delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.delete.long:DA-ELSE
        displayEntityBean.delete(id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.deleteByType.long.String.annotations:DA-END
    public void deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.deleteByType.long.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.deleteByType.long.String:DA-ELSE
        displayEntityBean.delete(id);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.deleteByType.long.String:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.read.long.annotations:DA-END
    public DisplayEntityBean read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.read.long.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.read.long.DisplayEntityBean:DA-ELSE
        DisplayEntityBean result = displayEntityBean.read(id);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.read.long.DisplayEntityBean:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readByType.long.Response.String:DA-ELSE
        DisplayEntityBean result = displayEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readByType.long.Response.String:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readList.int.int.List.annotations:DA-END
    public DisplayEntityListBean readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readList.int.int.List.DisplayEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readList.int.int.List.DisplayEntityListBean:DA-ELSE
        DisplayEntityListBean result = displayEntityBean.readList(offset, limit, ids);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readList.int.int.List.DisplayEntityListBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        DisplayEntityListBean result = displayEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.update.long.DisplayEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.update.long.DisplayEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.update.long.DisplayEntityBean.annotations:DA-END
    public DisplayEntityBean update(@PathParam(value="id") long id, DisplayEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.update.long.DisplayEntityBean.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.update.long.DisplayEntityBean.DisplayEntityBean:DA-ELSE
        DisplayEntityBean result = displayEntityBean.update(id, bean);
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.update.long.DisplayEntityBean.DisplayEntityBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.updateByType.long.DisplayEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.updateByType.long.DisplayEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.updateByType.long.DisplayEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, DisplayEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.updateByType.long.DisplayEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.updateByType.long.DisplayEntityBean.Response.String:DA-ELSE
        DisplayEntityBean result = displayEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.updateByType.long.DisplayEntityBean.Response.String:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.create.DisplayEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.create.DisplayEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.create.DisplayEntityBean.annotations:DA-END
    public Response create(DisplayEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.create.DisplayEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.create.DisplayEntityBean.Response:DA-ELSE
        DisplayEntityBean result = displayEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.create.DisplayEntityBean.Response:DA-END
    }
    /**
     * 
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.createByType.DisplayEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.createByType.DisplayEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.createByType.DisplayEntityBean.String.annotations:DA-END
    public Response createByType(DisplayEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.createByType.DisplayEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.createByType.DisplayEntityBean.Response.String:DA-ELSE
        DisplayEntityBean result = displayEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.createByType.DisplayEntityBean.Response.String:DA-END
    }
    /**
     * setter for the field uriInfo
     * 
     * 
     * 
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setUriInfo.UriInfo:DA-END
    }
    /**
     * setter for the field httpHeaders
     * 
     * 
     * 
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    /**
     * setter for the field securityContext
     * 
     * 
     * 
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    /**
     * setter for the field request
     * 
     * 
     * 
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setRequest.Request:DA-END
    }
    /**
     * setter for the field providers
     * 
     * 
     * 
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityResource.additional.elements.in.type:DA-END
} // end of java type