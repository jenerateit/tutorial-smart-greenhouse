package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class GeoCoordinatesListBean { // start of class

    private List<GeoCoordinatesBean> geoCoordinatesList = new ArrayList<GeoCoordinatesBean>();
    
    /**
     * creates an instance of GeoCoordinatesListBean
     */
    public GeoCoordinatesListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean:DA-END
    }
    
    /**
     * getter for the field geoCoordinatesList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="geoCoordinatesList")
    public List<GeoCoordinatesBean> getGeoCoordinatesList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.getGeoCoordinatesList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.getGeoCoordinatesList.List:DA-ELSE
        return this.geoCoordinatesList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.getGeoCoordinatesList.List:DA-END
    }
    /**
     * adder for the field geoCoordinatesList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addGeoCoordinatesList(GeoCoordinatesBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.addGeoCoordinatesList.GeoCoordinatesBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.addGeoCoordinatesList.GeoCoordinatesBean:DA-ELSE
        this.geoCoordinatesList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.addGeoCoordinatesList.GeoCoordinatesBean:DA-END
    }
    /**
     * remover for the field geoCoordinatesList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeGeoCoordinatesList(GeoCoordinatesBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.removeGeoCoordinatesList.GeoCoordinatesBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.removeGeoCoordinatesList.GeoCoordinatesBean:DA-ELSE
        this.geoCoordinatesList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.removeGeoCoordinatesList.GeoCoordinatesBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesListBean.additional.elements.in.type:DA-END
} // end of java type