package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'BarometerBMP180Grove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantBarometerListBean { // start of class

    private List<PlantBarometerBean> plantBarometerList = new ArrayList<PlantBarometerBean>();
    
    /**
     * creates an instance of PlantBarometerListBean
     */
    public PlantBarometerListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean:DA-END
    }
    
    /**
     * getter for the field plantBarometerList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantBarometerList")
    public List<PlantBarometerBean> getPlantBarometerList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.getPlantBarometerList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.getPlantBarometerList.List:DA-ELSE
        return this.plantBarometerList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.getPlantBarometerList.List:DA-END
    }
    /**
     * adder for the field plantBarometerList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantBarometerList(PlantBarometerBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.addPlantBarometerList.PlantBarometerBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.addPlantBarometerList.PlantBarometerBean:DA-ELSE
        this.plantBarometerList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.addPlantBarometerList.PlantBarometerBean:DA-END
    }
    /**
     * remover for the field plantBarometerList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantBarometerList(PlantBarometerBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.removePlantBarometerList.PlantBarometerBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.removePlantBarometerList.PlantBarometerBean:DA-ELSE
        this.plantBarometerList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.removePlantBarometerList.PlantBarometerBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerListBean.additional.elements.in.type:DA-END
} // end of java type