package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class BaseEntityListBean { // start of class

    private List<BaseEntityBean> baseEntityList = new ArrayList<BaseEntityBean>();
    
    /**
     * creates an instance of BaseEntityListBean
     */
    public BaseEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean:DA-END
    }
    
    /**
     * getter for the field baseEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="baseEntityList")
    public List<BaseEntityBean> getBaseEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.getBaseEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.getBaseEntityList.List:DA-ELSE
        return this.baseEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.getBaseEntityList.List:DA-END
    }
    /**
     * adder for the field baseEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addBaseEntityList(BaseEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.addBaseEntityList.BaseEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.addBaseEntityList.BaseEntityBean:DA-ELSE
        this.baseEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.addBaseEntityList.BaseEntityBean:DA-END
    }
    /**
     * remover for the field baseEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeBaseEntityList(BaseEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.removeBaseEntityList.BaseEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.removeBaseEntityList.BaseEntityBean:DA-ELSE
        this.baseEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.removeBaseEntityList.BaseEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityListBean.additional.elements.in.type:DA-END
} // end of java type