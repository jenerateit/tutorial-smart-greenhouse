package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantMoistureEntityListBean { // start of class

    private List<PlantMoistureEntityBean> plantMoistureEntityList = new ArrayList<PlantMoistureEntityBean>();
    
    /**
     * creates an instance of PlantMoistureEntityListBean
     */
    public PlantMoistureEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean:DA-END
    }
    
    /**
     * getter for the field plantMoistureEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantMoistureEntityList")
    public List<PlantMoistureEntityBean> getPlantMoistureEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.getPlantMoistureEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.getPlantMoistureEntityList.List:DA-ELSE
        return this.plantMoistureEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.getPlantMoistureEntityList.List:DA-END
    }
    /**
     * adder for the field plantMoistureEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantMoistureEntityList(PlantMoistureEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.addPlantMoistureEntityList.PlantMoistureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.addPlantMoistureEntityList.PlantMoistureEntityBean:DA-ELSE
        this.plantMoistureEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.addPlantMoistureEntityList.PlantMoistureEntityBean:DA-END
    }
    /**
     * remover for the field plantMoistureEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantMoistureEntityList(PlantMoistureEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.removePlantMoistureEntityList.PlantMoistureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.removePlantMoistureEntityList.PlantMoistureEntityBean:DA-ELSE
        this.plantMoistureEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.removePlantMoistureEntityList.PlantMoistureEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean.additional.elements.in.type:DA-END
} // end of java type