package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class Mq2GasEntityBean extends BaseEntityBean { // start of class

    private Mq2GasBean data;
    
    /**
     * creates an instance of Mq2GasEntityBean
     */
    public Mq2GasEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public Mq2GasBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.getData.Mq2GasBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.getData.Mq2GasBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.getData.Mq2GasBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(Mq2GasBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.setData.Mq2GasBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.setData.Mq2GasBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.setData.Mq2GasBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean.additional.elements.in.type:DA-END
} // end of java type