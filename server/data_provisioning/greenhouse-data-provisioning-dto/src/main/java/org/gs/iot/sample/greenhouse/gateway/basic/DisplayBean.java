package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import org.gs.iot.sample.greenhouse.gateway.AdafruitI2cRgbLcdButtonsEnum;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'AdafruitI2cRgbLcd'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class DisplayBean { // start of class

    private int backlight;
    
    private String row0;
    
    private String row1;
    
    private AdafruitI2cRgbLcdButtonsEnum button;
    
    /**
     * creates an instance of DisplayBean
     */
    public DisplayBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean:DA-END
    }
    
    /**
     * getter for the field backlight
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="backlight")
    public int getBacklight() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getBacklight.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getBacklight.int:DA-ELSE
        return this.backlight;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getBacklight.int:DA-END
    }
    /**
     * setter for the field backlight
     * 
     * 
     * 
     * @param backlight  the backlight
     */
    public void setBacklight(int backlight) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setBacklight.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setBacklight.int:DA-ELSE
        this.backlight = backlight;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setBacklight.int:DA-END
    }
    /**
     * getter for the field row0
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="row0")
    public String getRow0() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getRow0.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getRow0.String:DA-ELSE
        return this.row0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getRow0.String:DA-END
    }
    /**
     * setter for the field row0
     * 
     * 
     * 
     * @param row0  the row0
     */
    public void setRow0(String row0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setRow0.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setRow0.String:DA-ELSE
        this.row0 = row0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setRow0.String:DA-END
    }
    /**
     * getter for the field row1
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="row1")
    public String getRow1() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getRow1.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getRow1.String:DA-ELSE
        return this.row1;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getRow1.String:DA-END
    }
    /**
     * setter for the field row1
     * 
     * 
     * 
     * @param row1  the row1
     */
    public void setRow1(String row1) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setRow1.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setRow1.String:DA-ELSE
        this.row1 = row1;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setRow1.String:DA-END
    }
    /**
     * getter for the field button
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="button")
    public AdafruitI2cRgbLcdButtonsEnum getButton() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-ELSE
        return this.button;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.getButton.AdafruitI2cRgbLcdButtonsEnum:DA-END
    }
    /**
     * setter for the field button
     * 
     * 
     * 
     * @param button  the button
     */
    public void setButton(AdafruitI2cRgbLcdButtonsEnum button) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-ELSE
        this.button = button;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.setButton.AdafruitI2cRgbLcdButtonsEnum:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean.additional.elements.in.type:DA-END
} // end of java type