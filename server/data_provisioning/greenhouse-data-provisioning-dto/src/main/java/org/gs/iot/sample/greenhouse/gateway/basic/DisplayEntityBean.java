package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class DisplayEntityBean extends BaseEntityBean { // start of class

    private DisplayBean data;
    
    /**
     * creates an instance of DisplayEntityBean
     */
    public DisplayEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public DisplayBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.getData.DisplayBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.getData.DisplayBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.getData.DisplayBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(DisplayBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.setData.DisplayBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.setData.DisplayBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.setData.DisplayBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean.additional.elements.in.type:DA-END
} // end of java type