package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'MoistureSensorGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantMoistureListBean { // start of class

    private List<PlantMoistureBean> plantMoistureList = new ArrayList<PlantMoistureBean>();
    
    /**
     * creates an instance of PlantMoistureListBean
     */
    public PlantMoistureListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean:DA-END
    }
    
    /**
     * getter for the field plantMoistureList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantMoistureList")
    public List<PlantMoistureBean> getPlantMoistureList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.getPlantMoistureList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.getPlantMoistureList.List:DA-ELSE
        return this.plantMoistureList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.getPlantMoistureList.List:DA-END
    }
    /**
     * adder for the field plantMoistureList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantMoistureList(PlantMoistureBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.addPlantMoistureList.PlantMoistureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.addPlantMoistureList.PlantMoistureBean:DA-ELSE
        this.plantMoistureList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.addPlantMoistureList.PlantMoistureBean:DA-END
    }
    /**
     * remover for the field plantMoistureList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantMoistureList(PlantMoistureBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.removePlantMoistureList.PlantMoistureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.removePlantMoistureList.PlantMoistureBean:DA-ELSE
        this.plantMoistureList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.removePlantMoistureList.PlantMoistureBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureListBean.additional.elements.in.type:DA-END
} // end of java type