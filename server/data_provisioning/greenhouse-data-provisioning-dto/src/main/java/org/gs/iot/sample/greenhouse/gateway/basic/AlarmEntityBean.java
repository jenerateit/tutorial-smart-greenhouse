package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class AlarmEntityBean extends BaseEntityBean { // start of class

    private AlarmBean data;
    
    /**
     * creates an instance of AlarmEntityBean
     */
    public AlarmEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public AlarmBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.getData.AlarmBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.getData.AlarmBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.getData.AlarmBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(AlarmBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.setData.AlarmBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.setData.AlarmBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.setData.AlarmBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean.additional.elements.in.type:DA-END
} // end of java type