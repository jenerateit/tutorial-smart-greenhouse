package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'BuzzerGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class AlarmBean { // start of class

    private boolean status;
    
    /**
     * creates an instance of AlarmBean
     */
    public AlarmBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="status")
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.isStatus.boolean:DA-END
    }
    /**
     * setter for the field status
     * 
     * 
     * 
     * @param status  the status
     */
    public void setStatus(boolean status) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.setStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.setStatus.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.setStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean.additional.elements.in.type:DA-END
} // end of java type