package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantBarometerEntityBean extends BaseEntityBean { // start of class

    private PlantBarometerBean data;
    
    /**
     * creates an instance of PlantBarometerEntityBean
     */
    public PlantBarometerEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public PlantBarometerBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.getData.PlantBarometerBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.getData.PlantBarometerBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.getData.PlantBarometerBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(PlantBarometerBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.setData.PlantBarometerBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.setData.PlantBarometerBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.setData.PlantBarometerBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean.additional.elements.in.type:DA-END
} // end of java type