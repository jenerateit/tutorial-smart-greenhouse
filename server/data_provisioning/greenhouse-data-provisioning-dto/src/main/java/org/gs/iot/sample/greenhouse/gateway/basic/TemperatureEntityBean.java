package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class TemperatureEntityBean extends BaseEntityBean { // start of class

    private TemperatureBean data;
    
    /**
     * creates an instance of TemperatureEntityBean
     */
    public TemperatureEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public TemperatureBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.getData.TemperatureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.getData.TemperatureBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.getData.TemperatureBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(TemperatureBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.setData.TemperatureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.setData.TemperatureBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.setData.TemperatureBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean.additional.elements.in.type:DA-END
} // end of java type