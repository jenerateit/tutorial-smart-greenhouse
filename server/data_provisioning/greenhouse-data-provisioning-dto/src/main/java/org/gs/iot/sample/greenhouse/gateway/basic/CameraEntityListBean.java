package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class CameraEntityListBean { // start of class

    private List<CameraEntityBean> cameraEntityList = new ArrayList<CameraEntityBean>();
    
    /**
     * creates an instance of CameraEntityListBean
     */
    public CameraEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean:DA-END
    }
    
    /**
     * getter for the field cameraEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="cameraEntityList")
    public List<CameraEntityBean> getCameraEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.getCameraEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.getCameraEntityList.List:DA-ELSE
        return this.cameraEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.getCameraEntityList.List:DA-END
    }
    /**
     * adder for the field cameraEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addCameraEntityList(CameraEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.addCameraEntityList.CameraEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.addCameraEntityList.CameraEntityBean:DA-ELSE
        this.cameraEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.addCameraEntityList.CameraEntityBean:DA-END
    }
    /**
     * remover for the field cameraEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeCameraEntityList(CameraEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.removeCameraEntityList.CameraEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.removeCameraEntityList.CameraEntityBean:DA-ELSE
        this.cameraEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.removeCameraEntityList.CameraEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean.additional.elements.in.type:DA-END
} // end of java type