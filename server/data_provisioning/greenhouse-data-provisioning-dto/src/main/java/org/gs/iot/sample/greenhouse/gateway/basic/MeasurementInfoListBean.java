package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class MeasurementInfoListBean { // start of class

    private List<MeasurementInfoBean> measurementInfoList = new ArrayList<MeasurementInfoBean>();
    
    /**
     * creates an instance of MeasurementInfoListBean
     */
    public MeasurementInfoListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean:DA-END
    }
    
    /**
     * getter for the field measurementInfoList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="measurementInfoList")
    public List<MeasurementInfoBean> getMeasurementInfoList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.getMeasurementInfoList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.getMeasurementInfoList.List:DA-ELSE
        return this.measurementInfoList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.getMeasurementInfoList.List:DA-END
    }
    /**
     * adder for the field measurementInfoList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addMeasurementInfoList(MeasurementInfoBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.addMeasurementInfoList.MeasurementInfoBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.addMeasurementInfoList.MeasurementInfoBean:DA-ELSE
        this.measurementInfoList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.addMeasurementInfoList.MeasurementInfoBean:DA-END
    }
    /**
     * remover for the field measurementInfoList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeMeasurementInfoList(MeasurementInfoBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.removeMeasurementInfoList.MeasurementInfoBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.removeMeasurementInfoList.MeasurementInfoBean:DA-ELSE
        this.measurementInfoList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.removeMeasurementInfoList.MeasurementInfoBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoListBean.additional.elements.in.type:DA-END
} // end of java type