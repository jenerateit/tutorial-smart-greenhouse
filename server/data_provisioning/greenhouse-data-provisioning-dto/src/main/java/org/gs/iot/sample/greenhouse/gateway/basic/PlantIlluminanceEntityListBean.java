package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantIlluminanceEntityListBean { // start of class

    private List<PlantIlluminanceEntityBean> plantIlluminanceEntityList = new ArrayList<PlantIlluminanceEntityBean>();
    
    /**
     * creates an instance of PlantIlluminanceEntityListBean
     */
    public PlantIlluminanceEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean:DA-END
    }
    
    /**
     * getter for the field plantIlluminanceEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantIlluminanceEntityList")
    public List<PlantIlluminanceEntityBean> getPlantIlluminanceEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.getPlantIlluminanceEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.getPlantIlluminanceEntityList.List:DA-ELSE
        return this.plantIlluminanceEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.getPlantIlluminanceEntityList.List:DA-END
    }
    /**
     * adder for the field plantIlluminanceEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantIlluminanceEntityList(PlantIlluminanceEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.addPlantIlluminanceEntityList.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.addPlantIlluminanceEntityList.PlantIlluminanceEntityBean:DA-ELSE
        this.plantIlluminanceEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.addPlantIlluminanceEntityList.PlantIlluminanceEntityBean:DA-END
    }
    /**
     * remover for the field plantIlluminanceEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantIlluminanceEntityList(PlantIlluminanceEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.removePlantIlluminanceEntityList.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.removePlantIlluminanceEntityList.PlantIlluminanceEntityBean:DA-ELSE
        this.plantIlluminanceEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.removePlantIlluminanceEntityList.PlantIlluminanceEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean.additional.elements.in.type:DA-END
} // end of java type