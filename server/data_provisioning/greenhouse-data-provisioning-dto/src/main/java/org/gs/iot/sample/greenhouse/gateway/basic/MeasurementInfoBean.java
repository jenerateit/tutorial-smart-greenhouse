package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class MeasurementInfoBean { // start of class

    private Date timeOfMeasurement;
    
    private String timeZoneOfMeasurement;
    
    /**
     * creates an instance of MeasurementInfoBean
     */
    public MeasurementInfoBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean:DA-END
    }
    
    /**
     * getter for the field timeOfMeasurement
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="timeOfMeasurement")
    public Date getTimeOfMeasurement() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.getTimeOfMeasurement.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.getTimeOfMeasurement.Date:DA-ELSE
        return this.timeOfMeasurement;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.getTimeOfMeasurement.Date:DA-END
    }
    /**
     * setter for the field timeOfMeasurement
     * 
     * 
     * 
     * @param timeOfMeasurement  the timeOfMeasurement
     */
    public void setTimeOfMeasurement(Date timeOfMeasurement) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.setTimeOfMeasurement.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.setTimeOfMeasurement.Date:DA-ELSE
        this.timeOfMeasurement = timeOfMeasurement;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.setTimeOfMeasurement.Date:DA-END
    }
    /**
     * getter for the field timeZoneOfMeasurement
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="timeZoneOfMeasurement")
    public String getTimeZoneOfMeasurement() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.getTimeZoneOfMeasurement.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.getTimeZoneOfMeasurement.String:DA-ELSE
        return this.timeZoneOfMeasurement;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.getTimeZoneOfMeasurement.String:DA-END
    }
    /**
     * setter for the field timeZoneOfMeasurement
     * 
     * 
     * 
     * @param timeZoneOfMeasurement  the timeZoneOfMeasurement
     */
    public void setTimeZoneOfMeasurement(String timeZoneOfMeasurement) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.setTimeZoneOfMeasurement.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.setTimeZoneOfMeasurement.String:DA-ELSE
        this.timeZoneOfMeasurement = timeZoneOfMeasurement;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.setTimeZoneOfMeasurement.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean.additional.elements.in.type:DA-END
} // end of java type