package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class LightEntityBean extends BaseEntityBean { // start of class

    private LightBean data;
    
    /**
     * creates an instance of LightEntityBean
     */
    public LightEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public LightBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.getData.LightBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.getData.LightBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.getData.LightBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(LightBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.setData.LightBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.setData.LightBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.setData.LightBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean.additional.elements.in.type:DA-END
} // end of java type