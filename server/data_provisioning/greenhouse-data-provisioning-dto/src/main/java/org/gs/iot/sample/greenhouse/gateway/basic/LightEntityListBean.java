package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class LightEntityListBean { // start of class

    private List<LightEntityBean> lightEntityList = new ArrayList<LightEntityBean>();
    
    /**
     * creates an instance of LightEntityListBean
     */
    public LightEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean:DA-END
    }
    
    /**
     * getter for the field lightEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="lightEntityList")
    public List<LightEntityBean> getLightEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.getLightEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.getLightEntityList.List:DA-ELSE
        return this.lightEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.getLightEntityList.List:DA-END
    }
    /**
     * adder for the field lightEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addLightEntityList(LightEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.addLightEntityList.LightEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.addLightEntityList.LightEntityBean:DA-ELSE
        this.lightEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.addLightEntityList.LightEntityBean:DA-END
    }
    /**
     * remover for the field lightEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeLightEntityList(LightEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.removeLightEntityList.LightEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.removeLightEntityList.LightEntityBean:DA-ELSE
        this.lightEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.removeLightEntityList.LightEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean.additional.elements.in.type:DA-END
} // end of java type