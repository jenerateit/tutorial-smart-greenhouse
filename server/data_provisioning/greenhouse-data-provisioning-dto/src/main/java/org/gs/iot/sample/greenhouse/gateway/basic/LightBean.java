package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'LightStrip'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class LightBean { // start of class

    private short r;
    
    private short g;
    
    private short b;
    
    private double brightness;
    
    /**
     * creates an instance of LightBean
     */
    public LightBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean:DA-END
    }
    
    /**
     * getter for the field r
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="r")
    public short getR() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getR.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getR.short:DA-ELSE
        return this.r;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getR.short:DA-END
    }
    /**
     * setter for the field r
     * 
     * 
     * 
     * @param r  the r
     */
    public void setR(short r) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setR.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setR.short:DA-ELSE
        this.r = r;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setR.short:DA-END
    }
    /**
     * getter for the field g
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="g")
    public short getG() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getG.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getG.short:DA-ELSE
        return this.g;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getG.short:DA-END
    }
    /**
     * setter for the field g
     * 
     * 
     * 
     * @param g  the g
     */
    public void setG(short g) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setG.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setG.short:DA-ELSE
        this.g = g;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setG.short:DA-END
    }
    /**
     * getter for the field b
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="b")
    public short getB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getB.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getB.short:DA-ELSE
        return this.b;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getB.short:DA-END
    }
    /**
     * setter for the field b
     * 
     * 
     * 
     * @param b  the b
     */
    public void setB(short b) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setB.short:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setB.short:DA-ELSE
        this.b = b;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setB.short:DA-END
    }
    /**
     * getter for the field brightness
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="brightness")
    public double getBrightness() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getBrightness.double:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getBrightness.double:DA-ELSE
        return this.brightness;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.getBrightness.double:DA-END
    }
    /**
     * setter for the field brightness
     * 
     * 
     * 
     * @param brightness  the brightness
     */
    public void setBrightness(double brightness) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setBrightness.double:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setBrightness.double:DA-ELSE
        this.brightness = brightness;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.setBrightness.double:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightBean.additional.elements.in.type:DA-END
} // end of java type