package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'RpiCamera'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class CameraBean { // start of class

    private String mimetype;
    
    private String image;
    
    /**
     * creates an instance of CameraBean
     */
    public CameraBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean:DA-END
    }
    
    /**
     * getter for the field mimetype
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="mimetype")
    public String getMimetype() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.getMimetype.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.getMimetype.String:DA-ELSE
        return this.mimetype;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.getMimetype.String:DA-END
    }
    /**
     * setter for the field mimetype
     * 
     * 
     * 
     * @param mimetype  the mimetype
     */
    public void setMimetype(String mimetype) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.setMimetype.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.setMimetype.String:DA-ELSE
        this.mimetype = mimetype;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.setMimetype.String:DA-END
    }
    /**
     * getter for the field image
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="image")
    public String getImage() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.getImage.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.getImage.String:DA-ELSE
        return this.image;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.getImage.String:DA-END
    }
    /**
     * setter for the field image
     * 
     * 
     * 
     * @param image  the image
     */
    public void setImage(String image) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.setImage.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.setImage.String:DA-ELSE
        this.image = image;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.setImage.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraBean.additional.elements.in.type:DA-END
} // end of java type