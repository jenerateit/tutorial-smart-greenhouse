package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class CameraEntityBean extends BaseEntityBean { // start of class

    private CameraBean data;
    
    /**
     * creates an instance of CameraEntityBean
     */
    public CameraEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public CameraBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.getData.CameraBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.getData.CameraBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.getData.CameraBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(CameraBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.setData.CameraBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.setData.CameraBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.setData.CameraBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean.additional.elements.in.type:DA-END
} // end of java type