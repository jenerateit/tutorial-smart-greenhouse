package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class GeoCoordinatesBean { // start of class

    private int lng;
    
    private int lat;
    
    private int alt;
    
    /**
     * creates an instance of GeoCoordinatesBean
     */
    public GeoCoordinatesBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean:DA-END
    }
    
    /**
     * getter for the field lng
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="lng")
    public int getLng() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getLng.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getLng.int:DA-ELSE
        return this.lng;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getLng.int:DA-END
    }
    /**
     * setter for the field lng
     * 
     * 
     * 
     * @param lng  the lng
     */
    public void setLng(int lng) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setLng.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setLng.int:DA-ELSE
        this.lng = lng;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setLng.int:DA-END
    }
    /**
     * getter for the field lat
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="lat")
    public int getLat() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getLat.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getLat.int:DA-ELSE
        return this.lat;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getLat.int:DA-END
    }
    /**
     * setter for the field lat
     * 
     * 
     * 
     * @param lat  the lat
     */
    public void setLat(int lat) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setLat.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setLat.int:DA-ELSE
        this.lat = lat;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setLat.int:DA-END
    }
    /**
     * getter for the field alt
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="alt")
    public int getAlt() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getAlt.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getAlt.int:DA-ELSE
        return this.alt;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.getAlt.int:DA-END
    }
    /**
     * setter for the field alt
     * 
     * 
     * 
     * @param alt  the alt
     */
    public void setAlt(int alt) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setAlt.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setAlt.int:DA-ELSE
        this.alt = alt;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.setAlt.int:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean.additional.elements.in.type:DA-END
} // end of java type