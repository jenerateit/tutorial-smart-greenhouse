package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class BaseEntityBean { // start of class

    private Long pk;
    
    /**
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * For example, access control lists can be defined so that users
     * are only given access to the child topics of a given account_name.
     */
    private String accountName;
    
    /**
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway?s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     */
    private String clientId;
    
    /**
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ?CONF-V1?,
     * ?CONF-V2?, etc.)
     */
    private String appId;
    
    /**
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
     * identify a temperature sensor and ?sensor/hum? a humidity sensor.
     */
    private String resourceId;
    
    /**
     * Any client that publishes messages can include geo coordinates in it.
     * Those coordinates will be stored here. Otherwise, the geo coordinates
     * are left empty.
     */
    private GeoCoordinatesBean geoCoordinates;
    
    private MeasurementInfoBean measurementInfo;
    
    private Date timeOfMessageReception;
    
    /**
     * creates an instance of BaseEntityBean
     */
    public BaseEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean:DA-END
    }
    
    /**
     * getter for the field pk
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="pk")
    public Long getPk() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getPk.Long:DA-ELSE
        return this.pk;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getPk.Long:DA-END
    }
    /**
     * setter for the field pk
     * 
     * 
     * 
     * @param pk  the pk
     */
    public void setPk(Long pk) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setPk.Long:DA-ELSE
        this.pk = pk;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setPk.Long:DA-END
    }
    /**
     * getter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * For example, access control lists can be defined so that users
     * are only given access to the child topics of a given account_name.
     * 
     * 
     * @return
     */
    @XmlElement(name="accountName")
    public String getAccountName() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getAccountName.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getAccountName.String:DA-ELSE
        return this.accountName;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getAccountName.String:DA-END
    }
    /**
     * setter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * For example, access control lists can be defined so that users
     * are only given access to the child topics of a given account_name.
     * 
     * 
     * @param accountName  the accountName
     */
    public void setAccountName(String accountName) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setAccountName.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setAccountName.String:DA-ELSE
        this.accountName = accountName;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setAccountName.String:DA-END
    }
    /**
     * getter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway?s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @return
     */
    @XmlElement(name="clientId")
    public String getClientId() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getClientId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getClientId.String:DA-ELSE
        return this.clientId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getClientId.String:DA-END
    }
    /**
     * setter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway?s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @param clientId  the clientId
     */
    public void setClientId(String clientId) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setClientId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setClientId.String:DA-ELSE
        this.clientId = clientId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setClientId.String:DA-END
    }
    /**
     * getter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ?CONF-V1?,
     * ?CONF-V2?, etc.)
     * 
     * 
     * @return
     */
    @XmlElement(name="appId")
    public String getAppId() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getAppId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getAppId.String:DA-ELSE
        return this.appId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getAppId.String:DA-END
    }
    /**
     * setter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ?CONF-V1?,
     * ?CONF-V2?, etc.)
     * 
     * 
     * @param appId  the appId
     */
    public void setAppId(String appId) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setAppId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setAppId.String:DA-ELSE
        this.appId = appId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setAppId.String:DA-END
    }
    /**
     * getter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
     * identify a temperature sensor and ?sensor/hum? a humidity sensor.
     * 
     * 
     * @return
     */
    @XmlElement(name="resourceId")
    public String getResourceId() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getResourceId.String:DA-ELSE
        return this.resourceId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getResourceId.String:DA-END
    }
    /**
     * setter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ?sensors/temp? may
     * identify a temperature sensor and ?sensor/hum? a humidity sensor.
     * 
     * 
     * @param resourceId  the resourceId
     */
    public void setResourceId(String resourceId) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setResourceId.String:DA-ELSE
        this.resourceId = resourceId;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setResourceId.String:DA-END
    }
    /**
     * getter for the field geoCoordinates
     * 
     * 
     * Any client that publishes messages can include geo coordinates in it.
     * Those coordinates will be stored here. Otherwise, the geo coordinates
     * are left empty.
     * 
     * 
     * @return
     */
    @XmlElement(name="geoCoordinates")
    public GeoCoordinatesBean getGeoCoordinates() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getGeoCoordinates.GeoCoordinatesBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getGeoCoordinates.GeoCoordinatesBean:DA-ELSE
        return this.geoCoordinates;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getGeoCoordinates.GeoCoordinatesBean:DA-END
    }
    /**
     * setter for the field geoCoordinates
     * 
     * 
     * Any client that publishes messages can include geo coordinates in it.
     * Those coordinates will be stored here. Otherwise, the geo coordinates
     * are left empty.
     * 
     * 
     * @param geoCoordinates  the geoCoordinates
     */
    public void setGeoCoordinates(GeoCoordinatesBean geoCoordinates) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setGeoCoordinates.GeoCoordinatesBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setGeoCoordinates.GeoCoordinatesBean:DA-ELSE
        this.geoCoordinates = geoCoordinates;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setGeoCoordinates.GeoCoordinatesBean:DA-END
    }
    /**
     * getter for the field measurementInfo
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="measurementInfo")
    public MeasurementInfoBean getMeasurementInfo() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getMeasurementInfo.MeasurementInfoBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getMeasurementInfo.MeasurementInfoBean:DA-ELSE
        return this.measurementInfo;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getMeasurementInfo.MeasurementInfoBean:DA-END
    }
    /**
     * setter for the field measurementInfo
     * 
     * 
     * 
     * @param measurementInfo  the measurementInfo
     */
    public void setMeasurementInfo(MeasurementInfoBean measurementInfo) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setMeasurementInfo.MeasurementInfoBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setMeasurementInfo.MeasurementInfoBean:DA-ELSE
        this.measurementInfo = measurementInfo;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setMeasurementInfo.MeasurementInfoBean:DA-END
    }
    /**
     * getter for the field timeOfMessageReception
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="timeOfMessageReception")
    public Date getTimeOfMessageReception() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getTimeOfMessageReception.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getTimeOfMessageReception.Date:DA-ELSE
        return this.timeOfMessageReception;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.getTimeOfMessageReception.Date:DA-END
    }
    /**
     * setter for the field timeOfMessageReception
     * 
     * 
     * 
     * @param timeOfMessageReception  the timeOfMessageReception
     */
    public void setTimeOfMessageReception(Date timeOfMessageReception) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setTimeOfMessageReception.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setTimeOfMessageReception.Date:DA-ELSE
        this.timeOfMessageReception = timeOfMessageReception;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.setTimeOfMessageReception.Date:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.BaseEntityBean.additional.elements.in.type:DA-END
} // end of java type