package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'TemperatureSensorGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class TemperatureListBean { // start of class

    private List<TemperatureBean> temperatureList = new ArrayList<TemperatureBean>();
    
    /**
     * creates an instance of TemperatureListBean
     */
    public TemperatureListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean:DA-END
    }
    
    /**
     * getter for the field temperatureList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="temperatureList")
    public List<TemperatureBean> getTemperatureList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.getTemperatureList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.getTemperatureList.List:DA-ELSE
        return this.temperatureList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.getTemperatureList.List:DA-END
    }
    /**
     * adder for the field temperatureList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addTemperatureList(TemperatureBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.addTemperatureList.TemperatureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.addTemperatureList.TemperatureBean:DA-ELSE
        this.temperatureList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.addTemperatureList.TemperatureBean:DA-END
    }
    /**
     * remover for the field temperatureList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeTemperatureList(TemperatureBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.removeTemperatureList.TemperatureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.removeTemperatureList.TemperatureBean:DA-ELSE
        this.temperatureList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.removeTemperatureList.TemperatureBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureListBean.additional.elements.in.type:DA-END
} // end of java type