package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantMotionEntityBean extends BaseEntityBean { // start of class

    private PlantMotionBean data;
    
    /**
     * creates an instance of PlantMotionEntityBean
     */
    public PlantMotionEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public PlantMotionBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.getData.PlantMotionBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.getData.PlantMotionBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.getData.PlantMotionBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(PlantMotionBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.setData.PlantMotionBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.setData.PlantMotionBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.setData.PlantMotionBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean.additional.elements.in.type:DA-END
} // end of java type