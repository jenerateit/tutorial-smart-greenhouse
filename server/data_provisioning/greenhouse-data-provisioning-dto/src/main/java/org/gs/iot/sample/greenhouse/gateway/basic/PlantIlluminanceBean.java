package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'DigitalLightTSL2561Grove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantIlluminanceBean { // start of class

    private float status;
    
    /**
     * creates an instance of PlantIlluminanceBean
     */
    public PlantIlluminanceBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="status")
    public float getStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.getStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.getStatus.float:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.getStatus.float:DA-END
    }
    /**
     * setter for the field status
     * 
     * 
     * 
     * @param status  the status
     */
    public void setStatus(float status) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.setStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.setStatus.float:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.setStatus.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean.additional.elements.in.type:DA-END
} // end of java type