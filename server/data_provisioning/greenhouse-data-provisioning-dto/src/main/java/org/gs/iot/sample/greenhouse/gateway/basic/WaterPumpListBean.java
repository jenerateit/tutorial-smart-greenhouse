package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'RelayGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class WaterPumpListBean { // start of class

    private List<WaterPumpBean> waterPumpList = new ArrayList<WaterPumpBean>();
    
    /**
     * creates an instance of WaterPumpListBean
     */
    public WaterPumpListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean:DA-END
    }
    
    /**
     * getter for the field waterPumpList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="waterPumpList")
    public List<WaterPumpBean> getWaterPumpList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.getWaterPumpList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.getWaterPumpList.List:DA-ELSE
        return this.waterPumpList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.getWaterPumpList.List:DA-END
    }
    /**
     * adder for the field waterPumpList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addWaterPumpList(WaterPumpBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.addWaterPumpList.WaterPumpBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.addWaterPumpList.WaterPumpBean:DA-ELSE
        this.waterPumpList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.addWaterPumpList.WaterPumpBean:DA-END
    }
    /**
     * remover for the field waterPumpList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeWaterPumpList(WaterPumpBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.removeWaterPumpList.WaterPumpBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.removeWaterPumpList.WaterPumpBean:DA-ELSE
        this.waterPumpList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.removeWaterPumpList.WaterPumpBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpListBean.additional.elements.in.type:DA-END
} // end of java type