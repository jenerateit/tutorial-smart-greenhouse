package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class Mq2GasEntityListBean { // start of class

    private List<Mq2GasEntityBean> mq2GasEntityList = new ArrayList<Mq2GasEntityBean>();
    
    /**
     * creates an instance of Mq2GasEntityListBean
     */
    public Mq2GasEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean:DA-END
    }
    
    /**
     * getter for the field mq2GasEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="mq2GasEntityList")
    public List<Mq2GasEntityBean> getMq2GasEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.getMq2GasEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.getMq2GasEntityList.List:DA-ELSE
        return this.mq2GasEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.getMq2GasEntityList.List:DA-END
    }
    /**
     * adder for the field mq2GasEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addMq2GasEntityList(Mq2GasEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.addMq2GasEntityList.Mq2GasEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.addMq2GasEntityList.Mq2GasEntityBean:DA-ELSE
        this.mq2GasEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.addMq2GasEntityList.Mq2GasEntityBean:DA-END
    }
    /**
     * remover for the field mq2GasEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeMq2GasEntityList(Mq2GasEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.removeMq2GasEntityList.Mq2GasEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.removeMq2GasEntityList.Mq2GasEntityBean:DA-ELSE
        this.mq2GasEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.removeMq2GasEntityList.Mq2GasEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean.additional.elements.in.type:DA-END
} // end of java type