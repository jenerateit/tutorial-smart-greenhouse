package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'AdafruitI2cRgbLcd'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class DisplayListBean { // start of class

    private List<DisplayBean> displayList = new ArrayList<DisplayBean>();
    
    /**
     * creates an instance of DisplayListBean
     */
    public DisplayListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean:DA-END
    }
    
    /**
     * getter for the field displayList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="displayList")
    public List<DisplayBean> getDisplayList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.getDisplayList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.getDisplayList.List:DA-ELSE
        return this.displayList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.getDisplayList.List:DA-END
    }
    /**
     * adder for the field displayList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addDisplayList(DisplayBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.addDisplayList.DisplayBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.addDisplayList.DisplayBean:DA-ELSE
        this.displayList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.addDisplayList.DisplayBean:DA-END
    }
    /**
     * remover for the field displayList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeDisplayList(DisplayBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.removeDisplayList.DisplayBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.removeDisplayList.DisplayBean:DA-ELSE
        this.displayList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.removeDisplayList.DisplayBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayListBean.additional.elements.in.type:DA-END
} // end of java type