package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantMotionEntityListBean { // start of class

    private List<PlantMotionEntityBean> plantMotionEntityList = new ArrayList<PlantMotionEntityBean>();
    
    /**
     * creates an instance of PlantMotionEntityListBean
     */
    public PlantMotionEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean:DA-END
    }
    
    /**
     * getter for the field plantMotionEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantMotionEntityList")
    public List<PlantMotionEntityBean> getPlantMotionEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.getPlantMotionEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.getPlantMotionEntityList.List:DA-ELSE
        return this.plantMotionEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.getPlantMotionEntityList.List:DA-END
    }
    /**
     * adder for the field plantMotionEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantMotionEntityList(PlantMotionEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.addPlantMotionEntityList.PlantMotionEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.addPlantMotionEntityList.PlantMotionEntityBean:DA-ELSE
        this.plantMotionEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.addPlantMotionEntityList.PlantMotionEntityBean:DA-END
    }
    /**
     * remover for the field plantMotionEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantMotionEntityList(PlantMotionEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.removePlantMotionEntityList.PlantMotionEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.removePlantMotionEntityList.PlantMotionEntityBean:DA-ELSE
        this.plantMotionEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.removePlantMotionEntityList.PlantMotionEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean.additional.elements.in.type:DA-END
} // end of java type