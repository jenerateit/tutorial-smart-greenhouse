package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'PIRMotionGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantMotionBean { // start of class

    private boolean status;
    
    /**
     * creates an instance of PlantMotionBean
     */
    public PlantMotionBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean:DA-END
    }
    
    /**
     * getter for the field status
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="status")
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.isStatus.boolean:DA-END
    }
    /**
     * setter for the field status
     * 
     * 
     * 
     * @param status  the status
     */
    public void setStatus(boolean status) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.setStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.setStatus.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.setStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean.additional.elements.in.type:DA-END
} // end of java type