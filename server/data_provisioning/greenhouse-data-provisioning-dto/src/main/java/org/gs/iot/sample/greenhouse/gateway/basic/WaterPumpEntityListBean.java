package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class WaterPumpEntityListBean { // start of class

    private List<WaterPumpEntityBean> waterPumpEntityList = new ArrayList<WaterPumpEntityBean>();
    
    /**
     * creates an instance of WaterPumpEntityListBean
     */
    public WaterPumpEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean:DA-END
    }
    
    /**
     * getter for the field waterPumpEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="waterPumpEntityList")
    public List<WaterPumpEntityBean> getWaterPumpEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.getWaterPumpEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.getWaterPumpEntityList.List:DA-ELSE
        return this.waterPumpEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.getWaterPumpEntityList.List:DA-END
    }
    /**
     * adder for the field waterPumpEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addWaterPumpEntityList(WaterPumpEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.addWaterPumpEntityList.WaterPumpEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.addWaterPumpEntityList.WaterPumpEntityBean:DA-ELSE
        this.waterPumpEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.addWaterPumpEntityList.WaterPumpEntityBean:DA-END
    }
    /**
     * remover for the field waterPumpEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeWaterPumpEntityList(WaterPumpEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.removeWaterPumpEntityList.WaterPumpEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.removeWaterPumpEntityList.WaterPumpEntityBean:DA-ELSE
        this.waterPumpEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.removeWaterPumpEntityList.WaterPumpEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean.additional.elements.in.type:DA-END
} // end of java type