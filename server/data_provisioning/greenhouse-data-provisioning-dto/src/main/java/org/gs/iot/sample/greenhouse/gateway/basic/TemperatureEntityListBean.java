package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class TemperatureEntityListBean { // start of class

    private List<TemperatureEntityBean> temperatureEntityList = new ArrayList<TemperatureEntityBean>();
    
    /**
     * creates an instance of TemperatureEntityListBean
     */
    public TemperatureEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean:DA-END
    }
    
    /**
     * getter for the field temperatureEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="temperatureEntityList")
    public List<TemperatureEntityBean> getTemperatureEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.getTemperatureEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.getTemperatureEntityList.List:DA-ELSE
        return this.temperatureEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.getTemperatureEntityList.List:DA-END
    }
    /**
     * adder for the field temperatureEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addTemperatureEntityList(TemperatureEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.addTemperatureEntityList.TemperatureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.addTemperatureEntityList.TemperatureEntityBean:DA-ELSE
        this.temperatureEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.addTemperatureEntityList.TemperatureEntityBean:DA-END
    }
    /**
     * remover for the field temperatureEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeTemperatureEntityList(TemperatureEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.removeTemperatureEntityList.TemperatureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.removeTemperatureEntityList.TemperatureEntityBean:DA-ELSE
        this.temperatureEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.removeTemperatureEntityList.TemperatureEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityListBean.additional.elements.in.type:DA-END
} // end of java type