package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class AlarmEntityListBean { // start of class

    private List<AlarmEntityBean> alarmEntityList = new ArrayList<AlarmEntityBean>();
    
    /**
     * creates an instance of AlarmEntityListBean
     */
    public AlarmEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean:DA-END
    }
    
    /**
     * getter for the field alarmEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="alarmEntityList")
    public List<AlarmEntityBean> getAlarmEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.getAlarmEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.getAlarmEntityList.List:DA-ELSE
        return this.alarmEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.getAlarmEntityList.List:DA-END
    }
    /**
     * adder for the field alarmEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addAlarmEntityList(AlarmEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.addAlarmEntityList.AlarmEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.addAlarmEntityList.AlarmEntityBean:DA-ELSE
        this.alarmEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.addAlarmEntityList.AlarmEntityBean:DA-END
    }
    /**
     * remover for the field alarmEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeAlarmEntityList(AlarmEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.removeAlarmEntityList.AlarmEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.removeAlarmEntityList.AlarmEntityBean:DA-ELSE
        this.alarmEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.removeAlarmEntityList.AlarmEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean.additional.elements.in.type:DA-END
} // end of java type