package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantBarometerEntityListBean { // start of class

    private List<PlantBarometerEntityBean> plantBarometerEntityList = new ArrayList<PlantBarometerEntityBean>();
    
    /**
     * creates an instance of PlantBarometerEntityListBean
     */
    public PlantBarometerEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean:DA-END
    }
    
    /**
     * getter for the field plantBarometerEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantBarometerEntityList")
    public List<PlantBarometerEntityBean> getPlantBarometerEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.getPlantBarometerEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.getPlantBarometerEntityList.List:DA-ELSE
        return this.plantBarometerEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.getPlantBarometerEntityList.List:DA-END
    }
    /**
     * adder for the field plantBarometerEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantBarometerEntityList(PlantBarometerEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.addPlantBarometerEntityList.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.addPlantBarometerEntityList.PlantBarometerEntityBean:DA-ELSE
        this.plantBarometerEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.addPlantBarometerEntityList.PlantBarometerEntityBean:DA-END
    }
    /**
     * remover for the field plantBarometerEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantBarometerEntityList(PlantBarometerEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.removePlantBarometerEntityList.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.removePlantBarometerEntityList.PlantBarometerEntityBean:DA-ELSE
        this.plantBarometerEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.removePlantBarometerEntityList.PlantBarometerEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean.additional.elements.in.type:DA-END
} // end of java type