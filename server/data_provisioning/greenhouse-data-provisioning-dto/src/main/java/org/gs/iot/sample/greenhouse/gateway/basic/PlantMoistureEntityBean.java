package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantMoistureEntityBean extends BaseEntityBean { // start of class

    private PlantMoistureBean data;
    
    /**
     * creates an instance of PlantMoistureEntityBean
     */
    public PlantMoistureEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public PlantMoistureBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.getData.PlantMoistureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.getData.PlantMoistureBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.getData.PlantMoistureBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(PlantMoistureBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.setData.PlantMoistureBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.setData.PlantMoistureBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.setData.PlantMoistureBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean.additional.elements.in.type:DA-END
} // end of java type