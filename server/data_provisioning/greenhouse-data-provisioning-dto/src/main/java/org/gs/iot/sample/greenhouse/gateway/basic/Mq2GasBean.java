package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'Mq2GasSensorGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class Mq2GasBean { // start of class

    private float rsAir;
    
    private float r0;
    
    private float rsGas;
    
    private float ratio;
    
    /**
     * creates an instance of Mq2GasBean
     */
    public Mq2GasBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean:DA-END
    }
    
    /**
     * getter for the field rsAir
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="rsAir")
    public float getRsAir() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRsAir.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRsAir.float:DA-ELSE
        return this.rsAir;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRsAir.float:DA-END
    }
    /**
     * setter for the field rsAir
     * 
     * 
     * 
     * @param rsAir  the rsAir
     */
    public void setRsAir(float rsAir) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRsAir.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRsAir.float:DA-ELSE
        this.rsAir = rsAir;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRsAir.float:DA-END
    }
    /**
     * getter for the field r0
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="r0")
    public float getr0() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getr0.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getr0.float:DA-ELSE
        return this.r0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getr0.float:DA-END
    }
    /**
     * setter for the field r0
     * 
     * 
     * 
     * @param r0  the r0
     */
    public void setr0(float r0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setr0.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setr0.float:DA-ELSE
        this.r0 = r0;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setr0.float:DA-END
    }
    /**
     * getter for the field rsGas
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="rsGas")
    public float getRsGas() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRsGas.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRsGas.float:DA-ELSE
        return this.rsGas;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRsGas.float:DA-END
    }
    /**
     * setter for the field rsGas
     * 
     * 
     * 
     * @param rsGas  the rsGas
     */
    public void setRsGas(float rsGas) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRsGas.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRsGas.float:DA-ELSE
        this.rsGas = rsGas;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRsGas.float:DA-END
    }
    /**
     * getter for the field ratio
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="ratio")
    public float getRatio() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRatio.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRatio.float:DA-ELSE
        return this.ratio;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.getRatio.float:DA-END
    }
    /**
     * setter for the field ratio
     * 
     * 
     * 
     * @param ratio  the ratio
     */
    public void setRatio(float ratio) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRatio.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRatio.float:DA-ELSE
        this.ratio = ratio;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.setRatio.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean.additional.elements.in.type:DA-END
} // end of java type