package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'LightStrip'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class LightListBean { // start of class

    private List<LightBean> lightList = new ArrayList<LightBean>();
    
    /**
     * creates an instance of LightListBean
     */
    public LightListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean:DA-END
    }
    
    /**
     * getter for the field lightList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="lightList")
    public List<LightBean> getLightList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.getLightList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.getLightList.List:DA-ELSE
        return this.lightList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.getLightList.List:DA-END
    }
    /**
     * adder for the field lightList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addLightList(LightBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.addLightList.LightBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.addLightList.LightBean:DA-ELSE
        this.lightList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.addLightList.LightBean:DA-END
    }
    /**
     * remover for the field lightList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeLightList(LightBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.removeLightList.LightBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.removeLightList.LightBean:DA-ELSE
        this.lightList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.removeLightList.LightBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.LightListBean.additional.elements.in.type:DA-END
} // end of java type