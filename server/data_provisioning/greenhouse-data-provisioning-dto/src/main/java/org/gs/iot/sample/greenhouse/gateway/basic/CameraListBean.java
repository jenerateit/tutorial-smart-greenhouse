package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'RpiCamera'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class CameraListBean { // start of class

    private List<CameraBean> cameraList = new ArrayList<CameraBean>();
    
    /**
     * creates an instance of CameraListBean
     */
    public CameraListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean:DA-END
    }
    
    /**
     * getter for the field cameraList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="cameraList")
    public List<CameraBean> getCameraList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.getCameraList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.getCameraList.List:DA-ELSE
        return this.cameraList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.getCameraList.List:DA-END
    }
    /**
     * adder for the field cameraList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addCameraList(CameraBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.addCameraList.CameraBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.addCameraList.CameraBean:DA-ELSE
        this.cameraList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.addCameraList.CameraBean:DA-END
    }
    /**
     * remover for the field cameraList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeCameraList(CameraBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.removeCameraList.CameraBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.removeCameraList.CameraBean:DA-ELSE
        this.cameraList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.removeCameraList.CameraBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.CameraListBean.additional.elements.in.type:DA-END
} // end of java type