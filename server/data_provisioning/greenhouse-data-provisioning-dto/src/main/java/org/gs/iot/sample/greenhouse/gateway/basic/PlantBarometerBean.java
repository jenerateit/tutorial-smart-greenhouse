package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'BarometerBMP180Grove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantBarometerBean { // start of class

    private float pressure;
    
    private float temperature;
    
    private float altitude;
    
    /**
     * creates an instance of PlantBarometerBean
     */
    public PlantBarometerBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean:DA-END
    }
    
    /**
     * getter for the field pressure
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="pressure")
    public float getPressure() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getPressure.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getPressure.float:DA-ELSE
        return this.pressure;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getPressure.float:DA-END
    }
    /**
     * setter for the field pressure
     * 
     * 
     * 
     * @param pressure  the pressure
     */
    public void setPressure(float pressure) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setPressure.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setPressure.float:DA-ELSE
        this.pressure = pressure;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setPressure.float:DA-END
    }
    /**
     * getter for the field temperature
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="temperature")
    public float getTemperature() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getTemperature.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getTemperature.float:DA-ELSE
        return this.temperature;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getTemperature.float:DA-END
    }
    /**
     * setter for the field temperature
     * 
     * 
     * 
     * @param temperature  the temperature
     */
    public void setTemperature(float temperature) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setTemperature.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setTemperature.float:DA-ELSE
        this.temperature = temperature;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setTemperature.float:DA-END
    }
    /**
     * getter for the field altitude
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="altitude")
    public float getAltitude() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getAltitude.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getAltitude.float:DA-ELSE
        return this.altitude;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.getAltitude.float:DA-END
    }
    /**
     * setter for the field altitude
     * 
     * 
     * 
     * @param altitude  the altitude
     */
    public void setAltitude(float altitude) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setAltitude.float:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setAltitude.float:DA-ELSE
        this.altitude = altitude;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.setAltitude.float:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean.additional.elements.in.type:DA-END
} // end of java type