package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class WaterPumpEntityBean extends BaseEntityBean { // start of class

    private WaterPumpBean data;
    
    /**
     * creates an instance of WaterPumpEntityBean
     */
    public WaterPumpEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public WaterPumpBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.getData.WaterPumpBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.getData.WaterPumpBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.getData.WaterPumpBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(WaterPumpBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.setData.WaterPumpBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.setData.WaterPumpBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.setData.WaterPumpBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean.additional.elements.in.type:DA-END
} // end of java type