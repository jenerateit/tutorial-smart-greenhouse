package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'Mq2GasSensorGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class Mq2GasListBean { // start of class

    private List<Mq2GasBean> mq2GasList = new ArrayList<Mq2GasBean>();
    
    /**
     * creates an instance of Mq2GasListBean
     */
    public Mq2GasListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean:DA-END
    }
    
    /**
     * getter for the field mq2GasList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="mq2GasList")
    public List<Mq2GasBean> getMq2GasList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.getMq2GasList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.getMq2GasList.List:DA-ELSE
        return this.mq2GasList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.getMq2GasList.List:DA-END
    }
    /**
     * adder for the field mq2GasList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addMq2GasList(Mq2GasBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.addMq2GasList.Mq2GasBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.addMq2GasList.Mq2GasBean:DA-ELSE
        this.mq2GasList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.addMq2GasList.Mq2GasBean:DA-END
    }
    /**
     * remover for the field mq2GasList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeMq2GasList(Mq2GasBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.removeMq2GasList.Mq2GasBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.removeMq2GasList.Mq2GasBean:DA-ELSE
        this.mq2GasList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.removeMq2GasList.Mq2GasBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasListBean.additional.elements.in.type:DA-END
} // end of java type