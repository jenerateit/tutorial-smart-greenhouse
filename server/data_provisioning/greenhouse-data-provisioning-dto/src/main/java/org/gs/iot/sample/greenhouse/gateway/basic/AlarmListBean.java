package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'BuzzerGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class AlarmListBean { // start of class

    private List<AlarmBean> alarmList = new ArrayList<AlarmBean>();
    
    /**
     * creates an instance of AlarmListBean
     */
    public AlarmListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean:DA-END
    }
    
    /**
     * getter for the field alarmList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="alarmList")
    public List<AlarmBean> getAlarmList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.getAlarmList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.getAlarmList.List:DA-ELSE
        return this.alarmList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.getAlarmList.List:DA-END
    }
    /**
     * adder for the field alarmList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addAlarmList(AlarmBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.addAlarmList.AlarmBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.addAlarmList.AlarmBean:DA-ELSE
        this.alarmList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.addAlarmList.AlarmBean:DA-END
    }
    /**
     * remover for the field alarmList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeAlarmList(AlarmBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.removeAlarmList.AlarmBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.removeAlarmList.AlarmBean:DA-ELSE
        this.alarmList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.removeAlarmList.AlarmBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.AlarmListBean.additional.elements.in.type:DA-END
} // end of java type