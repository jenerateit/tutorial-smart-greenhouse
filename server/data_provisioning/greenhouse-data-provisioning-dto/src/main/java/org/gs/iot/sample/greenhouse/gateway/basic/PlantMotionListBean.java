package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'PIRMotionGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantMotionListBean { // start of class

    private List<PlantMotionBean> plantMotionList = new ArrayList<PlantMotionBean>();
    
    /**
     * creates an instance of PlantMotionListBean
     */
    public PlantMotionListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean:DA-END
    }
    
    /**
     * getter for the field plantMotionList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantMotionList")
    public List<PlantMotionBean> getPlantMotionList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.getPlantMotionList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.getPlantMotionList.List:DA-ELSE
        return this.plantMotionList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.getPlantMotionList.List:DA-END
    }
    /**
     * adder for the field plantMotionList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantMotionList(PlantMotionBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.addPlantMotionList.PlantMotionBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.addPlantMotionList.PlantMotionBean:DA-ELSE
        this.plantMotionList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.addPlantMotionList.PlantMotionBean:DA-END
    }
    /**
     * remover for the field plantMotionList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantMotionList(PlantMotionBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.removePlantMotionList.PlantMotionBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.removePlantMotionList.PlantMotionBean:DA-ELSE
        this.plantMotionList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.removePlantMotionList.PlantMotionBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionListBean.additional.elements.in.type:DA-END
} // end of java type