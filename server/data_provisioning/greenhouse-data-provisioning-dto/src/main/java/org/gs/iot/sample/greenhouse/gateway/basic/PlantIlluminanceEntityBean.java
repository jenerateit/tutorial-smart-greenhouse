package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantIlluminanceEntityBean extends BaseEntityBean { // start of class

    private PlantIlluminanceBean data;
    
    /**
     * creates an instance of PlantIlluminanceEntityBean
     */
    public PlantIlluminanceEntityBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean:DA-END
    }
    
    /**
     * getter for the field data
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="data")
    public PlantIlluminanceBean getData() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.getData.PlantIlluminanceBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.getData.PlantIlluminanceBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.getData.PlantIlluminanceBean:DA-END
    }
    /**
     * setter for the field data
     * 
     * 
     * 
     * @param data  the data
     */
    public void setData(PlantIlluminanceBean data) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.setData.PlantIlluminanceBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.setData.PlantIlluminanceBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.setData.PlantIlluminanceBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean.additional.elements.in.type:DA-END
} // end of java type