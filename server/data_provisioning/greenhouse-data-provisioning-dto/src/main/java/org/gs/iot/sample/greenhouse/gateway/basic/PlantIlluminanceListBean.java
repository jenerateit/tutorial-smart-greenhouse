package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * persistent data structure for hardware of type 'DigitalLightTSL2561Grove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class PlantIlluminanceListBean { // start of class

    private List<PlantIlluminanceBean> plantIlluminanceList = new ArrayList<PlantIlluminanceBean>();
    
    /**
     * creates an instance of PlantIlluminanceListBean
     */
    public PlantIlluminanceListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean:DA-END
    }
    
    /**
     * getter for the field plantIlluminanceList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="plantIlluminanceList")
    public List<PlantIlluminanceBean> getPlantIlluminanceList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.getPlantIlluminanceList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.getPlantIlluminanceList.List:DA-ELSE
        return this.plantIlluminanceList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.getPlantIlluminanceList.List:DA-END
    }
    /**
     * adder for the field plantIlluminanceList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addPlantIlluminanceList(PlantIlluminanceBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.addPlantIlluminanceList.PlantIlluminanceBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.addPlantIlluminanceList.PlantIlluminanceBean:DA-ELSE
        this.plantIlluminanceList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.addPlantIlluminanceList.PlantIlluminanceBean:DA-END
    }
    /**
     * remover for the field plantIlluminanceList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removePlantIlluminanceList(PlantIlluminanceBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.removePlantIlluminanceList.PlantIlluminanceBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.removePlantIlluminanceList.PlantIlluminanceBean:DA-ELSE
        this.plantIlluminanceList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.removePlantIlluminanceList.PlantIlluminanceBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceListBean.additional.elements.in.type:DA-END
} // end of java type