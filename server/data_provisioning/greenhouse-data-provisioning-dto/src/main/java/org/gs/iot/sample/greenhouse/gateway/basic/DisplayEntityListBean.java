package org.gs.iot.sample.greenhouse.gateway.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="org.gs.iot.sample.greenhouse.gateway.basic")
public class DisplayEntityListBean { // start of class

    private List<DisplayEntityBean> displayEntityList = new ArrayList<DisplayEntityBean>();
    
    /**
     * creates an instance of DisplayEntityListBean
     */
    public DisplayEntityListBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean:DA-END
    }
    
    /**
     * getter for the field displayEntityList
     * 
     * 
     * 
     * @return
     */
    @XmlElement(name="displayEntityList")
    public List<DisplayEntityBean> getDisplayEntityList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.getDisplayEntityList.List:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.getDisplayEntityList.List:DA-ELSE
        return this.displayEntityList;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.getDisplayEntityList.List:DA-END
    }
    /**
     * adder for the field displayEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void addDisplayEntityList(DisplayEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.addDisplayEntityList.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.addDisplayEntityList.DisplayEntityBean:DA-ELSE
        this.displayEntityList.add(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.addDisplayEntityList.DisplayEntityBean:DA-END
    }
    /**
     * remover for the field displayEntityList
     * 
     * 
     * 
     * @param element  the element
     */
    public void removeDisplayEntityList(DisplayEntityBean element) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.removeDisplayEntityList.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.removeDisplayEntityList.DisplayEntityBean:DA-ELSE
        this.displayEntityList.remove(element);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.removeDisplayEntityList.DisplayEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean.additional.elements.in.type:DA-END
} // end of java type