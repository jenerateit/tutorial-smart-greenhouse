package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.CameraEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.CameraEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.CameraEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.Camera;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.CameraBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.CameraEntityEJB:DA-START
//DA-ELSE:function.CameraEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.CameraEntityEJB:DA-END

public class CameraEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of CameraEntityEJB
     */
    public CameraEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.delete.long:DA-ELSE
        CameraEntityDAO dao = new CameraEntityDAO();
        CameraEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.read.long.annotations:DA-END
    public CameraEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.read.long.CameraEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.read.long.CameraEntityBean:DA-ELSE
        CameraEntityDAO dao = new CameraEntityDAO();
        CameraEntity entity = dao.get(entityManager, id);
        CameraEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.read.long.CameraEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.readList.int.int.List.annotations:DA-END
    public CameraEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.readList.int.int.List.CameraEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.readList.int.int.List.CameraEntityListBean:DA-ELSE
        CameraEntityListBean result = new CameraEntityListBean();
        for (CameraEntity entity : new CameraEntityDAO().getAll(entityManager)) {
            CameraEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addCameraEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.readList.int.int.List.CameraEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.update.long.CameraEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.update.long.CameraEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.update.long.CameraEntityBean.annotations:DA-END
    public CameraEntityBean update(long id, CameraEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.update.long.CameraEntityBean.CameraEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.update.long.CameraEntityBean.CameraEntityBean:DA-ELSE
        CameraEntityDAO dao = new CameraEntityDAO();
        CameraEntity updatedEntity = dao.get(entityManager, bean.getPk());
        CameraEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.update.long.CameraEntityBean.CameraEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.create.CameraEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.create.CameraEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.create.CameraEntityBean.annotations:DA-END
    public CameraEntityBean create(CameraEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.create.CameraEntityBean.CameraEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.create.CameraEntityBean.CameraEntityBean:DA-ELSE
        CameraEntityDAO dao = new CameraEntityDAO();
        CameraEntity createdEntity = null;
        CameraEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.create.CameraEntityBean.CameraEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromBeanToEntity.CameraEntityBean.CameraEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromBeanToEntity.CameraEntityBean.CameraEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromBeanToEntity.CameraEntityBean.CameraEntity.EntityManager.annotations:DA-END
    public static CameraEntity convertFromBeanToEntity(CameraEntityBean bean, CameraEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromBeanToEntity.CameraEntityBean.CameraEntity.EntityManager.CameraEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromBeanToEntity.CameraEntityBean.CameraEntity.EntityManager.CameraEntity:DA-ELSE
        CameraEntity result = entity;
        if (result == null) {
            result = new CameraEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Camera dataObj = new Camera();
            dataObj.setMimetype(bean.getData().getMimetype());
            dataObj.setImage(bean.getData().getImage());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromBeanToEntity.CameraEntityBean.CameraEntity.EntityManager.CameraEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromEntityToBean.CameraEntity.CameraEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromEntityToBean.CameraEntity.CameraEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromEntityToBean.CameraEntity.CameraEntityBean.Map.annotations:DA-END
    public static CameraEntityBean convertFromEntityToBean(CameraEntity entity, CameraEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromEntityToBean.CameraEntity.CameraEntityBean.Map.CameraEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromEntityToBean.CameraEntity.CameraEntityBean.Map.CameraEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        CameraEntityBean result = bean;
        if (result == null) {
            result = new CameraEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            CameraBean dataObj = new CameraBean();
            dataObj.setMimetype(entity.getData().getMimetype());
            dataObj.setImage(entity.getData().getImage());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.convertFromEntityToBean.CameraEntity.CameraEntityBean.Map.CameraEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.CameraEntityEJB.additional.elements.in.type:DA-END
} // end of java type