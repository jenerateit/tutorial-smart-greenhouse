package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.Mq2GasEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.Mq2GasEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.Mq2Gas;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.Mq2GasBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.Mq2GasEntityEJB:DA-START
//DA-ELSE:function.Mq2GasEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.Mq2GasEntityEJB:DA-END

public class Mq2GasEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of Mq2GasEntityEJB
     */
    public Mq2GasEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.delete.long:DA-ELSE
        Mq2GasEntityDAO dao = new Mq2GasEntityDAO();
        Mq2GasEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.read.long.annotations:DA-END
    public Mq2GasEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.read.long.Mq2GasEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.read.long.Mq2GasEntityBean:DA-ELSE
        Mq2GasEntityDAO dao = new Mq2GasEntityDAO();
        Mq2GasEntity entity = dao.get(entityManager, id);
        Mq2GasEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.read.long.Mq2GasEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.readList.int.int.List.annotations:DA-END
    public Mq2GasEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.readList.int.int.List.Mq2GasEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.readList.int.int.List.Mq2GasEntityListBean:DA-ELSE
        Mq2GasEntityListBean result = new Mq2GasEntityListBean();
        for (Mq2GasEntity entity : new Mq2GasEntityDAO().getAll(entityManager)) {
            Mq2GasEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addMq2GasEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.readList.int.int.List.Mq2GasEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.update.long.Mq2GasEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.update.long.Mq2GasEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.update.long.Mq2GasEntityBean.annotations:DA-END
    public Mq2GasEntityBean update(long id, Mq2GasEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.update.long.Mq2GasEntityBean.Mq2GasEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.update.long.Mq2GasEntityBean.Mq2GasEntityBean:DA-ELSE
        Mq2GasEntityDAO dao = new Mq2GasEntityDAO();
        Mq2GasEntity updatedEntity = dao.get(entityManager, bean.getPk());
        Mq2GasEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.update.long.Mq2GasEntityBean.Mq2GasEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.create.Mq2GasEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.create.Mq2GasEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.create.Mq2GasEntityBean.annotations:DA-END
    public Mq2GasEntityBean create(Mq2GasEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.create.Mq2GasEntityBean.Mq2GasEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.create.Mq2GasEntityBean.Mq2GasEntityBean:DA-ELSE
        Mq2GasEntityDAO dao = new Mq2GasEntityDAO();
        Mq2GasEntity createdEntity = null;
        Mq2GasEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.create.Mq2GasEntityBean.Mq2GasEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromBeanToEntity.Mq2GasEntityBean.Mq2GasEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromBeanToEntity.Mq2GasEntityBean.Mq2GasEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromBeanToEntity.Mq2GasEntityBean.Mq2GasEntity.EntityManager.annotations:DA-END
    public static Mq2GasEntity convertFromBeanToEntity(Mq2GasEntityBean bean, Mq2GasEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromBeanToEntity.Mq2GasEntityBean.Mq2GasEntity.EntityManager.Mq2GasEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromBeanToEntity.Mq2GasEntityBean.Mq2GasEntity.EntityManager.Mq2GasEntity:DA-ELSE
        Mq2GasEntity result = entity;
        if (result == null) {
            result = new Mq2GasEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Mq2Gas dataObj = new Mq2Gas();
            dataObj.setRsAir(bean.getData().getRsAir());
            dataObj.setr0(bean.getData().getr0());
            dataObj.setRsGas(bean.getData().getRsGas());
            dataObj.setRatio(bean.getData().getRatio());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromBeanToEntity.Mq2GasEntityBean.Mq2GasEntity.EntityManager.Mq2GasEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromEntityToBean.Mq2GasEntity.Mq2GasEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromEntityToBean.Mq2GasEntity.Mq2GasEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromEntityToBean.Mq2GasEntity.Mq2GasEntityBean.Map.annotations:DA-END
    public static Mq2GasEntityBean convertFromEntityToBean(Mq2GasEntity entity, Mq2GasEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromEntityToBean.Mq2GasEntity.Mq2GasEntityBean.Map.Mq2GasEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromEntityToBean.Mq2GasEntity.Mq2GasEntityBean.Map.Mq2GasEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        Mq2GasEntityBean result = bean;
        if (result == null) {
            result = new Mq2GasEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            Mq2GasBean dataObj = new Mq2GasBean();
            dataObj.setRsAir(entity.getData().getRsAir());
            dataObj.setr0(entity.getData().getr0());
            dataObj.setRsGas(entity.getData().getRsGas());
            dataObj.setRatio(entity.getData().getRatio());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.convertFromEntityToBean.Mq2GasEntity.Mq2GasEntityBean.Map.Mq2GasEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.Mq2GasEntityEJB.additional.elements.in.type:DA-END
} // end of java type