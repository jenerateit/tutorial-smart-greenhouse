package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.WaterPumpEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.WaterPumpEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.WaterPump;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.WaterPumpBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.WaterPumpEntityEJB:DA-START
//DA-ELSE:function.WaterPumpEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.WaterPumpEntityEJB:DA-END

public class WaterPumpEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of WaterPumpEntityEJB
     */
    public WaterPumpEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.delete.long:DA-ELSE
        WaterPumpEntityDAO dao = new WaterPumpEntityDAO();
        WaterPumpEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.read.long.annotations:DA-END
    public WaterPumpEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.read.long.WaterPumpEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.read.long.WaterPumpEntityBean:DA-ELSE
        WaterPumpEntityDAO dao = new WaterPumpEntityDAO();
        WaterPumpEntity entity = dao.get(entityManager, id);
        WaterPumpEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.read.long.WaterPumpEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.readList.int.int.List.annotations:DA-END
    public WaterPumpEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.readList.int.int.List.WaterPumpEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.readList.int.int.List.WaterPumpEntityListBean:DA-ELSE
        WaterPumpEntityListBean result = new WaterPumpEntityListBean();
        for (WaterPumpEntity entity : new WaterPumpEntityDAO().getAll(entityManager)) {
            WaterPumpEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addWaterPumpEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.readList.int.int.List.WaterPumpEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.update.long.WaterPumpEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.update.long.WaterPumpEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.update.long.WaterPumpEntityBean.annotations:DA-END
    public WaterPumpEntityBean update(long id, WaterPumpEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.update.long.WaterPumpEntityBean.WaterPumpEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.update.long.WaterPumpEntityBean.WaterPumpEntityBean:DA-ELSE
        WaterPumpEntityDAO dao = new WaterPumpEntityDAO();
        WaterPumpEntity updatedEntity = dao.get(entityManager, bean.getPk());
        WaterPumpEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.update.long.WaterPumpEntityBean.WaterPumpEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.create.WaterPumpEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.create.WaterPumpEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.create.WaterPumpEntityBean.annotations:DA-END
    public WaterPumpEntityBean create(WaterPumpEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.create.WaterPumpEntityBean.WaterPumpEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.create.WaterPumpEntityBean.WaterPumpEntityBean:DA-ELSE
        WaterPumpEntityDAO dao = new WaterPumpEntityDAO();
        WaterPumpEntity createdEntity = null;
        WaterPumpEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.create.WaterPumpEntityBean.WaterPumpEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromBeanToEntity.WaterPumpEntityBean.WaterPumpEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromBeanToEntity.WaterPumpEntityBean.WaterPumpEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromBeanToEntity.WaterPumpEntityBean.WaterPumpEntity.EntityManager.annotations:DA-END
    public static WaterPumpEntity convertFromBeanToEntity(WaterPumpEntityBean bean, WaterPumpEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromBeanToEntity.WaterPumpEntityBean.WaterPumpEntity.EntityManager.WaterPumpEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromBeanToEntity.WaterPumpEntityBean.WaterPumpEntity.EntityManager.WaterPumpEntity:DA-ELSE
        WaterPumpEntity result = entity;
        if (result == null) {
            result = new WaterPumpEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            WaterPump dataObj = new WaterPump();
            dataObj.setStatus(bean.getData().isStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromBeanToEntity.WaterPumpEntityBean.WaterPumpEntity.EntityManager.WaterPumpEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromEntityToBean.WaterPumpEntity.WaterPumpEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromEntityToBean.WaterPumpEntity.WaterPumpEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromEntityToBean.WaterPumpEntity.WaterPumpEntityBean.Map.annotations:DA-END
    public static WaterPumpEntityBean convertFromEntityToBean(WaterPumpEntity entity, WaterPumpEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromEntityToBean.WaterPumpEntity.WaterPumpEntityBean.Map.WaterPumpEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromEntityToBean.WaterPumpEntity.WaterPumpEntityBean.Map.WaterPumpEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        WaterPumpEntityBean result = bean;
        if (result == null) {
            result = new WaterPumpEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            WaterPumpBean dataObj = new WaterPumpBean();
            dataObj.setStatus(entity.getData().isStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.convertFromEntityToBean.WaterPumpEntity.WaterPumpEntityBean.Map.WaterPumpEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.WaterPumpEntityEJB.additional.elements.in.type:DA-END
} // end of java type