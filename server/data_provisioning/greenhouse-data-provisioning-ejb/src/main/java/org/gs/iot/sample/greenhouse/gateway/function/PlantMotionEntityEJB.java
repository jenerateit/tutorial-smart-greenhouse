package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.PlantMotionEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.PlantMotionEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.PlantMotion;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantMotionBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.PlantMotionEntityEJB:DA-START
//DA-ELSE:function.PlantMotionEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.PlantMotionEntityEJB:DA-END

public class PlantMotionEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of PlantMotionEntityEJB
     */
    public PlantMotionEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.delete.long:DA-ELSE
        PlantMotionEntityDAO dao = new PlantMotionEntityDAO();
        PlantMotionEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.read.long.annotations:DA-END
    public PlantMotionEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.read.long.PlantMotionEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.read.long.PlantMotionEntityBean:DA-ELSE
        PlantMotionEntityDAO dao = new PlantMotionEntityDAO();
        PlantMotionEntity entity = dao.get(entityManager, id);
        PlantMotionEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.read.long.PlantMotionEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.readList.int.int.List.annotations:DA-END
    public PlantMotionEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.readList.int.int.List.PlantMotionEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.readList.int.int.List.PlantMotionEntityListBean:DA-ELSE
        PlantMotionEntityListBean result = new PlantMotionEntityListBean();
        for (PlantMotionEntity entity : new PlantMotionEntityDAO().getAll(entityManager)) {
            PlantMotionEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addPlantMotionEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.readList.int.int.List.PlantMotionEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.update.long.PlantMotionEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.update.long.PlantMotionEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.update.long.PlantMotionEntityBean.annotations:DA-END
    public PlantMotionEntityBean update(long id, PlantMotionEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.update.long.PlantMotionEntityBean.PlantMotionEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.update.long.PlantMotionEntityBean.PlantMotionEntityBean:DA-ELSE
        PlantMotionEntityDAO dao = new PlantMotionEntityDAO();
        PlantMotionEntity updatedEntity = dao.get(entityManager, bean.getPk());
        PlantMotionEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.update.long.PlantMotionEntityBean.PlantMotionEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.create.PlantMotionEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.create.PlantMotionEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.create.PlantMotionEntityBean.annotations:DA-END
    public PlantMotionEntityBean create(PlantMotionEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.create.PlantMotionEntityBean.PlantMotionEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.create.PlantMotionEntityBean.PlantMotionEntityBean:DA-ELSE
        PlantMotionEntityDAO dao = new PlantMotionEntityDAO();
        PlantMotionEntity createdEntity = null;
        PlantMotionEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.create.PlantMotionEntityBean.PlantMotionEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromBeanToEntity.PlantMotionEntityBean.PlantMotionEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromBeanToEntity.PlantMotionEntityBean.PlantMotionEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromBeanToEntity.PlantMotionEntityBean.PlantMotionEntity.EntityManager.annotations:DA-END
    public static PlantMotionEntity convertFromBeanToEntity(PlantMotionEntityBean bean, PlantMotionEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromBeanToEntity.PlantMotionEntityBean.PlantMotionEntity.EntityManager.PlantMotionEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromBeanToEntity.PlantMotionEntityBean.PlantMotionEntity.EntityManager.PlantMotionEntity:DA-ELSE
        PlantMotionEntity result = entity;
        if (result == null) {
            result = new PlantMotionEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            PlantMotion dataObj = new PlantMotion();
            dataObj.setStatus(bean.getData().isStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromBeanToEntity.PlantMotionEntityBean.PlantMotionEntity.EntityManager.PlantMotionEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromEntityToBean.PlantMotionEntity.PlantMotionEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromEntityToBean.PlantMotionEntity.PlantMotionEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromEntityToBean.PlantMotionEntity.PlantMotionEntityBean.Map.annotations:DA-END
    public static PlantMotionEntityBean convertFromEntityToBean(PlantMotionEntity entity, PlantMotionEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromEntityToBean.PlantMotionEntity.PlantMotionEntityBean.Map.PlantMotionEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromEntityToBean.PlantMotionEntity.PlantMotionEntityBean.Map.PlantMotionEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        PlantMotionEntityBean result = bean;
        if (result == null) {
            result = new PlantMotionEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            PlantMotionBean dataObj = new PlantMotionBean();
            dataObj.setStatus(entity.getData().isStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.convertFromEntityToBean.PlantMotionEntity.PlantMotionEntityBean.Map.PlantMotionEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMotionEntityEJB.additional.elements.in.type:DA-END
} // end of java type