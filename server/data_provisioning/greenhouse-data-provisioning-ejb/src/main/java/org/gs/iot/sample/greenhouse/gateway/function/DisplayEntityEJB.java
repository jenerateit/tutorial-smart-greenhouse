package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.DisplayEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.DisplayEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.DisplayEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.Display;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.DisplayBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.DisplayEntityEJB:DA-START
//DA-ELSE:function.DisplayEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.DisplayEntityEJB:DA-END

public class DisplayEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of DisplayEntityEJB
     */
    public DisplayEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.delete.long:DA-ELSE
        DisplayEntityDAO dao = new DisplayEntityDAO();
        DisplayEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.read.long.annotations:DA-END
    public DisplayEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.read.long.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.read.long.DisplayEntityBean:DA-ELSE
        DisplayEntityDAO dao = new DisplayEntityDAO();
        DisplayEntity entity = dao.get(entityManager, id);
        DisplayEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.read.long.DisplayEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.readList.int.int.List.annotations:DA-END
    public DisplayEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.readList.int.int.List.DisplayEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.readList.int.int.List.DisplayEntityListBean:DA-ELSE
        DisplayEntityListBean result = new DisplayEntityListBean();
        for (DisplayEntity entity : new DisplayEntityDAO().getAll(entityManager)) {
            DisplayEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addDisplayEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.readList.int.int.List.DisplayEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.update.long.DisplayEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.update.long.DisplayEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.update.long.DisplayEntityBean.annotations:DA-END
    public DisplayEntityBean update(long id, DisplayEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.update.long.DisplayEntityBean.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.update.long.DisplayEntityBean.DisplayEntityBean:DA-ELSE
        DisplayEntityDAO dao = new DisplayEntityDAO();
        DisplayEntity updatedEntity = dao.get(entityManager, bean.getPk());
        DisplayEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.update.long.DisplayEntityBean.DisplayEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.create.DisplayEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.create.DisplayEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.create.DisplayEntityBean.annotations:DA-END
    public DisplayEntityBean create(DisplayEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.create.DisplayEntityBean.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.create.DisplayEntityBean.DisplayEntityBean:DA-ELSE
        DisplayEntityDAO dao = new DisplayEntityDAO();
        DisplayEntity createdEntity = null;
        DisplayEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.create.DisplayEntityBean.DisplayEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromBeanToEntity.DisplayEntityBean.DisplayEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromBeanToEntity.DisplayEntityBean.DisplayEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromBeanToEntity.DisplayEntityBean.DisplayEntity.EntityManager.annotations:DA-END
    public static DisplayEntity convertFromBeanToEntity(DisplayEntityBean bean, DisplayEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromBeanToEntity.DisplayEntityBean.DisplayEntity.EntityManager.DisplayEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromBeanToEntity.DisplayEntityBean.DisplayEntity.EntityManager.DisplayEntity:DA-ELSE
        DisplayEntity result = entity;
        if (result == null) {
            result = new DisplayEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Display dataObj = new Display();
            dataObj.setBacklight(bean.getData().getBacklight());
            dataObj.setRow0(bean.getData().getRow0());
            dataObj.setRow1(bean.getData().getRow1());
            dataObj.setButton(bean.getData().getButton());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromBeanToEntity.DisplayEntityBean.DisplayEntity.EntityManager.DisplayEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromEntityToBean.DisplayEntity.DisplayEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromEntityToBean.DisplayEntity.DisplayEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromEntityToBean.DisplayEntity.DisplayEntityBean.Map.annotations:DA-END
    public static DisplayEntityBean convertFromEntityToBean(DisplayEntity entity, DisplayEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromEntityToBean.DisplayEntity.DisplayEntityBean.Map.DisplayEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromEntityToBean.DisplayEntity.DisplayEntityBean.Map.DisplayEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        DisplayEntityBean result = bean;
        if (result == null) {
            result = new DisplayEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            DisplayBean dataObj = new DisplayBean();
            dataObj.setBacklight(entity.getData().getBacklight());
            dataObj.setRow0(entity.getData().getRow0());
            dataObj.setRow1(entity.getData().getRow1());
            dataObj.setButton(entity.getData().getButton());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.convertFromEntityToBean.DisplayEntity.DisplayEntityBean.Map.DisplayEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.DisplayEntityEJB.additional.elements.in.type:DA-END
} // end of java type