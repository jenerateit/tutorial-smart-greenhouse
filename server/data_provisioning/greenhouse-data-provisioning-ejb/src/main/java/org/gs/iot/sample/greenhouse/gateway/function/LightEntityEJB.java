package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.LightEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.LightEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.LightEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.LightEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.Light;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.LightBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.LightEntityEJB:DA-START
//DA-ELSE:function.LightEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.LightEntityEJB:DA-END

public class LightEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of LightEntityEJB
     */
    public LightEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.delete.long:DA-ELSE
        LightEntityDAO dao = new LightEntityDAO();
        LightEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.read.long.annotations:DA-END
    public LightEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.read.long.LightEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.read.long.LightEntityBean:DA-ELSE
        LightEntityDAO dao = new LightEntityDAO();
        LightEntity entity = dao.get(entityManager, id);
        LightEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.read.long.LightEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.readList.int.int.List.annotations:DA-END
    public LightEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.readList.int.int.List.LightEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.readList.int.int.List.LightEntityListBean:DA-ELSE
        LightEntityListBean result = new LightEntityListBean();
        for (LightEntity entity : new LightEntityDAO().getAll(entityManager)) {
            LightEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addLightEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.readList.int.int.List.LightEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.update.long.LightEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.update.long.LightEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.update.long.LightEntityBean.annotations:DA-END
    public LightEntityBean update(long id, LightEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.update.long.LightEntityBean.LightEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.update.long.LightEntityBean.LightEntityBean:DA-ELSE
        LightEntityDAO dao = new LightEntityDAO();
        LightEntity updatedEntity = dao.get(entityManager, bean.getPk());
        LightEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.update.long.LightEntityBean.LightEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.create.LightEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.create.LightEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.create.LightEntityBean.annotations:DA-END
    public LightEntityBean create(LightEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.create.LightEntityBean.LightEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.create.LightEntityBean.LightEntityBean:DA-ELSE
        LightEntityDAO dao = new LightEntityDAO();
        LightEntity createdEntity = null;
        LightEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.create.LightEntityBean.LightEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromBeanToEntity.LightEntityBean.LightEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromBeanToEntity.LightEntityBean.LightEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromBeanToEntity.LightEntityBean.LightEntity.EntityManager.annotations:DA-END
    public static LightEntity convertFromBeanToEntity(LightEntityBean bean, LightEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromBeanToEntity.LightEntityBean.LightEntity.EntityManager.LightEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromBeanToEntity.LightEntityBean.LightEntity.EntityManager.LightEntity:DA-ELSE
        LightEntity result = entity;
        if (result == null) {
            result = new LightEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Light dataObj = new Light();
            dataObj.setR(bean.getData().getR());
            dataObj.setG(bean.getData().getG());
            dataObj.setB(bean.getData().getB());
            dataObj.setBrightness(bean.getData().getBrightness());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromBeanToEntity.LightEntityBean.LightEntity.EntityManager.LightEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromEntityToBean.LightEntity.LightEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromEntityToBean.LightEntity.LightEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromEntityToBean.LightEntity.LightEntityBean.Map.annotations:DA-END
    public static LightEntityBean convertFromEntityToBean(LightEntity entity, LightEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromEntityToBean.LightEntity.LightEntityBean.Map.LightEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromEntityToBean.LightEntity.LightEntityBean.Map.LightEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        LightEntityBean result = bean;
        if (result == null) {
            result = new LightEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            LightBean dataObj = new LightBean();
            dataObj.setR(entity.getData().getR());
            dataObj.setG(entity.getData().getG());
            dataObj.setB(entity.getData().getB());
            dataObj.setBrightness(entity.getData().getBrightness());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.convertFromEntityToBean.LightEntity.LightEntityBean.Map.LightEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.LightEntityEJB.additional.elements.in.type:DA-END
} // end of java type