package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.AlarmEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.AlarmEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.AlarmEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.Alarm;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.AlarmBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.AlarmEntityEJB:DA-START
//DA-ELSE:function.AlarmEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.AlarmEntityEJB:DA-END

public class AlarmEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of AlarmEntityEJB
     */
    public AlarmEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.delete.long:DA-ELSE
        AlarmEntityDAO dao = new AlarmEntityDAO();
        AlarmEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.read.long.annotations:DA-END
    public AlarmEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.read.long.AlarmEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.read.long.AlarmEntityBean:DA-ELSE
        AlarmEntityDAO dao = new AlarmEntityDAO();
        AlarmEntity entity = dao.get(entityManager, id);
        AlarmEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.read.long.AlarmEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.readList.int.int.List.annotations:DA-END
    public AlarmEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.readList.int.int.List.AlarmEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.readList.int.int.List.AlarmEntityListBean:DA-ELSE
        AlarmEntityListBean result = new AlarmEntityListBean();
        for (AlarmEntity entity : new AlarmEntityDAO().getAll(entityManager)) {
            AlarmEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addAlarmEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.readList.int.int.List.AlarmEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.update.long.AlarmEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.update.long.AlarmEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.update.long.AlarmEntityBean.annotations:DA-END
    public AlarmEntityBean update(long id, AlarmEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.update.long.AlarmEntityBean.AlarmEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.update.long.AlarmEntityBean.AlarmEntityBean:DA-ELSE
        AlarmEntityDAO dao = new AlarmEntityDAO();
        AlarmEntity updatedEntity = dao.get(entityManager, bean.getPk());
        AlarmEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.update.long.AlarmEntityBean.AlarmEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.create.AlarmEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.create.AlarmEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.create.AlarmEntityBean.annotations:DA-END
    public AlarmEntityBean create(AlarmEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.create.AlarmEntityBean.AlarmEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.create.AlarmEntityBean.AlarmEntityBean:DA-ELSE
        AlarmEntityDAO dao = new AlarmEntityDAO();
        AlarmEntity createdEntity = null;
        AlarmEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.create.AlarmEntityBean.AlarmEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromBeanToEntity.AlarmEntityBean.AlarmEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromBeanToEntity.AlarmEntityBean.AlarmEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromBeanToEntity.AlarmEntityBean.AlarmEntity.EntityManager.annotations:DA-END
    public static AlarmEntity convertFromBeanToEntity(AlarmEntityBean bean, AlarmEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromBeanToEntity.AlarmEntityBean.AlarmEntity.EntityManager.AlarmEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromBeanToEntity.AlarmEntityBean.AlarmEntity.EntityManager.AlarmEntity:DA-ELSE
        AlarmEntity result = entity;
        if (result == null) {
            result = new AlarmEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Alarm dataObj = new Alarm();
            dataObj.setStatus(bean.getData().isStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromBeanToEntity.AlarmEntityBean.AlarmEntity.EntityManager.AlarmEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromEntityToBean.AlarmEntity.AlarmEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromEntityToBean.AlarmEntity.AlarmEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromEntityToBean.AlarmEntity.AlarmEntityBean.Map.annotations:DA-END
    public static AlarmEntityBean convertFromEntityToBean(AlarmEntity entity, AlarmEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromEntityToBean.AlarmEntity.AlarmEntityBean.Map.AlarmEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromEntityToBean.AlarmEntity.AlarmEntityBean.Map.AlarmEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        AlarmEntityBean result = bean;
        if (result == null) {
            result = new AlarmEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            AlarmBean dataObj = new AlarmBean();
            dataObj.setStatus(entity.getData().isStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.convertFromEntityToBean.AlarmEntity.AlarmEntityBean.Map.AlarmEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.AlarmEntityEJB.additional.elements.in.type:DA-END
} // end of java type