package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.PlantIlluminanceEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.PlantIlluminance;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantIlluminanceBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.PlantIlluminanceEntityEJB:DA-START
//DA-ELSE:function.PlantIlluminanceEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.PlantIlluminanceEntityEJB:DA-END

public class PlantIlluminanceEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of PlantIlluminanceEntityEJB
     */
    public PlantIlluminanceEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.delete.long:DA-ELSE
        PlantIlluminanceEntityDAO dao = new PlantIlluminanceEntityDAO();
        PlantIlluminanceEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.read.long.annotations:DA-END
    public PlantIlluminanceEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.read.long.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.read.long.PlantIlluminanceEntityBean:DA-ELSE
        PlantIlluminanceEntityDAO dao = new PlantIlluminanceEntityDAO();
        PlantIlluminanceEntity entity = dao.get(entityManager, id);
        PlantIlluminanceEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.read.long.PlantIlluminanceEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.readList.int.int.List.annotations:DA-END
    public PlantIlluminanceEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.readList.int.int.List.PlantIlluminanceEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.readList.int.int.List.PlantIlluminanceEntityListBean:DA-ELSE
        PlantIlluminanceEntityListBean result = new PlantIlluminanceEntityListBean();
        for (PlantIlluminanceEntity entity : new PlantIlluminanceEntityDAO().getAll(entityManager)) {
            PlantIlluminanceEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addPlantIlluminanceEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.readList.int.int.List.PlantIlluminanceEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.update.long.PlantIlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.update.long.PlantIlluminanceEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.update.long.PlantIlluminanceEntityBean.annotations:DA-END
    public PlantIlluminanceEntityBean update(long id, PlantIlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.update.long.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.update.long.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-ELSE
        PlantIlluminanceEntityDAO dao = new PlantIlluminanceEntityDAO();
        PlantIlluminanceEntity updatedEntity = dao.get(entityManager, bean.getPk());
        PlantIlluminanceEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.update.long.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.create.PlantIlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.create.PlantIlluminanceEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.create.PlantIlluminanceEntityBean.annotations:DA-END
    public PlantIlluminanceEntityBean create(PlantIlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.create.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.create.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-ELSE
        PlantIlluminanceEntityDAO dao = new PlantIlluminanceEntityDAO();
        PlantIlluminanceEntity createdEntity = null;
        PlantIlluminanceEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.create.PlantIlluminanceEntityBean.PlantIlluminanceEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromBeanToEntity.PlantIlluminanceEntityBean.PlantIlluminanceEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromBeanToEntity.PlantIlluminanceEntityBean.PlantIlluminanceEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromBeanToEntity.PlantIlluminanceEntityBean.PlantIlluminanceEntity.EntityManager.annotations:DA-END
    public static PlantIlluminanceEntity convertFromBeanToEntity(PlantIlluminanceEntityBean bean, PlantIlluminanceEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromBeanToEntity.PlantIlluminanceEntityBean.PlantIlluminanceEntity.EntityManager.PlantIlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromBeanToEntity.PlantIlluminanceEntityBean.PlantIlluminanceEntity.EntityManager.PlantIlluminanceEntity:DA-ELSE
        PlantIlluminanceEntity result = entity;
        if (result == null) {
            result = new PlantIlluminanceEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            PlantIlluminance dataObj = new PlantIlluminance();
            dataObj.setStatus(bean.getData().getStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromBeanToEntity.PlantIlluminanceEntityBean.PlantIlluminanceEntity.EntityManager.PlantIlluminanceEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromEntityToBean.PlantIlluminanceEntity.PlantIlluminanceEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromEntityToBean.PlantIlluminanceEntity.PlantIlluminanceEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromEntityToBean.PlantIlluminanceEntity.PlantIlluminanceEntityBean.Map.annotations:DA-END
    public static PlantIlluminanceEntityBean convertFromEntityToBean(PlantIlluminanceEntity entity, PlantIlluminanceEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromEntityToBean.PlantIlluminanceEntity.PlantIlluminanceEntityBean.Map.PlantIlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromEntityToBean.PlantIlluminanceEntity.PlantIlluminanceEntityBean.Map.PlantIlluminanceEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        PlantIlluminanceEntityBean result = bean;
        if (result == null) {
            result = new PlantIlluminanceEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            PlantIlluminanceBean dataObj = new PlantIlluminanceBean();
            dataObj.setStatus(entity.getData().getStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.convertFromEntityToBean.PlantIlluminanceEntity.PlantIlluminanceEntityBean.Map.PlantIlluminanceEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantIlluminanceEntityEJB.additional.elements.in.type:DA-END
} // end of java type