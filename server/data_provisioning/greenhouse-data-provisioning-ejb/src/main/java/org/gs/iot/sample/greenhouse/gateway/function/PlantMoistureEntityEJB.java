package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.PlantMoistureEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.PlantMoisture;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantMoistureBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.PlantMoistureEntityEJB:DA-START
//DA-ELSE:function.PlantMoistureEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.PlantMoistureEntityEJB:DA-END

public class PlantMoistureEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of PlantMoistureEntityEJB
     */
    public PlantMoistureEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.delete.long:DA-ELSE
        PlantMoistureEntityDAO dao = new PlantMoistureEntityDAO();
        PlantMoistureEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.read.long.annotations:DA-END
    public PlantMoistureEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.read.long.PlantMoistureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.read.long.PlantMoistureEntityBean:DA-ELSE
        PlantMoistureEntityDAO dao = new PlantMoistureEntityDAO();
        PlantMoistureEntity entity = dao.get(entityManager, id);
        PlantMoistureEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.read.long.PlantMoistureEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.readList.int.int.List.annotations:DA-END
    public PlantMoistureEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.readList.int.int.List.PlantMoistureEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.readList.int.int.List.PlantMoistureEntityListBean:DA-ELSE
        PlantMoistureEntityListBean result = new PlantMoistureEntityListBean();
        for (PlantMoistureEntity entity : new PlantMoistureEntityDAO().getAll(entityManager)) {
            PlantMoistureEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addPlantMoistureEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.readList.int.int.List.PlantMoistureEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.update.long.PlantMoistureEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.update.long.PlantMoistureEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.update.long.PlantMoistureEntityBean.annotations:DA-END
    public PlantMoistureEntityBean update(long id, PlantMoistureEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.update.long.PlantMoistureEntityBean.PlantMoistureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.update.long.PlantMoistureEntityBean.PlantMoistureEntityBean:DA-ELSE
        PlantMoistureEntityDAO dao = new PlantMoistureEntityDAO();
        PlantMoistureEntity updatedEntity = dao.get(entityManager, bean.getPk());
        PlantMoistureEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.update.long.PlantMoistureEntityBean.PlantMoistureEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.create.PlantMoistureEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.create.PlantMoistureEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.create.PlantMoistureEntityBean.annotations:DA-END
    public PlantMoistureEntityBean create(PlantMoistureEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.create.PlantMoistureEntityBean.PlantMoistureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.create.PlantMoistureEntityBean.PlantMoistureEntityBean:DA-ELSE
        PlantMoistureEntityDAO dao = new PlantMoistureEntityDAO();
        PlantMoistureEntity createdEntity = null;
        PlantMoistureEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.create.PlantMoistureEntityBean.PlantMoistureEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromBeanToEntity.PlantMoistureEntityBean.PlantMoistureEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromBeanToEntity.PlantMoistureEntityBean.PlantMoistureEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromBeanToEntity.PlantMoistureEntityBean.PlantMoistureEntity.EntityManager.annotations:DA-END
    public static PlantMoistureEntity convertFromBeanToEntity(PlantMoistureEntityBean bean, PlantMoistureEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromBeanToEntity.PlantMoistureEntityBean.PlantMoistureEntity.EntityManager.PlantMoistureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromBeanToEntity.PlantMoistureEntityBean.PlantMoistureEntity.EntityManager.PlantMoistureEntity:DA-ELSE
        PlantMoistureEntity result = entity;
        if (result == null) {
            result = new PlantMoistureEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            PlantMoisture dataObj = new PlantMoisture();
            dataObj.setStatus(bean.getData().getStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromBeanToEntity.PlantMoistureEntityBean.PlantMoistureEntity.EntityManager.PlantMoistureEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromEntityToBean.PlantMoistureEntity.PlantMoistureEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromEntityToBean.PlantMoistureEntity.PlantMoistureEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromEntityToBean.PlantMoistureEntity.PlantMoistureEntityBean.Map.annotations:DA-END
    public static PlantMoistureEntityBean convertFromEntityToBean(PlantMoistureEntity entity, PlantMoistureEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromEntityToBean.PlantMoistureEntity.PlantMoistureEntityBean.Map.PlantMoistureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromEntityToBean.PlantMoistureEntity.PlantMoistureEntityBean.Map.PlantMoistureEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        PlantMoistureEntityBean result = bean;
        if (result == null) {
            result = new PlantMoistureEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            PlantMoistureBean dataObj = new PlantMoistureBean();
            dataObj.setStatus(entity.getData().getStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.convertFromEntityToBean.PlantMoistureEntity.PlantMoistureEntityBean.Map.PlantMoistureEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantMoistureEntityEJB.additional.elements.in.type:DA-END
} // end of java type