package org.gs.iot.sample.greenhouse.gateway.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntityDAO;
import org.gs.iot.sample.greenhouse.gateway.PlantBarometerEntity;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityBean;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerEntityListBean;
import java.util.List;
import org.gs.iot.sample.greenhouse.gateway.PlantBarometer;
import org.gs.iot.sample.greenhouse.gateway.GeoCoordinates;
import org.gs.iot.sample.greenhouse.gateway.MeasurementInfo;
import java.util.Map;
import java.util.LinkedHashMap;
import org.gs.iot.sample.greenhouse.gateway.basic.PlantBarometerBean;
import org.gs.iot.sample.greenhouse.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.greenhouse.gateway.basic.MeasurementInfoBean;

//DA-START:function.PlantBarometerEntityEJB:DA-START
//DA-ELSE:function.PlantBarometerEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.PlantBarometerEntityEJB:DA-END

public class PlantBarometerEntityEJB { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="PERSISTENCE_UNIT_NAME")
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of PlantBarometerEntityEJB
     */
    public PlantBarometerEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.delete.long:DA-ELSE
        PlantBarometerEntityDAO dao = new PlantBarometerEntityDAO();
        PlantBarometerEntity entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.read.long.annotations:DA-END
    public PlantBarometerEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.read.long.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.read.long.PlantBarometerEntityBean:DA-ELSE
        PlantBarometerEntityDAO dao = new PlantBarometerEntityDAO();
        PlantBarometerEntity entity = dao.get(entityManager, id);
        PlantBarometerEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.read.long.PlantBarometerEntityBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.readList.int.int.List.annotations:DA-END
    public PlantBarometerEntityListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.readList.int.int.List.PlantBarometerEntityListBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.readList.int.int.List.PlantBarometerEntityListBean:DA-ELSE
        PlantBarometerEntityListBean result = new PlantBarometerEntityListBean();
        for (PlantBarometerEntity entity : new PlantBarometerEntityDAO().getAll(entityManager)) {
            PlantBarometerEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.addPlantBarometerEntityList(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.readList.int.int.List.PlantBarometerEntityListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.update.long.PlantBarometerEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.update.long.PlantBarometerEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.update.long.PlantBarometerEntityBean.annotations:DA-END
    public PlantBarometerEntityBean update(long id, PlantBarometerEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.update.long.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.update.long.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-ELSE
        PlantBarometerEntityDAO dao = new PlantBarometerEntityDAO();
        PlantBarometerEntity updatedEntity = dao.get(entityManager, bean.getPk());
        PlantBarometerEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.update.long.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.create.PlantBarometerEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.create.PlantBarometerEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.create.PlantBarometerEntityBean.annotations:DA-END
    public PlantBarometerEntityBean create(PlantBarometerEntityBean bean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.create.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.create.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-ELSE
        PlantBarometerEntityDAO dao = new PlantBarometerEntityDAO();
        PlantBarometerEntity createdEntity = null;
        PlantBarometerEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.create.PlantBarometerEntityBean.PlantBarometerEntityBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromBeanToEntity.PlantBarometerEntityBean.PlantBarometerEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromBeanToEntity.PlantBarometerEntityBean.PlantBarometerEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromBeanToEntity.PlantBarometerEntityBean.PlantBarometerEntity.EntityManager.annotations:DA-END
    public static PlantBarometerEntity convertFromBeanToEntity(PlantBarometerEntityBean bean, PlantBarometerEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromBeanToEntity.PlantBarometerEntityBean.PlantBarometerEntity.EntityManager.PlantBarometerEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromBeanToEntity.PlantBarometerEntityBean.PlantBarometerEntity.EntityManager.PlantBarometerEntity:DA-ELSE
        PlantBarometerEntity result = entity;
        if (result == null) {
            result = new PlantBarometerEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            PlantBarometer dataObj = new PlantBarometer();
            dataObj.setPressure(bean.getData().getPressure());
            dataObj.setTemperature(bean.getData().getTemperature());
            dataObj.setAltitude(bean.getData().getAltitude());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromBeanToEntity.PlantBarometerEntityBean.PlantBarometerEntity.EntityManager.PlantBarometerEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromEntityToBean.PlantBarometerEntity.PlantBarometerEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromEntityToBean.PlantBarometerEntity.PlantBarometerEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromEntityToBean.PlantBarometerEntity.PlantBarometerEntityBean.Map.annotations:DA-END
    public static PlantBarometerEntityBean convertFromEntityToBean(PlantBarometerEntity entity, PlantBarometerEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromEntityToBean.PlantBarometerEntity.PlantBarometerEntityBean.Map.PlantBarometerEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromEntityToBean.PlantBarometerEntity.PlantBarometerEntityBean.Map.PlantBarometerEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        PlantBarometerEntityBean result = bean;
        if (result == null) {
            result = new PlantBarometerEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            PlantBarometerBean dataObj = new PlantBarometerBean();
            dataObj.setPressure(entity.getData().getPressure());
            dataObj.setTemperature(entity.getData().getTemperature());
            dataObj.setAltitude(entity.getData().getAltitude());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.convertFromEntityToBean.PlantBarometerEntity.PlantBarometerEntityBean.Map.PlantBarometerEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.PlantBarometerEntityEJB.additional.elements.in.type:DA-END
} // end of java type