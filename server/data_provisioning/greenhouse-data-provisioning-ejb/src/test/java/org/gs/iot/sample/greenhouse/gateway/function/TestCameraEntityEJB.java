package org.gs.iot.sample.greenhouse.gateway.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

//DA-START:function.TestCameraEntityEJB:DA-START
//DA-ELSE:function.TestCameraEntityEJB:DA-ELSE
//DA-END:function.TestCameraEntityEJB:DA-END
public class TestCameraEntityEJB { // start of class

    /**
     * creates an instance of TestCameraEntityEJB
     */
    public TestCameraEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestCameraEntityEJB.additional.elements.in.type:DA-END
} // end of java type