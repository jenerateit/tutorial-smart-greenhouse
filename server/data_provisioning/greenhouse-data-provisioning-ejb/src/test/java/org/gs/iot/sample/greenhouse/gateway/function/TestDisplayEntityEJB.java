package org.gs.iot.sample.greenhouse.gateway.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

//DA-START:function.TestDisplayEntityEJB:DA-START
//DA-ELSE:function.TestDisplayEntityEJB:DA-ELSE
//DA-END:function.TestDisplayEntityEJB:DA-END
public class TestDisplayEntityEJB { // start of class

    /**
     * creates an instance of TestDisplayEntityEJB
     */
    public TestDisplayEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestDisplayEntityEJB.additional.elements.in.type:DA-END
} // end of java type