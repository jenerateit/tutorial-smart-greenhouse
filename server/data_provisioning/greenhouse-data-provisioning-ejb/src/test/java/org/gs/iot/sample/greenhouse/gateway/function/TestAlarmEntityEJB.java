package org.gs.iot.sample.greenhouse.gateway.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

//DA-START:function.TestAlarmEntityEJB:DA-START
//DA-ELSE:function.TestAlarmEntityEJB:DA-ELSE
//DA-END:function.TestAlarmEntityEJB:DA-END
public class TestAlarmEntityEJB { // start of class

    /**
     * creates an instance of TestAlarmEntityEJB
     */
    public TestAlarmEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestAlarmEntityEJB.additional.elements.in.type:DA-END
} // end of java type