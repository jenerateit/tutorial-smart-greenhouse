package org.gs.iot.sample.greenhouse.gateway.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

//DA-START:function.TestMq2GasEntityEJB:DA-START
//DA-ELSE:function.TestMq2GasEntityEJB:DA-ELSE
//DA-END:function.TestMq2GasEntityEJB:DA-END
public class TestMq2GasEntityEJB { // start of class

    /**
     * creates an instance of TestMq2GasEntityEJB
     */
    public TestMq2GasEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestMq2GasEntityEJB.additional.elements.in.type:DA-END
} // end of java type