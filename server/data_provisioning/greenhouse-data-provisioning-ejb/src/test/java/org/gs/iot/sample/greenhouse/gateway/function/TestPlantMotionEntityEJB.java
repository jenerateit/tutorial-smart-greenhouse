package org.gs.iot.sample.greenhouse.gateway.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

//DA-START:function.TestPlantMotionEntityEJB:DA-START
//DA-ELSE:function.TestPlantMotionEntityEJB:DA-ELSE
//DA-END:function.TestPlantMotionEntityEJB:DA-END
public class TestPlantMotionEntityEJB { // start of class

    /**
     * creates an instance of TestPlantMotionEntityEJB
     */
    public TestPlantMotionEntityEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.function.TestPlantMotionEntityEJB.additional.elements.in.type:DA-END
} // end of java type