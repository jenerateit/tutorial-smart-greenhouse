package org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto;


import com.vd.AbstractDto;
import java.io.Serializable;

public class NotificationAppControlFormDto extends AbstractDto implements Serializable { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.serialVersionUID:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.serialVersionUID:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.serialVersionUID:DA-END
    private static final long serialVersionUID = 1L;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningHost:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningHost:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningHost:DA-END
    private String dataProvisioningHost;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningContextRoot:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningContextRoot:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningContextRoot:DA-END
    private String dataProvisioningContextRoot;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningApplicationPath:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningApplicationPath:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.dataProvisioningApplicationPath:DA-END
    private String dataProvisioningApplicationPath;
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.fields:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.fields:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.fields:DA-END
    /**
     * creates an instance of NotificationAppControlFormDto
     */
    public NotificationAppControlFormDto() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto:DA-END
    }
    
    /**
     * setter for the field dataProvisioningHost
     * 
     * 
     * 
     * @param dataProvisioningHost  the dataProvisioningHost
     */
    public void setDataProvisioningHost(String dataProvisioningHost) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningHost.String:DA-ELSE
        if (isDifferent(this.dataProvisioningHost, dataProvisioningHost)) {
            this.setDirty(true);
            this.dataProvisioningHost = dataProvisioningHost;
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningHost.String:DA-END
    }
    /**
     * getter for the field dataProvisioningHost
     * 
     * 
     * 
     * @return
     */
    public String getDataProvisioningHost() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningHost.String:DA-ELSE
        return this.dataProvisioningHost;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningHost.String:DA-END
    }
    /**
     * setter for the field dataProvisioningContextRoot
     * 
     * 
     * 
     * @param dataProvisioningContextRoot  the dataProvisioningContextRoot
     */
    public void setDataProvisioningContextRoot(String dataProvisioningContextRoot) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningContextRoot.String:DA-ELSE
        if (isDifferent(this.dataProvisioningContextRoot, dataProvisioningContextRoot)) {
            this.setDirty(true);
            this.dataProvisioningContextRoot = dataProvisioningContextRoot;
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningContextRoot.String:DA-END
    }
    /**
     * getter for the field dataProvisioningContextRoot
     * 
     * 
     * 
     * @return
     */
    public String getDataProvisioningContextRoot() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningContextRoot.String:DA-ELSE
        return this.dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningContextRoot.String:DA-END
    }
    /**
     * setter for the field dataProvisioningApplicationPath
     * 
     * 
     * 
     * @param dataProvisioningApplicationPath  the dataProvisioningApplicationPath
     */
    public void setDataProvisioningApplicationPath(String dataProvisioningApplicationPath) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningApplicationPath.String:DA-ELSE
        if (isDifferent(this.dataProvisioningApplicationPath, dataProvisioningApplicationPath)) {
            this.setDirty(true);
            this.dataProvisioningApplicationPath = dataProvisioningApplicationPath;
        }
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.setDataProvisioningApplicationPath.String:DA-END
    }
    /**
     * getter for the field dataProvisioningApplicationPath
     * 
     * 
     * 
     * @return
     */
    public String getDataProvisioningApplicationPath() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningApplicationPath.String:DA-ELSE
        return this.dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.getDataProvisioningApplicationPath.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.methods:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.methods:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.methods:DA-END
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlFormDto.additional.elements.in.type:DA-END
} // end of java type