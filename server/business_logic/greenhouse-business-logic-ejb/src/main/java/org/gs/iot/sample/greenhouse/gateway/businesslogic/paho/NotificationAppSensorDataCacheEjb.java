package org.gs.iot.sample.greenhouse.gateway.businesslogic.paho;


import javax.ejb.Singleton;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import java.util.logging.Logger;

//DA-START:paho.NotificationAppSensorDataCacheEjb:DA-START
//DA-ELSE:paho.NotificationAppSensorDataCacheEjb:DA-ELSE
@Singleton
@ConcurrencyManagement(value=ConcurrencyManagementType.BEAN)
//DA-END:paho.NotificationAppSensorDataCacheEjb:DA-END

public class NotificationAppSensorDataCacheEjb { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppSensorDataCacheEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppSensorDataCacheEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppSensorDataCacheEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(NotificationAppSensorDataCacheEjb.class.getName());
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppSensorDataCacheEjb.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppSensorDataCacheEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppSensorDataCacheEjb.additional.elements.in.type:DA-END
} // end of java type