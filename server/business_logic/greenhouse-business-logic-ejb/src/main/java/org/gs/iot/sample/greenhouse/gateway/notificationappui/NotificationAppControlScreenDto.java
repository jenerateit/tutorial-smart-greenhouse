package org.gs.iot.sample.greenhouse.gateway.notificationappui;


import com.vd.AbstractDto;
import java.io.Serializable;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'NotificationAppControlScreen' in module 'NotificationAppUi'.
 * Support for dirty flag is added by the DTO generator.
 */
public class NotificationAppControlScreenDto extends AbstractDto implements Serializable { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.serialVersionUID:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.serialVersionUID:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.serialVersionUID:DA-END
    private static final long serialVersionUID = 1L;
    
    /**
     * final instance of the data container named 'NotificationAppControlForm' in UI model 'NotificationAppUi'
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.notificationAppControlForm:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.notificationAppControlForm:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.notificationAppControlForm:DA-END
    private final NotificationAppControlScreenDto.NotificationAppControlFormDto notificationAppControlForm = new NotificationAppControlScreenDto.NotificationAppControlFormDto();
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.fields:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.fields:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.fields:DA-END
    /**
     * creates an instance of NotificationAppControlScreenDto
     */
    public NotificationAppControlScreenDto() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto:DA-END
    }
    
    /**
     * getter for the field notificationAppControlForm
     * 
     * final instance of the data container named 'NotificationAppControlForm' in UI model 'NotificationAppUi'
     * 
     * @return
     */
    public NotificationAppControlScreenDto.NotificationAppControlFormDto getNotificationAppControlForm() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.getNotificationAppControlForm.NotificationAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.getNotificationAppControlForm.NotificationAppControlFormDto:DA-ELSE
        return this.notificationAppControlForm;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.getNotificationAppControlForm.NotificationAppControlFormDto:DA-END
    }
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        result = result || notificationAppControlForm.isDirty();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.methods:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.methods:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.methods:DA-END
    
    /**
     * Private inner DTO class for UIDataContainer ('display' element) 'NotificationAppControlForm' within UIStructuralContainer ('layout' element) 'NotificationAppControlScreen' in module 'NotificationAppUi'.
     * Support for dirty flag is added by the DTO generator.
     */
    public static class NotificationAppControlFormDto extends AbstractDto implements Serializable { // start of class
    
    
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.serialVersionUID:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.serialVersionUID:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.serialVersionUID:DA-END
        private static final long serialVersionUID = 1L;
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningHost:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningHost:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningHost:DA-END
        private String dataProvisioningHost;
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningContextRoot:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningContextRoot:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningContextRoot:DA-END
        private String dataProvisioningContextRoot;
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningApplicationPath:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningApplicationPath:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.dataProvisioningApplicationPath:DA-END
        private String dataProvisioningApplicationPath;
        
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.fields:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.fields:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.fields:DA-END
        
        /**
         * creates an instance of NotificationAppControlFormDto
         */
        public NotificationAppControlFormDto() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto:DA-ELSE
            super();
            //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto:DA-END
        }
        
        
        /**
         * setter for the field dataProvisioningHost
         * 
         * 
         * 
         * @param dataProvisioningHost  the dataProvisioningHost
         */
        public void setDataProvisioningHost(String dataProvisioningHost) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningHost.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningHost.String:DA-ELSE
            if (isDifferent(this.dataProvisioningHost, dataProvisioningHost)) {
                this.setDirty(true);
                this.dataProvisioningHost = dataProvisioningHost;
            }
            //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningHost.String:DA-END
        }
        /**
         * getter for the field dataProvisioningHost
         * 
         * 
         * 
         * @return
         */
        public String getDataProvisioningHost() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningHost.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningHost.String:DA-ELSE
            return this.dataProvisioningHost;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningHost.String:DA-END
        }
        /**
         * setter for the field dataProvisioningContextRoot
         * 
         * 
         * 
         * @param dataProvisioningContextRoot  the dataProvisioningContextRoot
         */
        public void setDataProvisioningContextRoot(String dataProvisioningContextRoot) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningContextRoot.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningContextRoot.String:DA-ELSE
            if (isDifferent(this.dataProvisioningContextRoot, dataProvisioningContextRoot)) {
                this.setDirty(true);
                this.dataProvisioningContextRoot = dataProvisioningContextRoot;
            }
            //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningContextRoot.String:DA-END
        }
        /**
         * getter for the field dataProvisioningContextRoot
         * 
         * 
         * 
         * @return
         */
        public String getDataProvisioningContextRoot() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningContextRoot.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningContextRoot.String:DA-ELSE
            return this.dataProvisioningContextRoot;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningContextRoot.String:DA-END
        }
        /**
         * setter for the field dataProvisioningApplicationPath
         * 
         * 
         * 
         * @param dataProvisioningApplicationPath  the dataProvisioningApplicationPath
         */
        public void setDataProvisioningApplicationPath(String dataProvisioningApplicationPath) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningApplicationPath.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningApplicationPath.String:DA-ELSE
            if (isDifferent(this.dataProvisioningApplicationPath, dataProvisioningApplicationPath)) {
                this.setDirty(true);
                this.dataProvisioningApplicationPath = dataProvisioningApplicationPath;
            }
            //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.setDataProvisioningApplicationPath.String:DA-END
        }
        /**
         * getter for the field dataProvisioningApplicationPath
         * 
         * 
         * 
         * @return
         */
        public String getDataProvisioningApplicationPath() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningApplicationPath.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningApplicationPath.String:DA-ELSE
            return this.dataProvisioningApplicationPath;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.getDataProvisioningApplicationPath.String:DA-END
        }
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.methods:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.methods:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.methods:DA-END
        
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.NotificationAppControlFormDto.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappui.NotificationAppControlScreenDto.additional.elements.in.type:DA-END
} // end of java type