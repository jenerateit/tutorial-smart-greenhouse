package org.gs.iot.sample.greenhouse.gateway.ejb;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto;
import com.vd.SessionDataHolder;

//DA-START:ejb.NotificationAppControlScreenEJB:DA-START
//DA-ELSE:ejb.NotificationAppControlScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:ejb.NotificationAppControlScreenEJB:DA-END

public class NotificationAppControlScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of NotificationAppControlScreenEJB
     */
    public NotificationAppControlScreenEJB() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.load.NotificationAppControlScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.load.NotificationAppControlScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.load.NotificationAppControlScreenDto.SessionDataHolder.annotations:DA-END
    public NotificationAppControlScreenDto load(NotificationAppControlScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.load.NotificationAppControlScreenDto.SessionDataHolder.NotificationAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.load.NotificationAppControlScreenDto.SessionDataHolder.NotificationAppControlScreenDto:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.load.NotificationAppControlScreenDto.SessionDataHolder.NotificationAppControlScreenDto:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB.additional.elements.in.type:DA-END
} // end of java type