package org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto;


import com.vd.AbstractDto;
import java.io.Serializable;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'NotificationAppControlScreen' in module 'NotificationAppUi'.
 * Support for dirty flag is added by the DTO generator.
 */
public class NotificationAppControlScreenDto extends AbstractDto implements Serializable { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.serialVersionUID:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.serialVersionUID:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.serialVersionUID:DA-END
    private static final long serialVersionUID = 1L;
    
    /**
     * final instance of the data container named 'NotificationAppControlForm' in UI model 'NotificationAppUi'
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.notificationAppControlForm:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.notificationAppControlForm:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.notificationAppControlForm:DA-END
    private final NotificationAppControlFormDto notificationAppControlForm = new NotificationAppControlFormDto();
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.fields:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.fields:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.fields:DA-END
    /**
     * creates an instance of NotificationAppControlScreenDto
     */
    public NotificationAppControlScreenDto() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto:DA-END
    }
    
    /**
     * getter for the field notificationAppControlForm
     * 
     * final instance of the data container named 'NotificationAppControlForm' in UI model 'NotificationAppUi'
     * 
     * @return
     */
    public NotificationAppControlFormDto getNotificationAppControlForm() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.getNotificationAppControlForm.NotificationAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.getNotificationAppControlForm.NotificationAppControlFormDto:DA-ELSE
        return this.notificationAppControlForm;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.getNotificationAppControlForm.NotificationAppControlFormDto:DA-END
    }
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        result = result || notificationAppControlForm.isDirty();
        return result;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.methods:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.methods:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.methods:DA-END
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto.additional.elements.in.type:DA-END
} // end of java type