package org.gs.iot.sample.greenhouse.gateway.paho;


import javax.ejb.Singleton;
import javax.inject.Named;
import java.util.logging.Logger;

//DA-START:paho.NotificationAppConfigurationEjb:DA-START
//DA-ELSE:paho.NotificationAppConfigurationEjb:DA-ELSE
@Singleton
@Named
//DA-END:paho.NotificationAppConfigurationEjb:DA-END

public class NotificationAppConfigurationEjb { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(NotificationAppConfigurationEjb.class.getName());
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningHost:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningHost:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningHost:DA-END
    private String dataProvisioningHost;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningContextRoot:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningContextRoot:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningContextRoot:DA-END
    private String dataProvisioningContextRoot;
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningApplicationPath:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningApplicationPath:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.dataProvisioningApplicationPath:DA-END
    private String dataProvisioningApplicationPath;
    
    /**
     * creates an instance of NotificationAppConfigurationEjb
     */
    public NotificationAppConfigurationEjb() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb:DA-END
    }
    
    /**
     * getter for the field dataProvisioningHost
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningHost.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningHost.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningHost.annotations:DA-END
    public String getDataProvisioningHost() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningHost.String:DA-ELSE
        return this.dataProvisioningHost;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningHost.String:DA-END
    }
    /**
     * setter for the field dataProvisioningHost
     * 
     * 
     * 
     * @param dataProvisioningHost  the dataProvisioningHost
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-END
    public void setDataProvisioningHost(String dataProvisioningHost) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningHost.String:DA-ELSE
        this.dataProvisioningHost = dataProvisioningHost;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningHost.String:DA-END
    }
    /**
     * getter for the field dataProvisioningContextRoot
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-END
    public String getDataProvisioningContextRoot() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-ELSE
        return this.dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-END
    }
    /**
     * setter for the field dataProvisioningContextRoot
     * 
     * 
     * 
     * @param dataProvisioningContextRoot  the dataProvisioningContextRoot
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-END
    public void setDataProvisioningContextRoot(String dataProvisioningContextRoot) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-ELSE
        this.dataProvisioningContextRoot = dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-END
    }
    /**
     * getter for the field dataProvisioningApplicationPath
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-END
    public String getDataProvisioningApplicationPath() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-ELSE
        return this.dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-END
    }
    /**
     * setter for the field dataProvisioningApplicationPath
     * 
     * 
     * 
     * @param dataProvisioningApplicationPath  the dataProvisioningApplicationPath
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-END
    public void setDataProvisioningApplicationPath(String dataProvisioningApplicationPath) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-ELSE
        this.dataProvisioningApplicationPath = dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb.additional.elements.in.type:DA-END
} // end of java type