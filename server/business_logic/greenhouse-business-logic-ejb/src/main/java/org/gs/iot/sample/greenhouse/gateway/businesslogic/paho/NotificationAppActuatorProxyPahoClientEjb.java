package org.gs.iot.sample.greenhouse.gateway.businesslogic.paho;


import org.eclipse.paho.client.mqttv3.MqttCallback;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import java.util.concurrent.RunnableFuture;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;

//DA-START:paho.NotificationAppActuatorProxyPahoClientEjb:DA-START
//DA-ELSE:paho.NotificationAppActuatorProxyPahoClientEjb:DA-ELSE
//DA-END:paho.NotificationAppActuatorProxyPahoClientEjb:DA-END
public class NotificationAppActuatorProxyPahoClientEjb implements MqttCallback { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(NotificationAppActuatorProxyPahoClientEjb.class.getName());
    
    /**
     * handles persisting MQTT messages if required (depends on the Qos)
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttClientPersistence:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttClientPersistence:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttClientPersistence:DA-END
    private MqttClientPersistence mqttClientPersistence;
    
    /**
     * the MQTT client that is going to be used to subscribe and to recieve messages
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttClient:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttClient:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttClient:DA-END
    private MqttClient mqttClient;
    
    /**
     * options that are going to be use to configure the MQTT client for its connection to a broker
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectionOptions:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectionOptions:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectionOptions:DA-END
    private MqttConnectOptions mqttConnectionOptions = new MqttConnectOptions();
    
    /**
     * counts the number of attempts to connect to a broker, this is used to implement a simple circuit broker
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectionAttemptsCounter:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectionAttemptsCounter:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectionAttemptsCounter:DA-END
    private int mqttConnectionAttemptsCounter;
    
    /**
     * this runnable future is going to be used to try to connect to the MQTT message broker
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectorTask:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectorTask:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.mqttConnectorTask:DA-END
    private RunnableFuture<?> mqttConnectorTask;
    
    /**
     * creates an instance of NotificationAppActuatorProxyPahoClientEjb
     */
    public NotificationAppActuatorProxyPahoClientEjb() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb:DA-END
    }
    
    /**
     * 
     * @param arg0
     * @param arg1
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.messageArrived.String.MqttMessage.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.messageArrived.String.MqttMessage.annotations:DA-ELSE
    @Override
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.messageArrived.String.MqttMessage.annotations:DA-END
    public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.messageArrived.String.MqttMessage.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.messageArrived.String.MqttMessage.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.messageArrived.String.MqttMessage.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.deliveryComplete.IMqttDeliveryToken.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.deliveryComplete.IMqttDeliveryToken.annotations:DA-ELSE
    @Override
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.deliveryComplete.IMqttDeliveryToken.annotations:DA-END
    public void deliveryComplete(IMqttDeliveryToken arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.deliveryComplete.IMqttDeliveryToken.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.deliveryComplete.IMqttDeliveryToken.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.deliveryComplete.IMqttDeliveryToken.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connectionLost.Throwable.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connectionLost.Throwable.annotations:DA-ELSE
    @Override
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connectionLost.Throwable.annotations:DA-END
    public void connectionLost(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connectionLost.Throwable.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connectionLost.Throwable.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connectionLost.Throwable.void:DA-END
    }
    /**
     * getter for the field mqttClientPersistence
     * 
     * handles persisting MQTT messages if required (depends on the Qos)
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClientPersistence.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClientPersistence.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClientPersistence.annotations:DA-END
    public MqttClientPersistence getMqttClientPersistence() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClientPersistence.MqttClientPersistence:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClientPersistence.MqttClientPersistence:DA-ELSE
        return this.mqttClientPersistence;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClientPersistence.MqttClientPersistence:DA-END
    }
    /**
     * getter for the field mqttClient
     * 
     * the MQTT client that is going to be used to subscribe and to recieve messages
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClient.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClient.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClient.annotations:DA-END
    public MqttClient getMqttClient() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClient.MqttClient:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClient.MqttClient:DA-ELSE
        return this.mqttClient;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttClient.MqttClient:DA-END
    }
    /**
     * getter for the field mqttConnectionOptions
     * 
     * options that are going to be use to configure the MQTT client for its connection to a broker
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionOptions.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionOptions.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionOptions.annotations:DA-END
    public MqttConnectOptions getMqttConnectionOptions() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionOptions.MqttConnectOptions:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionOptions.MqttConnectOptions:DA-ELSE
        return this.mqttConnectionOptions;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionOptions.MqttConnectOptions:DA-END
    }
    /**
     * getter for the field mqttConnectionAttemptsCounter
     * 
     * counts the number of attempts to connect to a broker, this is used to implement a simple circuit broker
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionAttemptsCounter.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionAttemptsCounter.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionAttemptsCounter.annotations:DA-END
    public int getMqttConnectionAttemptsCounter() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionAttemptsCounter.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionAttemptsCounter.int:DA-ELSE
        return this.mqttConnectionAttemptsCounter;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectionAttemptsCounter.int:DA-END
    }
    /**
     * getter for the field mqttConnectorTask
     * 
     * this runnable future is going to be used to try to connect to the MQTT message broker
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectorTask.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectorTask.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectorTask.annotations:DA-END
    public RunnableFuture<?> getMqttConnectorTask() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectorTask.RunnableFuture:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectorTask.RunnableFuture:DA-ELSE
        return this.mqttConnectorTask;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.getMqttConnectorTask.RunnableFuture:DA-END
    }
    /**
     * sets up Paho client, starts thread to connect to MQTT broker, subscribes to a topic
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.initClient.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.initClient.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.initClient.annotations:DA-END
    private void initClient() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.initClient:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.initClient:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.initClient:DA-END
    }
    /**
     * connects to the MQTT broker
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connect.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connect.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connect.annotations:DA-END
    private void connect() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connect:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connect:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.connect:DA-END
    }
    /**
     * subscribes to the MQTT broker
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.subscribe.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.subscribe.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.subscribe.annotations:DA-END
    private void subscribe() throws MqttException {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.subscribe:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.subscribe:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.subscribe:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotificationAppActuatorProxyPahoClientEjb.additional.elements.in.type:DA-END
} // end of java type