package org.gs.iot.sample.greenhouse.gateway.businesslogic.paho;


import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.gs.iot.sample.greenhouse.gateway.basic.TemperatureEntityBean;
import org.gs.iot.sample.greenhouse.gateway.paho.NotificationAppConfigurationEjb;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * notify service personnel when temperature or moisture
 * is too low or too high
 */
//DA-START:paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb:DA-START
//DA-ELSE:paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb:DA-ELSE
@Stateless
//DA-END:paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb:DA-END

public class NotifyServicePersonnelBusinesslogicMessageProcessorEjb { // start of class

    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(NotifyServicePersonnelBusinesslogicMessageProcessorEjb.class.getName());
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.configurationBean:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.configurationBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.configurationBean:DA-END
    private NotificationAppConfigurationEjb configurationBean;
    
    /**
     * creates an instance of NotifyServicePersonnelBusinesslogicMessageProcessorEjb
     */
    public NotifyServicePersonnelBusinesslogicMessageProcessorEjb() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb:DA-END
    }
    
    /**
     * getter for the field configurationBean
     * 
     * 
     * 
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.getConfigurationBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.getConfigurationBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.getConfigurationBean.annotations:DA-END
    public NotificationAppConfigurationEjb getConfigurationBean() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.getConfigurationBean.NotificationAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.getConfigurationBean.NotificationAppConfigurationEjb:DA-ELSE
        return this.configurationBean;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.getConfigurationBean.NotificationAppConfigurationEjb:DA-END
    }
    /**
     * setter for the field configurationBean
     * 
     * 
     * 
     * @param configurationBean  the configurationBean
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.setConfigurationBean.NotificationAppConfigurationEjb.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.setConfigurationBean.NotificationAppConfigurationEjb.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.setConfigurationBean.NotificationAppConfigurationEjb.annotations:DA-END
    public void setConfigurationBean(NotificationAppConfigurationEjb configurationBean) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.setConfigurationBean.NotificationAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.setConfigurationBean.NotificationAppConfigurationEjb:DA-ELSE
        this.configurationBean = configurationBean;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.setConfigurationBean.NotificationAppConfigurationEjb:DA-END
    }
    /**
     * 
     * @param topic  the topic
     * @param message  the message
     * @param clientId  the clientId
     * @param appId  the appId
     * @param messageArrival  the messageArrival
     * @return
     */
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.annotations:DA-ELSE
    @Asynchronous
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.annotations:DA-END
    public Future<NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult> processMessage(String topic, MqttMessage message, String clientId, String appId, Date messageArrival) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.Future:DA-START
    	try {
    		String json = new String(message.getPayload(), StandardCharsets.UTF_8);
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();
            TemperatureEntityBean bean = gson.fromJson(json, TemperatureEntityBean.class);
            
    		if (bean.getData().getStatus() > 21f) {
	    		URI uri = new URI("coap://" + getIpOrHost() + ":5683/alarm");  // use PUT and "on" or "off" to toggle the light
				CoapClient client = new CoapClient(uri);
                String buzzerOn = "{'data': { 'status': false }, 'timestamp': '2016-06-23T15:32:14+02'}";
				CoapResponse response = client.put(buzzerOn, MediaTypeRegistry.APPLICATION_JSON);
				
				if (response != null) {
					System.out.println(response.getOptions());
					System.out.println(response.getCode());
					System.out.println(response.getResponseText());
					System.out.println(Utils.prettyPrint(response));
					
				} else {
					System.out.println("No response received.");
				}
    		}
		} catch (URISyntaxException e) {
			System.err.println("Invalid URI: " + e.getMessage());
		}
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.Future:DA-ELSE
        // TODO implement business logic
        return null;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.Future:DA-END
    }
    
    //DA-START:paho.ProcessingResult:DA-START
    //DA-ELSE:paho.ProcessingResult:DA-ELSE
    //DA-END:paho.ProcessingResult:DA-END
    class ProcessingResult { // start of class
    
    
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.status:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.status:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.status:DA-END
        private final String status;
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.success:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.success:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.success:DA-END
        private final boolean success;
        
        
        /**
         * creates an instance of ProcessingResult
         * 
         * @param status  the status
         * @param success  the success
         */
        public ProcessingResult(String status, boolean success) {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.String.boolean:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.String.boolean:DA-ELSE
            this.status = status;
            this.success = success;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.String.boolean:DA-END
        }
        
        
        /**
         * getter for the field status
         * 
         * 
         * 
         * @return
         */
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.annotations:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.annotations:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.annotations:DA-END
        public String getStatus() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.String:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.String:DA-ELSE
            return this.status;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.String:DA-END
        }
        /**
         * getter for the field success
         * 
         * 
         * 
         * @return
         */
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.annotations:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.annotations:DA-ELSE
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.annotations:DA-END
        public boolean isSuccess() {
            //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.boolean:DA-START
            //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.boolean:DA-ELSE
            return this.success;
            //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.boolean:DA-END
        }
        
        
        //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.ProcessingResult.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.additional.elements.in.type:DA-START
    private String getIpOrHost() {
    	return "192.168.10.150";
    }
    
    public String getLightStatus() {
    	String result = "unknown";
    	
    	if (getIpOrHost() != null && getIpOrHost().length() > 0) {
	    	try {
	    		URI uri = new URI("coap://" + getIpOrHost() + ":5683/gh/sens/light");  // use GET to read light status
				CoapClient client = new CoapClient(uri);
				CoapResponse response = client.get();
				
				if (response != null) {
					result = new String(response.getPayload());
				} else {
					System.out.println("No response received.");
				}
			} catch (URISyntaxException e) {
				System.err.println("Invalid URI: " + e.getMessage());
			}
    	}
    	
    	return result;
    }
    
    public String getHumidity() {
    	String result = "unknown";
    	
    	if (getIpOrHost() != null && getIpOrHost().length() > 0) {
	    	try {
	    		URI uri = new URI("coap://" + getIpOrHost() + ":5683/gh/sens/humidity");  // use GET to read humidity
				CoapClient client = new CoapClient(uri);
				CoapResponse response = client.get();
				
				if (response != null) {
					result = new String(response.getPayload());
				} else {
					System.out.println("No response received.");
				}
			} catch (URISyntaxException e) {
				System.err.println("Invalid URI: " + e.getMessage());
			}
    	}
    	
    	return result;
    }
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.businesslogic.paho.NotifyServicePersonnelBusinesslogicMessageProcessorEjb.additional.elements.in.type:DA-END
} // end of java type