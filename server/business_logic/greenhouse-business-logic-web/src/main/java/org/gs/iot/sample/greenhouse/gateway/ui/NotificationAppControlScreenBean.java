package org.gs.iot.sample.greenhouse.gateway.ui;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.vd.AbstractManagedBean;
import java.io.Serializable;
import org.gs.iot.sample.greenhouse.gateway.notificationappcontrolscreen.dto.NotificationAppControlScreenDto;
import javax.ejb.EJB;
import org.gs.iot.sample.greenhouse.gateway.ejb.NotificationAppControlScreenEJB;
import javax.faces.event.ComponentSystemEvent;

@ManagedBean(name="notificationAppControlScreenBean")
@ViewScoped
public class NotificationAppControlScreenBean extends AbstractManagedBean implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private NotificationAppControlScreenDto dto = new NotificationAppControlScreenDto();
    
    @EJB
    private NotificationAppControlScreenEJB ejb;
    
    /**
     * getter for the field dto
     * 
     * 
     * 
     * @return
     */
    public NotificationAppControlScreenDto getDto() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.getDto.NotificationAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.getDto.NotificationAppControlScreenDto:DA-ELSE
        return this.dto;
        //DA-END:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.getDto.NotificationAppControlScreenDto:DA-END
    }
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @Override
    public void init() {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.init:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.init:DA-ELSE
        super.init();
        dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.init:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     */
    @Override
    public void preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-ELSE
         super.preRenderViewListener(event);
        //DA-END:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.gateway.ui.NotificationAppControlScreenBean.additional.elements.in.type:DA-END
} // end of java type