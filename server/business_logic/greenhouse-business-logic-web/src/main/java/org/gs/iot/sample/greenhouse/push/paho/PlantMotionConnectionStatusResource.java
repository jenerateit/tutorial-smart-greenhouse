package org.gs.iot.sample.greenhouse.push.paho;


import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.annotation.Singleton;
import java.util.logging.Logger;
import org.primefaces.push.annotation.OnOpen;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.EventBus;
import org.primefaces.push.annotation.OnClose;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.impl.JSONEncoder;

/**
 * Digital Sensors
 */
@PushEndpoint(value="/plantmotion/connectionstatus")
@Singleton
public class PlantMotionConnectionStatusResource { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(PlantMotionConnectionStatusResource.class.getName());
    
    /**
     * 
     * @param remoteEndpoint  the remoteEndpoint
     * @param eventBus  the eventBus
     */
    @OnOpen
    public void onOpen(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onOpen.RemoteEndpoint.EventBus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onOpen.RemoteEndpoint.EventBus:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onOpen.RemoteEndpoint.EventBus:DA-END
    }
    /**
     * 
     * @param remoteEndpoint  the remoteEndpoint
     * @param eventBus  the eventBus
     */
    @OnClose
    public void onClose(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onClose.RemoteEndpoint.EventBus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onClose.RemoteEndpoint.EventBus:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onClose.RemoteEndpoint.EventBus:DA-END
    }
    /**
     * 
     * @param message  the message
     * @return
     */
    @OnMessage(encoders={org.primefaces.push.impl.JSONEncoder.class})
    public PlantMotionConnectionStatus onMessage(PlantMotionConnectionStatus message) {
        //DA-START:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onMessage.PlantMotionConnectionStatus.PlantMotionConnectionStatus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onMessage.PlantMotionConnectionStatus.PlantMotionConnectionStatus:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.onMessage.PlantMotionConnectionStatus.PlantMotionConnectionStatus:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.push.paho.PlantMotionConnectionStatusResource.additional.elements.in.type:DA-END
} // end of java type