package org.gs.iot.sample.greenhouse.businesslogic.paho;


import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import java.util.concurrent.RunnableFuture;
import org.primefaces.push.EventBus;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.MqttException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import org.primefaces.push.EventBusFactory;
import javax.annotation.PostConstruct;

/**
 * Other Sensors
 */
@ApplicationScoped
@ManagedBean(eager=true, name="lightPahoClientBean")
public class LightPahoClientBean implements MqttCallback { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(LightPahoClientBean.class.getName());
    
    /**
     * handles persisting MQTT messages if required (depends on the Qos)
     */
    private MqttClientPersistence mqttClientPersistence;
    
    /**
     * the MQTT client that is going to be used to subscribe and to recieve messages
     */
    private MqttClient mqttClient;
    
    /**
     * options that are going to be use to configure the MQTT client for its connection to a broker
     */
    private MqttConnectOptions mqttConnectionOptions = new MqttConnectOptions();
    
    /**
     * counts the number of attempts to connect to a broker, this is used to implement a simple circuit broker
     */
    private int mqttConnectionAttemptsCounter;
    
    /**
     * this runnable future is going to be used to try to connect to the MQTT message broker
     */
    private RunnableFuture<?> mqttConnectorTask;
    
    /**
     * Primefaces event bus for server push functionality
     */
    private EventBus eventBus;
    
    /**
     * creates an instance of LightPahoClientBean
     */
    public LightPahoClientBean() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean:DA-ELSE
        this.initClient();
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean:DA-END
    }
    
    /**
     * 
     * @param arg0
     * @param arg1
     * @return
     */
    @Override
    public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.messageArrived.String.MqttMessage.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.messageArrived.String.MqttMessage.void:DA-ELSE
        
        // TODO generate useful code in here
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.messageArrived.String.MqttMessage.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @return
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.deliveryComplete.IMqttDeliveryToken.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.deliveryComplete.IMqttDeliveryToken.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.deliveryComplete.IMqttDeliveryToken.void:DA-END
    }
    /**
     * 
     * @param arg0
     * @return
     */
    @Override
    public void connectionLost(Throwable arg0) {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.connectionLost.Throwable.void:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.connectionLost.Throwable.void:DA-ELSE
        
        long sleepMillis = 5000;
        LOGGER.info("sleeping " + sleepMillis + " milliseconds before trying to connect ...");
        try { Thread.sleep(sleepMillis); } catch (InterruptedException e) {}
        
        Thread thread = new Thread(this.mqttConnectorTask);
        thread.start();
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.connectionLost.Throwable.void:DA-END
    }
    /**
     * getter for the field mqttClientPersistence
     * 
     * handles persisting MQTT messages if required (depends on the Qos)
     * 
     * @return
     */
    public MqttClientPersistence getMqttClientPersistence() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttClientPersistence.MqttClientPersistence:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttClientPersistence.MqttClientPersistence:DA-ELSE
        return this.mqttClientPersistence;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttClientPersistence.MqttClientPersistence:DA-END
    }
    /**
     * getter for the field mqttClient
     * 
     * the MQTT client that is going to be used to subscribe and to recieve messages
     * 
     * @return
     */
    public MqttClient getMqttClient() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttClient.MqttClient:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttClient.MqttClient:DA-ELSE
        return this.mqttClient;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttClient.MqttClient:DA-END
    }
    /**
     * getter for the field mqttConnectionOptions
     * 
     * options that are going to be use to configure the MQTT client for its connection to a broker
     * 
     * @return
     */
    public MqttConnectOptions getMqttConnectionOptions() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectionOptions.MqttConnectOptions:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectionOptions.MqttConnectOptions:DA-ELSE
        return this.mqttConnectionOptions;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectionOptions.MqttConnectOptions:DA-END
    }
    /**
     * getter for the field mqttConnectionAttemptsCounter
     * 
     * counts the number of attempts to connect to a broker, this is used to implement a simple circuit broker
     * 
     * @return
     */
    public int getMqttConnectionAttemptsCounter() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectionAttemptsCounter.int:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectionAttemptsCounter.int:DA-ELSE
        return this.mqttConnectionAttemptsCounter;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectionAttemptsCounter.int:DA-END
    }
    /**
     * getter for the field mqttConnectorTask
     * 
     * this runnable future is going to be used to try to connect to the MQTT message broker
     * 
     * @return
     */
    public RunnableFuture<?> getMqttConnectorTask() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectorTask.RunnableFuture:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectorTask.RunnableFuture:DA-ELSE
        return this.mqttConnectorTask;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getMqttConnectorTask.RunnableFuture:DA-END
    }
    /**
     * sets up Paho client, starts thread to connect to MQTT broker, subscribes to a topic
     */
    private void initClient() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.initClient:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.initClient:DA-ELSE
        LOGGER.info("initClient()");
        if (this.mqttClient != null) {
            LOGGER.warning("Paho client is already created, doing nothing in initClient()");
            return;  // this covers the case when initClient() is getting called more than once
        }
        
        try {
        	MemoryPersistence persistence = new MemoryPersistence();
        	this.mqttClient = new MqttClient("tcp://localhost:1883", "LightPahoClientBean", persistence);
        	this.mqttClient.setCallback(this);
            this.mqttConnectionOptions.setCleanSession(false);
            this.mqttConnectionOptions.setKeepAliveInterval(120);
        
        	this.mqttConnectorTask = new RunnableFuture<Object>() {
        
        		@Override
        		public boolean cancel(boolean mayInterruptIfRunning) { return false; }
        
        		@Override
        		public boolean isCancelled() { return false; }
        
        		@Override
        		public boolean isDone() { return false; }
        
        		@Override
        		public Object get() throws InterruptedException, ExecutionException { return null; }
        
        		@Override
        		public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException { return null; }
        
        		@Override
        		public void run() {
        			connect();
        		}
        		
        	};
        	
        	Thread thread = new Thread(mqttConnectorTask);
        	thread.start();
        } catch (MqttException me) {
            System.out.println("reason code: " + me.getReasonCode());
            System.out.println("message: " + me.getMessage());
            System.out.println("localized message: " + me.getLocalizedMessage());
            System.out.println("cause: " + me.getCause());
            System.out.println("exception: " + me);
            String errorMessage  = "problems creating MQTT client, cannot recover from here";
            LOGGER.severe(errorMessage);
        	throw new RuntimeException(errorMessage, me);
        }
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.initClient:DA-END
    }
    /**
     * connects to the MQTT broker
     */
    private void connect() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.connect:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.connect:DA-ELSE
        LOGGER.info("connect(), trying to connect to the MQTT message broker until the connection attempt is successful");
        
        boolean connected = this.mqttClient.isConnected();
        while (!connected) {
        	try {
        		LOGGER.info("trying to connect ...");
        		this.mqttClient.connect(this.mqttConnectionOptions);
        		LOGGER.info("successfully connected");
        		connected = true;
        		this.mqttConnectionAttemptsCounter = 0;
                this.subscribe();  // if subscription fails, an Mqtt exception is thrown and the while loop continues to try to connect and subscribe
        	} catch (MqttException ex) {
                connected = false;
        		this.mqttConnectionAttemptsCounter++;
        
                LOGGER.info("connection to MQTT broker failed, number of failures: " + this.mqttConnectionAttemptsCounter);
        		if (this.mqttConnectionAttemptsCounter > 0 && this.mqttConnectionAttemptsCounter % 3 == 0) {
        			// sleep longer and then try again, three times in a row
        			long longSleepMillis = 60000;
                    LOGGER.info("now sleeping for " + longSleepMillis + " milliseconds before trying to connect again ...");
        			try { Thread.sleep(longSleepMillis); } catch (InterruptedException e) {}
        		} else {
        			long shortSleepMillis = 1000;
                    LOGGER.info("now sleeping for " + shortSleepMillis + " milliseconds before trying to connect again ...");
        			try { Thread.sleep(shortSleepMillis); } catch (InterruptedException e) {}
        		}
        	}
        }
        LOGGER.info("exiting from connect() ...");
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.connect:DA-END
    }
    /**
     * subscribes to the MQTT broker
     */
    private void subscribe() throws MqttException {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.subscribe:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.subscribe:DA-ELSE
        String topic = "+/+/plant/light/#";
        LOGGER.info("subscribing to " + topic);
        this.mqttClient.subscribe(topic, 2);
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.subscribe:DA-END
    }
    /**
     * getter for the field eventBus
     * 
     * 
     * 
     * @return
     */
    public EventBus getEventBus() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getEventBus.EventBus:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getEventBus.EventBus:DA-ELSE
        
        if (this.eventBus == null && EventBusFactory.getDefault() != null) {
            this.eventBus = EventBusFactory.getDefault().eventBus();
        }
        return this.eventBus;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getEventBus.EventBus:DA-END
    }
    /**
     * initializes the manged bean
     */
    @PostConstruct
    private void init() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.init:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.init:DA-ELSE
        
        this.initClient();
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.init:DA-END
    }
    /**
     * provides the name to be used to look up the asynchronous stateless session bean that uses a REST-API to store the sensor data
     * 
     * @return
     */
    private String getNameForMessageProcessorLookup() {
        //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getNameForMessageProcessorLookup.String:DA-START
        //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getNameForMessageProcessorLookup.String:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.getNameForMessageProcessorLookup.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.greenhouse.businesslogic.paho.LightPahoClientBean.additional.elements.in.type:DA-END
} // end of java type