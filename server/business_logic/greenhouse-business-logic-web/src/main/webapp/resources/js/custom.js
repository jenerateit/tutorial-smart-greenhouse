/*
 * This JavaScript code presents the user with a message dialog in case there are pending changes
 * in the input elements that were not yet submitted to the server. The code is based upon the following
 * sources:
 * http://stackoverflow.com/questions/8259090/how-to-detect-unsaved-data-in-form-when-user-leaves-the-page
 * http://www.blackbeltcoder.com/Articles/script/prevent-user-navigating-away-from-page-with-unsaved-changes
 */

$(document).ready(function() {
	defineVars();
    activateFormChangesObservation();	
});

function defineVars() {
	var warnMessageToDisplay = null;
	var warnMessage = null;
	var containsNewElement = false;
	var containsModifiedElement = false;
}

function resetVDFlags() {
	containsNewElement = false;
	containsModifiedElement = false;
	warnMessageToDisplay = null;
}


function setFormDirty() {
//	var id = $(this).attr('id');
//	alert("setFormDirty(), " + $(this).get(0).tagName);
//	alert("setFormDirty(), warnMessage:");
	
	warnMessageToDisplay = warnMessage;

	
	$('[id*=save]').each( function(index, object) {changeEnablement($(this), true);} );
	$('[id*=cancel]').each( function(index, object) {changeEnablement($(this), true);} );
	
    $(this).css('background-color', backgroundColorPendingChanges);
}

function activateFormChangesObservation() {
	
	$('form').on('submit', function() {
		if (isActionAllowed(this)) {
			warnMessageToDisplay = null;
			containsNewElement = false;
		}
    });
	
	window.onbeforeunload = function() {
		if (warnMessageToDisplay != null) {
			result = warnMessageToDisplay;
//			warnMessageToDisplay = null;
			return result;
		}
	};
	
	$('input:not(:button,:submit,[id*=WithoutDirtySetting],[id*=filter]),textarea:not([id*=WithoutDirtySetting]),select:not([id*=WithoutDirtySetting])').change(setFormDirty);
	
	$('input:not(:button,:submit,[id*=WithoutDirtySetting],[id*=filter]),textarea:not([id*=WithoutDirtySetting]),select:not([id*=WithoutDirtySetting])').on('input', setFormDirty);
	
	$('input:submit,:button').click(function(e) {
		if (isActionAllowed(this)) {
			warnMessageToDisplay = null;
			containsNewElement = false;
		} else {
//			alert('click for input:submit or :button is not allowed');
		}
	});
	

	onDocumentReady();
}

function onDocumentReady() {
	
//	alert('on document ready start');
	
	$('[id*=save]').each( function(index, object) {changeEnablement($(this), false);} );
	$('[id*=cancel]').each( function(index, object) {changeEnablement($(this), false);} );
	$('[id*=delete]').each( function(index, object) {changeEnablement($(this), true);} );
		
	
	if (containsNewElement == true) {
//		alert('contains new element');
		$('[id*=save]').each( function(index, object) {changeEnablement($(this), true);} );
		$('[id*=cancel]').each( function(index, object) {changeEnablement($(this), true);} );
		$('[id*=delete]').each( function(index, object) {changeEnablement($(this), false);} );
	}
	
	if (containsModifiedElement == true) {
//		alert('contains modified element');
		$('[id*=save]').each( function(index, object) {changeEnablement($(this), true);} );
		$('[id*=cancel]').each( function(index, object) {changeEnablement($(this), true);} );
	}
	
	if (containsElementWithFailedValidation == true) {
		$('[id*=cancel]').each( function(index, object) {changeEnablement($(this), true);} );
	}
	
	if (typeof tour == 'undefined' && typeof $('#tourLink') != 'undefined') {
//		alert($('#tourButton').attr('id'));
		$('#tourLink').hide();
	};
	
//	alert('on document ready end');
}

function changeEnablement(element, enabled) {
    
	alwaysEnabled = element.attr('id').toLowerCase().indexOf('alwaysenabled') > -1;
//	alert('element ' + element.attr('id') + ", alwaysenabled:" + alwaysEnabled);
	
	if (enabled == true || alwaysEnabled == true) {
		element.removeAttr('disabled');
		element.attr('aria-disabled', 'false');
		element.removeClass('ui-state-disabled');
	} else {
		element.prop('disabled', 'disabled');
		element.attr('disabled', 'true');
		element.attr('aria-disabled', 'true');
		element.addClass('ui-state-disabled');
	}
}

function isActionAllowed(element) {
	return includes(element.id, 'cancel') || includes(element.id, 'save') || includes(element.id, 'delete');
};

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function includes(str, substring) {
	matchResult = str.toLowerCase().indexOf(substring.toLowerCase()) > -1;
	return matchResult;
}

// #################### for the following see http://stackoverflow.com/questions/11408130/jsf-commandbutton-works-on-second-click

jsf.ajax.addOnEvent(function(data) {
    if (data.status == "success") {
        fixViewState(data.responseXML);
    }
});

$(document).ajaxComplete(function(event, xhr, options) {
    if (typeof xhr.responseXML != 'undefined') { // It's undefined when plain $.ajax(), $.get(), etc is used instead of PrimeFaces ajax.
        fixViewState(xhr.responseXML);
    }
});

function fixViewState(responseXML) {
    var viewState = getViewState(responseXML);

    if (viewState) {
        for (var i = 0; i < document.forms.length; i++) {
            var form = document.forms[i];

            if (form.method == "post") {
                if (!hasViewState(form)) {
                    createViewState(form, viewState);
                }
            }
            else { // PrimeFaces also adds them to GET forms!
                removeViewState(form);
            }
        }
    }
}

function getViewState(responseXML) {
    var updates = responseXML.getElementsByTagName("update");

    for (var i = 0; i < updates.length; i++) {
        var update = updates[i];

        if (update.getAttribute("id").match(/^([\w]+:)?javax\.faces\.ViewState(:[0-9]+)?$/)) {
            return update.firstChild.nodeValue;
        }
    }

    return null;
}

function hasViewState(form) {
    for (var i = 0; i < form.elements.length; i++) {
        if (form.elements[i].name == "javax.faces.ViewState") {
            return true;
        }
    }

    return false;
}

function createViewState(form, viewState) {
    var hidden;

    try {
        hidden = document.createElement("<input name='javax.faces.ViewState'>"); // IE6-8.
    } catch(e) {
        hidden = document.createElement("input");
        hidden.setAttribute("name", "javax.faces.ViewState");
    }

    hidden.setAttribute("type", "hidden");
    hidden.setAttribute("value", viewState);
    hidden.setAttribute("autocomplete", "off");
    form.appendChild(hidden);
}

function removeViewState(form) {
    for (var i = 0; i < form.elements.length; i++) {
        var element = form.elements[i];
        if (element.name == "javax.faces.ViewState") {
            element.parentNode.removeChild(element);
        }
    }
}
