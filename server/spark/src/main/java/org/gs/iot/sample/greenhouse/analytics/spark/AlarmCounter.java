
package org.gs.iot.sample.greenhouse.analytics.spark;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.mqtt.MQTTUtils;
import org.gs.iot.sample.greenhouse.devicedata.BuzzerGroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import scala.Tuple2;


public class AlarmCounter {

	private static final Duration SLIDE_INTERVAL = new Duration(10 * 1000);
	private static final String CHECKPOINT_DIR = "checkpoint/";

	private static final Logger logger = LoggerFactory.getLogger(AlarmCounter.class);
	
	/**
	 * To count the alarm one up run: <br>
	 * <code>
	 * mosquitto_pub -t iot/gateway/alarm -m '{"data": {"status": "true"}, "timestamp": "'$(date +%FT%T%z)'"}'
	 * </code>
	 * 
	 * <p>
	 * </p>
	 * 
	 * Usage: <br>
	 * <code>
	 * spark-submit --class org.gs.iot.sample.greenhouse.analytics.spark.AlarmCounter --jars /usr/lib/spark/lib/spark-streaming-mqtt-assembly_2.10-1.5.0.jar,org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseApp.devicedata_0.1.0.jar,org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseApp.gatewaycomm_0.1.0.jar greenhouse-analytics-0.1.0-SNAPSHOT.jar tcp://10.0.250.1 iot/gateway
	 * </code>
	 * 
	 * @param args
	 *            MqttbrokerUrl topicPrefix
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Usage: AlarmCounter <MqttbrokerUrl> <topicPrefix>");
			System.exit(0);
		}

		String brokerUrl = args[0];
		String topicPrefix = args[1].endsWith("/") ? args[1] : args[1];

		SparkConf conf = new SparkConf().setAppName("AlarmCounter");
		JavaStreamingContext ssc = new JavaStreamingContext(conf, SLIDE_INTERVAL);
		ssc.checkpoint(CHECKPOINT_DIR);
		System.out.println("set checkpoint dir " + CHECKPOINT_DIR);
		logger.info("set checkpoint dir " + CHECKPOINT_DIR);
		
		JavaReceiverInputDStream<String> alarmMqttStream = MQTTUtils.createStream(ssc, brokerUrl,
				topicPrefix + SmartGreenhouseAppDeviceConfigEnum.ALARM.getResource());

		JavaPairDStream<String, Integer> alarmStream = alarmMqttStream
				.mapToPair(new PairFunction<String, String, Integer>() {

					@Override
					public Tuple2<String, Integer> call(String t) throws Exception {
						BuzzerGroveData data = BuzzerGroveData.createInstance(BuzzerGroveData.class,
								t.getBytes(StandardCharsets.UTF_8));
						System.out.println("buzzer is " + data.getData().isStatus());
						logger.info("buzzer is " + data.getData().isStatus());
						return new Tuple2<String, Integer>(SmartGreenhouseAppDeviceConfigEnum.ALARM.getDeviceId(),
								data.getData().isStatus() ? 1 : 0);
					}
				});

		JavaPairDStream<String, Integer> stateStream = alarmStream
				.updateStateByKey(new Function2<List<Integer>, Optional<Integer>, Optional<Integer>>() {

					@Override
					public Optional<Integer> call(List<Integer> values, Optional<Integer> state) throws Exception {
						Integer newSum = state.or(0);
						for (Integer value : values) {
							newSum += value;
						}
						return Optional.of(newSum);
					}
				});

		stateStream.print();
		logger.info("state stream {}", stateStream.toString());

		ssc.start();
		ssc.awaitTermination();
	}

}
