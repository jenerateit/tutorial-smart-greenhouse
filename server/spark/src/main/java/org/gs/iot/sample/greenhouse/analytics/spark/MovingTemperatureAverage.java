
package org.gs.iot.sample.greenhouse.analytics.spark;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.mqtt.MQTTUtils;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;

public class MovingTemperatureAverage {

	// Stats will be computed for the last window length of time.
	private static final Duration WINDOW_LENGTH = new Duration(30 * 1000);
	// Stats will be computed every slide interval time.
	private static final Duration SLIDE_INTERVAL = new Duration(10 * 1000);

	/**
	 * To generate some random data run: <br>
	 * <code>
	 * while [ "true" ];
	 * do;
	 *     mosquitto_pub -t iot/gateway/temperature -m '{"data": {"status": "'$(shuf -i 18-25 -n 1)'.'$(shuf -i 0-99 -n 1)'"}, "timestamp": "'$(date +%FT%T%z)'"}';
	 *     sleep 2;
	 * done;
	 * </code>
	 * 
	 * <p>
	 * </p>
	 * 
	 * Usage: <br>
	 * <code>
	 * spark-submit --class org.gs.iot.sample.greenhouse.analytics.spark.MovingTemperatureAverage --jars /usr/lib/spark/lib/spark-streaming-mqtt-assembly_2.10-1.5.0.jar,org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseApp.devicedata_0.1.0.jar,org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseApp.gatewaycomm_0.1.0.jar greenhouse-analytics-0.1.0-SNAPSHOT.jar tcp://10.0.250.1 iot/gateway
	 * </code>
	 * 
	 * @param args
	 *            MqttbrokerUrl topicPrefix
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Usage: MovingTemperatureAverage <MqttbrokerUrl> <topicPrefix>");
			System.exit(0);
		}

		String brokerUrl = args[0];
		String topicPrefix = args[1].endsWith("/") ? args[1] : args[1];

		SparkConf conf = new SparkConf().setAppName("MovingTemperatureAverage");
		JavaStreamingContext ssc = new JavaStreamingContext(conf, SLIDE_INTERVAL);

		JavaReceiverInputDStream<String> temperatureMqttStream = MQTTUtils.createStream(ssc, brokerUrl,
				topicPrefix + SmartGreenhouseAppDeviceConfigEnum.TEMPERATURE.getResource());

		JavaDStream<Float> temperatureStream = temperatureMqttStream.map(new Function<String, Float>() {
			@Override
			public Float call(String v1) throws Exception {
				TemperatureSensorGroveData data = TemperatureSensorGroveData
						.createInstance(TemperatureSensorGroveData.class, v1.getBytes(StandardCharsets.UTF_8));
				System.out.println("########### current temp " + data.getData().getStatus());
				return data.getData().getStatus();
			}
		});

		JavaDStream<Float> windowTempStream = temperatureStream.window(WINDOW_LENGTH, SLIDE_INTERVAL);

		windowTempStream.foreachRDD(new Function<JavaRDD<Float>, Void>() {

			public Void call(JavaRDD<Float> v1) throws Exception {
				if (v1.count() == 0) {
					System.out.println("No temperature in this time interval");
					return null;
				}

				Float sum = v1.reduce((a, b) -> a + b);
				float avg = sum / v1.count();
				System.out.println("###### avg: " + avg);
				
				return null;
			};
		});

//		List<Float> list = new ArrayList<>();
//		list = list.stream().filter(f -> f > 20.0f).mapToDouble(f -> new Double(f)).sum();//.collect(Collectors.toList());
		
		ssc.start();
		ssc.awaitTermination();
	}

}
