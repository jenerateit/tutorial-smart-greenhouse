
package org.gs.iot.sample.greenhouse.analytics.spark;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.mqtt.MQTTUtils;
import org.gs.iot.sample.greenhouse.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.greenhouse.gateway.interfaces.SmartGreenhouseAppDeviceConfigEnum;

public class MovingTemperatureAverageSql {

	// Stats will be computed for the last window length of time.
	private static final Duration WINDOW_LENGTH = new Duration(30 * 1000);
	// Stats will be computed every slide interval time.
	private static final Duration SLIDE_INTERVAL = new Duration(10 * 1000);

	private static final class TempData implements Serializable {

		private static final long serialVersionUID = -7867811584992680867L;
		private final float temperature;
		private final long timestamp;

		public TempData(float temperature, long timestamp) {
			this.temperature = temperature;
			this.timestamp = timestamp;
		}

		public float getTemperature() {
			return temperature;
		}

		public long getTimestamp() {
			return timestamp;
		}
	}

	/**
	 * To generate some random data run: <br>
	 * <code>
	 * while [ "true" ];
	 * do;
	 *     mosquitto_pub -t iot/gateway/temperature -m '{"data": {"status": "'$(shuf -i 18-25 -n 1)'.'$(shuf -i 0-99 -n 1)'"}, "timestamp": "'$(date +%FT%T%z)'"}';
	 *     sleep 2;
	 * done;
	 * </code>
	 * 
	 * <p>
	 * </p>
	 * 
	 * Usage: <br>
	 * <code>
	 * spark-submit --class org.gs.iot.sample.greenhouse.analytics.spark.MovingTemperatureAverageSql --jars /usr/lib/spark/lib/spark-streaming-mqtt-assembly_2.10-1.5.0.jar,org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseApp.devicedata_0.1.0.jar,org.gs.iot.sample.greenhouse.gateway.SmartGreenhouseApp.gatewaycomm_0.1.0.jar,lib/db-derby-10.12.1.1-lib/lib/derbyclient.jar greenhouse-analytics-0.1.0-SNAPSHOT.jar tcp://10.0.250.1 iot/gateway
	 * </code>
	 * 
	 * @param args
	 *            MqttbrokerUrl topicPrefix
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			System.err.println("Usage: MovingTemperatureAverageSql <MqttbrokerUrl> <topicPrefix> <derbyDB IP>");
			System.err.println();
			System.err.println("\tMqttbrokerUrl: tcp://10.0.0.42");
			System.err.println("\ttopicPrefix:   iot/gateway");
			System.err.println("\tderbyDB IP:    10.0.42.1");
			System.exit(0);
		}

		final String brokerUrl = args[0];
		final String topicPrefix = args[1].endsWith("/") ? args[1] : args[1] + "/";
		final String topic = topicPrefix + SmartGreenhouseAppDeviceConfigEnum.TEMPERATURE.getResource();
		final String derbyIP = args[2];

		SparkConf conf = new SparkConf().setAppName("MovingTemperatureAverage");
		JavaStreamingContext ssc = new JavaStreamingContext(conf, SLIDE_INTERVAL);

		JavaReceiverInputDStream<String> temperatureMqttStream = MQTTUtils.createStream(ssc, brokerUrl, topic);

		/*
		 * map to an object that holds the temeprature and the timestamp (has to
		 * be serializable for spark)
		 */
		JavaDStream<TempData> temperatureStream = temperatureMqttStream.map(new Function<String, TempData>() {

			private static final long serialVersionUID = -5568963739798485452L;

			@Override
			public TempData call(String msg) throws Exception {
				TemperatureSensorGroveData data = TemperatureSensorGroveData
						.createInstance(TemperatureSensorGroveData.class, msg.getBytes(StandardCharsets.UTF_8));
				System.out.println("###### current temp " + data.getData().getStatus());
				return new TempData(data.getData().getStatus(), data.getTimestamp().getTime());
			}
		});

		/* create a window to hold the data in this time frame */
		JavaDStream<TempData> windowTempStream = temperatureStream.window(WINDOW_LENGTH, SLIDE_INTERVAL);

		windowTempStream.foreachRDD(new Function<JavaRDD<TempData>, Void>() {

			private static final long serialVersionUID = -7364544249485786999L;

			public Void call(JavaRDD<TempData> tempData) throws Exception {
				if (tempData.count() == 0) {
					System.out.println("No temperature in this time interval");
					return null;
				}

				/* map the temperature */
				JavaRDD<Float> temp = tempData.map(data -> data.getTemperature());

				/* calculate the sum of all temperatures */
				Float sum = temp.reduce((a, b) -> a + b);
				float avg = sum / tempData.count();

				/* map the timestamp */
				JavaRDD<Long> timestamp = tempData.map(data -> data.getTimestamp());

				/*
				 * get min and max value of the timestamp (comperator has to be
				 * serializable for spark)
				 */
				long startTimestamp = timestamp.min((Comparator<Long> & Serializable) (a, b) -> Long.compare(a, b));
				long endTimestamp = timestamp.max((Comparator<Long> & Serializable) (a, b) -> Long.compare(a, b));

				System.out.println("###### avg temp: " + avg + " from " + new Date(startTimestamp) + " until "
						+ new Date(endTimestamp));

				try {
					/* get connection to derby db */
					Connection connection = DriverManager.getConnection(
							"jdbc:derby://" + derbyIP + ":1527/c:/users/demo/derbydb/greenhouseanalytics;create=true");

					/* check if table exists */
					DatabaseMetaData meta = connection.getMetaData();
					ResultSet tables = meta.getTables(null, null, "MOVINGAVGTEMPERATURE", null);
					if (!tables.next()) { // if table does not exist
						/* create the table */
						Statement create = connection.createStatement();
						create.execute("CREATE TABLE MOVINGAVGTEMPERATURE ("
								+ "PK INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
								+ "TOPIC VARCHAR(255) NOT NULL," + "CALCTIMESTAMP TIMESTAMP NOT NULL,"
								+ "STARTTIMESTAMP TIMESTAMP NOT NULL," + "ENDTIMESTAMP TIMESTAMP NOT NULL,"
								+ "AVGTEMPERATURE REAL NOT NULL, " + "PRIMARY KEY (PK) )");
						create.close();
					}

					/* insert values into table */
					PreparedStatement insert = connection.prepareStatement(
							"INSERT INTO MOVINGAVGTEMPERATURE(TOPIC, CALCTIMESTAMP, STARTTIMESTAMP, ENDTIMESTAMP, AVGTEMPERATURE) VALUES(?, ?, ?, ?, ?)");
					insert.setString(1, topic);
					insert.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
					insert.setTimestamp(3, new Timestamp(startTimestamp));
					insert.setTimestamp(4, new Timestamp(endTimestamp));
					insert.setFloat(5, avg);
					insert.executeUpdate();
					insert.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}

				return null;
			};
		});

		ssc.start();
		ssc.awaitTermination();
	}

}
