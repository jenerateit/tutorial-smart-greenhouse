package org.gs.iot.sample.greenhouse.analytics.spark;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.rdd.JdbcRDD;
import org.apache.spark.rdd.JdbcRDD.ConnectionFactory;

public class BatchSql {

	/**
	 * 
	 * <code>
	 * ./bin/spark-submit --class org.gs.iot.sample.greenhouse.analytics.spark.BatchSql --jars lib/db-derby-10.12.1.1-lib/lib/derbyclient.jar greenhouse-analytics-0.1.0-SNAPSHOT.jar
	 * </code>
	 * 
	 * To connect to the DB <code>
	 * cd /root/spark-1.5.2-bin-hadoop2.6/lib/db-derby-10.12.1.1-lib
	 * java -cp lib/derbytools.jar:lib/derbyclient.jar org.apache.derby.tools.ij
	 * ij> connect 'jdbc:derby://10.0.250.4:1527/c:/users/demo/derbydb/greenhouse';
	 * </code>
	 * 
	 * @param args
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args) throws ClassNotFoundException {

		SparkConf conf = new SparkConf().setAppName("Batch");
		JavaSparkContext sc = new JavaSparkContext(conf);

		Class.forName("org.apache.derby.jdbc.ClientDriver");

		JavaRDD<Double> rdd = JdbcRDD.create(sc, new ConnectionFactory() {
			@Override
			public Connection getConnection() throws Exception {
				return DriverManager.getConnection("jdbc:derby://10.0.250.4:1527/c:/users/demo/derbydb/greenhouse");
			}
		}, "SELECT STATUS FROM TEMPERATUREENTITY WHERE ? <= PK AND PK <= ?", 1, 10000,
				10, /* 10 partitions, equally distributed between 1 and 10000 */
				new Function<ResultSet, Double>() {
					public Double call(ResultSet v1) throws Exception {
						return v1.getDouble(1);
					};
				});

		Double sum = rdd.reduce((a, b) -> a + b);
		double avg = sum / rdd.count();

		System.out.println("######### avg temp: " + avg);

		try {
			Connection connection = DriverManager.getConnection(
					"jdbc:derby://10.0.250.4:1527/c:/users/demo/derbydb/greenhouseanalytics;create=true");

			DatabaseMetaData meta = connection.getMetaData();
			ResultSet tables = meta.getTables(null, null, "AVGTEMPERATURE", null);
			if (!tables.next()) { // if table does not exist
				Statement create = connection.createStatement();
				create.execute("CREATE TABLE AVGTEMPERATURE ("
						+ "PK INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
						+ "CALCTIMESTAMP TIMESTAMP NOT NULL," + "AVGTEMPERATURE FLOAT NOT NULL, " + "PRIMARY KEY (PK) )");
				create.close();
			}

			PreparedStatement insert = connection
					.prepareStatement("INSERT INTO AVGTEMPERATURE(CALCTIMESTAMP, AVGTEMPERATURE) VALUES(?, ?)");
			insert.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
			insert.setDouble(2, avg);
			insert.executeUpdate();
			insert.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sc.stop();
		sc = null;
	}

}
